`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 10/18/2023 09:13:35 AM
// Design Name: 
// Module Name: WG_TB
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module WG_TB();
	reg clk;
    	reg Read_EN;
    	reg [22:0] Address_Data;
    	reg [8:0] address;
    	reg [13:0] data;
	wire [13:0] WaveformData;

    	WaveformGenerator dut (
        	.clk(clk),
        	.Address_Data(Address_Data),
        	.Read_EN(Read_EN),
		.WaveformData(WaveformData)
    	);
    
    	initial begin
        	clk = 1'b0;
        	forever #1 clk = ~clk;
    	end 
    
	initial begin
        	Read_EN = 1'b0;
        	#120 Read_EN = ~Read_EN;
    	end 
    
	initial begin
    	    	address = 9'h000;
    	    	data = 14'h0001;
    	    	Address_Data = {address, data};
    	    	#4
		address = 9'h001;
    	    	data = 14'h0002;
    	    	Address_Data = {address, data};
    	    	#4
    	    	address = 9'h002;
    	    	data = 14'h0003;
    	    	Address_Data = {address, data};
    	    	#4
		address = 9'h003;
    	    	data = 14'h0004;
    	    	Address_Data = {address, data};
		#4
    	    	address = 9'h004;
    	    	data = 14'h0005;
    	    	Address_Data = {address, data};
    	    	#4
		address = 9'h005;
    	    	data = 14'h0006;
    	    	Address_Data = {address, data};
    	    	#4
    	    	address = 9'h006;
    	    	data = 14'h0007;
    	    	Address_Data = {address, data};
    	    	#4
		address = 9'h007;
    	    	data = 14'h0008;
    	    	Address_Data = {address, data};
		#4
    	    	address = 9'h008;
    	    	data = 14'h0009;
    	    	Address_Data = {address, data};
    	    	#4
		address = 9'h009;
    	    	data = 14'h000A;
    	    	Address_Data = {address, data};
    	    	#4
    	    	address = 9'h00A;
    	    	data = 14'h000B;
    	    	Address_Data = {address, data};
    	    	#4
		address = 9'h00B;
    	    	data = 14'h000C;
    	    	Address_Data = {address, data};
		#4
    	    	address = 9'h00C;
    	    	data = 14'h000D;
    	    	Address_Data = {address, data};
		#4
    	    	address = 9'h00D;
    	    	data = 14'h000E;
    	    	Address_Data = {address, data};
    	    	#4
		address = 9'h00E;
    	    	data = 14'h000F;
    	    	Address_Data = {address, data};
    	    	#4
    	    	address = 9'h00F;
    	    	data = 14'h0010;
    	    	Address_Data = {address, data};
    	    	#4
		address = 9'h010;
    	    	data = 14'h0020;
    	    	Address_Data = {address, data};
    	end
endmodule : WG_TB
