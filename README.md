<title><b><em> Project details: </em></b></title>
<br>
<body>
Code and resources to manage FPGA development. FPGA programs various chips, takes in data to processes signals being sent to a computer.
</body>
<br><br> 

<title><b><em> Main contributors: </em></b></title>
<br>
<body>
- SPI: Rodrigo Romero <br>
- Digital signal processing: Michael Levin
</body>
<br><br> 

<title><b><em> FPGA details:</em></b></title><br>
<body>
- FPGA: Xylinx Artix 7 <br>
- Development board: Alchitry Au+
</body>
<br><br>

<title><b><em> FPGA device number:</em></b></title><br>
<body>
- xc7a100tftg256-1
</body>
<br><br>

<title><b><em>General Information:</em></b></title><br>
<body>
- 102 I/O pins (3.3V, 20 can be programmed for 1.8V LVDS standard) <br>
- 100 MHz clock can be internally multiplied
</body>
<br><br>

<title><b><em>Resources:</em></b></title><br>
<body>
- <a href = "https://www.xilinx.com/content/dam/xilinx/support/packagefiles/a7packages/xc7a100tftg256pkg.txt"> xc7a100tftg256-1: Xylinx Artix 7 pinout </a><br>
- <a href = "https://hackaday.io/page/9697-alchitry-au-clock-talk">Information on clock scaling</a> <br>
- <a href = "https://cdn.sparkfun.com/assets/1/3/d/d/b/alchitry_gold_plus_schematic.pdf"> Alchitry Au+ Schematic</a> <br>
- <a href = https://docs.xilinx.com/v/u/en-US/ds180_7Series_Overview> Series 7 datasheet </a> <br>
- <a href = "https://github.com/alchitry"> alchitry github </a> <br>
- <a href = "https://cdn.alchitry.com/docs/alchitry_ft_sch.pdf"> alchitry Ft schematic </a> <br>
- <a href = "https://cdn.alchitry.com/docs/DS_FT600Q-FT601Q-IC-Datasheet.pdf"> FTDI chip datasheet </a> <br>
</body>
</html>

