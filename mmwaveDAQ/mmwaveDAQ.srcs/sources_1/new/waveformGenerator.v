`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: Matthew Caron
// 
// Create Date: 10/16/2023 02:44:25 PM
// Design Name: 
// Module Name: WaveformGenerator
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
module WaveformGenerator(
    input [22:0] Address_Data,
    input Read_EN,
    input clk,
    output reg [13:0] WaveformData = 14'h000
    );
    reg [8:0] curr_address = 9'h000;
    reg [8:0] max_address = 9'h000;
    reg [13:0] storageData [0:8];

    always @ (posedge clk) begin
        if (Read_EN) begin
            WaveformData <= storageData[curr_address];
        end
    end

    always @ (posedge clk) begin
	   if (~Read_EN) begin
	       curr_address = Address_Data >> 14;
	       storageData[curr_address] = Address_Data & 14'h3FFF;
	   end
	end 

    always @ (negedge clk) begin
        if (Read_EN) begin
            curr_address = (curr_address < max_address)? (curr_address + 1): 9'h000;
        end
    end
	
	always @ (negedge clk) begin
		if (~Read_EN) begin 
			max_address = (curr_address > max_address) ? curr_address: max_address;
		end
	end 
endmodule
