// Copyright 1986-2022 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2022.2 (win64) Build 3671981 Fri Oct 14 05:00:03 MDT 2022
// Date        : Sat Oct 14 11:53:44 2023
// Host        : DESKTOP-QRFTT4G running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim -rename_top design_1_blk_mem_gen_2_0 -prefix
//               design_1_blk_mem_gen_2_0_ design_1_blk_mem_gen_2_1_sim_netlist.v
// Design      : design_1_blk_mem_gen_2_1
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a100tftg256-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_1_blk_mem_gen_2_1,blk_mem_gen_v8_4_5,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "blk_mem_gen_v8_4_5,Vivado 2022.2" *) 
(* NotValidForBitStream *)
module design_1_blk_mem_gen_2_0
   (clka,
    wea,
    addra,
    dina,
    douta);
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME BRAM_PORTA, MEM_SIZE 8192, MEM_WIDTH 32, MEM_ECC NONE, MASTER_TYPE OTHER, READ_WRITE_MODE READ_WRITE, READ_LATENCY 1" *) input clka;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA WE" *) input [0:0]wea;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA ADDR" *) input [8:0]addra;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA DIN" *) input [13:0]dina;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA DOUT" *) output [13:0]douta;

  wire [8:0]addra;
  wire clka;
  wire [13:0]dina;
  wire [13:0]douta;
  wire [0:0]wea;
  wire NLW_U0_dbiterr_UNCONNECTED;
  wire NLW_U0_rsta_busy_UNCONNECTED;
  wire NLW_U0_rstb_busy_UNCONNECTED;
  wire NLW_U0_s_axi_arready_UNCONNECTED;
  wire NLW_U0_s_axi_awready_UNCONNECTED;
  wire NLW_U0_s_axi_bvalid_UNCONNECTED;
  wire NLW_U0_s_axi_dbiterr_UNCONNECTED;
  wire NLW_U0_s_axi_rlast_UNCONNECTED;
  wire NLW_U0_s_axi_rvalid_UNCONNECTED;
  wire NLW_U0_s_axi_sbiterr_UNCONNECTED;
  wire NLW_U0_s_axi_wready_UNCONNECTED;
  wire NLW_U0_sbiterr_UNCONNECTED;
  wire [13:0]NLW_U0_doutb_UNCONNECTED;
  wire [8:0]NLW_U0_rdaddrecc_UNCONNECTED;
  wire [3:0]NLW_U0_s_axi_bid_UNCONNECTED;
  wire [1:0]NLW_U0_s_axi_bresp_UNCONNECTED;
  wire [8:0]NLW_U0_s_axi_rdaddrecc_UNCONNECTED;
  wire [13:0]NLW_U0_s_axi_rdata_UNCONNECTED;
  wire [3:0]NLW_U0_s_axi_rid_UNCONNECTED;
  wire [1:0]NLW_U0_s_axi_rresp_UNCONNECTED;

  (* C_ADDRA_WIDTH = "9" *) 
  (* C_ADDRB_WIDTH = "9" *) 
  (* C_ALGORITHM = "1" *) 
  (* C_AXI_ID_WIDTH = "4" *) 
  (* C_AXI_SLAVE_TYPE = "0" *) 
  (* C_AXI_TYPE = "1" *) 
  (* C_BYTE_SIZE = "9" *) 
  (* C_COMMON_CLK = "0" *) 
  (* C_COUNT_18K_BRAM = "1" *) 
  (* C_COUNT_36K_BRAM = "0" *) 
  (* C_CTRL_ECC_ALGO = "NONE" *) 
  (* C_DEFAULT_DATA = "0" *) 
  (* C_DISABLE_WARN_BHV_COLL = "0" *) 
  (* C_DISABLE_WARN_BHV_RANGE = "0" *) 
  (* C_ELABORATION_DIR = "./" *) 
  (* C_ENABLE_32BIT_ADDRESS = "0" *) 
  (* C_EN_DEEPSLEEP_PIN = "0" *) 
  (* C_EN_ECC_PIPE = "0" *) 
  (* C_EN_RDADDRA_CHG = "0" *) 
  (* C_EN_RDADDRB_CHG = "0" *) 
  (* C_EN_SAFETY_CKT = "0" *) 
  (* C_EN_SHUTDOWN_PIN = "0" *) 
  (* C_EN_SLEEP_PIN = "0" *) 
  (* C_EST_POWER_SUMMARY = "Estimated Power for IP     :     2.78965 mW" *) 
  (* C_FAMILY = "artix7" *) 
  (* C_HAS_AXI_ID = "0" *) 
  (* C_HAS_ENA = "0" *) 
  (* C_HAS_ENB = "0" *) 
  (* C_HAS_INJECTERR = "0" *) 
  (* C_HAS_MEM_OUTPUT_REGS_A = "1" *) 
  (* C_HAS_MEM_OUTPUT_REGS_B = "0" *) 
  (* C_HAS_MUX_OUTPUT_REGS_A = "0" *) 
  (* C_HAS_MUX_OUTPUT_REGS_B = "0" *) 
  (* C_HAS_REGCEA = "0" *) 
  (* C_HAS_REGCEB = "0" *) 
  (* C_HAS_RSTA = "0" *) 
  (* C_HAS_RSTB = "0" *) 
  (* C_HAS_SOFTECC_INPUT_REGS_A = "0" *) 
  (* C_HAS_SOFTECC_OUTPUT_REGS_B = "0" *) 
  (* C_INITA_VAL = "0" *) 
  (* C_INITB_VAL = "0" *) 
  (* C_INIT_FILE = "NONE" *) 
  (* C_INIT_FILE_NAME = "no_coe_file_loaded" *) 
  (* C_INTERFACE_TYPE = "0" *) 
  (* C_LOAD_INIT_FILE = "0" *) 
  (* C_MEM_TYPE = "0" *) 
  (* C_MUX_PIPELINE_STAGES = "0" *) 
  (* C_PRIM_TYPE = "1" *) 
  (* C_READ_DEPTH_A = "512" *) 
  (* C_READ_DEPTH_B = "512" *) 
  (* C_READ_LATENCY_A = "1" *) 
  (* C_READ_LATENCY_B = "1" *) 
  (* C_READ_WIDTH_A = "14" *) 
  (* C_READ_WIDTH_B = "14" *) 
  (* C_RSTRAM_A = "0" *) 
  (* C_RSTRAM_B = "0" *) 
  (* C_RST_PRIORITY_A = "CE" *) 
  (* C_RST_PRIORITY_B = "CE" *) 
  (* C_SIM_COLLISION_CHECK = "ALL" *) 
  (* C_USE_BRAM_BLOCK = "0" *) 
  (* C_USE_BYTE_WEA = "0" *) 
  (* C_USE_BYTE_WEB = "0" *) 
  (* C_USE_DEFAULT_DATA = "0" *) 
  (* C_USE_ECC = "0" *) 
  (* C_USE_SOFTECC = "0" *) 
  (* C_USE_URAM = "0" *) 
  (* C_WEA_WIDTH = "1" *) 
  (* C_WEB_WIDTH = "1" *) 
  (* C_WRITE_DEPTH_A = "512" *) 
  (* C_WRITE_DEPTH_B = "512" *) 
  (* C_WRITE_MODE_A = "WRITE_FIRST" *) 
  (* C_WRITE_MODE_B = "WRITE_FIRST" *) 
  (* C_WRITE_WIDTH_A = "14" *) 
  (* C_WRITE_WIDTH_B = "14" *) 
  (* C_XDEVICEFAMILY = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  (* is_du_within_envelope = "true" *) 
  design_1_blk_mem_gen_2_0_blk_mem_gen_v8_4_5 U0
       (.addra(addra),
        .addrb({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .clka(clka),
        .clkb(1'b0),
        .dbiterr(NLW_U0_dbiterr_UNCONNECTED),
        .deepsleep(1'b0),
        .dina(dina),
        .dinb({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .douta(douta),
        .doutb(NLW_U0_doutb_UNCONNECTED[13:0]),
        .eccpipece(1'b0),
        .ena(1'b0),
        .enb(1'b0),
        .injectdbiterr(1'b0),
        .injectsbiterr(1'b0),
        .rdaddrecc(NLW_U0_rdaddrecc_UNCONNECTED[8:0]),
        .regcea(1'b0),
        .regceb(1'b0),
        .rsta(1'b0),
        .rsta_busy(NLW_U0_rsta_busy_UNCONNECTED),
        .rstb(1'b0),
        .rstb_busy(NLW_U0_rstb_busy_UNCONNECTED),
        .s_aclk(1'b0),
        .s_aresetn(1'b0),
        .s_axi_araddr({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arburst({1'b0,1'b0}),
        .s_axi_arid({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arlen({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arready(NLW_U0_s_axi_arready_UNCONNECTED),
        .s_axi_arsize({1'b0,1'b0,1'b0}),
        .s_axi_arvalid(1'b0),
        .s_axi_awaddr({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awburst({1'b0,1'b0}),
        .s_axi_awid({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awlen({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awready(NLW_U0_s_axi_awready_UNCONNECTED),
        .s_axi_awsize({1'b0,1'b0,1'b0}),
        .s_axi_awvalid(1'b0),
        .s_axi_bid(NLW_U0_s_axi_bid_UNCONNECTED[3:0]),
        .s_axi_bready(1'b0),
        .s_axi_bresp(NLW_U0_s_axi_bresp_UNCONNECTED[1:0]),
        .s_axi_bvalid(NLW_U0_s_axi_bvalid_UNCONNECTED),
        .s_axi_dbiterr(NLW_U0_s_axi_dbiterr_UNCONNECTED),
        .s_axi_injectdbiterr(1'b0),
        .s_axi_injectsbiterr(1'b0),
        .s_axi_rdaddrecc(NLW_U0_s_axi_rdaddrecc_UNCONNECTED[8:0]),
        .s_axi_rdata(NLW_U0_s_axi_rdata_UNCONNECTED[13:0]),
        .s_axi_rid(NLW_U0_s_axi_rid_UNCONNECTED[3:0]),
        .s_axi_rlast(NLW_U0_s_axi_rlast_UNCONNECTED),
        .s_axi_rready(1'b0),
        .s_axi_rresp(NLW_U0_s_axi_rresp_UNCONNECTED[1:0]),
        .s_axi_rvalid(NLW_U0_s_axi_rvalid_UNCONNECTED),
        .s_axi_sbiterr(NLW_U0_s_axi_sbiterr_UNCONNECTED),
        .s_axi_wdata({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_wlast(1'b0),
        .s_axi_wready(NLW_U0_s_axi_wready_UNCONNECTED),
        .s_axi_wstrb(1'b0),
        .s_axi_wvalid(1'b0),
        .sbiterr(NLW_U0_sbiterr_UNCONNECTED),
        .shutdown(1'b0),
        .sleep(1'b0),
        .wea(wea),
        .web(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2022.2"
`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
VHPlDkoDlWlBfBMvPBmGYmaek3s9hXXhjF28kllYPnaNm3TSnzzpXHWHc8Ye9/2L2yiQfJ1hTWou
Ia/zeQ8h9/dtr6QB5YkyW4wlb/LbMgXb+DGIXPSllNl0IMsRQIcQDbcQm1bO/nlhb+2pjxiuaQrl
DbvxoDwPs7z3LunRxsg=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
lmIhoX8hXuc7tNV1sXY1K2/gXL7Y7Hq73qQF7+x03UWWTRd3uhGmVQtOMVbhIW+66UkWUHiD26zL
fzqGor8bgSNGpSFyS11k4TwLQT4OfAMGO8C9Qmmh4+VENBnpS9TW+wHzCv8oUwht7xYtYRZvOvYK
F3fMppz2sBkUd1lciw98ZE/UmNkhqBuMfIYF43j45DEJ55PBhOZNg91Ls4v3qBHyBAaYPFFoMry3
d5Fw1PZyFQSEOSSpwgyds2aN0g6oIwl7zm0LJrM9VDAOxBUE50hk+oHr4jj8J8UhHQJnlEHm1Idm
rvxKygNKRvfSpa90NYxZJFYgqnrMYg+19+9aZA==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
VkyCjO2onoeZWEoYQ/4ue7X5mkHyTYVW9xjdoTsGS4GdP/Q64VaCZL/jr6R8DVDXPMnH7tRMrDpo
jpYBnyzSgOkfgqM+96ioC2fDyAaG4gYgGLmrBR6qK3/mxXwAZZX+GJ9R/eWXkc9h8xN+gsSSX6/M
jIQCgeT6q7PB4dWT6KY=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
Iub91V+TnhVlZCSLu6iKmFjix71y6/l83OPTs8uewWvkE7WcqYxEKi9fonXEkzAtWzuKwEUqnOlN
VBsNJqPUdKcd22q523mrdt89mpdosWD+hvZdO7ELhJniY5u9h49FFkubpN2JiUTcIcKEYxVNlds4
wyvaYUqbPVH5v2ooJwDdimS4GVn9HerCOgPwfshvQDNlMTxLcYju4v8BHMc5Rub9Q/ihvpQU74v2
ouZ9XIwA+C6pBLwvaqS8jE7HXOokgqJilaX/W/t+KEgiFry/txRTMU9WMD7tCN7lcfjCydmS3Lq+
3u6Hsr0S8BwNjcaDpZDnBTygUJd4JSqREnk33w==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
U46EWFmKmpZGaWfyL+dokyQtJtaOYsa7HCW/+fdtw9/yHKTWFpmqKBZngBj5rPkNhtTDDCJkqsYj
tUXg1j4tgIBaCQn9B0q/aG+B3gPLrudp9hLL25mVbsfiTzdekiV2hJMmhuMoavKKPJHC6zyW7kZi
80er82OQy8h+Df/fe6TRjH9xEt3/b80tRKUMbxkLfnnkAyyf1KfOhB6/uyI4mwXuQR+DsAbzybKR
YtXpOiW72tGrXTFlzcwbHamWZefqsilVpBw6V5dh33vYKGx50xwWpj76maAkpQrOpB7zufeldJe4
W1UOEN84AZdRTLkVSxamWo/wp8nP9fiGS/ItRw==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2021_07", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
qczgIJYpE/SzErzK7eWJBGcDFEzDLm8cKbwJbPXuM6YnJxx44W+E60R3war7K2QGFAkOoCDUtDC7
SghJGF32btaDLzeKm0tQ669sBtQmMIaBrlt7I9QBkNM8zN9GL92qxNC9o3UVWMOYy5BmH8nUPgcE
O6lRubeltlrTuDe7UJQ2nEPHcXjpUJJ8dxktyW+LovBy1OxW8g4GRAsmEJsoOEg0HuDdWcc4IshJ
PvwPJ7LblELAKsdkSt65y9VaklaEm7MlH4ImlgIa74TgRmutLUbWxM1QYhGE5rAzFhGU5i3RJOdx
L3N7GGGvLMW2z9NSHbIFX+/eNII9fNJ9nZbgLA==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
Ti1NUgDv8YPk90APMwfu/mRr38QYwAxZfv0T6zQ89YS55t2EquEGVqrEafYX6rTydLOw8le1Oucv
f2oERpSSSTih/ScZneSZmuPE/Zh2BU1Ajv0j+/+0uEWXU+5lLPbDJjnapTmJXih1MYPf0SHpZZmE
BKj2IEBI9MPZlh6bxpa5BWJnyPdAvHf+UNaMXU9+pmbtrzUVebql4mFJu45Z3+ehmFY4FBW3zXMF
44C4TlHACLwL3vHVMCVfeKhgdVDbpE+/IFhTStz7mZ9h9RKGanQcs6YDVM1R+2RKA1QT1fX4FiQc
1V+FGmrm1ujxmFGXwpfNKByVlfCY0oWhRJCYYQ==

`pragma protect key_keyowner="Atrenta", key_keyname="ATR-SG-RSA-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=384)
`pragma protect key_block
HuEXFK0NXt09xU2yxxjng1OLsT+ZEM4EhqBgpr9D2ljw2vDaMBrqEsRQTc2B9soDq3ewDduHJXBd
OGYxkPnoN6LhjULtB2nTgjcH6NxA4puZ1ZNcndDndVBo8rTW5W1OqHq6InAG0CqPpTIkuqz3ECPl
EysI++MCDfH6tIzlekxJFIJ1McJsTq5rFuLzMMcrmkBxgcayDpOcCFuzZzCczxmt/cCCIKmDybwT
OQXmOcLJoYLP4sFu6R9c6xO8i6p++crv2N3eIxZHKbek9xBBZqQM9EYuEtsbkqAs9XZpa16i5njR
BDFxTKcP6r7JgFALJE89AZhBbate5JXWp0v4ECZD18aEL17CipwcWPutNMdG1apzSPP5y59n7rMG
yxBPz1gKHc3Emkl4WcO0hjICxqmO6dMXoY8JvBSf6ry2l0sH9Ihr3Bq5WWmlhPHnoaNr5jl//vNe
KfToWtn97eoVSt1LnmXXnSpdigbHr0UIg8AdkpdkuNRaWdVicDdgSo49

`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="CDS_RSA_KEY_VER_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
mokwst2bn6UxD6V9UdIgCIG1QQ/d0FiJqYGOTI2eHPV6YElaLjnJ8DnQmZnGS95o3x93FDOoa58C
RwYsX1fVoVtXkj1LuZq0k7q9vEe4T8xMjpkeYtIHY9k0Xhy1Lq/xRlfzGAf9fvf9e+f4r7aR/Sb/
uCZxxugG5niTwLENY1n3NthYL0jvo8Fmdw4Qg0nTCGWlVCws+09K0g9/lx6I9EcuHHemcHO3fOZG
lMc4NaPNozKwnyDMoWUkwiVxyFEPFaQLNYqzjvR+CqrWfhFLo96JWhL+eaDoNuZoBVYQtNH5ZwBL
BoO27Pw10lgcReGlZBz3BLO7T4ddynCx0+eSnw==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
PiP7AjOQqqouyQMoBQqgWIDhUSViq94rIvGiIJ/UKMDspM/yXw1caE8AhWHTjYckC4yLpPAz5P6s
1Z6flzDPrzVwg4e59X2cc4IMCHhedna0rDO804njcc6amRDTeLsMLTkWfvomB4xwszm2AgT+PRnB
WHd09ZUDVFjiBXT+Oa9AicgGJHrX3w823yBPuAa704kje/SzgtiDpcTU1eLmLhLW7LpEd9KIHd9s
ER7Uk9Orws0Kq9PMTqMX4hMn5K5mFakOeOURiEbUjdv5RiIJ2g/PlQXSItM8fHsBTQa6fOaJwQTI
vHwK3a8ZBHpfT1YH+n7wNiNUZwD4SFXm1QVx4g==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
Ul5ZfTHJwMctaNhYRortUZizYMPYRef7uYqPSuMkxsArnxI/cjGh+KRMwzV86hyp/6TXSJIjm5ec
2wX2UONdPN+DOJ84jYC4JbgJQrPnTj7ioD8uLX/WlyPcQzyF5keqFgj5eR5s13FskVWCuAWf5m9w
mhFEKFjVXDAr7gVgAJh/hL8P6Psrnf+LGfiM8JhnDepsHEYykGlpD3fzru2BGgqHWqPqFMcnyVGl
vysaIXiJz/eYKvO8RGcgd3DJAM/wPm9A0m/DWcmSnczOgTjoqkHcBg2H5uJMLvufzmjImi6LYEqq
v04ESDEN31cSUzqUYcayvMFOnI/WNsWbFIa5+Q==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 20240)
`pragma protect data_block
ae5vnRqjrMy+KAgDgVhwOaDTW+QgUK/0OnQFx6qfaubTXnmwEHQMglFk+KcbWzmaghYk71Oh9Jfu
vZHW9iZ7CmbWMjWJhj1hjCVGCab1LzGqSu5ujWq09FIvcGv/3Ee6/G/PHIxfMrAvPiyVa4wNycM/
22AcSvTDcIBg5xAfxokIFkOmpVhsWzCOLjYyE+emuJHMOQT/FQRklSBr0FGR45d9kfghBymqxfrs
IckQR7toZNNX7jQqZD1XCN5HGs54RzPsuB308n/46FJ/SSlFk2xF/pNqXyFHPkDJ1mFHHdzm4iyl
lbcweP9M1S/6AtR1MxBG046nFinG2XT1jlFuxbRhnwHFppaJMyKYUNHDWtmWryq9Yd8X7rGPP9le
6jTaWctZFLVhGxeTqkN5pr4D/BHq2qAECuDMbaXBeULqHqg4We7/rnSQZXHjKB6JdRGNrDjqWpxS
gZRAtkA8xSfbUopcfRlwlHFO1NzsUWDxcQ+haro3wN5m+HJZ4sTUykHBEmB4hH9rzOUHcD/QJpSd
f86fNnHy+j7sJXx72En5bcEPQAeA3bBROyyuK3CgbaN0ChvFfJvADPGnbaV7z7jPkpOaCLd6qGn3
+8dhiteFLvBDCuy/6qv+6tBMREox4lkDrSa+zc+ZjANPvhAfIm1lZChj2CsJ8fd6HGnJFR6RDEcn
3T0f1jRIl/d/HUlFnBPhCbWHKmQKJ2cc24ErkjYrVQ4Q+2senACMwdMYfExZ2n9khwcHO15f1Tqn
74VELXU53GDxxAZIMdfHzyr36lVgrapPNWmWzOm/ycXnUJtTIzKBL59FXj8ZD16puIZl7XFr31pX
049KkPl9NO/ruECVF8XQY50glCH3wISJdIGCcMOtIW3HcmQh2NK1Ya1S4ZA2ejfxgyCAeY6NleBo
uBrDr8PzbMq9W1hKetCie6GYMxRjMKFjYqqs6w5tlActfya1Dk2Rl/XBKUOWQ9cQVBywWk8zlaSE
JK0pcqYkCI++AgQ01JUAnrxok4HE5e9bvnXAxLkLxbUut1iZ6o0pZRSAHC4CWfJP+4NRIr3rgxL/
zkndin6D0M0XRxW1AaaqoArV6PEpAwMiMg5sr2/I9H/XLSHrNbPTyyBdMQaqwkUKmvpNUcgT5kvX
nKHdQvionJWMntArxCzGuQ3hnszfd9u/+gxZK2GMplSdQ4aQNx+YrT7m3enxWtuDTBGPpqFlMOQk
3SHdgASfshk4R3sLjXAZ1FXk70hgYKLd4TPAgDBSmOupa9LiPOw9VSFZpUBexpiDLFaTRcJ6Dnrw
sk2h5GGxyhWjKPU4lb+6ujvOtPaoIdusOs90VDCpAhZpReYnYsnAqj1j9D155lPD1Nb/2PBfATie
YNe2pKqoM3zsM08Df8d89X4S67Aq+IpgSJyj/6Jvc/whXXwLZvQaYlWdM81O7A1wzE48iY57+8CT
wL0YT/52Oqr8/WeBncwvim4WVjCuVcMJoF3G82sDYabGdlB57Ce7ZueStbme6J3JAUKeuclaFZDk
qceHF/F7iTpbOsK+l0vztRV2k7uBq1aGkr2w/KYxaANWUffS7SsH2mrHVPtcKkWIYfrzzGqi1OjF
romDaQL6wTPHyMK/aTqSKuEcYHHkVdJzifbV1zgRLmzkt+Ts87X5T2B1VT/MAYXrEzYHitAZ50BD
h5JXB90YXTRcS9BZ9j0ro/9BvcHlqHqx5gT79VQZsjeXcH/sop9Bzgz8YI2uVj1L+4A4d54TSrdn
uwVoUFlnAuaZjQUDwp4nVE3zSur0qUZDX1JEnUyhafCfOc1qmPl6pIWWaUZqlvCJ9D+OFFIbJOFW
jdU9xkRkW4svWZeKq2/xdZBQ12aDfTWPOP9x2bTCK1NsLL3Gu4rJbplH2QQEmLft3gc6rZG4QVRQ
3zuSicUf8MDU4kuw4jy0M99z8+o/UORsKbTkC+t97gra4FvnxVItAPdPlOTjegCr55BevJ29oz2T
GRZMUpsk8OpufOMlfY0vlS9ZDTbloAwBwdkwugV/E/PSbDeFNCNVuBMzsy8NLDUEU4HvUdD2iaxs
MGKJa+XYNyx/4eInziCO6fsG7YsP3zIk9R45lht2WP9YjX2Py3lHsEyTR/AtYhnJAo1BIgdjVKil
sLNjxH/4tV8WfjfgOjsTeXyVLI6qicmo8D/x+0CJYSYKGlsjQhB3jRb+eOMW9AFEKP/Qo6Hnxapv
RkZx6nIPSIYrPvKxGPG2j1Qx2E5B1bIkXS7Q1IbFjVd8QSsJPHTNkDxDXt4QWyiS1+IU+SYYFMvb
4hqQhZV8+voMqndwi4/Ba9EKedvQotM2MKbGV31bSlkLB4a68ntT9fqIIcjrluoAOYqLYjyVSVTX
NcYG2v0/OwSME/IiADg+F1/XxX8bhAr6HzHCyhxbNLB7/gJAxEwzXTedU2qEjUKJ9qQ1npEvxKdq
NpO4QPX0034/giKaf9XHNAF/JdMvo69hqcFh1x5T1GsFdY9tWva104X8Bp3UXcbc1lz5lNi5dyWn
5tP+tlTqU8MvV0gqBprgnYYMJPpskW/kIha+Iub6lz5ei1v4LjL2LyZYgWm1oXUzdJ6xiz8bkGWE
nj4A2IhgsdWL5AaCa9uhHl7wbd6c6sXbnN4CVSpZpfKCuQBm9SlqwPU4rLl8ar9mVCO+BZLvpE1X
tkS0e81gCbZ6788PVvqZniXG0GpefGB+LC++m6uEuh1R920UELDsMiYOmR9m2fmjp+2NfhToWP+b
RNrO3oPvVohJPtYzhxByO0kDvmmy0hsTZuw1k4gdzM1hCH96KhsCcL4UFZYGmnoTLdkCptsJePpt
y5xI21Io1ekuDiYQHzEyYGABY6XeqM6DMF9CSQ0O/b80L1YVjEOWxO23b0cp4RMWFdYVKs6/vcyw
THEBRNyzHgRhzuOtV8wTZfZKPI2rvq0BHy5AT9ejJuOKU30+Q1+CfSJxqB6gR5A7ULzNGsJ95NNT
UmduK5Imk6xbHRJ+Xoof2f+rOvf2yWWwVW9zpF46vuOqGWkORqMJaDroPll4vBbWRtd7YDo+SKKK
0XRl8w8s8l+b8+lnlELGsQ4WVFCY/BDx4Ub/+gs+yPB+qbwnV4ox4exTVVsnQZXN5xV80ZPPbm70
XxgNKWrNiLRjyQXB0I/uWqS2V3zOD0hNQj0aoq/wO5h5gyBhUYa8IScc7caUh29muILTz0q5159I
SghO34nQat/n+zn/kq6vzp2jGViQlWmCkSY5jdk3SQJJwHgj8siEza8O0yq9sBYm+gP6c9Nl1O4N
YfYb9EcwAXAIgsZzB58CX9HnrUIRL72VFACq4m7r598BapFih+k7EE+nU18qE8jUA5smph0mzWmJ
sEGHEsDyB7fUqOMss6+jmuvLFoI/mvWchtvHeZdQ5RXIXCAUaKLyr+z4xhQfLCHPPZcmOpbGhPNU
ZPzfbFTU+Yrw7rnmwTwwFKtY93J1SttUpH2pqKO13CjBnxj+Ayot9x9srufoe50gwaSF86WXxPd2
5jPWubP/lAH4hj6q8OaC9fpAkHONLWCZ6N3BRcJkg/64jkTSBqiD4UVqJISxVMQfnKAzWAGf5QIY
pjA1KAsZZ+GvrNZ6vsyO2ot/aYYDCJUJn73vFpzoZB/bWP6fkH2N8+VIJ4VvgpNxKc+R/jbPGm8A
wwJa4bhFkhYv6EAq8lFzcwxghE9wGF5jDPsgq5L9pkxsEZ70+l8Hi6e+iPhXhvFMiw5t1P796YIb
2eyEH/i4qN67epgsX0QUIAjAZ+dgXLN1LydWd/bMDHXCqHrfzifjo0QI085tBCeRF8VF0dKL+EvI
9qaWHaJQTulPMMftjDRHVjZ/aRGN5VYL004yVE/qO0yxxjVZoG7Iq0UX8yRLakbQUuIDR3/9lKB1
rv0k358MN33X7Meo3hfwfyRzFH+AcTujWrFL12Q9zO1y8Ro9nPSv51Rsc/pRDBJevnzjhrHAh+04
6lHSypfNq9QDVCe/Q0RJ7PaHZp0LabYCQY8q3erU6d1F9mNOKK7Obi872/7iUTxVW3tqLNymztr/
2SgigLDtS/vc8sLg1BtFHACDX80vgNx/d8v94WcEUzyDyN5XUJhuW2HA3PwKtmtkan0PUZQgQpbg
j9CLPC1/qMBx6RwOEdh499AOwt4WBvWRgGgwnIxr5SqOCpU03btHel5wq2qkENAF6g8UBtUzF51g
6vM4ZvruZOkuGF1L9/f3HLK4ZCH9mGwFcM7Mmos0r5UWi3EV3cGypQt19zAW+aQKn+LPjeT2MfLv
CJG6t/vJluASllwlbdiW8kitULJ1zRhRiKzASBrzhnF3jrAS9nmxn2TKZ2AN+2/qOY2wy1bFeYiw
Xk8+mqhhl5Q3AEwGK5yNDDFhzdZjvSnE1sP3swo+gkGvWr2/lRpVzPeGgpbwUeNx9eyupIO4Cz4m
WSRJFmBlysPg6G+FPFMIccKGiWDYofUEv5sQ004f/zNdMUsSD2rJix0P/NxCyUO56PtEDqJHeYGW
pcb7Y9tN95qHosYLvno9/9Wou4BVrEqeaLMF8LIaXhhkpyyWYXDL/B5xJcN+nPWXDxCiQm0NQqAs
7rXi6QA0PXVvABGwoCG61E8OHnJqellgNIc2AburFKNUe/jUM+Za0aVv7IRMiabJQbZ6T+PWP4ow
5ZKhEhoywqJWu1ViNJ/6FZZ6YixFcn4JwTBVt7uogNw5hk6dJ2m8JAmdCCGicXpLCznQsCZu/s/T
Ur+qjSvv/GEnctDfCwovYjAGquFZj7GlSL5zOhV6b/dUK03SDOh3nzKzB9t2mtLSMEbmqqc+siQ3
szwEHt98jYMS29lJeyRekXhkVL+cG3hXN+TkCRTLnmEibLEVY1YV7P8KsIaM15pyzY2hxng01vqi
Fl85utq+ZFnyJKpQKQLiIRRSd4IDPHrorzwVz1FZHbJLQGTxsALQih7In6cr7RXHxTeyABWlpikX
9fDxC9LWia7u7KH2l0fXNT29mAdL46WIlEhZ7q3C1O6sU6rzqXgPZn6W0UFEVZgARRXpe3EKDDA0
ysWYiNq8M5BzbQ6wExGcUsFO6Vq6mQWOKR6XE2cxRpMGcswMGEtcQf78bo0xuNYbw3Q+b/16Uo3C
ZqIqKw/ZdECCH9IzuED/u9ShCMakKJpOSiVIm7TD7URkIsSHCsakkM/4ubC+q3j8PEr0+8USIvZB
x8jeJ0rl/L6wmdxcqhc3AC+KoS1YB0SnKtt7IUzENB+G8roLKenUoLj+E6H1j61PObSHpjUkGjJ7
knOkrBOnoNNQoWxpjGBA/ZMhh5BIaSI9lUPYsmyufErovO1IYfBXpeI4wUmS72GKH3IFmNDGtnfL
qhgbhlkmqxDOFSkenMinvf45POftzTggnmPhw/OeVgXEP5blnpyP/2vkxz4taXh4NJTpcDilHSj+
ZMEdC7tfGmvVKwyO7SoOYo6Lj3L99+VOooeNXTmm2Rd05rJOwW3+yyaRoqfGUnUhvnhgwoGmvM1x
u6phcTocH3qvW5/11WJV14PDZFsq3lm+hny8tj45rfyoIWXqRmCLOICwAlPIWRRtZEktUjV/8JMy
WjSv2/gkcqS9RNFP/3TB3uTvCanM38i7v0Xf3QQv/TgLNepv0EI2s2wiRQsw4UxX7UVorAUz7gjz
eUsqKw+35rdyGrIqhNzp6sxuM6wRRAnokLm3uWPRDFADEy0PX0hHliIxqdiGYHgBMPhHEd3a3n3M
dID/QRa+oH05L39KQmBOtZurzj8eoD1PoqLF0SrA4Ch4kCeBVmC5YgeR1h8Pzq8gr2U6v4V/EZp6
7Y9JO0cHN1DLyflfR0ZJUAUsEAhuhVmjjMTWEvj8ZNxr0CpC35mzbCW2P7Mu9URISdiqV3AsVtNY
BTWltfnRA7qpGtAiOuoh3PtanLVqrPdQEOP2+qUzA8LHLY3FhnMVTE8iM9Zc/+8DLD90i/CbTpoP
UohxcTY/MoJG1lKdT9XmTWuc/xj4v7ulf2jr28awKuLsBUvPTYo0sklIrOoiXa4iypxFcUIxKU59
wZGLdxcNx8Wn33nlwd6YRAW5IoW+Jw+YDccG1WlSLU+4teVd41UdxBn1smrYBs0TQmCY3SbO/M6x
9JAskrRW6v7BT0ho3NwMPR3elfdw0Cop+J9LeQfvMImmsZRZc431sCVL2zqaL4k8EV3aBueyBmtf
DMKOHR5dviuFpXOHf4Q8sAnjAxOe064QUD7Lxd25GsnzEarwJ54Rm2d7CJJApT5vmYvo48yJ38cd
5hDW8UHT8bxOv/AD7uvQfCe8Kb6EUP3ZY+jEPQtltu5RLh8Hq7A2iU2a4xEDlFqQmRJBH29tznHn
h7HPxP01BdDWoyfPJaFasyQTiRXqGNCsLGzlkcjNsAbkLTgPlx0ecw25jeM68Jq4f/TfqsxEblvT
qNL/4FycMTYJ/wKxZo/L/LuXx+vUVnnXdsl+e5640JVKrl3nOm6F8slXrdXMuKkLTJj+4skrZyzi
KtdylUbhygHtIecLUsE13JIQ5Yh3KINsdAvGZi4WZPFHQk0/1//Rl90VKC5xl8P0OU7P1xnMi32b
iJnfBDnp2z1P7Wj3eidiBEp3INif7BkHFjfBRQFgafnvM/1NQdREtyQK5iEQFXlmf9pi1RZGQ0gH
cu6Th+B6wKA0pDy3mBwY/YC/POLwMT1dfoPrxNwx2JOIJ61NP1oOaaxK6qKO7jbKDmxttAdvWDH+
tzcAhOjIdbA5YccMkMUkRx1Okz0RQDHX5f36vMIyYQPMptI1j/ONVyNvxtG3+MQPVHv4+2mATwMl
hqZxLld/dGrxZMBgMxOyEb7vc65vZH5AWMvXyrRzo+DN+blGsKYDtzk5xE0MyhuVy4OvVsYnBw+o
ARFU6+dcL2UgP0Ty6ZkajRyynDH9QrTuo0AfLU+vfCaDyYm1I9whEoxnYXqcWscTTl7vHq6pZy1Y
wkvJxE/f4ugkQLOtOCglqn6njESkeNeZrfTElUsZ3ukV5T8CFYyC5mGqBtnCvi5yr892sfuTCrgv
7XOxXxFOqVDC8yRliITiibXweBEyt/Ln/1AA/B0LdVdryJLGeVbFUvzjaBLYAnNIO33L12KljD/X
gvx1bZvLeKkp9g57bhUyYbGDSWN66uUJnuYUYpLZshfGCSs8am8Fu1jHpHW/0Rx3f65/XcEaqstH
u9WN2p3sfbCQxNVvoarTu5okClDYR/BSduSTXDCF7ai2uBvGhuFkrfjzrvj7G02LMbv9WZJ5kb1n
eC9PGMYIhUVVMwJAeeNh00jJM71GVYm5zDNgtS+54oET+/KQcQUDUviEuskWvqLdg4e23rl+Dfad
Po0SLlJpwd5Pr+CWabYYWI3VSIUvGjg54pIzY8TDOTTQXSxpZGHBAx0UZv2dTB4rIF11ccrW7Niv
yaC1VXrtipRa1+/1nfipwXErokh2lVhMbx7q78ucbW+3mm7ivye24isaSah8Gj+bAwjgVQKqk9zS
ZF5Na7PDbB0LkkjT+LION3X/JJWLPTAVpqtXcTWGEllknmAN5NTPa+ABbtDo/LwoqYbjEk9VyBOw
v+4jmMhOS9/snbbX2K2tXY+XYKwiquFOKq3Brw5Eha2V0teIm+jqLO2570E72JVaHXeNpTG2AadQ
Zk4MuK4C3O4kGNtSeVJkOPtvU15sIfBim4YBg4bQAfDN2CxkqbxOR5sEBXdYWlq205MpqLhb/0Ug
2dLFc1a1bzj6JO8cuXSUQXDUS+6jU+SwbrKzf1NLmB6mS/m0c9WI9B8DKlhI7C9rSAp1D8vHcGta
UBwKrfzlKyhZBebiCOReBQ0kGAVnNSDP00A/PcRWeP0RFvS6lKkwQB2TZkrr2Kb0szn/sngniPnn
iY0GExS2y3dwRuiqaovUP3KJjDZ/SqxmFkMyiAYVURdTAhymUuLDDeT5aWicirB3aY4EEgmx7uYc
14V+AsDw38z6aUSO/7mgItG5euGfPAEuA9TJQk+oyfM3uaitV9J5NHYsm33Y2HOpvA1qZ37PCxaN
1xndtimJ+M7XkRSKtrcGlOvOLTHK2E0yo6gqrYkz1jke7OuW7dFNShMgVzbljPdIuax82eLsBmfj
RuHprIH1VUGKUuSVwcp0gG0l2CFGDmMN+jOyhOgtU31yDtKQy061XyC9BQ0LIw9ZLdZbAiK7/eFw
V88Inq4w/Y97K/HAiVSPaJUyUxhkIZoGfQSZrXjD5RGmLGVTQtz7BSymAu5tr2uh8HzbeeSeKcOE
amOHqt0ARtVHZ8ZiwRqgz9V7xj5CveyQV0ZYrJFJmA3pwq5LXwTfzwZ1bvBI9lfAvY3ED5lXpXRH
DdkSXsqCu7NsdCYNsTL6wheNEyik9wuKUmuasw7fCkhwZRiQ2xBeGblwtMnpnqyPZHYIVP2CB2ii
wI9wnUdyRovku2Ps8InIiDH4mJtmUVQzdZ53IIwSQExvaq/kFVFTqbKRpArqteVpYs1IOtwlKcoN
yvYCvfsMNYcwm87miUok4fomj1LnutvDP8PAlmVmw/I9GdgmqbOsPkdNKtq71VLAv2NXVcwVNapR
fzNIUTFMfBrNk0zfdV7yvmvvadzdGi0pCSqg+p+vsqwpP6eft0V5TIyPofTgDhIMYTOy4u6LUBC6
m1+z4SMMsiGqaAd3dk8Rh91omBGt6aApBo2EJOAnVWkOV83hES8wFwKFb1eEGLEkswBlH6jBIgRk
GG1PFETu7O5/m+HHNsyrvRIFltmjFW30nhI6c9z2KGb7/ahQufkr9st1LxLsci37BEcltIekIGXQ
HhpMlSvvrU0x/BIsiOy0Jg5ggGcq/90ODDqfn0ClsERoaWLuPQSmvfmvxEWUrqEXLGZFpcmKEAEN
C3IxJDGlzjOHzCle7YR7j01SFpiC3m++Qe2gtnYLyO0UhCeJZxKPSa4JcOwU6HG39xVpzDKKQT5m
2aRQxeOLRwU3Bdxed2iyXhtdY5C3Aq5VYi2eoDq7y20ZZZ+buCjzT6OM8BrCfo4LeaHBLy4d+zlH
7hyKNTq0X2Q29qCxjdAAlrxRKaBy6k1OzABRcA2WzccLVOt9zGp76p4UJWhOkZePsDSZVNfFOlRI
ce+Ba9FJ9tih0BB++gitKA0iItOq3t7/0vOf8XRQatzdmhLyqmeGq/waTZYyHvoNLJCn7n+nv5ul
C9I0qlBTIUNANL0wDMaanX97wCu8RjyCD00nqBt5loyZMrKMuyQXWsw/oi+XjW6l1kWH6oynOg35
y60K7qY70Uxb5nET/4syZALk4qannWK4gbO7mPb2QE0PfaC8+RPszQvJ7x/r4pLoUxb/Rzxidupr
CWU8l8IE5eAjwiDlYlbyj6YHQxx0nkfKESHZSmFD7269YwLSSMGhVxA9TQdD1oUxzSOoTogY/L0b
1E9gXGEoL2z2jDmQ5a0lWt4UDoQDP+MwjqifMQGBo3btknYKSDOveQtPagvq4tnLn9djPU5j67+a
KKI5DVgbHjRitzzWBqE0D6X1wjuzYpRlXgaecyWZ2W0aZuQoScO9dh3PuJwpt6UdGIgwWggjXHwJ
2J5nPuNi2FrK372slBqFOn0NGRS3GX0vuo9YbjF4395Nsys44mud3KYMj1Ri03w6xDW5sF5TPaqQ
eYHUgftaaV+O6gJWqLAon7sXrszxBt7nZ54sNkbNEFQQMn9L6i8yOR1HQzgGI1tTjOwcBWLRFd6z
qlfqp0EG8F8FYjc4sbCbMggXE87U6Xg/yMhLkW/GFbUpYmQ4zyrxtqtSPxh2Nk3MbD27pCgcp45g
tFgnDSQ+kfg7xF3ZIfDVp1AKLe4SBv4vzvw6CI50bMsfWea30ZNy5ndZhrmgPydXrD+P1SnA3R4/
LLmpz6ydm8T7g5j4pvwZLRlqNV2n9xT6le9jlgI4vSEGVkWycz/qHHG3uzu6oMk0+6/xwnciREPs
HqjDCZSV+zV80F1O9Cr3K5yug4vrEI/Ig5rSBbPjs3tvG5Y9nV60LawwzXA1Fd9Cqz0XAtlIiMA4
+cUV6+NZAZOgOaxBVNkwmOTRxBNrwJRv9dtgvNNnAldvMJPfzvwn1c8KGbge7nzqlpNgn8YQX9JW
peBZcP520+XSa9dg1YMt7Xks7yKMhgk55QpyvVXiFu6t1GB+ypfvif78HT/8iDIeB6FoO33kIsH7
+eUVCEHKP5RBycOB8m6zENKLgDn9SeKVvT5zhgfDQ3JnUUJYlQrfaFAmsF/V588SzIznPa3RfvAj
a9w5woFa/Exj/VsYg0q110nr0Ur/ohQKE0JWJ1zOaXFhBeo6OCZwwBu7ghQpb1Hoe0knSPoO4KLV
JvSiP0rNguuplJvfrKjv7hp6d4c7DEXm8I1uhN6Rsx9PdlGuopEBbZK55YRHsc8tE/RvwQi0xnQd
fAyKPhAWbXRZ2EW4kKkfmI1GYKs3BgYXLZw2rD0tWgOZJv2m5Pv1H5FC4jYD0K+U6rIZp2++ttr/
6M0eDoWFHM1M2Mlml4hTzx34X+/JfCumRwubJu7oEykjUbiww3T/H/SMTOGI4iborxQaX/A0aubq
YJlWryGKu4HpruYlw8sFcDGsQ/WqTUj+zTlJ7fyAIbuTDkeKM474xpo2t42kwQpGQfRVUuZ+F88c
DjdCzEiggtqydSf/yBUv3q/2XSBzlrNDnOSrjppu+5k6J6RkeNGbBnHxv6rk/F2T0L71Pa1stupo
nx5mFJtkaEBDFcUs3y8XSXf+u4corMWbHKX2fbt+LEqHvAhzT5BEgzR/i9Jzj9xsqoa89TSlg/2Y
UKPeNdGYHQRpzsjwJafzJMlHT8LVikJ6cJzZhvCVq7cYDcIw4eGta+A//rJAdnJo0BQ3xLMiKs9f
chZYsSQE/xE6B+DZXrkjEGv4FaiB2zBv/6J5yWa83dAq82xZkn8YRoF9r4bFeCR/PE6K3vkarQpu
ocv5stwPCxeYlvOHOhKJkwxkujDCnHAko/tdkyKWqTtciFNTRrG0lYXzdW+1aVkeNsqlfoGgpPbQ
LftvXXWbWJexH0EU00/2/kq/ShhJO/ODYcl4U4IiTrg421JxB6zswx1vlqfnCC0XxuqWFa+hET3A
aJEr/wfUVvw1WRVSrfpQBedoTfT16kdnfQE+oVFjRIj7MFFcgM2vv2olydsYxR5A9RliStFNQZXx
J0Lx7viQBiZXdXkd6BylYYnl+ltmBs8Z4ZFDDLC4TkJcKz0XlGOYARQ67f/runSGK7AWZ/CNwYHC
zDwkBYRHFj46GXjStIC0eXb4oArGc/WdV0KlaEG0fyRaApSDmjgYSzFVZW0JhQ0or0lqqdnUXQ7w
U7iQy5lo3zl0SfYHrPpd2d7HlMBtz5xlfyfMFHEN8+TzjmAupnmsApnQej87/39i11M+2dM8pYPJ
QUWVIDu0+QUEyLnmD+oa1Z5Y0h9V/AywtsYfyt53S1wkQMkPOjDZkbIvJbRyqCyJm+9tk+et5DWU
ktOVvulr3Vi6VVR4B+BvCScBxrf8bdnBA9RUcv8QiXV2asU1kof8/SijwZVdrwxQEZ4g8ydQbK40
ibhONHlkQEwj3YJtFeyP2mo2NHPbDaemlbRyH2ISwGAL4xrmGHicF9annNvMzDhEv3uFCh5tpxrl
6f6tdcw/P77beAi+7bM55fIToasBQBIo3g1DA0BoJ9k6Iv7YVjt/F6QAgZZxoH+iLMdV9MRczM9p
6K9KuQa3JuN1+jSE+vjw5/7eLnTeucJ+hn1jBrxpkbOPwh7T7cKh3ZLSjAmnw8Lceks0uZXAyueN
TnFeyWsyTzLtM2dC0mPZuprmuFsMfiTmJ85iDLOKBjJ+Ws+L6DMCQlxq648MiBymJsg+C6tNawPn
LaCQBo0+i54E9uFbg6S2z1RPBSjozjypqL17AZCE2tc6igypkPZkDtGOCypetOJmOM0/biHa9/0X
ftoWNSw26eNEYVkPhPU601sw8q4++w2CyQcoVVbUHoo7BPjsQaZJaMO/KKjtcHscbFEO7ZRgR/NG
P+7c7F+/frPRVzBhGhi07GZvYx3IKkrTtmxZYIeq4bdK1i0ddkpdyvQvj+NXrLtbY5facJ2b4y+g
eOeM1O3jRhekOZR4U4aJDqo982fJXB+fvdr76F2o0n9oOEl+1Nl9bhrfWm4aQAQF3tLvrEemY6Xi
4DRF0DNaWcoWsHCh8ggHMHgLoZFl0OnsDg7uXH0ozAVvJf0w4S3dH7H+E6rbsrMeZkplMPoGgCdn
dM7dxiHVjpL6p7/twRKU0cat2YnAXDl05kLTQicLsDLCqM3t9Uz1T49VzrxPmTPyvMtMN6lqpyEt
NnWHF8rf/kWJ/RFTGhnpFABeiav4GMtcrPpSYBWHu/Q4L7SfVcXLeGh5EgqBpLA8LNUvkAyYemmK
NdKbBd49QWqG9JUHvkvlOKrbSK3O1hkU5Z/jshvokWlhosH8rNvvwgDUkJJi/tOUmgl52o9jW+uK
mTHjOgKxr2AzaEyW+GDMw9wg6HXESHtbtgjVgJV2nqpGC+9zSnYSl9wEkEqvJwlvQ663UQq3pRGU
+x6ammxMuZbyqrbID4rI2g4wlLcnvF41A+aSUfl6NpqC0plg1ha+LHzmCLTotF9Sab2m5wAp34QU
eVxS8TzZ2WlAzKqkgzfS3yDmhxwQMYGk1QsdNigWy1aASvhAO4zGbHSVfQdbYGf0BWjcS1JpIZFd
oHoOaehLzvI8iMKS4RS9eBFqst4ESeZGXPfr+pJh58ECCPjbNAb4f4aTqygtSZQ9ComJDArilSdV
TVuSU++/BIKzghmA/CiP8TLMogBYaxwOU4DObWq6ZWz1ppjp/34wqj+hpazn24B/O88yA22POwmY
e7kES395dcRcFDTx8ZASkaV6rvx5Oa4kqAxmPNJ9x65n8JzhWlW9VevNX9j0qRGXzC0RMR9c7yKs
DIiwFXajF4ZUsFcYIj7c6tOWkZzclaqphaURXPCgQiEs6F/wb2HaJYurj/41+MhWMDwkqsTyUx/A
PWmnxDy1Ho01wabGIUJ62vdIlgsB+Cu18OP7PwytaO2WMjQ3V2R72UHWqiKcyYhfHV6FibZM0RQN
toGfs8uTKO+vvrEpkIfx0mQTww126W7/ktxGK8XLz07Gt1A6EidwJOCpGdB1WAw52s+DIrP6Bb7/
geMjoXRC0WSbq9N3/gjfMTtVqebzH6CjtvAp/rLKmngbu1i75Dr2CtD9Wmg55E4NooN1Nrwct2HH
EF2bkzPhHbWaSZSbl2qHgILT3oU6M2EBXrlrJ+9gAYy7xYqaYOMTc8veuJE7uwv78mJCiq30Un4l
+p75D3SXIvLWG0pT3muEuZWjl9nJiPpA1x2dp8gbE9TGh64+AWme8G06hlt8nwVV+RwuhcXYxpCt
B/redjj4DpFBtrWvNdg4kUN/S0AkpHYnhj6UzRO7tN0gTnQ1F2obqs3FHWbDBl3rSc1NHDG81HXf
RgmwD8sIFVqQ9SwTwl9t3f2IYec9tx+eaDVNdx+GhYKO7q+Tb115dItjYnxucguBFk52c8BsASDL
Z+KgRXTtWsxb4LGlbWTKxy8SrIpuaw9NMInXj7ocgQGQGpC80pn3tRUAwMFrM+RvMRwEui5xAHHl
d4dGR5I9EMHvr063B2fd87M2lC35lAGZr1bFD67CX4Zt0RejZUumIp7UOr+I8zqz9Uj5oDA6ETTA
D/0MZsJntXUV2klXbADzfxNzrcpiHauJhSgXKuoXbPtMJdjdYm541P8fLOxGNGeBYfOXlqlf/0pP
Yrc+oXiy/O58rHGYUVxdjIkPDSwfl1aFoBxhUctcrKGnl87zVJ7BIIOKIuKvLQUkw4UK4G4a2kcW
Z+p9vJmGK4uskcpxFXTNB/7zDJmLSDm7iGCO1rzl5H0ZGk9+tk+SAIvMhmr8qCV7FAHWwU1kIWyz
iRj2Xs5BfOv1ACSw/p+SidUHqiuqkRr/QPhhiHU5dkSmdpUCtCFiNcHa+IWl20CJdLWaoldeTxTo
EPBq692H6miRwEoz4G8Ebe7382+6NiSJs/AsbY7OwyhbJm9RXFrA3Ixv1pwcMK7iiTRE5URaLttw
hnqOW0SvxTGniKiz4dwEU8Oo8hYLIgydVfaD1z1YEnDDbU+fifJ++dnwFI8KWak5TLtoKoM0gY09
CM84+TebrJ7gYq7LLfRBL3uLNjFfbWvJhAYNjcn4g2hzIcYEEmvn4Iobucoyr0FK28OP7ifDHbGG
o9xA/m9LPtrNecpg8683RD0+Zi8t5yVXOL3p4Rlygy5n44NISPPjLTznNCOtAvgbBQOq3rzDjxYB
9YjngENGUKffHiLgHdXkZow+JRgq1wnaiOBF1XGUNR92I4WtRjFmybuACDsQ+uB9aFe8yKTYWhmJ
IT17NIXbhpMYFLZ6q+wRTnFtMTmTDwxNRI2eBLSko6ABMVInLaBzN6/gOu5bzyX+hyDkuBHPSKn4
7jZuzVcoisk1mockI3+ET3Y7pnfAtsDG0OQxajrco8nZO89MM7Bk5E9Mpxbs0XUOaa/iap8dGsEx
HQlsWoRD7p0aCKJzsSlmEVd4Cs8KjzqgpzRysRJiUsjKulyvRFjg7QX6z/rYoY+g2d6lN2FLum91
ixr2VlibJ5vL1OY85jh5fT3X/M5dPW/lD3w9kXwcsrxcnYGOHB+H7BRWmNGMymDguFmfz/yVLrVA
hA9CWifO4SyPqJ4Y809bNp3XUXtRWAgF8pWLy/IXmYoJ8spTuOurEwei4bulrCEwKL0TiiJpgn+4
s77dnSeuh86j+JGRjB10hDDtX4QJOJp0kyayI+ib5d81txjHmgeWDsTrJzyInFD0nJQXycfm5Np9
7m0tpn/NvtTgRrXYoRAD/mHxkI6ozMIfaLT9j5E66shR9DCYpJQDmrAvM5ggdE99oXon8XHICyXc
RhjDMHOnKTaANPIKYyz7Z9v5jer/jUmDpHQR7xnjLioID8HQPmvVnbobAFKHY7KStBhzE04of9dN
k9gSUaX9kLrioL7QGhA1HJ7BqjrCoDEnMSuakM7v7NKQxAB1Si6AHIsg1l4/OwGURs9ehX6dP3KT
sPvh+TSmKWtlopjuBB6A+HJP6/DAwm31FS4kvLNUK0Kud3nUNMcfDkmJN4fXWetmPPY3ATtl7/qg
oc/t/yCp0XuDArmNf4oQ5vtbUHKN1tv7t5GL+wzekQJaFYT66fbjk6PMZ+qkPt2ckgOaeKfVioc1
NDvKm9IUut8SkTWkc3755Y/g8aXTUOr43jWLNI8q3kI/S0vHifUnRVNW34S9kffE4J9KGWH6FK9H
gz91vmhT4aFHMa7S7Pwv088mv0D1Vfw23rtqnjMKFz79W7j58pTLf92aIp2WG04e4v66PlkUcGHG
xgif6L/iuCG32L4IRsVhw0n8HOdwT5fqYkzYcrpFOOzwgeDh9xnX4nlEcyw1e3vwJ+INLT6iC9ca
pZ1i3pMxoCm3jwy9xgbBrfjTncD9R5Wsh+JoaJDw4bUKRUDc+3GjG1LGCPUvv42qB+5eDAc2bAuL
2UbVuk3eYqX2Ooi+waqTUGpZmar4OAjWAUPq7Ff7koUIB5xAZsVRGnJ5FR8vES+LxnTFijjElJm+
iNE5zpfzKjEZzWpR79K49bFuRz0YG45NcNcWyUz3A0E6U9LpXRoKNeJLjPK5iP6upu7D1N8X2Yt/
1XJuZIwyWnrmrAiMrVrHRu+cMDSu4WVju/tDywdXCdzEyq9x8g6R14WzSMgmeVSErDobBCb0TAP9
2Bn5OAD748Sd9uKfBlgdTo4QJpFhBdsNE4bkPblNFnF3zpg2B+2ryDzTmV+gP7eT51p8FqegwE1H
Etqcz0Vb+3yV6SA4i7a3moWwJrcl3Y6SGlje3oSkG90Vzh4uYeFKsuTnTaAwjPZQmgDdfuHyWfWO
e0WIrAC3a2bq4hhnUxB9CzBsN3St8uwWK0cX7k7O5LL8xSL/+CyYpAs9eAzOW0BSPxG0LAYrV4jz
anAE0e0D7LiXVc82IW9ialwbyvOozrahLMpgQ202MVPEatcxO+WPUqPJF2mbLRXUvYcT7zD0J462
qxedRucZtSqXfPs4HG4Hy3guZyPPWtZDM6Ghr466YRVgFZrFDm4hXWRFrX5bnXhMfrP77FX8X8Rn
6WopAfUD0++LzgcXIvZHJ7nCL3nYeFXOISC0UNXLcAu2W5dVCluLLg+wLzR59mNU+2Vx4lVsWGzK
XO7MSbuCe3zGWVGK6YJK1VlkiBxR1869AkHudbWpx4MMCFJ1EmEdxt4RHw5kwB8/sDySIJD00m9U
l461d2ix0hUBp7S3CeZ3TxAtEtTiYeOlMX69VIY7HSWLC0wgQaipIV+Cr2MYz4gv5uf5mZC5s0Oj
nLiGhY7IhIQ4UgIH6eATVlDviON+d1ALacXj4Ks73DHIKqPKexpDiUAI3rheHP34+5mqdl/8qnj6
D8TwtUERYwOUDYGvr28RH2tYevzZdsCP06Sfc/u1OQMARTRQZO43kcgMlVYjp39015IRvo9ObB+V
NbjMo4DsUcCTfcssCAdtO+ZtolHGd4nWjYuG229wBsXlFz6W1s8MHVWxB/vAL/hnuaMwRAjRNgKS
GkknDNmYjiA0JJSHinOWikJPUOkl5U8Y+4IgTkRz2KPmSv2f1ddVhN+EL03vWenRXANQoVuqXyBB
byQCR1elyb17bHObXmiiL+vyZH+IrhFEBmd006BdqeKmWQGVRUCzleain29Xy9+V2FlCxxurMCeb
g4wt2njowAlTkAM2w2JYKA+I8WZZnv7LqqA6zyXW5LAjxV3CM7Qlypqu50/9cANhuFN33Um2L16W
WbONfEbX3pAC/b7Jm+WgGKUv9zjyc9FFQVNQBSvmqkDqWpYoHI1IdrjGzF/5kbZUHW/tBm3b1wWs
JkB4OOgj/ltnBouPnVDVe2MciTTVv8RLpZn1j0gWuGoG8B31Z2vQBvaTbBlX1oieH078Nl07TGT5
MhuZtKufzTWpFaZJcFDIhQsqGFPnEq8XlQ9UVcNLpGxQlt3IXxljJ9gOAvyJHm8lQQSfI3t5Y0XF
+B+AdiHvCg3WdM8FObxARh23cZPtSC3MVdiV1WBCPVdMqOXqcdn2RM9xd1vVYl/4BNrBvcPw9okn
qgOa7d0Kl5YBV586eQUsxOW/cxv9FdzpTqtd88QPFC9miuO+V1UaaULdFId1UnmdtOHYJSyu4yoQ
y54SOs3tFLCwyhuY8AXw7IPGgKjpJkXYvuvglkn3kOBiuD2Pv3HUceclxyxLQLEphjumPKY0Fh3R
nvRuYGh95tBUa5CVyCvhY5hUwPYljNdM+hZ4b5f/aqporFIG43rJFQLz1GTp/CMGWk3RBt1E/wem
GbvspiNrkpiy/BlF7f3EkW7E16fi8WkBBHDp6ebFbbFxEeXld3yAQJsb98FNhBjILhSWazcZy4M8
0G3ehqRyXQopRETizQk9+BP08OW6WWTVZhT/k4Y6VRIe4SJhIIP1u8T6GsGYfaiDFNPcH3l+hLbS
AHJwvVxEFdk+0q57VC0797GOZzdt4lylTRnjN7Q9IajkEtedzqEHK3zxLLnbW2pUTDxcb40Xfdra
EeaurKd+GoQZ2TOjnae2NtDPHQn+8Cd/faDiN5vFWqRldRMO1KsMqylLcV7/jA06Mj6OX0jfaNXR
JQZ+qII5rEtQe9XHyNKtrD76vvSh3AUeJi1AS5JezNv9tW3jDp1uUy+/t3jaS0Xh79jpHu+PDz9Q
jYZaiBm1VBbDcQHo9E9GbR6U+JETXi3PNba9FpT4iMDAvJBJwaVqnP9D3fTFGj9IbO3g8sQ/MFxn
qVEJpvLudrwvm4uQo9Z/5DP6g34O0qOn40xCrxDP9P+Spuvyv9KaoWvYoRAji+Qc6DmuXVXAsz9B
sXC8OQgXNlbZpI6oyKoaZWPVJYYA9nOrVqHAiZXm9Ef0p/oaNm7DQ10PHW4s+1n4uCSaW8ZaQFZ9
J9Vm2CxzD7AzUmD0mIpzIj7DZDQi9OkWkm7570YjSaC1h5oWS9SkhN76+PLrhI5Peqiz51DtaH8o
fz8amGstpmKDSX1jHwj4xIvtxky/lpdGWRwwm0kZ9vpAfJsACNwvFvqcC3x6WO+AztfNT1o0vxth
vBypgFTMR4wijUSIKwDTP4BmSGl8dc/CqMqMx1E6OxhOsDFOIEl3vaoUDKv+jk9N1lbUmLupSULf
s814hXPHoEQqwmEGe94lZjzOUidmLj4qCUNPZ0BCiw02tsv4niC93iw6EPDFIiU8X4UhkQNTjA5A
k/OjrSq50gNqSxsCcMlnmAFZwphO3JZPv3QCEpMRahxM3jKDaCv77I1gi2HAKKHcNu3+usLE7p5c
jZY4MzM06kRrJhr0D1oejBQCN1odahGXMd+z6OJTZPZUoqaAHEDDW/wvHC9+dVVBoBJwDdKDCql6
g8sgjBsQS5GQCjXeQm4xqII7WMkRVXiDgMcpHj1sW4bXTWKfwjqSQ352DHA10HbxbbDOq3j8gaSm
bL+nQQnCDruLdLsMEdMfR2UMUix+DrP/x6/VFxh1tuRrDakK6dIrD0KpHxzco8c1/JwLMeLKZYOe
1j3l7Q9IyyMgX/lTPKm1OL299pn6OVdcjvp9mMoV3mRH42uJBaWH0d3hS7qmJSzD7qsTUU81nT0H
y+aiDyLhfFNtiKj29bJbZ2mlGJKWPrtTNUx1XUR11va3SQBxAFpliWr4xkWE0SydzYxMJTs6CvZ/
1K5aU324iQ42/3XC8Tt6uB0js0r9JEX7WJsoOPj+W8LbGgy6A5R5QIkVow8CPadIWeiN9xa5tF35
WHtdEY8GX6TKs2SVi5WvOPmMZe6u7Dx+qjHzrSCdjEaHkl+PLo16xIsnxjHfedeRnQwXNmFg8iHL
ykAjalp19MXjzMX571QffRrOOUTl9OcsyoyyKrAg+xIVD1PmvEYCqpU/dxNR77ZLrVgnnMEfV+VD
Y6E37UJuGSFRq7lVNoSM7zkBZmef8RklrJGs3fL8CMVIEzZRWAiDyaTyyNLNRMR8/adeDyOiAY9U
AXUQRip/iiC5hjlZlD3VXYDNzuSwJg92s6gnktJt23c9UvzOMFEcaZsS4e9EXsb/qKqz5ZoM9mzP
PAknjP1HC09P2ycguMk4v/OZexe2OzBCNI735T1nhowho+eVLPSvjqNdqLsZq/+TsoKwliyFXeEh
FFtLXU/SluziBVGFYRYhI15SaaI+vTPxfLnnIDURX3OkNmbdQO1TNoVY+LmKJVNKzmD7XoFI3qAr
yKwlBYZ3ufX/flePXbbEISt89pBhRMD6NPEcPcMR+WiXC/+yBpX2z/BFKXZURvPOrGPs22qnLIly
VBrVOYTnlfwlESjgPIRkAzdXdmw6XfZLbyt6j73gB0ne4MiukHQkOjaoN6vdLZZkXdoH27DvO7fr
z5YmwXwUWmdDvTXhQfJjU8rWOKQfXFhvFuDbE6SBMOtYH9Ww7jyPkVGzfMyQmqFPj5LRGMKVjOPt
HTXS5Y6fHvy8z0h9WVlpNiHyDK7TPvb//wtbPLw5mw3LNeQJdoLb64zxP6sHHVVk7CT5KGysLfb6
KzSVPk584IrA0oOtoIhIE6WmRihhmIs3WaSpr/EheaaX3JVc1mOW1Kaw/xUAplTnaGNkHWhIxc13
RLOMFw20Q9sNN/MACFO3ObfxmGUSYuorq1Mh5iA6tqG3yYVvuMRj8v9U7p1JDZEyvAtpWB6sdB3f
HtduQYJr7zgGfM2HF5tsc7U30jewvzSakPa665dC3WOiZxduAvv0utiXfSdM4tRuD9z1NxOR7FiM
YWdZ0QrJlgNp1kzQTsOxLIYbCbGULHiT2jFaINqN4Oe43LtDWArpvHAq2TOLjA1z7BwqAS5sNd9i
XGvLukZNtg21qP2FVlkBewl4FuJXkIHN4muuXbtM5AObk46Efpp2Y3qVJH76rDvjhLHBc/ZLLEPs
4gQCLh+0yXveAbPgRHEMKrcRNEyX913XvW/2OGemh4zFlJnQPKqU34/33GPR6XabzxwT0b7MeLhR
RxHFWExdAI74HLK1s2mIwKOKlxxac9bFsZw+pNHkTQ90Mn3q0j09X25OLH+6YvY9262nUUvageWO
AQcEgkKHDSgMP9V65p74fbYMKUAWnAy5u67bjoiUsgFJM83sP/Sh3fdER5up+jkxACQV9s1xaUCy
LRBkXnQVu1kuCLTCVN4Q19Tfgj5MlGfvIWzgRWHnrCALHkZGr23mzlooUZCgfO3EO7oBREQwTIdY
8KK3oD24b+h6P4OoxiCtRzW8uosH3Dx3Y48KkMKF64g0vS5rHkK87ooT6Ycg38lqfto9cvSaxFB+
+15xRaKs6qO488PzB+a7CP+o/edq+FTtUJdbzBO2RFuc+0p9gsb2Z/ld2Nv4Yp3KMU1B35FEql/p
WIXqZ4NfDBSgkHAVbz/O9wUORsKSGW738TTtAWrCNvRH6x16CguhULyrEabzZrcRhetEGbbKkQf9
apNgywWAPZTS74/fAb1gixf4DPCpZE5p/ll3chWcu+zA4vO3sZBbyTnu2q3MWPhJqKxZRBZQ9tLx
bA2AEmSCwu5bPSsFBGAUjUNKVItKTieg35RbDEv+M05JdCw7GR9ASKNgVAtYKG7U8DYkvVdUoOhw
j3nbzoUK/IRs79+a1tkjQ04bjBbz/Sb6TeqO8dedzmQfmDGQ+UnOHLyqjjCq0l+27VrbkLIwjgUY
50yk3QnJD2W1aEUn7dbY6+knPXg0exd6msMj1cQX0otgqxwNBgPcKHkfQpgsqjEnP/6DyYBWfcTF
yGMb+fNVEeJY7Oup2XRCIgXZwInslK48zWemspOHeOrKpQm3CTmlvhMyYgAudDJMCeA6ia6d4y8N
qkHdDzwqrxEnX6IumpLQjJ5JD56hDfJrQXDeQw9QeiPjx3Z0Mg10jQqGFNrZNKAVc638J2awaL1u
8w8myfA/s6wWl7t+GwNv4d9WBr5qS+StJqB6H29i6JcqYbxguuiJIFs6J2iAcYHk0eS8hEwamgkc
AquxoLd7gUCdzuSyvngtZ15XUqZDMPXyqXmyxDgYLnY2xd1Pks+UbZCC64A01G3zz/rW+ga4Rf1y
9pq8sTdzyFD4Hzekvh0b85B8bHRu5ssvAd3zTz8LToUtmskPD3EOAKUVPOHweo/Qy0lNiBM4gu9/
UC8xxC2iFw2mU/yosv4AjIq1iy24CF8ILWYXIUEvN9aWcgXlQDuw4m7pycCF3Q90h9EozWJtNPoH
3lO4cOWhsxlycXyf/wJHKVD7iCybtZqNxq6/ejgGnpZ0jwLK3FzBj+A1IbWE1GSub9i3XYyjlmgS
RPY1h8ImwNwyYJ9ztFlnEUhtirtesAj1gPHCp3RN5XFS8UTI98qUL03Y7MIUFjeA6ak2IL1orJgj
lF41GelWxsArXV2qaEzhk4orW2XW7UCbtzLSp/JCN4xxUJQjv5TLmqOXiS26vUZF3EzL2B77jWxo
6t8MkDitwinZFGcIw0UrUoCDBhnWoUo31rXgtnqBPT3RnVktY+1akHjM/v7PB6cX2tJNp719tpU9
mZeTwXErgD6G1FVMsJffi3hw7lAVTu29dyP6+f3YmEIGel4PCqqyB1r7lwIwqFro6P9jttiMv88B
Scu/OYouv9VY3wkYaFb03WPLSOnMCbmF9wU6/dp12/R9z8yLafgxQmCq83tPB5XLG2EjS4sUXCzD
vQwoC+HDqRF8LLKsDgkFHnCKPCrk9V9LPvCns/98cWTNeN+/LagxsRl9lRfeytZyPEmHARl9s2Sv
L6+kDS7no0SyHyZG1lE/vlA0LGq0TVaZQbwnG+kcVMUV1zqZBbA3svlf+xmb4k+8RWGCzno1fZJV
Furut291VPnFiFap+3f1O82Tk4fbiIglNdvH5y7szBJF13R6ysIPRy/vJEbQsxLPAuTwLoLtHkpL
uMN9viR6X4OJ1tSKMneZiHs3xguu1ePx9VJLmWxEPTCAPyKaTWyMKVEpr0R5HE1VbjqJlAa6DeUg
D0n0e9DqYHTcxvoF8//93vev4S6wejQzXrDNPR0AJLha2w+nkSnKkKsS80e/+hxUOXZ7IhooceY0
78T0yWc26NWW2QWS/v8MZKlidn9ZeI7/QsJ1jrHDp/NR6LcsR09b6BwR9rjTd/fJwnRzFoQ9Bigp
LlIz+2G8XZCghcaONOB5ifK/X7+fuKXvdwKSzRBDxNQVce+ZSiAnB9bZF9tsCTE2Bqj+Fhl+UKh4
hdVHUjMk7M0t+vcq+XQRAPzCWtHcW5URwUD6jyQwajPHSRdVmAl2XSH/5NABEvBRwIncUxbPyfhz
nEdwuWh3spsQv6jGUjzYbbrIqbutFXhQGNbDHm+d1EApAqS9Y/697osc5zQnWr/TCio65YVxaxWQ
tD5x+5OHDRWydkywkfypiulDRxMshYc2p78JZqtIP7TSjIPSpX2LM1sgx3fLCEK/Sn+JGvsOwY1h
87DxrCy8XJlJ2vD8FtmKTFf69+ShwajyjRurpxro8+jYjxIvApfy29bNQvhSXIbFrDZJPocJSwQt
xmEsHTBoiM+dHvnxwu3IUYy75Irtt4euo/VgGK75kVmIAgLayEryBKkE1BPfKIK3rrTO2HjwhWvX
gI33LBeKOuWRDgZq3bk/L4duoGmL6LMVk+PCspJ+6XgIYgNzLLJP77kr7RSm1FTnxrEXdrdWHuhZ
6WkDgt6dllU/3xbZPwkwc686/W18Jv/676mSjW1rZ7fu8q9TdUcGJ+GCYEBLIUGvaXbQwT1s4evd
aMdUNwXPO1u2zEyeeUJmhG6vhFFW2W7BaCxWLOxs52Ehp87Mr9jXZdjOmfzKcQEY8WWG9I1YuLFv
l2fr3QZ2HYCnck+5i54aQntuarjCD68rDms7XXbTWTod9KJWk5QdS7yj40B+aoxyqpz3IqJULgl3
7peqDJk4/INj+3g2Fj2I6U1w/ggToVtavVKBbvmV+4g2t2kakzRnE7hnpiyxd09d1E7PCuxMGvnQ
vrLXPbA2DV7I4bTwM5KzokKZzLzsYbMLR/D0+JJHDyt0GCdT3DuukgXN6CLxGs/XyIsQ2nI0zKna
wucPdll7ZUxULinM+YFibFnkYpkrJ1jEU39N66IbXdejmppc9kyB39DKjHhWRiAHWT1xOxWTaiu7
m1I8pUNwsA+bIQQAj1SN3gleOKPyEjr+XRSFFdmogN8oTN8wf1Bd8Av+Tetnx04BAreMy+y9vxdm
Ul/nJOYFOUdHdmeqodNVpEwHY/pW7x75zdjm1p99ZCRJpvS8J7QJDNusx4t3L6QYGJzmo2gcGtDp
bCdRQok1xl00ghgFatAWPSCy4rMLBCXwh0oQvrfROIlFIhqxKtzc3+iRxWMs5t5SI91QDrBOqSgH
t5nWYPjyXMt7ba34OzJqhkVwJm5UFyhwin5dHl+z7qoqz/tybsbgjW8IXJh7UXE1HNld/0WRUsNI
AOmER2196fSiInWt5W6XuQfxmYnTeLdjPjUXBsS7qKQ3xry3CeXC0PXDrjmua7qpCkhQbhIlSDTG
U7T2y+QH71tZEbmdDI38p72xtwImLTKOxHlZcviypDjtr3rCj4EwgDwkaAMt4If2h03gCNz4uNnh
b5P0yMK1YnprnSGeHru7aafQpXkugstOWx+oeN6lRsRr+f7eioXsP/ZGBiOmFYbCwyJXE9pax1nS
wnVGQCaRp/HiBHQ01PxHHwkw6biBqMLdFtmTw0IEJ37+J+xezQeiatlx5GTVVXhkmVvN94ubX04K
K9mkNDQu+scyWtjAGHmo3IYS2gHE9+p+GIJ3TU58GcQguIXgsUQ183Rk5DzpCo20OjYZbPdEUDVm
MDcfvagppJSkEl4/fleg4uaw27VbHCl/AbIxFnZ36q1c7ChIGUmVHjsdQnxU0aPG+JDct9fAzJnO
7yuudH2fXMOvgGm5dFBnsstn4MADqISxNJegS2iN1MDAjdJlbPbcqTNcfAy3enAwqt2EXEFlxi0K
NJFP4yWJu+qNU5SWAxIDUlKRsXFbkO5dPRkKHTv0HPFb4gX+HgEGRF5hFGtV8GYwBgFeDsLvWGi2
bfv93oAAgGdP8pDt7QQbM8u84tEWgJv/plbcMPN8daIwz7mZB06w/Hg1M4cEA03jkjN9sYqD0+tr
9Marrr9WxaBlELU1SVP3t/KNyZxc/sOExGdG+56OjcS/N6di5mBy7E3hLu70k9wdxPFC8vYqICV3
aGuFn0BbOSFdmgIHiInWID96xmsFLtBIYqiSSQJYImrDtsHrUW56PUZTh/b+LULZKdmDi+rNr8/H
siTx0YcXqlsr+qb5ZLcxvvEo0Lj6VzM9gb6ZBC/l8QVPDWNYEWzdArSss+C64AzxFjuyuBKKdqp4
Lg1vUGY3vLu00T9r7vcwS9vcTl7YnvV6l7tQG6nv6PhR1c6aZf4tOmLEJSawnBl8J111JG+Koyoh
OjEVDG6lPLccGbFj8B3lbY/1GFQH7GuS+NId1W3EnrhSTRU3toUHpaR0H2eTCJ85+o/Ml+Gl2jAr
9s1wruyrSDGHCTH7rqyKvlE6QjNwgzmUl0LYF0l7T+mBNVIgfWBNH3clBzTIk29xnsLV+RUBDpMZ
vP5s98aBl7RVB9TFARu3DP/5lUcWEzg/z/G25TkFpji/EL+txdylkKpQ7tXfxXXfQhKhUUJJHklr
G/m+U43YqkmPbsNbtFIBg+MLqz7NOBuItmoXthUo0FlekL+jZNEa/O6ix683gY0gT7ii5UQOMl6W
P+785NXBzQSuVADKqd7hymNF5lSb1QjMZ6UET5QVd6NlHi8AWS0mi+SG3Fegi+eSV+z2tMGuoinF
9cwRoziT2RZkWYJYKuNH7ucow5vEP/9hM8MZEhrHq4rlFzwT+5uWG2WYjnZlEibFKhEYDJfbOlU/
92sTwWkscrDrJCcC7p4jGTN88SUjzvw0gU6D/+JFdCkN8qkjefaVUKZ65ud0NgUsP8de/OMog7Sg
1tJJioR8DbECndESuxAuGVYNXWARSV6lvXYuiDtNPKX7ZbFqjoVlpXL6tipxtUSECyq+ixqLHzaa
SK+KwPzBXF/9GowdIHBIOdnx2Sfv01SUtj5fa5eBILEWap8PX5ewnMhE0fRCJD5e29WpLjaBw2xK
16JYLYqO8QkM1qGHoAxCAGWjYsHxvuc7F3ItvWj/xieDLcxcgkt8gNsZmzlhBM33FJ2guf8ME9gU
QIshM2FUgZOMs58STRAs28gwZ2ZcGPk/jQB3yI9lksFJCkye5ZRtOb9oRPkvYQzwmgMvihsl7QXX
uuM7XmryTm+LVw4woGAwXMfX+34R6kghwRwc5VPQVdHCZrrL2az8AVGE993VGbacAoOyCzXjCGsE
tcbFM4grSa+x/+HGkNRGw272nhcrOcAad9D5GJ6UioXnvWviEl1oqgEpdwhqAwaERLKA2uTBwOjG
Ui/8xTX2q6QiH63pRGbQYrWLBSwGyht3Duqt8huBRpfwg8RAKnnxUFV60qNzlYiFmu6pPPPpJdMd
GDWQIPvQav//dCdPKnRq9tkEfxvwwiVTl6Abh5oKEUhG6/+HiyN7Uy+Bt7O0lSP+TIxHqxYFziaa
i8QNtxs8jcGdB+e2OF12YOxZj3ZJl+1IjgQL8sYf0tuQvVVLuFHEZm8QdSj+7zVBP+4oppRTpSB5
kmaLhWVp9wVnNYbM2PXK2Nwq88J3iBXk7iu1Jl53QNZw835LwZN1pNS7k2ZJP6fQ1R9IgfPyvqvI
Hb3Xmm8HFExQmhKR8C7J9K+ToOrzaLAhI6+A4Cse/mgZoeLDcdfyXXdVUepdCXGrg/ptBYxTnWfb
/QGMLBtSAHJgg5rvvu9kdbJwaxuvyVs6eRGE5bhHVjKStZzHCncaA4vCy351ZuX2XC9ADkWTVGZx
8j3qlMR09wMNWDyWQbGan9+A6R/zp8gXripSTQNOIgkU4gEznF7BfTzrEdEvcCCLLSraIfLzGJ7O
dLNxdTAMpLRC5sdINuDHxIHZycF/CZwznIGBNsVA9zYIrkax2YOAX3+a15WiaqiF2Sd8L40MfjPA
Eu/NM6EbP9WpEGeNJXrPJAFBmdpVu6sbkPuK9JGu9/rvWOn32YXJQYY2JWtW2qrq1HC7OwhWpqfX
WQ9d/ZBS/zvjPqP0dMgcYjx7OIZWhEFXFEqoW2tWgB/58ykeZIMyEvwWtzznMNOH2alAkwGrglAU
Mqqx03xt5GrV31UsG4Fy3sOF4uEFul68uuFqECXZDU//YFRSN2jn5cKpcI+Z1W0RSdFssPpDhGbK
KSJ+pXLLUIeielq7LaoDZZPMJiEqwB+AeMu/lFLhQv8NCLGXsfpdIu54SQADGcOINpMrP7CpTIwm
72tEPTmGhjWAlk196i+Wi7yB/wWVKtMXfEwMMjKA6wengoksnUhK8O2P6oGOdLgZRPPeq1sriwfd
9Uduc9SbAoIUNjYq9u19Q38O2RRL0hHwqXgQH7/SAYX9f5rWTwYw1tVWULGfavnB8hc+e/7IyhgW
P6P2rEPYiLIVBeSaz90QE+RnuyCZkuM1Sp0RiNjEzBzIQOTQ49TEnhE46pZQW26bk8ZMj1vuowIW
1D2YhOKKZWli+AiyIR1Njj4+ibkjv36UwOnchdo7AS9zV1Keh1sZUHYW019rwV4dwACNHT/GKmPp
ODBv8NvJgeH2cgMy/c0cwQrAQ6ktT5MKedx7nvcF5vrut327JYLz8+CUTbQmYj4muu5+k/kvPd4y
zODPai4Jethv9YFR7Tk7u2IziLALvezRWfaKRx+iu65wSA2wjtel/ev3tni4DKj6/qj2IF3TFv0j
FWloFGxSb83e2fKMDU8AWH7wZ7j969U1+A4sut8dHX1lpIJLAi4//Nkaj1QnmjmcFZGfb6umKGp5
8aUoxV/I+rLNbrp4aFezMEnmfweV1lD6uvK/cdbFB9t/D9K5UxkFCYlkATtn+oH2hse6+uYu/24C
kvwR+5em60kKUyhZ5eHcKGEQddXBMkWvMDs+PsYfL0d2Mp0gdm1IEXQYfMuIXO+ou1icnp297wwI
dcqwwMcAkGdLR2sqtDm0Pn0l8bYMLiJMPUNcZoEre5g8OXqOlqHnWIaCC0IsMMTFeOtUqbSc9Z9E
p1xM3u4=
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
