// Copyright 1986-2022 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2022.2 (win64) Build 3671981 Fri Oct 14 05:00:03 MDT 2022
// Date        : Sat Oct 14 11:53:45 2023
// Host        : DESKTOP-QRFTT4G running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim
//               d:/FPGA/DAQSeniorDesVivadoProj/DAQSeniorDes.gen/sources_1/bd/design_1/ip/design_1_blk_mem_gen_2_1/design_1_blk_mem_gen_2_1_sim_netlist.v
// Design      : design_1_blk_mem_gen_2_1
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a100tftg256-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_1_blk_mem_gen_2_1,blk_mem_gen_v8_4_5,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "blk_mem_gen_v8_4_5,Vivado 2022.2" *) 
(* NotValidForBitStream *)
module design_1_blk_mem_gen_2_1
   (clka,
    wea,
    addra,
    dina,
    douta);
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME BRAM_PORTA, MEM_SIZE 8192, MEM_WIDTH 32, MEM_ECC NONE, MASTER_TYPE OTHER, READ_WRITE_MODE READ_WRITE, READ_LATENCY 1" *) input clka;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA WE" *) input [0:0]wea;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA ADDR" *) input [8:0]addra;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA DIN" *) input [13:0]dina;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA DOUT" *) output [13:0]douta;

  wire [8:0]addra;
  wire clka;
  wire [13:0]dina;
  wire [13:0]douta;
  wire [0:0]wea;
  wire NLW_U0_dbiterr_UNCONNECTED;
  wire NLW_U0_rsta_busy_UNCONNECTED;
  wire NLW_U0_rstb_busy_UNCONNECTED;
  wire NLW_U0_s_axi_arready_UNCONNECTED;
  wire NLW_U0_s_axi_awready_UNCONNECTED;
  wire NLW_U0_s_axi_bvalid_UNCONNECTED;
  wire NLW_U0_s_axi_dbiterr_UNCONNECTED;
  wire NLW_U0_s_axi_rlast_UNCONNECTED;
  wire NLW_U0_s_axi_rvalid_UNCONNECTED;
  wire NLW_U0_s_axi_sbiterr_UNCONNECTED;
  wire NLW_U0_s_axi_wready_UNCONNECTED;
  wire NLW_U0_sbiterr_UNCONNECTED;
  wire [13:0]NLW_U0_doutb_UNCONNECTED;
  wire [8:0]NLW_U0_rdaddrecc_UNCONNECTED;
  wire [3:0]NLW_U0_s_axi_bid_UNCONNECTED;
  wire [1:0]NLW_U0_s_axi_bresp_UNCONNECTED;
  wire [8:0]NLW_U0_s_axi_rdaddrecc_UNCONNECTED;
  wire [13:0]NLW_U0_s_axi_rdata_UNCONNECTED;
  wire [3:0]NLW_U0_s_axi_rid_UNCONNECTED;
  wire [1:0]NLW_U0_s_axi_rresp_UNCONNECTED;

  (* C_ADDRA_WIDTH = "9" *) 
  (* C_ADDRB_WIDTH = "9" *) 
  (* C_ALGORITHM = "1" *) 
  (* C_AXI_ID_WIDTH = "4" *) 
  (* C_AXI_SLAVE_TYPE = "0" *) 
  (* C_AXI_TYPE = "1" *) 
  (* C_BYTE_SIZE = "9" *) 
  (* C_COMMON_CLK = "0" *) 
  (* C_COUNT_18K_BRAM = "1" *) 
  (* C_COUNT_36K_BRAM = "0" *) 
  (* C_CTRL_ECC_ALGO = "NONE" *) 
  (* C_DEFAULT_DATA = "0" *) 
  (* C_DISABLE_WARN_BHV_COLL = "0" *) 
  (* C_DISABLE_WARN_BHV_RANGE = "0" *) 
  (* C_ELABORATION_DIR = "./" *) 
  (* C_ENABLE_32BIT_ADDRESS = "0" *) 
  (* C_EN_DEEPSLEEP_PIN = "0" *) 
  (* C_EN_ECC_PIPE = "0" *) 
  (* C_EN_RDADDRA_CHG = "0" *) 
  (* C_EN_RDADDRB_CHG = "0" *) 
  (* C_EN_SAFETY_CKT = "0" *) 
  (* C_EN_SHUTDOWN_PIN = "0" *) 
  (* C_EN_SLEEP_PIN = "0" *) 
  (* C_EST_POWER_SUMMARY = "Estimated Power for IP     :     2.78965 mW" *) 
  (* C_FAMILY = "artix7" *) 
  (* C_HAS_AXI_ID = "0" *) 
  (* C_HAS_ENA = "0" *) 
  (* C_HAS_ENB = "0" *) 
  (* C_HAS_INJECTERR = "0" *) 
  (* C_HAS_MEM_OUTPUT_REGS_A = "1" *) 
  (* C_HAS_MEM_OUTPUT_REGS_B = "0" *) 
  (* C_HAS_MUX_OUTPUT_REGS_A = "0" *) 
  (* C_HAS_MUX_OUTPUT_REGS_B = "0" *) 
  (* C_HAS_REGCEA = "0" *) 
  (* C_HAS_REGCEB = "0" *) 
  (* C_HAS_RSTA = "0" *) 
  (* C_HAS_RSTB = "0" *) 
  (* C_HAS_SOFTECC_INPUT_REGS_A = "0" *) 
  (* C_HAS_SOFTECC_OUTPUT_REGS_B = "0" *) 
  (* C_INITA_VAL = "0" *) 
  (* C_INITB_VAL = "0" *) 
  (* C_INIT_FILE = "NONE" *) 
  (* C_INIT_FILE_NAME = "no_coe_file_loaded" *) 
  (* C_INTERFACE_TYPE = "0" *) 
  (* C_LOAD_INIT_FILE = "0" *) 
  (* C_MEM_TYPE = "0" *) 
  (* C_MUX_PIPELINE_STAGES = "0" *) 
  (* C_PRIM_TYPE = "1" *) 
  (* C_READ_DEPTH_A = "512" *) 
  (* C_READ_DEPTH_B = "512" *) 
  (* C_READ_LATENCY_A = "1" *) 
  (* C_READ_LATENCY_B = "1" *) 
  (* C_READ_WIDTH_A = "14" *) 
  (* C_READ_WIDTH_B = "14" *) 
  (* C_RSTRAM_A = "0" *) 
  (* C_RSTRAM_B = "0" *) 
  (* C_RST_PRIORITY_A = "CE" *) 
  (* C_RST_PRIORITY_B = "CE" *) 
  (* C_SIM_COLLISION_CHECK = "ALL" *) 
  (* C_USE_BRAM_BLOCK = "0" *) 
  (* C_USE_BYTE_WEA = "0" *) 
  (* C_USE_BYTE_WEB = "0" *) 
  (* C_USE_DEFAULT_DATA = "0" *) 
  (* C_USE_ECC = "0" *) 
  (* C_USE_SOFTECC = "0" *) 
  (* C_USE_URAM = "0" *) 
  (* C_WEA_WIDTH = "1" *) 
  (* C_WEB_WIDTH = "1" *) 
  (* C_WRITE_DEPTH_A = "512" *) 
  (* C_WRITE_DEPTH_B = "512" *) 
  (* C_WRITE_MODE_A = "WRITE_FIRST" *) 
  (* C_WRITE_MODE_B = "WRITE_FIRST" *) 
  (* C_WRITE_WIDTH_A = "14" *) 
  (* C_WRITE_WIDTH_B = "14" *) 
  (* C_XDEVICEFAMILY = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  (* is_du_within_envelope = "true" *) 
  design_1_blk_mem_gen_2_1_blk_mem_gen_v8_4_5 U0
       (.addra(addra),
        .addrb({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .clka(clka),
        .clkb(1'b0),
        .dbiterr(NLW_U0_dbiterr_UNCONNECTED),
        .deepsleep(1'b0),
        .dina(dina),
        .dinb({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .douta(douta),
        .doutb(NLW_U0_doutb_UNCONNECTED[13:0]),
        .eccpipece(1'b0),
        .ena(1'b0),
        .enb(1'b0),
        .injectdbiterr(1'b0),
        .injectsbiterr(1'b0),
        .rdaddrecc(NLW_U0_rdaddrecc_UNCONNECTED[8:0]),
        .regcea(1'b0),
        .regceb(1'b0),
        .rsta(1'b0),
        .rsta_busy(NLW_U0_rsta_busy_UNCONNECTED),
        .rstb(1'b0),
        .rstb_busy(NLW_U0_rstb_busy_UNCONNECTED),
        .s_aclk(1'b0),
        .s_aresetn(1'b0),
        .s_axi_araddr({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arburst({1'b0,1'b0}),
        .s_axi_arid({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arlen({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arready(NLW_U0_s_axi_arready_UNCONNECTED),
        .s_axi_arsize({1'b0,1'b0,1'b0}),
        .s_axi_arvalid(1'b0),
        .s_axi_awaddr({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awburst({1'b0,1'b0}),
        .s_axi_awid({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awlen({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awready(NLW_U0_s_axi_awready_UNCONNECTED),
        .s_axi_awsize({1'b0,1'b0,1'b0}),
        .s_axi_awvalid(1'b0),
        .s_axi_bid(NLW_U0_s_axi_bid_UNCONNECTED[3:0]),
        .s_axi_bready(1'b0),
        .s_axi_bresp(NLW_U0_s_axi_bresp_UNCONNECTED[1:0]),
        .s_axi_bvalid(NLW_U0_s_axi_bvalid_UNCONNECTED),
        .s_axi_dbiterr(NLW_U0_s_axi_dbiterr_UNCONNECTED),
        .s_axi_injectdbiterr(1'b0),
        .s_axi_injectsbiterr(1'b0),
        .s_axi_rdaddrecc(NLW_U0_s_axi_rdaddrecc_UNCONNECTED[8:0]),
        .s_axi_rdata(NLW_U0_s_axi_rdata_UNCONNECTED[13:0]),
        .s_axi_rid(NLW_U0_s_axi_rid_UNCONNECTED[3:0]),
        .s_axi_rlast(NLW_U0_s_axi_rlast_UNCONNECTED),
        .s_axi_rready(1'b0),
        .s_axi_rresp(NLW_U0_s_axi_rresp_UNCONNECTED[1:0]),
        .s_axi_rvalid(NLW_U0_s_axi_rvalid_UNCONNECTED),
        .s_axi_sbiterr(NLW_U0_s_axi_sbiterr_UNCONNECTED),
        .s_axi_wdata({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_wlast(1'b0),
        .s_axi_wready(NLW_U0_s_axi_wready_UNCONNECTED),
        .s_axi_wstrb(1'b0),
        .s_axi_wvalid(1'b0),
        .sbiterr(NLW_U0_sbiterr_UNCONNECTED),
        .shutdown(1'b0),
        .sleep(1'b0),
        .wea(wea),
        .web(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2022.2"
`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
VHPlDkoDlWlBfBMvPBmGYmaek3s9hXXhjF28kllYPnaNm3TSnzzpXHWHc8Ye9/2L2yiQfJ1hTWou
Ia/zeQ8h9/dtr6QB5YkyW4wlb/LbMgXb+DGIXPSllNl0IMsRQIcQDbcQm1bO/nlhb+2pjxiuaQrl
DbvxoDwPs7z3LunRxsg=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
lmIhoX8hXuc7tNV1sXY1K2/gXL7Y7Hq73qQF7+x03UWWTRd3uhGmVQtOMVbhIW+66UkWUHiD26zL
fzqGor8bgSNGpSFyS11k4TwLQT4OfAMGO8C9Qmmh4+VENBnpS9TW+wHzCv8oUwht7xYtYRZvOvYK
F3fMppz2sBkUd1lciw98ZE/UmNkhqBuMfIYF43j45DEJ55PBhOZNg91Ls4v3qBHyBAaYPFFoMry3
d5Fw1PZyFQSEOSSpwgyds2aN0g6oIwl7zm0LJrM9VDAOxBUE50hk+oHr4jj8J8UhHQJnlEHm1Idm
rvxKygNKRvfSpa90NYxZJFYgqnrMYg+19+9aZA==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
VkyCjO2onoeZWEoYQ/4ue7X5mkHyTYVW9xjdoTsGS4GdP/Q64VaCZL/jr6R8DVDXPMnH7tRMrDpo
jpYBnyzSgOkfgqM+96ioC2fDyAaG4gYgGLmrBR6qK3/mxXwAZZX+GJ9R/eWXkc9h8xN+gsSSX6/M
jIQCgeT6q7PB4dWT6KY=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
Iub91V+TnhVlZCSLu6iKmFjix71y6/l83OPTs8uewWvkE7WcqYxEKi9fonXEkzAtWzuKwEUqnOlN
VBsNJqPUdKcd22q523mrdt89mpdosWD+hvZdO7ELhJniY5u9h49FFkubpN2JiUTcIcKEYxVNlds4
wyvaYUqbPVH5v2ooJwDdimS4GVn9HerCOgPwfshvQDNlMTxLcYju4v8BHMc5Rub9Q/ihvpQU74v2
ouZ9XIwA+C6pBLwvaqS8jE7HXOokgqJilaX/W/t+KEgiFry/txRTMU9WMD7tCN7lcfjCydmS3Lq+
3u6Hsr0S8BwNjcaDpZDnBTygUJd4JSqREnk33w==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
U46EWFmKmpZGaWfyL+dokyQtJtaOYsa7HCW/+fdtw9/yHKTWFpmqKBZngBj5rPkNhtTDDCJkqsYj
tUXg1j4tgIBaCQn9B0q/aG+B3gPLrudp9hLL25mVbsfiTzdekiV2hJMmhuMoavKKPJHC6zyW7kZi
80er82OQy8h+Df/fe6TRjH9xEt3/b80tRKUMbxkLfnnkAyyf1KfOhB6/uyI4mwXuQR+DsAbzybKR
YtXpOiW72tGrXTFlzcwbHamWZefqsilVpBw6V5dh33vYKGx50xwWpj76maAkpQrOpB7zufeldJe4
W1UOEN84AZdRTLkVSxamWo/wp8nP9fiGS/ItRw==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2021_07", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
qczgIJYpE/SzErzK7eWJBGcDFEzDLm8cKbwJbPXuM6YnJxx44W+E60R3war7K2QGFAkOoCDUtDC7
SghJGF32btaDLzeKm0tQ669sBtQmMIaBrlt7I9QBkNM8zN9GL92qxNC9o3UVWMOYy5BmH8nUPgcE
O6lRubeltlrTuDe7UJQ2nEPHcXjpUJJ8dxktyW+LovBy1OxW8g4GRAsmEJsoOEg0HuDdWcc4IshJ
PvwPJ7LblELAKsdkSt65y9VaklaEm7MlH4ImlgIa74TgRmutLUbWxM1QYhGE5rAzFhGU5i3RJOdx
L3N7GGGvLMW2z9NSHbIFX+/eNII9fNJ9nZbgLA==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
Ti1NUgDv8YPk90APMwfu/mRr38QYwAxZfv0T6zQ89YS55t2EquEGVqrEafYX6rTydLOw8le1Oucv
f2oERpSSSTih/ScZneSZmuPE/Zh2BU1Ajv0j+/+0uEWXU+5lLPbDJjnapTmJXih1MYPf0SHpZZmE
BKj2IEBI9MPZlh6bxpa5BWJnyPdAvHf+UNaMXU9+pmbtrzUVebql4mFJu45Z3+ehmFY4FBW3zXMF
44C4TlHACLwL3vHVMCVfeKhgdVDbpE+/IFhTStz7mZ9h9RKGanQcs6YDVM1R+2RKA1QT1fX4FiQc
1V+FGmrm1ujxmFGXwpfNKByVlfCY0oWhRJCYYQ==

`pragma protect key_keyowner="Atrenta", key_keyname="ATR-SG-RSA-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=384)
`pragma protect key_block
HuEXFK0NXt09xU2yxxjng1OLsT+ZEM4EhqBgpr9D2ljw2vDaMBrqEsRQTc2B9soDq3ewDduHJXBd
OGYxkPnoN6LhjULtB2nTgjcH6NxA4puZ1ZNcndDndVBo8rTW5W1OqHq6InAG0CqPpTIkuqz3ECPl
EysI++MCDfH6tIzlekxJFIJ1McJsTq5rFuLzMMcrmkBxgcayDpOcCFuzZzCczxmt/cCCIKmDybwT
OQXmOcLJoYLP4sFu6R9c6xO8i6p++crv2N3eIxZHKbek9xBBZqQM9EYuEtsbkqAs9XZpa16i5njR
BDFxTKcP6r7JgFALJE89AZhBbate5JXWp0v4ECZD18aEL17CipwcWPutNMdG1apzSPP5y59n7rMG
yxBPz1gKHc3Emkl4WcO0hjICxqmO6dMXoY8JvBSf6ry2l0sH9Ihr3Bq5WWmlhPHnoaNr5jl//vNe
KfToWtn97eoVSt1LnmXXnSpdigbHr0UIg8AdkpdkuNRaWdVicDdgSo49

`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="CDS_RSA_KEY_VER_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
mokwst2bn6UxD6V9UdIgCIG1QQ/d0FiJqYGOTI2eHPV6YElaLjnJ8DnQmZnGS95o3x93FDOoa58C
RwYsX1fVoVtXkj1LuZq0k7q9vEe4T8xMjpkeYtIHY9k0Xhy1Lq/xRlfzGAf9fvf9e+f4r7aR/Sb/
uCZxxugG5niTwLENY1n3NthYL0jvo8Fmdw4Qg0nTCGWlVCws+09K0g9/lx6I9EcuHHemcHO3fOZG
lMc4NaPNozKwnyDMoWUkwiVxyFEPFaQLNYqzjvR+CqrWfhFLo96JWhL+eaDoNuZoBVYQtNH5ZwBL
BoO27Pw10lgcReGlZBz3BLO7T4ddynCx0+eSnw==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
PiP7AjOQqqouyQMoBQqgWIDhUSViq94rIvGiIJ/UKMDspM/yXw1caE8AhWHTjYckC4yLpPAz5P6s
1Z6flzDPrzVwg4e59X2cc4IMCHhedna0rDO804njcc6amRDTeLsMLTkWfvomB4xwszm2AgT+PRnB
WHd09ZUDVFjiBXT+Oa9AicgGJHrX3w823yBPuAa704kje/SzgtiDpcTU1eLmLhLW7LpEd9KIHd9s
ER7Uk9Orws0Kq9PMTqMX4hMn5K5mFakOeOURiEbUjdv5RiIJ2g/PlQXSItM8fHsBTQa6fOaJwQTI
vHwK3a8ZBHpfT1YH+n7wNiNUZwD4SFXm1QVx4g==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
Ul5ZfTHJwMctaNhYRortUZizYMPYRef7uYqPSuMkxsArnxI/cjGh+KRMwzV86hyp/6TXSJIjm5ec
2wX2UONdPN+DOJ84jYC4JbgJQrPnTj7ioD8uLX/WlyPcQzyF5keqFgj5eR5s13FskVWCuAWf5m9w
mhFEKFjVXDAr7gVgAJh/hL8P6Psrnf+LGfiM8JhnDepsHEYykGlpD3fzru2BGgqHWqPqFMcnyVGl
vysaIXiJz/eYKvO8RGcgd3DJAM/wPm9A0m/DWcmSnczOgTjoqkHcBg2H5uJMLvufzmjImi6LYEqq
v04ESDEN31cSUzqUYcayvMFOnI/WNsWbFIa5+Q==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 20512)
`pragma protect data_block
ft+Tz0iueN6l8Sx0WpuHv6liLOkptFWVXqEGRKtsU2jLmnvWeMt/xRlzcnitN5v917Kg6zlNaDKl
LxDxlWg2DS8ppxNFJAbrbMh5OBHdq5tp5x1Qb76XXgxljWNPLqZj+DfuEyCGB8t8K7JuDWKjyLXf
Ecw0I0yeWur+h7F9A0kh31fPF8m+caprxJZqrl4WP8U/YOCJGu3T8bYlzflbR3GQvogRObexWphX
vNJ8IwXY5OOHB3RioFF35ZV84w2m3+DIOyMGuP7VteR7b8RYr/OvE+KrYIvHKJesD9+iuyHwnwj+
30IHIXuuemL2pChsKg0196JnMe18rMhtRQnpos/e7R+cP2efZ8+qGJmBPzmOhr+DN+zOPR3Y1sH0
a21tgsPq0bNKzHZEJurtFAiuAysTe0Xc+7GRWKu07lAg0IybZ8BxlqqvFYy/vF0rsHUjPXdpoKWg
pvN3xlMy42oxSZi//0jyWgnyTI8NkPWRbtNHmzJSc4Xt8nzn58ZpLIXGf8NxpwS74J6suO6tjy/d
eb+tlSM2w/3gMhcTsPHycn1PyU5Pa6ZViC4HiVh16rhhivMYmSrS/wJCicV/Eu67G36xDw29yVpU
jg6mR07h1bVpaQ7y9Indir3L+cUinl/gPNnJn8J0kRqvNXdVZiq4XeCta0AbVP/pJbmj1sQytFTg
hK4Mqb63uvolvTei5eCFXt/nwtfgbiVIHEpwu6xo5N2j8lZPLIxQgO85kGXQ9dWtAZPQ3xEmI2hL
QJxnCw2xp028VBFB+T5Q5xx/hYxxPBe1c8KYvIvGEDwiHPHcDIrlt9sTIG3bzuKm7M4/unzPWcvj
BTNA67RSUMip7d9tb5o2b/uuupZ/BGNY0BsspF2/09TqryjTPlVCsPm+fh1E5IMLWT5tNJRr4l33
VIPIqS9piHO+os4cLbm9UAkYV4V8beFmkYZX0xm3Q8KACUNI4pU3M1UkDwfTodF2K+DtmdU5g5yl
kEoF6zINOgf5oq9PTT5lPqJmU5YyIO4ncgf8IwrPkrU4XxSYyY5cXzRfi3e7NqNrTqCvtIBTXXzv
vtP7JqMnguhmzZt+6aU+9jyiFwgYQg0bL6o04y0QUZmncnnibnYk8zX7EXJTDibV5LCj1IIb6jtV
mN/pxMFSBmB0hRoRO35dEhGICaMZa3p6MjdoO6+Kgb5mdVaYEOZvGjJQH7ZaG3X0APsK0Flcu8vE
gbtLnpGzFgbctr3gr8YIGtuPYj1f14mZ8nVp8qEMT6nw8hM5Y+29GzZfa4nv2pxdVz6GClHiJtRJ
VS8fEbg0cb5+/puFnkg3hkoZrrCwBIa6xH9g/+RUSf+uwBPgh99m+fffKpy3EKmXn6D4mOr8NNWH
nO/HX+mdj4tCTVC2p7V7nXqaejFUhxdr8RiHVr3N5MnbEVaQ9PWvUS9VlbOcE7J9ikZet1Cb748V
uG0szmtW+7tRNAHAIa4tb4l9NHoZUjB83zl35mTwiH/5p8Y+F1sEu6t2GdfDnDVYNfFNBGXHp0ZN
8alKNVo19gV3xYBcqiSpcbvGaP8NHwqUUixYvu1p2ch+4RwHgCMyt2LbAzjbEFGPsDvJ/CruwAJY
vJnKtao3AQ6yrTtJezZZFQeUvTnIMuu84/mYtPnudj6m4m57doFtua8ZxTQe4UmHF1bc/5ulxpGm
nt8i+hR5L6k74efj550jHc+i36wZUrbZGyNjjRaRPxkmtCMx9EaVBhUdcITMGaaaOz2gUuCOZ7vO
9fN9Zlwo/A9Wgh8Vl+elZwDeSYt11QGVIVDHDQ3iCH7HOGw3Q4bBzlKngiRzMaGW8AXcwjl1gzUD
DbcP6TwoYOCM/x+Rzmdrp+aHwHIF6bjEq4sDBqZYNSp92Jhot3zYWYh6P2VzzgpEGzQHPI3yyyhp
cyLrWMLPXJPrBUCHrI7VZ2jEWWQE0CL9lKDNhFQaahOIb4xZsyRacLFKVftWFSlCKyVIYlLAeEfO
l3qU1O1O4zfPbnZyv0kAuLiTQEyyoyej8NTGEKdTZS3yQY9jKwAhryzy283KphVUYJwH7sDI9OIz
hqsL4dztBgwoQxXQ3hxp/XD8gBhyMEqfdCRhMdV9NBIrhTApsgOOJ+qKMXgPhqFLKuI1hWqn387i
KTHKiObsVjO1pcaHJ2tJkkmiiDlytD2dYYw0tdIJv+UWXkwqK1Z5beZ92lalvwUXYDZIVfmDYpUP
+IyPj0U4XwqIdO4mE0GO6TAspH7LxKj5ucYjSLbmY5KMan80Pwoac7ZHIh834TGP9D6+nTvS+fmJ
cZ+Kf2txvQWejb3n4oYGqsFv3agJlguaU6iHsHOvHH2VV+DfKeLVO7+N/0UdKvWt/WSo4SOI4CZk
cVVjChA8FB6VJomRlvg6Pf/2/Bn1VhffF7SEGevib52Y0bWz3RHeIKaE/cQDbYBmC/nva5B1VRZB
URFU3Esi7zEXFUmT8O+XpyJhgdJO3gWW9QDvuuBTeDyZtnbBtidXzjU1wSWf44p65T5trWAfNW2R
9RW1bT7LEuq3YVAVFva66Njp4dKPnxuG/bnEMaPQel+NE3wamNHKcxxbhDSipTc8oxDdtTwgNINe
RXNXbFGCkd57oDFnBkd5OOEfJ072kRkUfqyeILsGpvUeA8jFl2cci25gDa+7KfUZsr1fxnuXuizl
YVZ/4PJ1lz/AEf9Nk6BU+/WPfBHlxSE02f+pneMttXGw/m1y9+cFodOf7wpiiPTLV7nu9al7vQja
RqAiVYnTowvmNyyJlqjXu+RLZHigdMXiQWF5uB1031gN3eWFAb1q1OkWjVctpYmcJaa2ZXFLKMAB
+TB9LS1Kp81WCyGy2XQMEhnp5JqDbGDobszPF742F7iBaShy/LRY6svFJmkvOqg0ORID2U1sgoJm
uazNN2bI+EV6Kigb061zykQTHeF9wFhXMNKS5af8S8raNjH8h1E422BaPrkfFFsomtomKv9hM1x0
Zn75gaFzreOktaNJTwddhs602Q1VENFMsgaZYmejR9IiAcAxMiI99An93jiZS2TQUAp2aHTJmZ6w
60AnEF5bZEJGJipJiqN7ST1NWZjIHehoz4XWtKWhcRJQnfnal+YeLbvKBKGkziMD2ofoq8w8j70z
djjqwXcGoap5S54HO/RDNFkZ/eh3sbtHdWsdIkOPPsvFux9Q6BUkWM7e6a7zsfwOnMvSX7MRsPRm
6l9CCvL9u1jqrGDhJwDtYXgPDN1NVwqKwp5tWqmG10C+ytoB0DEdxViHNorXYwiCjC83Yad5Cujp
TBKD51/5zqH9Vd0Dz8M9TCyeQ8MnHu3GKBGUu6Fya64J+GYGmfIlQIoVywRX1I4DCsLvw3z6lywu
yE+myS2K/2l9YgT2AM2DPhBTqYWpJc7wBASe5X+dfZS48/CpEjojklUUAMlXjtcNs5UoMq2aKm9s
j4w6imihBEHYv2xsp1R3GULiDozRVcAW1+6D7cLK/6b+Ubx9uiMwW8fb35zERLUylrIkpDybGjla
lelNIZtB9HDuLiStLRP8Mvs7yK4lGMaQsF1+NAEuVItl2txV57Y5pwV5efAdlMJMkwhvAXB4JIM0
QHroNNjCex/YdVP0yxqE98ZgCK4mlW4JhnGO1wZSfKHOSVfdXgT0ZfupYH7zupeWIobBQ/1hPyhf
2Rn51pvLRaxcnTdRqL9bNsU3XD0E3qptG9xLr75udUDWDck2E16y+wUy6+YGgtK9BA13pmUI35cp
P+uSeXWKr3lLtTXLp+X4DC4dyAKNxsDWZDLzSL6plstf2Hei9QpEI1ZllBHXIo8/Apf2qLpmzM3U
CtZWjTLTN/jDYR7u5eTEHiY8BkiPDp9RPt+UqQ8/YbPaiSiGt1y/zGg0dhl9qY5P91JgkKi2q9/4
83hKjoxqOSgtSEcScvo59fT2vphsoTV6ScgmFN+CSnUAyOgSGk7D+sLJk5HKO+IsrXvg3DCk7kpi
OzZxG4u3sGO/LzMsVbBhM8rLF4YRQ/9fOUROxPBo9qn+MAY4nKeqt7PwKghSaMfInbOK688PV/hN
DMfjXivKo4CS3h/uYEWsUZUFvs+p7JMxrHkFOV9xKfH6lI0lpybQU+o0SUiYt6W2PzD/a3ayv2Yd
JuLMqHDtltxD6KqRzqS5ZgElyi0ElyO+6QsHzc9TZ72lqEOaPgEvTjau/D5SgVfyVckBh9QE5xMI
01uQwVUrCsnMYOATKu9a9iYRPAXU7KoYv6AdLy+Zu+uSZz8v8WeSseP7v7aIl6LUZRhUbs3CN+iA
IRRTGweVYA8UVysYee+iBCh/Y76z61lZjKUdul3KVR6sbAvqDpDosQalxGJ4AkzZygx3VaTx6lti
uACxcExT2Ylz4jdfVu9f+EUS9Bd3EjLqC0B4YXbZ6kpHJxZKpEBdcppiSwRFlTyIuQwCISkLHR3f
9TPKe8ezhve8hXfWBFw6rDI33fu+GqEN1oDtFZDNkXUO84S/LAWV9cS4FJU+8+Kr4jFd1hIE/vd8
OC+7ssf+12Uc6QulyGo7GbtPNaDQGj4jaVj4/2cDoXWwiswMwrorH/p43Ngv9/sGueQGCiMd4GW6
xWIsebCD3Uk19eeYKBSmVS8e6O+HtsdXmqYIM+/4HXvlSMzT1K/zdm8edNFUf7hETDUL3mN1vegS
vrbOBO6iBVKDfuxNJbkg1TDnjLmYPuOoKGdH2vhT723GlF+5v6xWQnb3xl19BILWbs7Ceqck5k8r
ZvVwHvgYVE5SDw29dKq3wI5cnxW3kTFl2gxgtUJO+Tsw9MGP9h505Ku+5FnTuWJtL3YO0Jzh8H9Z
yBDcKbK92dyuFfzp6/FDhIIGpc1Fo9IwnGJ02I/NCdSbAN0o4sWBx7aD8Jw3UWCBQ6mSD4XcX0Up
Z8qJHxNfUj8W3hDni9ouEd+V1r99G+pUraOwRUu5cLe2X3O+J3ZFIQzHx9DgIYpH+NGIQ0uhoCtS
LJ/ZQlCOH/KGRAzrn+yVkO0fl8HdAuxml7npE3djsl6sMq0Zl6bN4OLsPRQ5N1N+bIMDQ84K20Z1
XNNVi0aFax/jIFU8KWYS8aOaiNrjieRmCIu/2jbUsIpSvufvw2y6fNE3/0K7BF+EQx9oIQ8b/uqb
vwEcG3iFMzqy+SSkkUq7n39F3o5pz7mjxmVMsnkeWExb4yr5PfvfLiO8WMAA3H6tBVLWLK8X7s8T
Ht9g8qhhm2z7rRqBS6zO4YaTi+Nn1NxJkx4L7P8TTuk9mgQz3r98D3YyAmfItq30ZbhGzcQ7qBak
uAm8QoR9OALeoSYMLeWTZOnIZhH1HOkStrZg4/PxpN1v/w9YDQhRz9IeTcYRH2h+ZA91ZO8VQu/S
gzVoCKjnXlbU5ANldimYJKfPhKsN4ikrkI7EAc/C8BLdv0oM1BsMrSakC5PB4XTJutCuS5iF4Cvr
6HyZYzJngNQqK/pX2xJOZSDopppfb9uqm2iXcvn7j3fvSUcsaoZYRqC74p9K9w+4zv6xnTq79zsV
K/WQOh/ysRC2Z3OEifKO/EpwTc2fXdHgh7pXzmRm48+nTHNVIGzwxFR4k/rfTbZKHYUgmV6QUf1x
lu97T6BHNdDEomDPNCzyIGSZUxkWD7ZQhZnUycDbui3pXUKhFE5gHZmKfHz8tDEtGiYipzlyNDdi
kEz0P76CTGvNNCHdjJ+x/EaFEaJm9u5Yv50mUELGkQav0J0G5BWFaCfq7Q3XtSfh87uO0dxm/idr
2AoVlbs/wpQJYgh8zvsF1YBSB8cusg7eCiv2ISWCBoPbV6AP8H3fR7eOqOsLAaBTraWGPsv0SfKv
4xzrnIRPi9c3DVtEI0OYSXI13yTn7rF/aSAiLbivNPjLLOuv8o3m2VfHJMRJscLwTukg1CzvxPI2
p7p9j9/lu6A1nwnQCBTvUKrWb3QfdEaJciknJaQJIDn2cztboq9vejS1v1MGJpA13BQmX0pwQBJe
fFSMh+0DKVRxjAZA5vPsd8OYY12PUChEFautV/xAXkLtA6HCWiEG7Qz6gmSDglFzK4Qa8N0cnfuG
NtXdMWNR56CBgQD6dlVYqpNvhvybkY3cCkSCRFFNZUIYPCu09bnJLnTUTuD0mQIcFZZdzpt99N1h
gmVsrX3dekOAWVFnQbuAHBZgW0mwOXI0UISbK3Gbl4e+8mZwb5DnrCtLt7JkzrpNDFjverP4WFXa
o/vagWzkv2St91yDd4JVnkYqKWhMTZkBWG3/rX54VriJsxW4cbmzX40D8/3jTBm7Edv25Pgd73Nh
Ak9TQUdxo9a0o/qCMHKU6D8sumFk54wvzzLspEFKdnBDJ3mw2ll7cmFrO5D58Q0Tsn/9hXzp3mUQ
UbEGowBQyARJvVhsAqkMhkaoGMCOfllg+jkA4u7QzUs6OOzFszkm0cdQhQudfiRWJMwdumSbLVPX
wxGFR5rfKKLRoXMRtZA5EuMMoqJs1W3RKKI34njiHirYj8a8ZqfRqAu8kVBw1LGr5SK7eIuDW/XB
gWjrU35hpHjfcBzZjoZQpn04SbZjI0qgTA3/F6bnF8kD4MAbkQSgtHFTRxLqFmgfsIa3QW7aBr9U
kMu84m/nSwzV4ZLIO5cY/bYWcv5jwNBLw7LUX7YmGxlBDW0X7uX6Smd2bStiY/qAsOpfAuDwSw9o
K00QVnCVKOR4KqbLlHID2Cv+bNzIQ9mvgAel7keMQnutblA9XAXW+EdRCbrhEwPv7GFGeFSMhH/a
1L8zuIghNGEART1toeNKErwwVP5yxSCDCaNOooYW1qm7kQ3ol04edbqRAxtsv0bdv8lNhwaAXZnM
IU0NNQeo/xwuxxzZejlkTslvRXLUOCqBEnp6po58YDenfkhyOFJYwx8tOqxhbN1NXBG91GMTWSVX
g2MIX7KGXymO8/1/kDpIkJaD9xTaVyhmoOtZOecLJTNg+HDQUxfFQQz8u+Md4iPzBGJqKi5XHUeF
By81vKOnaqLmSHBwRkAfYYymJAm4B9We9webMEkMPClyhbqy50A4U9s6CrhHMC0AZCmFr/49NeZa
go2r7Xu+X5FZa/lM4DIjMT5I5bQBie5TysocJjEX5xW6Sb/dgggJfrVXNrSwwG2rgP9mkecoTOmc
n/ygzfBjWpusrrpUPcrC/oYiKX9RypWSRX6HRlSCptscFm5Pj7Pa9+jRj3KA4rePhTny0LKlI6U6
HWP6JACOrFHO1u7VMmWbwygUsjRoo07+XUMff8ydzbPa3ALvvKG52twZfHZKzzprkZBn7KuhQ9Vk
P2nZYMLboeNqwlsKOZa+5kw0KJ8ZtIaHCQmp4HMyxVUVaZ+M77uZ5e0akTUaEpIXVqc+BEZbZcx/
Loh9jKpJuSGowd/fnHrbzKmGZ/Vkrxtk/rUhSF2frppNsu5SwvgU7zS54SxNgZ81H14NKIwnB7Tq
nLKTn5s1FX70+Vpe6U15tALw6Rpt6KjunQtPUkEZz5Jq0i9548nXjXmHwBwmVGgjRFgIUZ6dzBKq
0L/+K9RMBgHf6eOnLeLFgZl/GuT9QnOxQjcb5Ry1OcZvjujkPRDwBKluS/Ul3GT2k2e+ARiKh6Id
JQ9UYfwpT8pbAKgIsqkS9hK0Rc16rj3w1mcCRZHf9Og4FI0sNf9fGN0o6/nlv6rIAZ/ZnD2b7fiV
xJYzIiEmZH8XaFY+nVZfjhot0yi44yjMvRRxFV++OHsVWlYGHyBke39wG+zA4t6h3FXhSUT5HQZs
B60H4pRuG8dm+Qo1gJlZcDfpVEciV/mORSZIXdzTYD08pQ2z+WKXqxV6V1oNzcy4VGHPpb8RH2Xf
X3011nX2GpqUn1vzfV/uH08irndJouAI2voMiZnDIxwL3hHlreE9/M7SlGoG2yZkSeMRneQ5N185
SgCHn5G8nsz25L6nicV4EVQQtgeajea8Qqs6nabfmJof+t/4ca7NvHoNC31GIGWwCoiitq4eXg4p
aAE9IjdbT2om7ZyuFqL23pvGn9UdLiQZu9RxYHOm7b+bdXFBD0ej5Z8knf6eWsuFnr3iDntIbXk2
HDi0ibhV6dkfjAU6JFQ1taeDkcMiJy5lsih1NggR5grmqRc9CoWh26PEes5KLEY6L00FD28f6wFt
NsNTOh04sSyblD7eq076ncZtBeEWjZL90Ae6UjikVyA/ahhzE0uNNfUC6REab9afDSEKW8MuOgc7
0Ze1xr7/30gBZPobvdk2CVRT+Y19SZtDsMSbp07aj3/0WX+HiiiklY0A672Zr6HsQWFsYdrSGVA8
7MwbQPecp4YcQuUPjm5AYtzmz7827LKJQXogim07I2RlPjVcUphW9KarBTI13WdjPDrQziRv6QdO
L79ZEt9c7jsikUEdjnc8W8o6U9/GVM5Io7IdICErV5becR0lPYptj21NDxnpw6YXibfqrJBvmg6M
g6a9AgunNffUKgBHPemO/6WAXFldEcayN0lbMod6OfeDrHjzHGu+F4II6SZRygd+CjFlVzx+MihB
ctbFB5eUkxb95ic1O7oICsLNZOLzybHRZMTGvS0N5RH22zd9OM1JcKez+Uiww/Zi0ll331SBj23J
EHRJzn/nj/DUL9UHddoXhzX6xA9hYHFeDAU+k6lCFRYtyBzLkTyLPbHq0RR7UZ8bL9ZVO2InWdk4
E4Gha1leUqnJZw4nMYlI8Wx9eMqionm7m6DtUItfpk1MStsZiVICWoYopf4rUriHzRDo2LofKQuz
ph0rAvlqOQWFLflKzrvdwc4hNuHWTzlNNmIrwHj9Bc983EbSJnbOdKwzSt0c/y2LNGip2MYSla3K
FZw95KKKb/0sWI3jviLQVLeEbJJuwDN0eotEHGBGfREKLZjaGC60Q8nXlDI3HVMd102/ylfNV2+i
koWZqsEEI6wpAkMjcJGFUyBQtq6OaMVIe07PG87mEGX7HayPcS/uH1uey+raC/WZZkPQZ4Kx3P71
mFJRcSNPASWYFM9qTJ7OZCy6jA1Yg1eFuEi6UOCIRoTj2p+wZicpBnERec5xUz2g6ESm7YbG3nmN
3hngMjt29Z1YZujY5aGVhm+4r0T5zYCKSLiZTJFVECKTO0/CyWrml0owuTYsP04f1ov2FEev9Le3
6KSrK95lEkKtjPg1gxNUkXAYm040gnPHQh1h2fgieYk6eWh4ATPnbBiQERDV2hlakkNNMUCYeJ5/
zkGNqH+pj9Qr8Ab62HeLtx2A9SP/YRhkrvJYVQURbKXvhJW+BfTHF+2twvxb+wKN7Gg2GLOySTnS
BmxpUMpWAXZmfNgvDFVQ4umKJC5K89RAJCYjaimQXg03FrboZBzvkUFIqVUcZTOfYRZ+rhAcxCIS
Lw+ZLsjRrF8aSfUcRv4aZ3LAUfEY2xPdWV8g/eEW7g46mWq8hJv0pUW9d9vilCzl8aWt68HRNapL
8i2uGurs4ZTzOE4kkMUIU+Jb/MGzy6+wQNA+6NoEot6pfPjadGHp1evv9XrUhdUyps1dfyi509DT
B2BbpmasLhmCOc57KsfTZGD29rlrByTLQ/x5uVtUlL0fuNc3aSajaP0/GC8D5aLBQS3srjsM1vvN
Fjk7hTbG+UdaZhdOqv+21JeE3Ew2Ra+VeFBp3wC/AnvHZc+TE0X2lDp8GoBoSR9CrmEUGKtKKIdW
O7icJT5BvV/WhGnZYFQFlUM02hjPutfCTxJfQ2o3+Dq/nLQaxtb6KGcYhadD7XL+55uVNEk+JAJj
nt/lIEEczmMIagYCyJhL1ZtdU8nWkxJadd3EuP3ysaIsQLdLmLvM+nEgTNZN4hKIOBCRmkdxbysr
UwO6z1mItUoVkM25Jr0dUc4FjcVKz3i6prUaq/tGfx1O09WHgRswS+UpjaaEijaGGqMZQ2FsCLwH
kr+p4LWH8K/lOPH/fbMGwOYgHpv10YkJqUlUpk1pMwJgRHerlFZUcDly0gB4PDQT1qSxOkCusLIQ
HN7bM8Xv4HavJzeQisX7Z47ioLqEMZONO/XH6klMyQYO9mW0kOhiIZRVCezKcV9iomGMnYCP0D4Z
Pv44s58gJgtnIAWx2MbYtHfTAS6FmnfHhdIn+KnRNcch8i8338wSyBCN2MukY8UZKAdIqElRsCN+
dnWz/SKFVDjC3FUAVYSzLYgn7YvUC222qJ9HPlfdiI/W0U0aCU+nvgOM3moYqSRo6DuO6KCju/pp
FCYy555J5JRJBuIDCr62otzK2nVfwdMJ7DfRIqMs32OSFMFB8Sgj5yNGgdFYH8MCP53kBzvprXfj
oK8uDf7/EvqYD5uoAG+5nJWQPpGJ2q7wHXbNZwOmcap17rVl+dsxSZLko4StD7w+utoe9xOnKiYu
qLJsdLN+Jq+tl5BdzHNFZYW/44AaCNblAToKWgF5+0FQB5RAQwn3GdV9Hj2llUD1cwrMp6zGoxzt
xfeXzR5mzjdHW1oBsBJi+7nY7vfj48Gw4G7QHON4o01KIxH+eqT2JFzeMO+LJ1/DRI5sxsDCtXp1
nvDb6JH/rSwIo9I3zB8cYO3sjydt6nMwyxSURYtNEGWYRK4K7LfqABYgsK2e+XP3lECmNEq6Qpjj
KyylRDqT645GtCK3cKsoJ/0eeqahQB3wU+Mdf6SlXnDYsF4yr7ZxcOLy+vZS3dg2YusL/wG2oskB
+fzasYCnwFaFo2Rj000hFe1WuAZew5AX0JX1WFKOKKfFq0JnDJChhB/XvAWGwaozKcnXst3TeKwv
v5YaDecomGjhEQLb7clCLd3UaMr+nU7qgacFPhjjeYfz5/30ZfLOhSOHfhqP+uqL9DX4HCJTuxN6
46q1OkpnaT1vv1EOW6QPdQYidN3txmexgG8wec9mQyAG7TMfDKSvvwgLm9uMj8Hr67i5utnL4lMw
ktkoZfmLhkFzvpyYD6A9wrMtgs/g8eYCsLT6v3dDtDo3Ggl92nEMD2MRDltc3HvPvOevdlDX5TYb
OwiGSIpdqgqRHXTpCPisJoJXCK42uYz4n8QyJ5lmAqtvlnA0vAK+s83qrAOYWMxGsvAkBYiFvqMZ
PNEcctSt7uEZT+hXeaHHhOKI4cqtY3PBHVyvXKceh0yF3zFUA5POzg8v19G8VW+gP5Nm9teYGH8d
CSVF5EQAYH2VSpsOKU/OPO3VEC3S5gq3ycnfHbrPmDdkmyCG9/s+BBUvjee1CosPFM7eD40Q4O4y
xaF/HpKx/9qZGiDnAWLn05KHWhTk9hybkFYbtbjBgZv8rdEucHn761+CQckLM07Uqgq0hXliyena
et74un3ka4KDFrOPNxTsGvAaBDDa77v9+C6cer1LSr69WN06yKapEkFTD4+u8bv36SeZ7/THQ5ag
OocJgR320IxPXyipITZATmytHKQIkY+YDEB8q8/Q8670iJlcJU4UDWJKABmbc30xV0jkYRqA696C
PrNNWdK6eq2Defb48ZysjUGTV2Djpyr2ZjADXNxNWMyO1BLNifD55f5CGTjioGeRjl50CI/ZYrq2
k4BrBrfU1OIxXIrrVRFRo9pD7mgrbIlm8ROO/GicTOhgx0kCmYphBGFWVXR00iItmOyS2MBJ62tl
o8Cyxzmo6gQN9HyUTilBp97gslTAJ2KvUr708Odn+x7LrYA0caDE8kOAkDHA7ZXzDoX9CSOtdhjg
0/eVEZLODPHGoHIXtQTWR+fMcqhpqT0mJJ9tpdC0mykiOGwn0eXepCPEqP48xpHBY1+9a1YfQwQA
XQUR0sWlpoK+LWOFN3g2A4eYHDkW1iHLEwT/3rYlhWQCUmzr3HvsbFCXs+CWkvN2seut54bIpeVZ
pLPIjqq+ZWQ8xxnTBab7Cm1JpxB2GN26puXJJkg9wgN9Q9zVncgCI1dUMML8DU146wzKtxSAQ7mE
CFcy35P/+yILadD5q+ftZXlUzE45dr78O2BLd5Ds0SSkdwldSIBOANFVBDjs2LKd3CioHhs54fJT
VNkDQzMWH/VwmlWuxjyt2mneVw2agUJ9TEbzBpPabrgSeYQwjigh0aSIS+UFNMcVI0D+7WLyPdQl
/9nzYVhNWf3q8nzG8AkjjjVJJTO3VEJUsuPpFOBykU9QyUOyratV8aHsskI+ELbGNv/gSCFcLz1H
vYPDsi9PuHr/SyAevztFIHPa23vPlUB+ljvEeLCJMFcHO7jJP6pYdO6PiQoD1e4ibjeQaMgh76O9
A5KPRhsPwhEPuFCphL08oS30t2gnBAyMW/MoBgp0xbLR3g8Q2RYK+9rtfbggMwgkF2A8pMs2bXIy
4qY4WTc4gIh9uDTtXEPbz7lEQD9ybg+S07l1UbIFl2wL5a7moevyndReqSUixDaR0t9o9Mbk0VPe
FPfJ0TDSA7sTgNx0tA0BiDW4bHbnSSuTB5OeToY0vS5XUIzWvXVI1+UVo5gqg82gfkzojSAVX9lk
a5XAsYO4kTPwNSUPydByWJ6ii7O7EcNb9/+j6zt/VquhtXqbrEhztLm/oDogJ/HKRY39H9jK5zmQ
qXc8ET74u00oPq2Le74WZtHwz31Hei9OK/bmPV9h2MywakwWGTrQGvFtRKNqnoxDxuwkHps86O3g
Hc/eVcPpB68PHQth7CmzJKEKKFf7kLyASn1P1Rw7GEbakrSdtE9yjzTdzWwKzfrmWbEkuCGxS8S2
5r3mCmt1/HZkEU+TXCRY9Y3tYcVcf36QXQVZSpcaA0gocAqL7vjpPJRZeQYlXicH6yZjMrcncdM9
dCODyQcHPOc0OXNaSLch2lcs5cUHwWbaz0NS87CbZkMcBFDE3joSRWBfNdfR8cE9lLcnzIAOliy8
EgkWtavMmWAM4OPc+mN/Qwp4gVexZJijW/kiRBf5WQAM9C2wHruZZpMKUw/aYs6Z6cyFXCBS8GbL
3m8FWn8QSBU7mdlrgD2nqRXZWvepCbqYDyIAWxApe9JT1cbWdDvTZpmbRvu6PS+PuRJvEYIoE6F2
J/tXw/icPWYwYUoeqF/3xhVzpzBJE2dffRQcsT85ou/6n+SS9pZXZPY1WAwk/1UX37twXC8zMzh3
n0wc0jRIdczucmTqFBe1HklIXVrh4U0ku8bUjPI+ZxPB48tTb+wVdH94sKMD1t7cEQkB/hdC1EqU
ZNVxyeP0UbwspAtGPEXq/401c1OQYlIr5A0D0IXoK+HSvJVGSa6wTPSMyoKN5cKTNRJoDfPRs9o5
NmscF1WJZJur7m2Epv3u26isMv8zfWoeGfaTq3o2Cbusikyud0BQCRl5tVJ0OXuILJmTBmPK7Nxj
PFsPCj8feSIpBCJ2tRd82PM9e++6cUH46M6Ez9zNztv22Z9pzJaVK2jsJVI++K9zIWlO6imoS8Sd
EkWCwub6XWMYEXDCM7BwEp451ypBrTbMThdSIxZO8qZunt3UsvllrRolbOC24hElXHVyQwyUic0B
qV1zNUmKukZ32jfPemnAKvqBEghrkC42GctmFZyRVfJkuh0n2ZaFqsNRL7E/ULJpyZ6mLatrxjeM
w2hK/m3jyt1Ok5fZEKxMmXlUUpejPqjRmBwrkJ3Mgr34ZJKoAdjcYDcH08rB6FeOIhOvWRgDecVF
bD3al4CXnsJq8jwr1vlNgI3xosUEcmOijSBsUM3qliv/ptDC0F7k7Hj/toGscyjpWssrL2WdvCW7
V9NP80r12oJHvnabhFKbLt1Sk6P3obxoPLVx+kI0KnkRO4MbUMk3qKz+hox+25ubqQp3rlOEkM8g
X5O+N56EBM7lCJuUWeiK9wVf6H4oyQRIeqRh7H66IbJYEpH6ng3qa2l/e71gYCa/G0G2ZqzYJe/q
ZNW7N4MKzG//w9mJsbW0SHnjtypwACuhlq+67mOR9VU/ES02+VvjKg+ZCRPutZB84F8TAxahqC4I
abeco872GeN9Qq2xxUObdhTuagBRhWCRGSxKF53sNDkpFkY7GjITJZyqe88L53agRHKbmRXNEa+a
k50zrCoCTE+Pj6I+ui//7d6mAw3c5SCLv4GgqDraSOv2hPOrS34zISys1CFY+OPz56AkuV98fqj1
iPeNFX0Cgx1jkGpWFwUyJ+Tdgwnvw8f9Ul8OupVhfkRTvLTBE4B92dUDdc7kX4F+VQEjIYuQlYiF
z1102qXtLEJhamal2/AVDD7uvfqsIpbeBReeoiGnTP1+75PyUBVucSIGyuEqREWZLAfO7os/Rlk9
CwPrnJOtHSZr52zbNfrU/o2dAx7SFCGn3HNe30VHsETcqgK6dQwIh/bMGfkPjqNP9EY4Ut1cyMNv
kYd2cbnXFcF5m7rHWJ252XUyLaarRXnhj6KeB20scsRHFiSEudBceA2UVdQMbuHLT9aJ3izXZruJ
Z+Xs8on48x3UcDTbY0OgNZrgh5j8I7J/2gkKkBuAudhTl7wroaJA/LAnNsEmO2XPkdVKvgppTJoi
vzguKCVDJMbGiSpZOPKU7ulNNos43lpGP670ubq0H0JG+jzG9tP3JNmsH3Q3L80hPcA9xdksclEC
uEmArBsmYSxgbi7/MI3pt5oyIuq044NIG4t0ylxpWYrzcZS6QhaTJ3CRIyC2JVhWly/iId9vr2fp
WTpEfwgKO95HOKVBTQ+WqtS7g5VKjEhJ2RA39SSwlPTYBQHubkFbnJgyxZgQqQYWFylIQSYM47nE
XQ45VeWfOPG5kNGuNdLYe6Tyg5QSWU25r2E79PK2i0qEhv7kSrloz+iNE4aLW6f4/sPYpFgSfdW3
sdm8RECluqoJ1QGTn2e8Urt3E6wF4ZWXKxAA6SaqkDA3vmMKI0KQVYzJDSYRTX8TCrsjCTgHOgO7
75VVRgP5+294PkqldajXJQglEndIM/gm1YQgXiOy9EeUsh8d655RSYwgj7YukgqPCL5d8ufJXtFl
AieNpKJFmgyIXErMcbPn78RcFDmHVYofNvzzxKOjh7DihSNusjQMINp/Q4XipMWEm1EMfGNDG9IB
VcKG0E8CdD0yHWncdDchDpuUlxBwoWTUn9IoaI0TjoJ8UqnbLWpOLNkRa7CP8MpFGCx/RyiqNAvy
+Uq6n0p60R9XL34S9t97vCtpd4yx5fdUDuPB1HIcW0ZLxA/ffxkvlW7Xz4Y1+tvuynwqC8J3sQg+
MJ7HVA2N1nfzj7AqLoHrT1ZqzTNh8TU2G641vnJUzu18cj6D0BACuRcq8fVsNONS41AwxXfnvnc+
9tuTF/ZKFhjjeMsnoNBJWjhxeFS9ri6FhyOKGwil5bCqWyWdfhRIBwFRp01tXeL/8h2/2f7tIdXn
OcWuk2kjrQ5bCKnKnhrsh9kcLwmH5fWLSH3E0csYDW9bzvyV6YnGvC3XlN5uuGdRli5JexQWThT2
smAyv9ux0oFidNn0lpXYt7r7O3uCXnCMqn3X/UbNnPkikT42xhBTdl8iIOK0aBUx1rRNgfg2kV68
x0FWknRc9FEu/f503YBmKi1kwlK+zEc4b0Iia+orbQtiX6/ApnLKL1IHcCnMKkAkIqR4Xcel8dXr
uO93OutyLPtUeugVxV8tXOW3+obC3iXs4rrP43uQzKfgGLjqT8Ve5XPZn1ohg1++pMI08jhG5wUo
qnwkqOvfOIpj9a88MuQNE4PZYLvEygCvAqSGYjcM9WDQp2O+u0V+Zj3mKGmkwNahyUowxQy/IdcQ
N92vTnup1ER9OiRbisKaO/nSqe86vObr2EVsd7Ga/DHY9As91mFD/laZ0Ko+bsDn0HrNWBhF0Jw0
eikQzPM7h94VtZlw2grdarigugpeQSnOq4Kyt9UVUxXflvBSJuTOThn7LJj7f7SCr+4z9RqYiWBg
DvaEd5VKQc8srUwmFRPa90siZd6x9cWZmQU5VZWKNJAKhbpLdLdncSbQS1+3AJvcjkVat256JbW8
/uEc/sVKjyV/a/+UInMIRkdLKCRdB56Z6tfblLqOPX0PNe2aIqUHoB4nW6fjbc6Z8RabYCiR6SRt
QkvaIER+pCZ7vwglkZGq4sJL97tQ65fTsWBunjZyNVpvxsglR7uYKTbnGqULkWbDNEo+9Ehhe1k/
YjMeyUccXAXERWHQuQRddq4/5UqmZ6zQq4vB4DBCnCkzoJvLcMTQUvf4QBgfdCw1dEuAT+2rlZtG
sVtm26bfGn/2Yvcv0vwfh9OJVacr1aoRict/5ylge3sUlJtLeEHTs25wXCNpUaeDLAO/Onj14hfq
/zzN7ry5g9RM3B7G/UCRpGlWlGjcSBNkxXfDuOBBKsoYwM6BEnUmsLBYGjA5ZUM/1NOnKOI5W7jH
t+LF3Zmim/h0BbQ+UnAs4JNkDw/F+fGdccUmLw5rHBNTKt+XPCX3Ww7FHPPRPyEJhPBMeKW1Gt6B
Qsq9mpF4ad3ZIhFLZeckREAMkVfyEUBPDrUWCPfIxZRokv2X+VqHp6kANqw5Xd20ZppZ3GEuWomN
ncLrDmJWDHjLH9nt6VvY80NFTJARCs/A5kiLv7KCkcNeCacdaRQye5MUm26flob5Aq4ye4jFikoI
Lmib05nv1ETP8fz9Ags8PotjfWhC3bChCCp+1Z0BES3v1ps+jQ6Z/rkCHLyBK9SaULpYX6/gkUWX
qHRav0jlEVpA1pE37tvYFqg6eRLp+31fkMwCir2Yx3TVyy4ikk93GpbX3EW5qX5i3M6PZX+Po73a
8aT0pTzsxMZMbNAsREkKossIfvQN6SKUfVJn1mCPGjjmOSQ7zwMxpUNp92jIvd8YYJINUnAf25si
KLzqWhJWx9butPu+74KdtGwb6B2bXrqi1YE7l790+rM4jdG+YgJsQakfZuuoXBebbEiCqqD828Qb
51vqGrSHl6BIBQMEwMNuQn2K/1a9buldw9KSv2rLRlKbo7xcIHqAXipyzBDh89oQbkjwnY1BlQ/y
2wS+Bb98CNKEFMRjOtkElS0F/ggUSeAhpW15ybpj26sUhaSTtBCMHO/VpVf0wwklXxOa9oEJvoM3
voBZZzv0W87bW7B0uPRmIp8JCh/gjMitwCIZA838rsCyhqLsZB+A4ewAIMDmUYGQVLDWVTWifKkt
DfksSKlz7xQTL7HkijAdgQkNXQv8dBcVm3ULljkXO9zy1dilQoFPbZxUbRFB37dJUyThofMkxYSV
veqwhI9utREn8KZYIhIlfx8etZW0KmkqZfX7QFqOmB9OAqsleU7D+mfOMcxXx1k2ScvExf9u/Af0
u4CrapDZXAL/whdczlE7XnPvLmAbNpSv1bPGjfcJ/tRjYxp9LH6IYfBQmZp3vjbOXYLZ3mPzDswG
boeVHLFaMggnE2mNne3gVJWp43x55OZGwFkJuopwD9/77znw/zTkyEJHarXE+MmQDQ04O7rq/Omp
AR+sSgTNa2rP5EVIfFuG0omt/am5kgCJ3KZop/D/zO7/Syi/UVJv3QObIYKVKRx8HCDST2he3MwX
vAlHBR461+5aQJZVv/VwkPR2btDCTnqkfjFOAm7AZtczsFoLbx0uoyxuHog2OxWcPYzqtCRMWlUD
LJFtTFsnFX7KeRsb8bAgp6J2tK+37iEPPiAlEmzxgAo0TMsUTAh8gILcDiJFlqUj7XTDLmcfpL12
ALWqfZoefYoPT9Ek7UH48d4ULVE1Fl2WNk+v86RHyR8u9X7GH33X1HzjqsD21iU1IhbP0/EkaFs4
bzkPvLW1DR0bc4wpOR5hzAx+tf8c8iVuTff+f0ofwCjFJVYyWmnaF697zc0YVqP6lkwHv2xAG1NV
fL7ZR/JVGxMs06Sm0lYGu0af3Gd9XzOCAmJ81q4r2fUmUdw58syGekcTYz18WQ07EJcUgZimavAC
4Vf1417MrAjd1I8WFp2nIUEP53NKzFQRe+R5RDwUoOIeYCPDo8phB/5NF6nD+wWcX/CdwigoWv9G
/0QfJN+6xt0Ved1ufVXZiu/c0cBkKGeOB94UhtDQ6XqKTJREbhWCCZ1UppEEjSWMHULK9Yu5oYM6
oPXwrsnOF7mqd5gQyBkTdATIeQlOHPbhSAH/3H2rhkKaBxnLVACjmDMaQ9+gqGxeE3ErzLiEQIdD
QEciKDD8o573vfLsXHPaHLfFmei22OegNVTB76zflzNPtI/Qnd4Ds9hEHy7EIgZIhl/j6V3zxMpS
7q+TZj5/saPZ4cWL8Hi8z9oHi1qnZ9MINzFrk5jB7ASn5D2qvG/ompgaO+cjJOeiaevCk2E5IX+Y
AcVLrOLvwgBCoCglgUTuVJ8pOBFtBiMxmW36dSL80JHbw1JUzS5hbJs77S+bmKtvv/7RFaiMDFRk
e8Q0I8kiogHe8chPfu3BjiurzbPdlyM0fM5BWj6H8gtQSsi3WkPIJd4d/B4bezFQKJFdyXUpnSva
OKcl38AgXmouc/hiawGScanWw/9zb+mBn7vl4B8M2arbsdGzYNrAdMK3dd2L/AUeCJLnkiFjvSad
hGl/nykkN3PgPvQ/PEx2rxGBZhhKHbB/mIiydJpzBJiD6Y9bMha+4XPSF72+gCHSJPfTJXkIszAy
dhMkoXoOSB/4j65fVpG85shd1mqnLExZa9hSwVIjc6M/I/Z1fe8qdnSOOMvyig/wIVQmjuDHzMl+
5iUGrNFDwMG0ZycKG6kJrRMeYZmnspbo4CcMuCXakwbdzHXSYu3sFMZQ/WjDxFGoOlMz8b+3EDtx
SwIDiFqOmqZUi64trW4FM35zdCATD12Cam57eLWsB9mfdLJKhWI08qsBiZx+jjrSfpPtJTQh5I02
Bwt1jy9yIrB5srj8ijyVySDwmtTTabygkZI7HYfFy1UCY5fx8T4y2cq802/120Xjh9Q3WW8L7wWj
lAacraUgbteG0gtvoxP5ee64gCpgtgfgUjS8vLCJf46IC1icuhU1ShgpHVj0oss36CyjCJ5hFasy
cXUoSNohspfIduY38J3JVCfV2qTJVOM/POu1UZ5+wRUFc/AFAJShoKuxGfKgJTCkC+yLuNrzxdub
yrNZ71gq5YnAhC/+9NITT9o37eD309uDKWPWRNeOmLKmBAzrxXN+rUGDs0uhDdF5lSJly7z3QLvg
cHU11pYtfqIheRDoclM66yEQ8C06MoSjReuBM5GXvxxVVw1GIyEZxuVjUl6C5+jJkNWrRvmK1ZoH
Am8fmp3v9Z+4lsJRlsyW48/CRnxphCRq2xJNlZAZ8FX3gKXZFtXP9mNZvfuBv47yAmN6z6s5uZkq
Y8e/822pkAkT83sXomll6L1szXgEJVCDvkprVABJMP5clL1ht3HDsNulNY478s6agILmFDWYkAOp
L/ZScP/juowW0ICdCyLkwiRTubpYNbIfwO3siv+znSrPZUGd2wRMdQYA6xdTYvycJnfTGbQdUKIw
OJXvhvZZdR0kWuG1TDUEoubE8FREr5oyl2TOCuLeZCRNVR85Le1Qz0K4SiUG/uGulOD4pDokSx7w
530wyV1ipFbH+gErbblnFQrYbAjppvJZvwOfIZy3YiHNN4AH5u8d7kL3Ix9tKdgw7+o349KPoAaZ
IAHXkC5grsXfvLnii9eiSxU039Ls5ODG58wdpYxvvqYXk08E/ojF6UZZp3la4x69/je+lYW3qXTJ
kAtNLhGGctGRFuIzgBVBzr+cr2n4MiqB2fETXggahdoU3BH8f7LMr+HNb30eqRhBvTkPssXDsjH7
pf1t+L/NWswIQkPyoBP+fa8DTROaAimpTYcxYYGxhufzOstvQ2s4rLY2nQbyZ0yffKvjXfKWceW2
3k/hXby66WEpqTzE3RwhGTL2KDLq+OaybIWbv46fyKVXu/Qzf5oAUqBzILhov4w/0GIotdGWHWUd
XIW8ZPPaRCHR7G8qeVY6sp2iu4yPjeHPn7GBFjT3J4rlUFX4YMhqDeKOv/pK3TWt+eKu5RILEXPg
BKRep/BFVwHxs4/FbaZ9/XcQjhK7vi59jvYf4gdq2gohahwS3jw1f/ysrtPkobjbxjF81FkqhKOw
IyhAsOuYcLu6Uj9yfbFB7FihnJk2eXpFF7VLOehTSFdTlmbR1r6B1yAvo72+ruGURZbEKXeiAV+K
2yA+2ipODJY6vwPby0eUaNCM2cetROGV43xZMIbrihpgBdPWRZEAnggzmF4Vx0BPnWihdij3vH5M
68Fx0JqFzS5Yc0SnuEDnO0RORrJjTETVSI9GUhWR/7UjHglTDIDm3Cs19ZgJYBk/aI0OqrviVIqu
FqDgYvEDKWSm1R/tJ8opLP9WAqOO7kxWi+bVIJ+6uhG3OXh2mm9u/dxYIq9rRxakHozwvJfnvRCj
hibrjb2lLI1vCpLOvigAgl5v+Q+sGn8sgYhJtSQ4qNjh6J3aEC5Ko6hoH2It0hprZJMnQXT3VlLI
rXCW7/QLtYylmZCEF7iKSxWgYugZunNn78/4NsNPQGafeIV8qHm3Z1Yx1dU+3BcQrtoYey75s9lJ
utMntjEuztb/RqpgrAYnXr+Q+lpIi7DYtbkp4coiX0vnobFpqys2+CV1v2vK6vespH48zpdMzq0M
Ybq0dxM7M0WzDvW7LYHBKJ92B287herhulVbRwd9QeZUR9vdW4p1n8h/lmQCwt3pk57GOmvnGicX
oEOXsqwqhq+3UDUWh23PBruntfDx7Kg+p47XUK2vzk6WBzg5OryGfPMLURKvonBuwe0uvSMxzD+y
TXaaPAnLMJsMFzu4e9+R+dsKlWVnxg4B/TI3ywi0vV/3GMpXADzGWxRJiQA598TCyzIsegh2n9kF
+TBeeRfF+kNLmdeziMvFScYXwQFrnjMKN3pH1X3AI+cgx6Pmv7YLwGVc/XuV5BS6fglzEqp4qwQ/
LgkCiUwNE4ysYyTsepiH6JVlywBzMaFlGrB06RTC3JjPJ7tyhbi3eQXPo9uZEuBKcJQ+/at0PehN
pbv64bHQ4Tgt0104MeF4XZ838nBK/zcvwfCES8Hxzce/jSiPG4gaMFOo6rNvY7+1dpqhq4fe5WcG
8QcOpPJ+aSQp1QgGMrbawEOfei38oFDbxpQFHZOY8dZudRAlSS9y7pKS2cm6CM6oZM+LsnOSHuHJ
JFILfJBce/nsvJ3tC+nWoi4LQPCwU4GaQsnoVkatqUfWHOI7BcE/wdoYIt8rAmdQJa0HmQhn5Cj+
GVkieeea2P6lrZTWQIZrduWv0H3nWBw+LipHR5aceVCMvfXOXEX5VXbD5alWGjyRqu8fbOjuaBoa
F+Nepyn56ZyZkvs/jyaS0uuTI2F+8xnR/ijLTWMl5rJab36R+f1PHI5sYTHfHjF0HdKhC9gr17QC
Mrd19vG5raLVH27tQgl0+yRkB0C6E3K5oa5KX0KiJtPlcr4uJ0nrfz++COtcRTcwbCD/y6JYIWnR
cSS2cD7tInEPr8Lhia7FV6/3HHH0G1R66WQYtkp+05Ucanct+Wyi0/F4RnEA42OAFIbp21tZncDu
hlr7GkaaWQuFbQhn2pTUF/VXK0n3MohvA70BS0WJBSzp7lhyJxT5+y4WQtfh+mG9FAewOb68PZ+X
sY7O4NJfDeB+JwTpZoJZN6cQuUsTZF6FtYZVkRkqUkaARzWbLqsU7hkguRipJzHqHizOfTL0UHY4
5ORxIRzqFxgX/OHiEhVWkPdTRczwGWtAy4LjnQlSzRR0J4IitHF0eJYG2rWFmU7qlWf41nqO6AJb
0oSONCKRdNnh19FWIRPPcUCH+w1mIi+cYxS2Q14aqh1D3CsKlRtstmMCJcyxQjIGUQT8X87yHemr
UwmmxSnYgojm/3avXaFUlWrvcPCYrWA0X8z31psFtk6HKaAlmKTIt7rt1j4B0VVOC48dEUqDUE7B
ulZYUD8aMpk4YfQUjS5CMP5sMOqLUg3RnN9zYqVb7b2cwYJ58UlYihXq4mlViZtsXK3LhzOHndAI
kBf6ztl4DB6A+FX81Agp1e1ibA7R8DIEK9+Fv4flvh26afGt03UxJkhFA1eET2aP20XZZEWUZES8
YGDgBObPp/cLrRYp9aUevpfTG7UhpA4c6/sNi/+U4bGSrz8npEWY5T048m4yCvJp+pPoBtf1xt22
IZcJqKTjJk7Kwtrf3XzJoWnCX3e7GZrIk1sYWbhYkSVJUFvo5cnxGX2/P4VvrHKft2RsPd6ts4g/
Ez74AKOsiL7AWnMbA8/zYyCO5nqCMtnv5K3volsk5gHkedI1OEwd7gM7u8ypFvCpic57oD3nvbbf
6Kwjun8W24tf0BqWmPTmf7zkmEvwDpY/NSUbmZBF/fh51QMcRw8T6sgcxnvfQJXISW4eWkj/IXQ5
xi9hbzTBccscyHnMsWZ9eU6ZKaFmpgzl7wGptJpO/MhmcUJalfbkeHBFbhgpMFJvuCfnkPT0ucQg
ihRz91NWxpzArqEUHXzBkAeuzk9QQ8RIFDRTRTmnH2rhnFMg39ACeX8vJGW9k0JSaKr0t7ecKJYD
qh9K/g9yu0jBU7XpuGrbM4wxRXVIbHdrh1PyjIcktw1g9ytwF32Nf0K1FhuPXOYct29fNHo6g6O5
FYq89Bd+czR28ca5lZOKAeo2dLU48Bq/oRC40W1IyPx5D8MJzypt8z5HZSSuscxo+sJ9YNe8HHEM
P2FX2W+hawrmDKFDFh4WwAC4+u9swTJsBUDC5lzHLK+ImD+69SVlIxbl9Nmr3LSS/E6hU1GBjouI
Sjq8xDke7xhKWJN2UF1+k9/QzwuQlBOKBfoi+B1sPEra78dVvBLzL6Z9qzkX2kuW8l866NCRmlTe
j+MpvB381VlHI94pmbZqkrISP3z7EIBgBKldfTBibEZMQrD19/IyQcALHTbmgWpxKfaQ1kBZeJn3
FEl46J7HLSzw2r+my/CUP0H08F8X3Npn8gg7hmsuZlJ29SiEllSydj+VuDIDAfcHi2XQszXICw4g
5KoxeUZn8SlxbfNHjVUGCMPT+jYWnnsJ9khoaV7TVNxSXJSKsDC5XrwRvlNiHiKtWKMzSjhaRtDo
WrLxnWjgWC7sD5xgFWTdE0v5EqoaWVVjFtuMxqg7AkxS9Gupr28azhH30Jxy81skw8LOochFuTkL
33MsNyKicnYhO24er9D141ppLjNzwr1IOZULY/S8N4s6dqm4sCaXh7+nkASptBrhkNGYJN4UI3pB
SjqRU3yn3EIlXXmu4JAc120UuRRAMCz9T+sFb2V8UVa3+IESdyJufPA777Hxaxq9nn2IxfG7yUmR
oqwH2MRIUzc6RvvczjfKq99tj3aK2TqWNq130ygR3BMwewaaoLNmzmVnP8+AzOpr+8eH6ak9h45V
6MxjDXc2kjeSdertnFed21i9f6NJ0VSSzBcuazp00/Zxv4jpeCMmh/swHDX+s24WB65F/XlS17mG
1UHjNMPWIsbfzRsG8I/8krnHtreJN0uYHfGdHOBam5+GC7CVQugfrKWpHM3Adnh3CO9Ov2vVImYq
3f4JVbMXegqbk20Qdx8e/X73eeFTfI79jDUCoWTr8VnwTlVCZXkZ4wxSMtDJUIPXZl1h6sXLKRs6
7E9ctB6HvKlQzlWOIs/gq4jF2g2IpRFSraEq7qSb2OQAqhhKOkKnhf8gMO+pkutG6shTg1jPXs2p
Q9hTFnipdff08z3p9++Hkr2a2Dpx91omWv2bS68w+1wYAGB/sVT7WRsqEvpTEwyxj/xZLHYwv9jX
aqErya83WDtnEBDuLuu4XEeSvEfWZxWmBn3z8ggRTfVGu0WJ4/q32vhNxhYcptldnfeTN/QieF/8
+f1AniWWAfEKNEWAZk2lqb++GPut+XQoC6IKTz2BNBtqY8llqLqqvrJW2NIbApO1Jmjyuf3991tK
nNvVTyddTBP5WKsJDss8QpJLA+j9XezHTzEFXig30CgN3YFXqkubKRJFerGIahMs7hbvEJFpeOP3
CvIy4mqLFI71hRgJ8ETdDpD0pJVpaitG2SNw/ojbjQ8ZtzLYeSGEEE1L+cyjST5TQwta8aLslLKs
8nxPpUqiaMGx9yNf3p9DMw7uWQQH6a+GjxWDR+jCHhkQJO6S+vhKO1Su2CqpetS5u2HmDrMrUfdC
8dF/bF5qpYAZM/odnv0TJBrz+QFmkfuNCVHRweXTNgfPjnZ3rT4G/Vk3M93AtFRAl9HN17xfawlF
juLFz3f7It0JXDnjAu+ldOXOM90kkoSZQuTXeND57uWY5SNqIDW9PD50GhVaUkVir1PGNrqLI+gW
nbRsLep2uK2f17kM7cDnlElMvtGwodxVcnE/VcuxT0fdOJSq0wmCuYSi7bMXvPQezvthtoz1jV59
TMRk0RKfL0NxqaNNYnF46aCTCbfTxiRWVU1DYKHi3Cqq2DWwZ2FN91uCNwR9X4GEmCZ8HGQoQKKY
b6ltAJvffgLNiIp48IfV3K7oEBuhrd2AvvN1lib+ruv7w+qsJbVFgtxDvW79zGiObcrsjt67Qds7
DusDbuGycl/yYLzq8jauVInyolt0zfrA7aXnja+0dKGVDLUWEqEpet/Dh5oM/0V9roZCHwGZje+w
7fhFF/XndEgruONEKt1oiIhrLY2hK+w1zXmZDnLOA6CCBjvLk+AkAFeN95obktuSBglwJj+5+9AM
FqCaX6Is1doY5llzZVu2rsvetc9n/MA5IbAXqVgaVLzCIpoQbiIA2WfXglNJKT7cLZPIJeJw3ZKQ
/vWk/zbSBQG13Yf8BbmDzsTdP/5SyqHknv4zKpp3yCW719UMzp8QIwq3SQiwQzW4RHETf4S+P+Yd
tZGw75FrP4WptTkM793Unvs7g0B+rJKdcnL1FdAi2cIiajyk8ck/v22IPwmCEHdTawtf9uu/Y4k7
meRd0JurAvZpE2aE1xjXzUV6QvAIqDx5UvnLrCD/r+ZJN3wGLQ+NMGXnkDbbTCNu3BixsWmEnFFV
UWGwK2a9HiSHsLZa8mO8TCw8xQehBBjltDk52rIGM0VFqPQSqNY9PYI9HpwUENSeZu/PbqtkzvRX
sn0hSk82ia7M0KuGVG0tUOe23vZ8ZIDFeF27ZAE7UtMy2doL3v+V89jJ9LsAzejeo51kUnDeAlXi
+iJyRX3trYSnwYvhr8cvZ721Zoq/6SZA1h7mgdErw6T52plp4LY2A82455/22y+YZP5CCQlEbMMK
tOYg1+WDsQQj2ylKBlCWZG97m+sZhN9sYbocOL8VHu4ktIDhz8r20yAK/fJtCkTCk7sdJTqT3uhz
LdxLqpBvt1WBSne4YKgyRZYz2ekzBsScvuUKzDhXETLSmFi96/u6c+DfuWjDkvsAPPKttnwwm1Rs
VKN5tAwELd/fa8hbwxEFXbyQTXNRMgRqZkBxbovLj3kd4RrJYKBGgtJqU04UJS3lVigQCFe6a9YE
2vHcqLzW2meZFv8B9eUeH8CkIQOmSa8ybTkg00AgOZqDe/pkaWsv3iO0E5jVIuILyKDRDPKXw5Hm
29Ygw2BMJwR7EjJYUay/EzIppdfiWoBXVOgexNnMXyi/8FrRc/wNVIsIhG/wlpiuXxVyK7G5OWgs
Vf+rcOFkd/yPMg2uClNrSsz1odMsUn6kRcgcCCWjbGNRki6OOCV2PcdelAGhk9OCZJ+LbzEOpwic
jHxsDY9H4+eB4Q9rIZHwQ3zusMxqmpbMIS8BpsJKefanmKART2yTZp4Es6apjSvbJyiPfI4q3O/3
FwJsXOcidrWUspVCP09X8UhKTGUZzb6Kl0ml2+nIFvLrN5VMtj8WYEVBVQbl/byrtbCNNJBIbu1h
aTQFrMVPI8MXfFUTeEahBwjwpqS34SBx2eJ43vUHNn6X3oumHN5fv0gZ1AijtzclREAK8c7SXUqr
YqHxT4cZxHnWKwkZdKE+2a41byytgZWqQBzK3S7qpODbo//3CxDD3IhQtOLZ/rUC4YSA5CvtEPQt
AZ4SSvVaYxAfLeZkC4lo3guZ6lP93rmIE+PBOK3bKkXM8ffEVMFHH2AjOmaY8PhX/l9E2EVupa5r
qjUj+Y8EI+1eTJ6i9hReVNn0ExG90z/Og4dM2tHeWx3R+39hjwZpixFBthSx9yW/PlZZ13V8GkmK
CbULMCY8UoIwsGJ47juGk3od9oK2tSIt2AaKji4FDXvF0hd6hikt7WDrR7V5+85cd8PpFQoDtXOl
Q9DfHH0jlZms2/T+dN4GxadbB5RYg8WetF2SVFZYZoSeww3oGyhbcKcp+E0bBrT0ytYfrMa7buNX
fw3GyCNTdat00n8voTjxq4zHZ5D2yguAwbXxNWCqS9ezPQeSfd3vWgJaYfB2YOAyMzyZGYfJRifQ
VMEseHkOTb1Q5fNXB6Nuv8Fs9pXIFmsHUiapdDASpaEEujLlhfqhMYEC7Ojx7894pEDg9l2oGtlP
/NQro1JqrAtVlHYgoyHsUAGTo0RfMzD05UgMUGer6TdSBzWjosCGMmGC4a70EbOWJtZi65skBqel
wa9bXTB09trWq12u1Z5zi+ck1/lcWWFC7yTBdEmghsynfO8VT14yG5ufPFnsc08N27prpt8eVUh+
HH6UURVJCBib34tN9+UjpeJxEzuHiJEnFEfs1mH/s52KVhJSqd/UMfAURmv+wwbHyPIg+S+2ZCpT
A7jMietWvTUpe0sziHQd8WQ13V+0KN2G567Ct8cT8YQM7qIDDzdFMPiXlIG1aAwDntMZmSNjtZGH
9wZ8Mi4xqLPidXJiyTqls5HrCroo2NERx9I8ApBJzsgrS0Z2s42CH9uCu3QZ3p2OEWvQuN63Flwu
KolDEBF+CegYNHVlPvDgFestfwyb+9wuFgVkUSZllYfjV+f1wnNHT9vHZwn/LfSXYoNuv+lwzNni
aMSulqsvfxwCt2SVAWp/Mc0Q4NGnnw+PjFbX2jCTRn/0pMJcb1lpxVNB3f+WzULW8Tnnu5uUBj3M
gUrRC5y3zbsyIcccpVn6S/dnYT/ZytT4aB+bwANwKTkKZ+xqpbpw17gtQSl2RjMzxcoS3MO7St6P
KqY7sw0CEvd80F4ENP+lwTCPYhDlWCInQWHmRm5OJuO7gJD+qBfyUjSg12p9nVcF7h/xCKzPY96c
7IJz1CL8ZaXfM+nAItsxmDTLMeSzsWGoZLy54KNVlS32OVH8S06jusiyvi0l2FruAnok0Pd+ULVi
M0CP0ieA0bZX/th/8RCx4sfY8/wuJ86l4n90OgXIJNYr9gZ9tg32HtYfytns7o+tszU1YOWGBblA
Ji3t31lojWW85Cq1EuzlJdjrds9GZBWN4nGx/wjBTaBSAy0LXq9skxbJCmrF5NWqj4cOW0vcHGZU
9NhbIFUrEookP7el4rgUrIwyp7Y3new5kEeX7/Ox48d7HWEXss5SLcYRmZAZCSD7vsp40FjwrWiW
MYyodmW6xDCVvY+ZaZ9umjwFU0xlRiG+Xf9vNhPYOr6doJ3ra1cVdkUAkw6s+1EmgNZknz8GxfeE
ARxvbZTZanOxm9jfCkkICYOCgzpxjwNw8qCKBpMVW/borI6dU0miQrlx7H8Ntf30A2FS0Gp28LkY
ABJbt5JWli5BnvUitbD8KfuVfpGhkVLA9mI0FvXhoMItxEMIy0Ef4iLugY9lyS3sUGbSrSUN/9mt
o+ptT2da4/iFfpExoyCM3JpOiP2Z61De1EJ6puasRRaGYzBjOAsObQ1iZrVtEg05Mg==
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
