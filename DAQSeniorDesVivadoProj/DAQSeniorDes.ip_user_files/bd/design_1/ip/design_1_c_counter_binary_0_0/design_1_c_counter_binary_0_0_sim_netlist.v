// Copyright 1986-2022 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2022.2 (win64) Build 3671981 Fri Oct 14 05:00:03 MDT 2022
// Date        : Sun Oct 15 16:07:10 2023
// Host        : DESKTOP-QRFTT4G running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim
//               d:/FPGA/DAQSeniorDesVivadoProj/DAQSeniorDes.gen/sources_1/bd/design_1/ip/design_1_c_counter_binary_0_0/design_1_c_counter_binary_0_0_sim_netlist.v
// Design      : design_1_c_counter_binary_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a100tftg256-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_1_c_counter_binary_0_0,c_counter_binary_v12_0_15,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "c_counter_binary_v12_0_15,Vivado 2022.2" *) 
(* NotValidForBitStream *)
module design_1_c_counter_binary_0_0
   (CLK,
    CE,
    SINIT,
    Q);
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk_intf CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:thresh0_intf:l_intf:load_intf:up_intf:sinit_intf:sset_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, PHASE 0.0, CLK_DOMAIN /clk_wiz_1_clk_out1, INSERT_VIP 0" *) input CLK;
  (* x_interface_info = "xilinx.com:signal:clockenable:1.0 ce_intf CE" *) (* x_interface_parameter = "XIL_INTERFACENAME ce_intf, POLARITY ACTIVE_HIGH" *) input CE;
  (* x_interface_info = "xilinx.com:signal:data:1.0 sinit_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME sinit_intf, LAYERED_METADATA undef" *) input SINIT;
  (* x_interface_info = "xilinx.com:signal:data:1.0 q_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME q_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency bitwidth format long minimum {} maximum {}} value 9} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0} integer {signed {attribs {resolve_type immediate dependency {} format bool minimum {} maximum {}} value false}}}} DATA_WIDTH 9}" *) output [8:0]Q;

  wire CE;
  wire CLK;
  wire [8:0]Q;
  wire SINIT;
  wire NLW_U0_THRESH0_UNCONNECTED;

  (* C_AINIT_VAL = "0" *) 
  (* C_CE_OVERRIDES_SYNC = "0" *) 
  (* C_FB_LATENCY = "0" *) 
  (* C_HAS_CE = "1" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_SINIT = "1" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_IMPLEMENTATION = "0" *) 
  (* C_SCLR_OVERRIDES_SSET = "1" *) 
  (* C_SINIT_VAL = "0" *) 
  (* C_VERBOSITY = "0" *) 
  (* C_WIDTH = "9" *) 
  (* C_XDEVICEFAMILY = "artix7" *) 
  (* c_count_by = "1" *) 
  (* c_count_mode = "0" *) 
  (* c_count_to = "1" *) 
  (* c_has_load = "0" *) 
  (* c_has_thresh0 = "0" *) 
  (* c_latency = "1" *) 
  (* c_load_low = "0" *) 
  (* c_restrict_count = "0" *) 
  (* c_thresh0_value = "1" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  (* is_du_within_envelope = "true" *) 
  design_1_c_counter_binary_0_0_c_counter_binary_v12_0_15 U0
       (.CE(CE),
        .CLK(CLK),
        .L({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .LOAD(1'b0),
        .Q(Q),
        .SCLR(1'b0),
        .SINIT(SINIT),
        .SSET(1'b0),
        .THRESH0(NLW_U0_THRESH0_UNCONNECTED),
        .UP(1'b1));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2022.2"
`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
KdkdvVsuosc8qR9X5PxQ/ghTeTrEz4qKVuenhDR9wRSL/BO/mhSwQtiFj74UO0sGv0zvjAntaq/3
l2/v8gOiVKmM666gbk/2UCISA4OFA3FDR9jYmiXdNXb2qHeS1ywQz5n/sTR5iu4KFEfwrl3IXtQw
aEiGegL+CQMaovJsto4=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
pZCj3qT3VD1SCS5RiZExsqqu16KpMtHXilQL9p5/eBl7qrfQjT1VhFtVbYUusepbChjsCCmCn7hr
72SuHmOmDWG78UARN7MLdO/+sePuyS06ak4nAw5xwjT0g+9970uMWYKvTeeYqoz2i+k+zX60Cuvu
iwBfxWM22DqukHlYzbEFWhNyXIkgJe71p67vGdXBmqu4/2wmlwGApqBxlwR+alwZ9UGHlxNQS4N5
z1wHu3Cp8LwGRjlaXjElcY8RDpvyz5l59ey8ar5HXR9Zqf6e1unE2NdhzHhEGRerRFXoKZppk1HB
6kIEY4EHAWz+HvPcqoP9eoYKDazoAGkJRVP6YA==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
gLgm7VvY3cNcNvdXvikCQd2nRniE4ae4hePOcAUlPDMoHDzQAD7Ngo12MGFns9JNPcCaUXfAmxL2
JNGojjrDRUWrv8FPV6FOEbDHs96fef8+gqLF4OqLck4kWpKhnJwaJjjzQirvXEzZxP+GsBKnkSp8
ceVlZJwP0F6XRv+RpQA=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
GeZP242oKQSNuofqDs4oIIXZEufPhRVrlFFeRSLY4VCxhMEMwfPrNXe33xO0zIEBoPW2X9mvUoTY
izdWQEtWImFzjzPCjkSLhEdIMmUBH02Y+Tw3eW5x23T0cK96pmoV2MH8kl99I27MN6stVd977fuB
Mjao5MnSXIGZ/uXGtgfUO9Zjs4/2wGmsI2/lANN2WOL9Sz4xeA8k40c2dNYgxgHoCwx8Ya/RYIZS
Cpuvzq4ZyFSNT/kMXnUmqj75/flpXT3mmyW+frexux3j9PxpKHmxAE9crvDx85rMamGiA4ftl+ac
H0FtL2cBqdlP60x+FjqleWCJoN6AYdxA0YZaeg==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
URmEGftuxvv0+tViRUdsFNnPXucZlVDfUQpjjXkpOA38QUzsIL9j1pGGp9doC4jcg/9MD149BTSw
vAG8684a3k+Tx/8sFGl/viK1q8ty9nktEABSahv8Etm5ZJVAzQJT7EaOzrYqyywSwabogvGUmN/7
DE3eOn6+sMCiMl6BLUhYyK39ntTWNFYVPiheclbBb36V1vzMOQl0mvPuS4hDXqba/+qBZXhqeYWK
ceNfwci6SsRRef6hLF/1S+20r2uBxJeYJjyfWGGFEGfxlAOz1MiYUUR/bEHWnbjwIcJTBHQNRdq4
4Ryb+iPuKcsXU/8ApD14i6ScW+VBPWSqnH9w+A==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2021_07", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
NtQgA3rUKfJt+21sTot44yr4gmte57FoFl8Q/327tsRJeEyNAiwWZaZN2mbo2NFcvyN2GhDw6avJ
NsF1Oxs36P8shoqOOiloWWrdTcyAdMhdk+UjeZgKcNSqd4Js87w/5LVQTwjB2mcBDfe1jrivv+IW
ZRBC8NvlW5z/1wF7+vzXRMziLQYeOkLB0OkpIY+eT5cZXDKuZ+4l0FMPjd+El96JGAEHG7Q0qS3F
OEApYEp8+nSZnragoytq4pkhVJEC22ye0hBhoBClJpszCcg0u+Ugf+mYZsj8BC2uqSY6Hh/gpjjw
enQ7aEYBaUR7GCwQN7fZmNhZYtBkyvNqydRQcA==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
CpIFM8Y8dBmpjtOVnOKcfppEFV+c1cRgsQtewNUe+5apiLDoRCdMyTqoCay7nz+Xagc0OvfZDg/Y
jSTsDjKVcEIyxOfix7iwjKW8Rz+a5wBIatI8wfCo7uLtuucz9otOWWI7BFQ2gn4VdQ73HJJlZMMY
OyEOd33tGjNSjxz3W07knDr1FwTE3BOfhq+Qj2ErnuV1dQbrTb3MiQMTnHaTCwtz6ip0pD6b5G4K
kBRUYe+UNXCMvSfNIN9MPSmolO4MjNwM5gnZZqLcR1hGuzH/Yeb/jPnhsZ7jFvlTT3nsM9JzMRAE
QwlzVuulHKQDS2I96arFosYPYMsalmn6CQW0gg==

`pragma protect key_keyowner="Atrenta", key_keyname="ATR-SG-RSA-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=384)
`pragma protect key_block
qinIxHFISC9r9LS7OKOuYVGM5EBkuuQNV1nDRui+QVNLn2QFCrWPeEClQIlNViKOt8MX9urHvu4e
l2L+eZKw6+St9cW9yUsYu36yoB4LqwG+vKvfR9CW82LGPyMAxdgk/p3n+F0Xp9Y2HaERwWDL99tW
V7cDvLLhyIwz7w4rI0BWWV+KMjXP2F5MNgykzZn7tzV8oY6MxOykFqRdI8DLAdlYGAs90wjJ3x84
S3fHciSox97FYpDi64v31Vb4RmRrwueXcvCc3w8gzjuwg7qraWLMYyPB+mERB2v1htX80PsWWVHE
QXkWiHWYvvrXEykUS04MmLNHpV8ZgBXO/NBEGn7mrITDEswk3u1Yviqy7CW2wLPQBoo5xW+uiu2e
8YZV/E+bAt+P/EH5RsC9alBgtuVKU1s9DaiEH8eUPEgJQ/TXwQW01pg8ECTYgiBS+IQSbld23aq3
goVo0ZMzRu/SA00Jmwt7upvsMkh9Q+2732ahu1FmlSNmyNGB1+bYf782

`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="CDS_RSA_KEY_VER_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
T1jqx5hmzZZMhPApzUC1oZLMAkHma8Ki4b2CvLNqxSn+MNWoTPomvQ775DMBEDai/gahYALsohdX
0f/e6LuPqt4zYtyAzmH+nRgOG/tilS1J674KsaHxudAfo4sM3awB/C4Q3VdYsO9FgvPQylnYKSGE
gJ46W+1Y789VQqPbt4dpnprhix6sLlwfww7We6cq2wu4PilFzovejouUBZqNMZHYi4suKcMcenp3
C7QRKloo8IF9yKrhGPcRJLQt2nus3bI0Q3ICxRk13Nrfhh/z4cdm0OGXz42q44snFEVy1lLxPOs7
W9tSe5ag3923oCT4NGGgK/gMTx5qXxFhV2MJUw==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
V6Ji7PWX/QyJaHa5b5xeVBEy2//CKwoRlN3Hiw077XPCGenAAGJNVrlhsrhykvzGIWuq8EGvjz8F
jpENKT+PhosP2zr7ojvt4/+c3uqTy/MmS8JhHQudAmp6O6lUHzEDUXMm7WVRrZCenwYkbJwmARQp
PFnNqzYjquikMayP1guvxh30w1XQbldFivDnO41UQ+NJdGxCK7trGkt499kdxT+GYw+HWe7AIm5A
4gGLdIkcrSuRpNd8SQSiRK8VDVi9xQ+axNOiVPtnXeQEeJt93O8AbGA4pvoq9HSup00SZo68U/WH
TC5gRnkG+pjDBvEWx9EGL1K8FcH0ZSf4W+/bwQ==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
XkTPeD8Csa1Ii8ov/ioPPxyMhyOAEopGx+qJn90IhOOtGfgtn8fdfKR5s73RvNt5bRuFMlIfTh9E
oukShpCoEWlYdRSNHcqLCjq4A6M4+lvhjra/E0vnGmwDcBUvDRIhhdUMKxWU+cFRZD/rlc5Ur5rK
CndL5lmRDw8rbWKVmEadZYG+ZQAXyAcPNltpWnf4PzXGaLQwferZLzuEnn0p+aIH4qW35c6SRe7K
oreYePyk1v0F1drtrskmLTQUKra60m9hjZGGTpfxIpC2S9oAxBfx2b/4gp9zufm9sFj23akSa38E
NhS0lDc2FQXbpFJCwkoDu5ReGI4MoJz+k+V8RQ==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 10608)
`pragma protect data_block
OBPLCUgej1lL1jbUpFUCBMWLcsXfw3ZtJue3ofYCU4ikK/mEqCLtp/v4D+BqIa6b6ZMiOusiHC9/
pcs3+Ink6/2yav12cvzyOFFUx+Di3C3nMvIfgwcEFnN54rCT6U/OLp1yv4IDBkzAI7Qa1N0Tj0Wa
h4/Tvvmu2qxlOSCfURN67Wjswb/EUTDHNszzSY7uxlq77wx9sa10fTkCq0Qtm/q1D1dS/os2KeuX
OB2UgKEgn60E8eUbDUC31V1RLwpXfC3fOG20zQXougnSoBqm1mvirA/5seW7ivG5CfNfOqyqSAZ2
ZCnMtdbZexXz5bi9TwqEtMg3kcYwSXE1LyKLOWbDYZFJh6mEVsRGJsJe0Xmj5sayLvdojKjXV8Ab
GdUcO4JxLe69cDCMN5X7TXIsCwW3KpP3OOlaD+/NOxtvTcNGYet9N0+AZNBALEqbCO28/rhUVT8V
wiiTzYkONWas1440BBpxypwOtOIOKDTvrGstwQA8d84GdaSZ1EDdwkEfYNHLXx0MoApCFwxRLRJx
YNIp2LhM2u/vcgnRUOzzdZKWNV4h4y7KFaTbeHA0u1Cb67KBiKvsto6tx2AS/NbVkrEaz9uwpTM2
7KyM1tHOmwMDJzN6JxeHu93bxGm0npSqGf0aKTO1wgOAHN1zUBFhQa3KECOlqtpVFgmUUurp43LU
QNKg8SJ/gUrikoEHy0BEMYhjU6juQtjnVAecKXAqetxYzvmYqLaeJM9eDYIzN6DxRCL7tGvRA0JY
AhXeaQ+tzzUHj3OKihnrxsPuTlIp+xs3JV6xSNzUUw0G5fRHmjvYvF/hBDRdZu8CaFDPfhKDu5fu
KLSPkhlj3czD1nv7mfilEZaYPm4RHt/u8gmKa6Ui4ruivkXbOZ4Slf3xlQg43F+1FDafzgep4c2t
4U9m+KIenyzOeN3Uq5x401Gb7sI2oBx1UA2/DzpB9p7a9TNAPNqqdD95Evrib4cQXQ5SHfLo5xkt
qLAw2nPBqYY0vR2We2xmDG1SUBK7xS2UiM0eznxdgIZN/U0//5EgXi3Iu5T0CcphZNMPkVhI/NhX
10oBzmS7ckoBytLdIj4Yx2ws3cUIgeUe2Wtj/eNH2qXrzhXD0srQ6Q2YJ9GznPxOIB54CyNV3zMI
W7ErL4yLxguyYwwx0oFZBgkJ81r/Aot8MxFAFJtOU3H5wY0ggNNXVPva1W3gvklCqlRI5NGVJC0/
xUCUmLgizUV19G8smxWTpJ1SgL2aP0eVFkGaYQBuZbDaqqNNPzFHQ8U4Ugip/6AXUJhIhSOJwHbH
b4HhU5eb0nnaMyFdmbMNMaHRa+HYxIAHNFkP4XPXGlftXYoTZDagpX4QUIgwKy3CVXwWSEH8+i/d
KJFUiZh2yoVQRTVNou5UN1uIe/x6GtL0kk0c3o8PswMwSn17uoQUNA7/YHnuYExY2/SYEfc4msh1
fTHjtMAXK2yT9HvpWHFxIi0ZaUnI/aHT0ggw1SfIWY5be+yWZq06tkmr3R9ejkWbRsplOm75nDIl
1XZ7fey51DE/iM07uoRadxEVLJSfD5YI7cpEwYlMBoy2N17bNJb0UD+0039fR2jX+oaaqNCvJZaN
Hjt+f8Gq6Xo7o93/BB7Ggsnr19tHIX4loa+cHjKsrKICxEelDQBiKo0LiHHhUAJ3aHAhYPxiDbUI
Kx8D7wgvGob1uZPkQYV0RkPIZiKR54L5tFUGppQk9EKz+RLKTSqqOOCHj+ZC4l7oQvzFKQRuE0sX
mlAkwD6B6T3Mzhboca0BwwfU9rZ9P5Uh986DJAVwuvumSatuF59eUZQzPaAzshb4bOra5yG39ujJ
O6Zo7jB9xjm8HieNE2/ayIKXse1g2m6/7TRSkM6ns5qj6rg7oq8r6rPLFkHhfuKIrz4CQdmW6byR
Uj3dzGCRd29wIVE64e+fQmj2kAFv3vA6cUYjASUsEtQFmKyCW5Mxj8x3sdi7j7jG4BQaXtwjF5It
DxizifO4ttCKoIHXyS1aU0FEwFd9EKd6MaRyfhoZyxgxv88WlQHbVAUMkhktrYohXsxMzvGxNgpw
jSWN030tk3JaF1p7IEYDYhaWcO/kd44RFlv6o4HG/iFW9veLkfqiyrpVf8XLRvOW1NOinfI1V34c
6LoDhhlB0LZDmR5vV9zDBO9ueKpDFEIjpLjQjZs0inIPUcps9JSASUSxYjGyXegIabjsRtj7+sQI
cdBPLw+H0RN/eXXkUbMpOWcFsWKCodEuR7Dc4qmgkh9dP8ln1w3pq/wDkf0EasvuEBEM3YKc0AB8
LKTRD8XjVy1clTM8I3b5uZJOY4dS4sHykvYUiH4B6xgD4uy1ol1zRMnp2JxggWIc/t+ZPHFkYKP0
ijvAA5IxucJhKm8rj7j5VhO05F7Fk0oZoGrCWEmMTNfFzOuEawUfYWi6L770K6j4QQILkgfyQzX3
Ar+7BPOz8NA9S6txLVCPSUKHL1A8edMTVrQJ7PFiMOQBZQmaxC5Ac4h8fXa+wZdrbUXRbBh3MMNf
hcsNjDZw1ap+nlzRWyLpmc/DyoDMlhMTx0GOoYBps1V+fTRT67fcwN2gI492dqyl/Hb3ioZ84g8z
thSAHWsn4eRkhuEaj86lHxMlUr6Z18M1gX0/DbJq1o4273OSAgBi9CHxByxwPFAKyOPeRv3DxIcU
CfxIQYcziqHCFgdZOGgVmmFjrbOdEWH2GCVLVio0hYcwCGV6piwDRirqFeRiHq4gBTiwySva/EQz
HF+nvVzoFI+bG0fAZr75A4U3I7AhquiS8ry0wfMbbR6bIH7aWIeIs0G++O3ZFNGPMA/qICJ7dmAj
a71/mDWdeOGhresi+8PCCAqBrrp2RUqW+kSVNBOdL/BI0JYJBgqQn7nxds1GaGeWbNKr80jTwXqG
CaiVa0E66ZBXzbt5BKfZ6B+onf4sTZf5MlCedNjJXOtR4tx3UxojDxjAzEOnO1+/T6HohShXxEw7
TOTV+PeXwRHsO8vOcQSidRJ0ELbMXiyP+YKRtiaCSp2eROwNz7j76O9LP8XiVulUfS/sM8EW56zu
35aJTteWpMRKfwJ0ChQEII+5JXtUYkDkJnmhfuO1h0GDjWLUlJngonkdgrp6Kg+y16Ffcm22phSq
ngElam0t5Vs8XHsHwRsVi5O+3bASMdfZZFZikWo9PvUSDss0IUxsB/bT27gq5vjS1+jJ26TTMl2Z
y1wI7CQduWg0OKz4WuZZ8/eYmG5A9Ku9gzHnhwQs31DI89k1ymhHIpPNf/vw5X5tAuYIDtLu4DEB
1A+aYMcn8Xgo3yUiYxqcor8aTquxrA/5Y7txAQRWJPE8iA9SOJLwD7E27R29tG2nj4CHTP5Nh7CO
YGFTL4Nn88YO5HP6K+x8uoZjNH2/oFeJVUMoaom/2PyGvZarnBJDhrBGlsI2iuZibcrTvNebRivG
k9eCa4cIcDqCDHbT6NWVtLhO9EG/UsNyxLCRZQrE4i+m0hk7UBHu/x1bmhROVPs7AH9iNwbyR6yb
4tWPeOhteUggM+B+9Y0dv5kPwa/WtqbXONN6+xtDy8sigXnRXdwVtZGW1lVXiHegUG4ty2nG+Fk3
Z8KzzZ89yImNd+DuMAgHSQbeSjX+7kghhfMdEdTjDyIAmYZDgwRQ+kGPR+Oeastmeav2E5C7uxwK
kNwSI/OHIDYtN4LQ71DNgOf+VoO2a5nmsXT2W4eDkG+AShdtftTOTdTwWG+yUJzqimcSJnoz45hh
hvbcvCdd66Xm0Dm7PKRJltMOcnkRa46scO7OQvgM8IuZLW8dgCbYFZ2lL3wEgb57fYGMPwWoCMM6
laODEV7FCdZyyI5tPAeBXSJcQOQJNnuWr5qa0KnD26nDaHig0SXk8V5ajcZBTV1LFAjnfdLwjo9Y
fihvgNGHEXMMoNB0DhCFvgSkHxY7hb1sAVkCHtniKAhXssalggZ3tCWiP88oi+B5715kFUcu5Ucq
DDMO9iwH2o+AcuJfdJvj0Wq0fS8pViWFWJ68hlhGI1b216lBwQhuvp0H9oPYuU8cMvwD2QhO9OSQ
zskKqkHBmiU8M2O8XFtnLX2N2Rg1Qu6aKQ+PMrySK9F27SMpkU6kIXHPvgxhc/Xt9QIq89GScjaY
SSmN/LeyxmrXXfpFfEAKNOSYnBthSs3mGEYeVsRq/MH4HiVBl6ieoOtGsJNsDFR3Hcidlt72UgaM
cPR9PNs8pk8WQpwrNBFfqxeiacy1dV4Kkrmvesb0bybvy5lzJdhqMeQ1TQya0zv/UqXOg+koJ6Pp
1u9NV19eiHFlOpFh6Yq2WYpZnSL4Q1aNGywdQWHdeWK2d5IH2G0TxkhqUJH2RfY2FDtm0GZjm9av
O7zUyHJkxMm1ZbgC46/X9KglDZgnTNixZGQJ0ogg/61mAsEpON1AfIyPO99ChkSzfEeVcBcD9dE5
W6TFZY6z7IzdOTSrKRQh7G15R2NP+BJpwkIGPX+8Kp0d2E3oIjgTEnWt+9KB78ukkAx85u6zDSbi
qtl75SK+ksyt88GELs9f/GLkxsIkMecBahBWLVLi2aHQPTOrmIbOKK3WxhTdllXxCc6PpNGNRnre
et6ApdwaRESti0Qfa+GAFN9VDyw1P54/Cb7Fb3cDKeybQ8PERVCIx2hHHOsHJPQ/DiuEJ0maG9Xg
XfzYQiy4vpgC3POyJgIsLSOmM/Crv1gmIbldZWlsAiw8ItWmZkEjG8O6y9cLxmRoJ1cx+wnU00hI
n7+kGin+emzbrDBMT7OjMH+nEKRX2WaQR4TQ5VNDZYllCK29htI0u4colSWFZlEediK8VU6t9hDI
KQw7vTWrEC/IzzsGDX4KZbGYN1UEFQpCRfRdaQWOy1h9KNbsThh4nJpQ50jZFsjfizgRrg0VWU6g
Wdn05xcoTEf7rfSx08BX00hleoBEQH4d7B09G5oJnSjmesIREeUd1zogLApB6sP6mV193HwS+qwX
QoI7wTXJtcMjTw/mRUniUOhC00bE+YLZ3aH3QCgPBJsSA1M5N38LcxLflMeVpWmRQUTeOCZ6UJNx
Nj6UWLqIca9db8Tw4NegJuTdVNpFD8MFlsuaEkdltAwFQ9NNpRKHH18fxWP0SWM+DRJInZPLyQ3d
yEaR0QCOLJ2ED+5weRFvlbevnQI/+997GINaMLZmffkjJPdiPk/MSlvfvkWY8CwsDBA2xpKotM6i
Evr8tu2xfvGxIeaWzzp689+Aj5foLo3Qnv3xcPZxPDWd8EvN7TptSW1ga2kUAHumaZG7L2YcbWfB
1k7bTPufMktxAtzuHRQXOkWNE8GkrRmacAjmX3oeWUnb8fp0PPTr/NaOm0V4kEJsZt09J/UfUi2a
jh68Ya7UryLZDk6sFC3qj+CNfZfqetqLudcI7YO8Z3m4Lzu+BP2qQEP6rsSa/+TFquLhgrCn3MUD
q848hEg+f+qGEE/fwFkxpRqFx6kNXDkaAAmQsv+shuJk91JjFj3b6M3IyVQJNakjOuds2nMZmDdZ
0P/RZ2kLqAQTQQseNgjI6sm63o+xeOUEL4AQ8BqblGkcEB1AVv1TnH6eiJGL8bdTDp9gFmTv751A
WS28dUEFxplIeoVMU1YR7oFs9ssS8H0x/qiowdk1jDaKHfw3g+ETmKjtvlnGUxuQNpZGS9/s0cNn
so3O66FFX/JqGZtD0zHXZ3vLs65NXBR2S4Ctyuj049ugX/d2Mqb6vgz1+6Ej/stbdhp05yxF1hBm
YRKSXrx6HAFLf3v3dvx9kSaNQLBUZ6JGpx/86HfJPwZZWGwNntKttD11bwf/8jSE+WLCOloogzOa
Knc9cii86kIbgCno/b+AU+waKRQUt6kv/jOculpukigk1jQdIpOZXUGsPBaDtAMOssfkce4DWGBB
ze56uOoDwlQBC9kdYt/f7CD3Iec0RalOyej1x+dnR+z1wxfDwCVl7iDJTyOQzzxlT52Z39Bt47oO
+gA7jeBonF5ttCsWaov84SSMUhnFJouGL6jnbv2NvVzC1s7ZpxfZ5iIGuM+QuNKQtvk09W75nhU2
irJkqiVwUCJ6SjGkWYiOexC57aqNvaPvpFtHwQZebZ8g4LbCbF1FWOHAhn2ak63nnxJjI68fc75+
26UaJY7+JlpV3zqk6oUpz/TOTwPN4rXU06DgJ3MUIJhJNi9fs/dRl3sIo72/ovAndIkFgCiuj3Ms
ZmPpgm9bH1qrNgNkhrvJCiVPD59S94s6EopGY386e1pBj83yMG/bBxLhQ+ui/WCqD8FnSUkiCpKs
9x5nMT/fIQeiXnO7MMxPkW6AS5LUw8fCqu75h87adzKX+NW3ssK0Nvzq5dUZsJ7i4zx1+Wtin9Pg
wdpePXFQAjehsW0h/QONSgE9a/HdaJ7TQTJcX9HEf1fqHBfpY72efLyvqB2uxi+R1s8V+FgITU9O
2sFa2l3ooADniOa5WYNHQkfI1zG5SJMEmfDeTzrdmEHt31baQOFa16tfEc0hJ9C1n/P/bF1FDLwg
xhTWZyf5o2SFDQH2lXXLTneuGip50JkGXJIm+7uhETaCiBHcKnp3cTL1tZ9VHYarzFxhzLxs5KED
J466CRgl5VtyNotX6Lnvp1+VFdPjclENLIULmjkSaEDlT4GYEVkfayOsOFiPe9pE65kFKIBZM9ZJ
dCIeAKx4b+cFfXrZsIQ/3Te5l/OhpOxak3visYmB5HwS5lKqQsMmLNaT9LRxSmU3JD4KJzgWrgnY
26Hx8jPHPC8RhOSCXyNw6y1km5tZqkWU4qrswXMClIflC7DDyIxTyP4OHVoY0ud+spdBVoJ0cLeF
8QQ9u5u7xEYd8d/yYWC576GQVEQOWuTuGlKTz0nZQiaeg01j8F9n8vQRylPinBylXj0P15IXEQcb
8jyBH38lJCPvxFIYU/vecHXdakx9vQ/5j0evxHGlH5faVJhcSJR9SwNnXmEHO6RQp/nhOOGya14I
GkLcP1AF1L7EhKbKGIcFcM84f3wqJKKSnYcFuYOh3Yn0JjUeM2LasAnOAn7BTEiSFhRgjz6NBo3b
fMs8IQSaLqYtv5FChsiZ7wv1JZ4hBHEDnOr4XKgfXGBT4IqP6xVB7gEGHw7hTPwVq0ICHXo5GBPy
XFKD59Zo1WEowesJxn0Jf1qFT4x+CZrr3zSZHxKbhVG5MZzppSEq8QpvExddXbQj54szqLzXww5U
6/PcJNN0/YNzDipr9yK+dmXTGuJUaSTI7/s14a7bAFi/kfCbRfuGKXWEYF286jCdm5Gb350n2Tvs
55eG6BfLEYcDfUzxykn/d13ZCtf5LPYZ/tg6goXlHOdu0a5HBB0HrNxGWrwxyE6KljSWTmEbdtDr
9RJxDMNLHFJUqaur6s/5PujVJmDgdiIb+TQ2jGM9YLLZSiJXHwvDIoXkxc6rsSO3A1y9YA4Gc8lE
SzbNtry0QXrqvS6EmYMy7zQQShu3EeSNrGiY4sVbilnjcT4xDKBBlUMbyw+wLDzpy0DN2yzBPsOh
oJ2Rlx3I2vKxkpmO0TQNlPo0XTWIGxtbGaWrTjSow5qu4JOw7Tpu81L5xAdeJ2YK5oJKkccM38pu
p2sIGfWm/TZZ4ST43LtHhAZeegXWzENMcbXNMEUcrLpmVdj98KXEEykdmewbGrSl9do7sPx97ohI
DOIYcpWfSOFbFKlAsWHPQa13lkTcW+bcTDxyk4n/8SSPaOYYHm0esJ0ovYQikgfXilM7uUvEgD/N
rOhRMJObAB9tAriqSYjHmlHUprq1/x91keqsuyI0y/TNZi/WgRs7miE7w+TYyKZZUiH4E8qtw4Ze
g8ED3g8C6U7f38il2pzD7JZrqzdUhYLy+FqojnlT+4Ds4elVMnIPAbuVyQktHmXR/2+h90sbrZsu
gjENQuPP8ECRubw6r+2cfr9CHwyJRqKtzcnSc0HuSjBCguKP11WmDs5ZXXiDgH40a4HXhAXbFkcn
PooECE1o6QGLVhqIV+ahZMZyk/nd4VakeFo5BSF0/0rmAcE07ODeha5YH9JEzGN1wGEEYLFwCasz
//MBSzz7o5rG4jr3Bf71vhwm3D0bjledHzvxmWO5iktuRlV2IJO5s92HPfd9Vw6IpVDWxc97/sq9
UO6tJVQqSQGrjyrNMgwVkQr51P48pwKHM7KvDLBeDC7mnlYwVF4ud4ArG75PKlyigCGigSjAq5lx
WTWqozHz8SHsXTh5+OzNJ0jm2z4WmUC09ScWWU9yKvYu9/ContnTej6W6ttTnvFdQJgUgAPa3tml
9QeD7ElgkoDP+5Dc5/+mHA0PJjsyEVzawM7hxNyOlDOJ3kgko2WOUeaBQJtc4Dzk8KHomsX/Zw5s
2dUcxuDIANZLrUR89q7sbBaPwd2bMdVsvv+JlCcfDkFhmKhAj+smjDgWnWUgLSN/4UA1u6QQq9/f
X4ui9Kgwe479iNWdrdE9UY+BnxIpoGP57AKC1RpHvmPyKWN/WCHXR1N83cGAUkUsbEahp+SrlzOF
DRCW+ut6s5rms7ytokFeOEtc1Wzjl7H+8/hBKOyn26AZhEwZacuD9G7Q4cmH2F+zqNtI/0N18sbg
fHyANh6yb4TuawjFkVgsvU73N+OC1GCnaJvlfHRZxhtETH9TQeDoxjTqCezxPV349jEq1mBMejzE
BPhK8CdTvN7Ewo9lDdByEawWC8TLAvQjrn6X7wXOeADxIhhNb3DaTqD0XSo9vzgwSpChBE8UemX2
N8Yx4NkV709aUR/lNM9n9N8tp9wsz4T/2Bzvd9yNaLEuJDflyL/liiE6PcKNJNj/jvHsA0XeAFcQ
RGnVX4mvqw5wAtPpmjQxWRs1oOg6ocj6xDUQE9nBsO9FcxDNVp1jMoswFOh0b4VhUCdsNgZlQbKA
PlxjWvtqkCks9Ew/Q6n6g8pKRiFhMyO1yeU6fvd9oELs9k5Lvn3eNXfJlRi/z3AKm7tyaoea3drv
g+w8JSGy4uggPaW/fDIYs6UAlRj8qPdu5oGYv2pF9/mhVr0Ud1FAsPvIPoS4S6mY6yQnf2pxi3sU
i157Wk9q0UBsgUtNBqouQgfJuLGH2bF8CgQOCc5tR152n+jsiHPzKdrsQEYoa2HqfWkrxsoVDqbY
XTKwkN5rcdQo8666GhTrltydmz5HjQjdSPXx9jh8cSO2RlNnLD/JduXUrLA6ho8uZjAdbp/9su0h
7KbRU0KzP9Gfb+OsQ9f8uHohbyY0AAhpYjMR/+89Z/gec5VWQEIW6k6jFwGBQ8eW2/YnzFtBKtCE
+y21BAAPJQFpxDVywtOYxwWvU3LpOEDB+2AEGMHcmVfykze7uuEiEWPlNtrmcnLuKJXdGOg7KuUO
BVDsjsJQile7XsdtK+9oi8tL8itm29hIcQNJlDvq9NU0NjmvmNFqPyh6OrxWvi18U9FoWr6NGeD1
X/co8RdBF9OAKbUr7e5mQRjNrzKVMr1Ij1ds2zZ8bs+6kBRDeI9Mz9b1n9epkhqSNGaba+6fTay7
S5mdGycckS3jazPagzjxR94VqdcRSUkEmQlh6f/Z1VeVIyxx48pGa4PoUnCg5pM7z4NVswiWN4t6
Tebbq8YlmRSUbFfUBIJ6PjF6l8qxoGxD3uUXVHFSFTJraO0muJEuN5xIubD/Gd4WtSH2FkOARZoU
eHcmuA5qb+EmjFor91YAllZlf6v/w1TDHGbVmNuEqbYWqYlmwruam/XEFkO0luXvn59y3IuI0E7L
7DiqOtr/UPbn5iHZ4SQtUvKoBjr0dpj+Pm1ouawx8hHnnjvvyFe5wLdYvstbfjUf0RqGHmR04pc5
lfooVBYewcUDpvfLX3hgasvq0J9d4A7bgzpU/DJ+6gaAoHdCexDFRduZTZAcnH26TsVKOAY+1Oie
tGxsjoLXZncPp0zkNbpLmqDu1ksI9ZvdCH9ESAwAsFs64MiTTkyjGwaBZsOHUx/soLpRsu2Zp4lm
U0lVoGDRgUyuhLfwMTM5Bew4mBwOvVFCh2Yu6ZuYo8aIUfCbf/3+HA+TI5JvKAgthF+zx/LrlXxU
WsmS5tR+wb6VCHnK01bkHj1ghRejt1UP5Tc398b7O+hLptRsAfI7xL8bltD3oOK9dxIBBksTSGUt
HxcqePsnGPr4nQoJEFZS6V5ykgpb66EzSmC4jAkXw72tIpIYCp+kUfTiheoe+azE4MN9YHYnJPV2
yNC2gvjvcyAPptDThFGferXUL+OKFzAxwlXN2Rh0RvyHIlvf+p1XF2gcu4LN+jIkYIb7kltRPReB
c4JtIednVMG2XO8RzRNNRbIVdG9Z/AdjzqtTOf63Hw6vlhmML3MTjeEVZ8GRG33vmYLMolly1PwA
reNd2hJUHvO8ij7OuWFYr4Vh3xQJojJathB/H4tGbaFiV1rVunhr6C9nFVOK/zAr1z6hKpw7J+Ih
Zkg0I0QhIqBnm7R2rVuBPHaq/mpO/CxAovfrBlUXwnWiQXzkkBThKF9o8ETJL3hWdD+7H5+vzAgz
9bYtSk5zEWVzpQE0oSMBTi3QCnJDJq1CbSobsJ3UyVUGtkOjw+y2Ta3cJdFeYXZHoLLCuIdTyFXU
DsUzbmlqrwFlMqTClvghdrfRsNiShnUziRsTdMKidJtejt3SGlNaysyUzNb/DIAIeeAfTHkrerZK
oc+/bOS6goNCuAftf7PkZc0etWxXtTn+tRXQojzO+iKIarwloFddn+/u+DHf4Ng1wY25yj9py/rJ
pDvjOplU7E1DEfwKOFFLXZAh1gtQKNS1XW9zaxVokxn2qn50hc1S9Pmr4TOOPIo7Jxr+TVQPGGx/
haAkLoh0KMoWfskJjLSDpTm+4FlyXhgcQ1OLeMBUz0HmzHwLuzw7tlOVuiZ0o5k1AElM+aSugXTf
P5+BZJ9iGUq9vGlPOVm26WI11Tv6aSi8RXYPDIHgKOLYvwTdBpgJ6Zjgaahxh5w/x8o7gR3A23bz
P4KYfP2EmUyFNR0kj7P76Z/REf7ER+6PrdpKz9QCA4wBedGCukAWfYi/ILkPd2/7+REYbVzksVr4
AnswDAeBCToSb1xTw55WeDulQUvzUOEHWOGaavLdhV/1hoe6NB0ibM95ZWCVXQN7e7Ow68hUMcOV
UoduQ9dT5yxfSZ8JbopIY9fbCvySCvZD31MxqfppfF1kaN0L0jZ77hI5MpLGwI3DSL8E/Nogg54B
CohNey9BUEGArwwFaFsbWg0MByMGwKom12XkiaiMvXE17BAjtLACP1C8idZp9HoMD7NJQqNXcdCe
dDMw+Jpo7uI919LIuH3//fTrTFMYPDfq7StF4yDEH1xoYOtw0m0hXK3Dl0M/yEw9dfXy/KtZtCYB
OAcZU2eja7f0wmWGOIie6b7TTCq162I9gxpmwD8xEsinfR9GS83XxVgyP3KmSyK/006Yk8xrq6CS
zYNpfc7ZdpvfhBxMOx7Z8TduMvUpTHs+ctbhV9lxDMSEzmwIlkNSiMKX3NRnggwZAd4UVHcGEgSO
ZHRxduMdzpxAgSxEtmdyfTHKRj5YDtIAB12CbeTtQMHYQLwtLZbF8XdslbZMdHK/zDIqG2w3Acyw
ieTuE+Ezkxg4os/XD46nQo2tZB8rMZRAlMebDKh24vbHM/rLNE2/+/hioDm8FRDiDNYuXLXG3tpP
ts0WaK0QGQ7V6dVQYK4zI0sjH8vBo5Pu6vAF5W+eWPrnaBNNYFb/B6F+vejcpfTYrXdNAES5zwSZ
Rd6tuKXxh4TVueN4r7qamSSmWAuvlXAklqq9KUKR2n+aAtU03MJ9bJ5QzhbhqIF26vPIOG3obEYo
IULgHU8BQo684ENQuAwtNuGa6OkMipCgG2ITk4Mhf0zAtVv5bIMS2w8wMIf5b8fWR9yWWxdKuLtT
i+rCQ1O6XCbyPlrtQOfP2yUD6jEoIHq7q88j5U4OEWWPExwcFoOrEioOz5eeONQbglDnlDX6dhD2
B0dQ8Ysjs0MOKY3Xgp2gM48CtWQXW1KxDu2/KzRC5m2/X1E9nQ5hD89+AwIPNcfv3efOUZJ44aSr
Qd86GFBf8gbX80uuqsgr518+jsJ4Tes204DcgzCneIBKPScgynTpzXlUJogznEVB79xV6vq4jajv
+L9dai5l6spTl98wKbGeJshBsVNar5FFy+Sw0eyRUXjNLu+xBcoaCiFxcw1V9aZqR947yahDGss8
VM3gPDQusHHL6KXnagokNQxnQ398RgV19wm57S5KhwFTVlKCvGmEbg1/dMh6R/5ocXKCcfi23jM/
PuBxBjIpmft/l2dRsLkXIE984Je9EV47PF6PT7WInGD5X8eFPm4TnILR0Os/w99Mkhrn+Z+zXDOR
ESHFp6AORDjCafQGrWSsJDfuVVWgXUoTSYe4UxoRsULQcTW5TKvVbi2QZGMp2j+EZxIHT6EhHmwg
e90Xg94rDxAzxhPiRmvS0NCbCKw42uUyl6ZiHvzg8iupFguyP/VUGNz1U4B4WZqso5R35mw6b8KS
VzM/O5FWpjQv9tWLNWSzHzKq7tx7M0mhPi5pCGD5KiXj/aiJDl7V6XKwING7ZRlTgcipZmKJFrZd
6AUqC+dUrHsNeT9porqz2qU8Wx1L43SM60DamzvJ0PIPpI58ltGGrtFIrt0f8hqVSZShUb3y1UTu
fVgJGcc+tn6kK8Nd5ZXJ1UnRMaxw1zlqIbtVSwUeWLZm6qaDnFu4486VJKxXY+vqahmEMdjPDjPF
7xxy6fDd3GSYyjul+lskd5D7Jz62RM9Tlr28Y+ZncclAOD4FW2GYd7ozCeYwVwnC+1pzRDYZfNml
jBiWv+7DkkOiK0qgM1UMDQRrMi3DH0fiyVPPKPvHswtEk5gfry/qg25EYbihurvysK+A4DZrJB8Z
kEJdDwTzhp3z6AVCQbeoCfl/jPfjc5mbcres3GQ90iIO2R7j0+0Z9HwIc8rlqBemlZDzZSi6RYnU
J+rsCUXXOaPQifTt68AtrU3iwEtuUP1QYr8y0v8ZxracoLyVryLssW3bUDECT6CrdUiDEFsJae/P
FgautTzhv/gInmHtgDuVhHVZMKfkIGbmJPjr2oJwGaMA4iMP8NMxSIlZzLScNDmglzYwtn0SgPWd
rH+mWbBNHVtHVIOJX1lm61l5Ty9gMyMpayr458CJINBE0BPbGYax7ehhVQM/oc14gDpUQf4vpL7W
2mC7WVBfSqwa4LSt2lH/O2fBGcKdlGy4T3foNf/KN3rfnn9dYwyPO1HUw+6SA0cuSzIW4OmcBkqE
o8ky+ar3+6xyd43pRrm1PneVxIji1HABEXT1vR9aS/iXizOGJiykejaQVFfPrMRQhlUn6KTtEKSl
TbDjRD2Be1f7Q46HB3Il83+bF14n+JkA9coPXJiXQbGZsF+bobqtm0mf32dVO/7TyFQockFbMpwT
dJFtLdrATayjPXsw3YkptlijKJP0oIdVmwf6rx5MWHcm5QkmrFHKgmE27yFJwaa6JJH9PaGiQTkW
JqxXQEQgm2flmyJ70c7N7UA1tCAnSNKXWH11WbLImcocMFE4EsNu9XVwG8ZPucHXFcex8pS0KP9E
aZ5f3XbomGYCpwsTRYBUURda01GqgWgY8SMFQuj4MFi8sNSjA3H5ghMgqbCUs/ZWyHTTk7p/vCTW
9f0rov2cKJQA7eGeeTAyUADGz61uA3t7xab69Ovp7/TNMcsCpttL9oWrnhk/o7gAmFxce6flDTXO
om/MzVt48B1cMqkFWNspSNcBuRedJi0tWhf1eX/gBeDInBbtXIDJ9SD/4sf6UE8E323HmwPNyyLB
AiWyziF0ySSB/HMRZJE0JCj84m2C514yrP/QdPqewUmgebiMFvOFl+yak+HRghnUR/lh5WdrDPJ4
5LiOp8I2stmPMaXriKaVlz6ssJtnCq0+S7MqVL6xCXJN01XjN1FYLJ4X4qFRRW5UbOONqJH5aucX
zDWe5jzCcNI+kj2i2nKcVnCd9pLFyJk7X3dcgUBxCD7/mU6WQHTqkJPrlESXuf/Tb+pu8bLdparg
eJQ1dgD+2mvIUTzarVwUMY4J9c1daqaIIe/iUz3yIVN+cu81+ySDvxHsjZ3JJNLraIC29UsEl4VY
u/NGoExbMlSobg48Xvd72ROHTpUo83su6aAM1yuIMY3E1GLLY4oVaBpRzlpU9aYno1KvwL2nuznB
SCrLz/lO3jR+FthzFeij90QAax1JdFXht5CgBAlVmdPD2pvt56WTMoWc5lCj787J1NLU/7G0yDsw
RO00D+sT
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
