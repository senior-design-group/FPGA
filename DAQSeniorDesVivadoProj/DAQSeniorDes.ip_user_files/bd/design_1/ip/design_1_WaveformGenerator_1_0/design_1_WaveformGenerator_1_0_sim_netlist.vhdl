-- Copyright 1986-2022 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2022.2 (win64) Build 3671981 Fri Oct 14 05:00:03 MDT 2022
-- Date        : Mon Oct 16 16:00:24 2023
-- Host        : DESKTOP-QRFTT4G running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode funcsim
--               d:/FPGA/DAQSeniorDesVivadoProj/DAQSeniorDes.gen/sources_1/bd/design_1/ip/design_1_WaveformGenerator_1_0/design_1_WaveformGenerator_1_0_sim_netlist.vhdl
-- Design      : design_1_WaveformGenerator_1_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7a100tftg256-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_WaveformGenerator_1_0_WaveformGenerator is
  port (
    WaveformData : out STD_LOGIC_VECTOR ( 13 downto 0 );
    clk : in STD_LOGIC;
    Address_Data : in STD_LOGIC_VECTOR ( 22 downto 0 );
    Read_EN : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_1_WaveformGenerator_1_0_WaveformGenerator : entity is "WaveformGenerator";
end design_1_WaveformGenerator_1_0_WaveformGenerator;

architecture STRUCTURE of design_1_WaveformGenerator_1_0_WaveformGenerator is
  signal WaveformData0 : STD_LOGIC_VECTOR ( 13 downto 0 );
  signal curr_address : STD_LOGIC;
  signal \curr_address2_carry__0_i_1_n_0\ : STD_LOGIC;
  signal \curr_address2_carry__0_i_2_n_0\ : STD_LOGIC;
  signal \curr_address2_carry__0_n_3\ : STD_LOGIC;
  signal curr_address2_carry_i_1_n_0 : STD_LOGIC;
  signal curr_address2_carry_i_2_n_0 : STD_LOGIC;
  signal curr_address2_carry_i_3_n_0 : STD_LOGIC;
  signal curr_address2_carry_i_4_n_0 : STD_LOGIC;
  signal curr_address2_carry_i_5_n_0 : STD_LOGIC;
  signal curr_address2_carry_i_6_n_0 : STD_LOGIC;
  signal curr_address2_carry_i_7_n_0 : STD_LOGIC;
  signal curr_address2_carry_i_8_n_0 : STD_LOGIC;
  signal curr_address2_carry_n_0 : STD_LOGIC;
  signal curr_address2_carry_n_1 : STD_LOGIC;
  signal curr_address2_carry_n_2 : STD_LOGIC;
  signal curr_address2_carry_n_3 : STD_LOGIC;
  signal \curr_address[0]_i_1_n_0\ : STD_LOGIC;
  signal \curr_address[1]_i_1_n_0\ : STD_LOGIC;
  signal \curr_address[2]_i_1_n_0\ : STD_LOGIC;
  signal \curr_address[3]_i_1_n_0\ : STD_LOGIC;
  signal \curr_address[6]_i_2_n_0\ : STD_LOGIC;
  signal \curr_address[8]_i_3_n_0\ : STD_LOGIC;
  signal curr_address_reg : STD_LOGIC_VECTOR ( 8 downto 4 );
  signal curr_address_reg_rep : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal max_address : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal \max_address0_carry__0_i_1_n_0\ : STD_LOGIC;
  signal \max_address0_carry__0_i_2_n_0\ : STD_LOGIC;
  signal \max_address0_carry__0_n_3\ : STD_LOGIC;
  signal max_address0_carry_i_1_n_0 : STD_LOGIC;
  signal max_address0_carry_i_2_n_0 : STD_LOGIC;
  signal max_address0_carry_i_3_n_0 : STD_LOGIC;
  signal max_address0_carry_i_4_n_0 : STD_LOGIC;
  signal max_address0_carry_i_5_n_0 : STD_LOGIC;
  signal max_address0_carry_i_6_n_0 : STD_LOGIC;
  signal max_address0_carry_i_7_n_0 : STD_LOGIC;
  signal max_address0_carry_i_8_n_0 : STD_LOGIC;
  signal max_address0_carry_n_0 : STD_LOGIC;
  signal max_address0_carry_n_1 : STD_LOGIC;
  signal max_address0_carry_n_2 : STD_LOGIC;
  signal max_address0_carry_n_3 : STD_LOGIC;
  signal max_address_0 : STD_LOGIC;
  signal p_0_in : STD_LOGIC;
  signal \p_0_in__0\ : STD_LOGIC_VECTOR ( 8 downto 4 );
  signal NLW_curr_address2_carry_O_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_curr_address2_carry__0_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_curr_address2_carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_max_address0_carry_O_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_max_address0_carry__0_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_max_address0_carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_storageData_reg_0_15_0_5_DOD_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_storageData_reg_0_15_12_13_SPO_UNCONNECTED : STD_LOGIC;
  signal \NLW_storageData_reg_0_15_12_13__0_SPO_UNCONNECTED\ : STD_LOGIC;
  signal NLW_storageData_reg_0_15_6_11_DOD_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  attribute COMPARATOR_THRESHOLD : integer;
  attribute COMPARATOR_THRESHOLD of curr_address2_carry : label is 11;
  attribute COMPARATOR_THRESHOLD of \curr_address2_carry__0\ : label is 11;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \curr_address[0]_i_1\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \curr_address[1]_i_1\ : label is "soft_lutpair0";
  attribute COMPARATOR_THRESHOLD of max_address0_carry : label is 11;
  attribute COMPARATOR_THRESHOLD of \max_address0_carry__0\ : label is 11;
  attribute METHODOLOGY_DRC_VIOS : string;
  attribute METHODOLOGY_DRC_VIOS of storageData_reg_0_15_0_5 : label is "";
  attribute RTL_RAM_BITS : integer;
  attribute RTL_RAM_BITS of storageData_reg_0_15_0_5 : label is 140;
  attribute RTL_RAM_NAME : string;
  attribute RTL_RAM_NAME of storageData_reg_0_15_0_5 : label is "inst/storageData_reg_0_15_0_5";
  attribute RTL_RAM_TYPE : string;
  attribute RTL_RAM_TYPE of storageData_reg_0_15_0_5 : label is "RAM_SDP";
  attribute ram_addr_begin : integer;
  attribute ram_addr_begin of storageData_reg_0_15_0_5 : label is 0;
  attribute ram_addr_end : integer;
  attribute ram_addr_end of storageData_reg_0_15_0_5 : label is 9;
  attribute ram_offset : integer;
  attribute ram_offset of storageData_reg_0_15_0_5 : label is 0;
  attribute ram_slice_begin : integer;
  attribute ram_slice_begin of storageData_reg_0_15_0_5 : label is 0;
  attribute ram_slice_end : integer;
  attribute ram_slice_end of storageData_reg_0_15_0_5 : label is 5;
  attribute METHODOLOGY_DRC_VIOS of storageData_reg_0_15_12_13 : label is "";
  attribute RTL_RAM_BITS of storageData_reg_0_15_12_13 : label is 140;
  attribute RTL_RAM_NAME of storageData_reg_0_15_12_13 : label is "inst/storageData_reg_0_15_12_13";
  attribute RTL_RAM_TYPE of storageData_reg_0_15_12_13 : label is "RAM_SDP";
  attribute ram_addr_begin of storageData_reg_0_15_12_13 : label is 0;
  attribute ram_addr_end of storageData_reg_0_15_12_13 : label is 9;
  attribute ram_offset of storageData_reg_0_15_12_13 : label is 0;
  attribute ram_slice_begin of storageData_reg_0_15_12_13 : label is 12;
  attribute ram_slice_end of storageData_reg_0_15_12_13 : label is 13;
  attribute METHODOLOGY_DRC_VIOS of \storageData_reg_0_15_12_13__0\ : label is "";
  attribute RTL_RAM_BITS of \storageData_reg_0_15_12_13__0\ : label is 140;
  attribute RTL_RAM_NAME of \storageData_reg_0_15_12_13__0\ : label is "inst/storageData_reg_0_15_12_13";
  attribute RTL_RAM_TYPE of \storageData_reg_0_15_12_13__0\ : label is "RAM_SDP";
  attribute ram_addr_begin of \storageData_reg_0_15_12_13__0\ : label is 0;
  attribute ram_addr_end of \storageData_reg_0_15_12_13__0\ : label is 9;
  attribute ram_offset of \storageData_reg_0_15_12_13__0\ : label is 0;
  attribute ram_slice_begin of \storageData_reg_0_15_12_13__0\ : label is 12;
  attribute ram_slice_end of \storageData_reg_0_15_12_13__0\ : label is 13;
  attribute METHODOLOGY_DRC_VIOS of storageData_reg_0_15_6_11 : label is "";
  attribute RTL_RAM_BITS of storageData_reg_0_15_6_11 : label is 140;
  attribute RTL_RAM_NAME of storageData_reg_0_15_6_11 : label is "inst/storageData_reg_0_15_6_11";
  attribute RTL_RAM_TYPE of storageData_reg_0_15_6_11 : label is "RAM_SDP";
  attribute ram_addr_begin of storageData_reg_0_15_6_11 : label is 0;
  attribute ram_addr_end of storageData_reg_0_15_6_11 : label is 9;
  attribute ram_offset of storageData_reg_0_15_6_11 : label is 0;
  attribute ram_slice_begin of storageData_reg_0_15_6_11 : label is 6;
  attribute ram_slice_end of storageData_reg_0_15_6_11 : label is 11;
begin
\WaveformData_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => Read_EN,
      D => WaveformData0(0),
      Q => WaveformData(0),
      R => '0'
    );
\WaveformData_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => Read_EN,
      D => WaveformData0(10),
      Q => WaveformData(10),
      R => '0'
    );
\WaveformData_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => Read_EN,
      D => WaveformData0(11),
      Q => WaveformData(11),
      R => '0'
    );
\WaveformData_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => Read_EN,
      D => WaveformData0(12),
      Q => WaveformData(12),
      R => '0'
    );
\WaveformData_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => Read_EN,
      D => WaveformData0(13),
      Q => WaveformData(13),
      R => '0'
    );
\WaveformData_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => Read_EN,
      D => WaveformData0(1),
      Q => WaveformData(1),
      R => '0'
    );
\WaveformData_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => Read_EN,
      D => WaveformData0(2),
      Q => WaveformData(2),
      R => '0'
    );
\WaveformData_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => Read_EN,
      D => WaveformData0(3),
      Q => WaveformData(3),
      R => '0'
    );
\WaveformData_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => Read_EN,
      D => WaveformData0(4),
      Q => WaveformData(4),
      R => '0'
    );
\WaveformData_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => Read_EN,
      D => WaveformData0(5),
      Q => WaveformData(5),
      R => '0'
    );
\WaveformData_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => Read_EN,
      D => WaveformData0(6),
      Q => WaveformData(6),
      R => '0'
    );
\WaveformData_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => Read_EN,
      D => WaveformData0(7),
      Q => WaveformData(7),
      R => '0'
    );
\WaveformData_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => Read_EN,
      D => WaveformData0(8),
      Q => WaveformData(8),
      R => '0'
    );
\WaveformData_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => Read_EN,
      D => WaveformData0(9),
      Q => WaveformData(9),
      R => '0'
    );
curr_address2_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => curr_address2_carry_n_0,
      CO(2) => curr_address2_carry_n_1,
      CO(1) => curr_address2_carry_n_2,
      CO(0) => curr_address2_carry_n_3,
      CYINIT => '1',
      DI(3) => curr_address2_carry_i_1_n_0,
      DI(2) => curr_address2_carry_i_2_n_0,
      DI(1) => curr_address2_carry_i_3_n_0,
      DI(0) => curr_address2_carry_i_4_n_0,
      O(3 downto 0) => NLW_curr_address2_carry_O_UNCONNECTED(3 downto 0),
      S(3) => curr_address2_carry_i_5_n_0,
      S(2) => curr_address2_carry_i_6_n_0,
      S(1) => curr_address2_carry_i_7_n_0,
      S(0) => curr_address2_carry_i_8_n_0
    );
\curr_address2_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => curr_address2_carry_n_0,
      CO(3 downto 1) => \NLW_curr_address2_carry__0_CO_UNCONNECTED\(3 downto 1),
      CO(0) => \curr_address2_carry__0_n_3\,
      CYINIT => '0',
      DI(3 downto 1) => B"000",
      DI(0) => \curr_address2_carry__0_i_1_n_0\,
      O(3 downto 0) => \NLW_curr_address2_carry__0_O_UNCONNECTED\(3 downto 0),
      S(3 downto 1) => B"000",
      S(0) => \curr_address2_carry__0_i_2_n_0\
    );
\curr_address2_carry__0_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => max_address(8),
      I1 => curr_address_reg(8),
      O => \curr_address2_carry__0_i_1_n_0\
    );
\curr_address2_carry__0_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => curr_address_reg(8),
      I1 => max_address(8),
      O => \curr_address2_carry__0_i_2_n_0\
    );
curr_address2_carry_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => max_address(7),
      I1 => curr_address_reg(7),
      I2 => max_address(6),
      I3 => curr_address_reg(6),
      O => curr_address2_carry_i_1_n_0
    );
curr_address2_carry_i_2: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => max_address(5),
      I1 => curr_address_reg(5),
      I2 => max_address(4),
      I3 => curr_address_reg(4),
      O => curr_address2_carry_i_2_n_0
    );
curr_address2_carry_i_3: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => max_address(3),
      I1 => curr_address_reg_rep(3),
      I2 => max_address(2),
      I3 => curr_address_reg_rep(2),
      O => curr_address2_carry_i_3_n_0
    );
curr_address2_carry_i_4: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => max_address(1),
      I1 => curr_address_reg_rep(1),
      I2 => max_address(0),
      I3 => curr_address_reg_rep(0),
      O => curr_address2_carry_i_4_n_0
    );
curr_address2_carry_i_5: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => curr_address_reg(7),
      I1 => max_address(7),
      I2 => curr_address_reg(6),
      I3 => max_address(6),
      O => curr_address2_carry_i_5_n_0
    );
curr_address2_carry_i_6: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => curr_address_reg(5),
      I1 => max_address(5),
      I2 => curr_address_reg(4),
      I3 => max_address(4),
      O => curr_address2_carry_i_6_n_0
    );
curr_address2_carry_i_7: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => curr_address_reg_rep(3),
      I1 => max_address(3),
      I2 => curr_address_reg_rep(2),
      I3 => max_address(2),
      O => curr_address2_carry_i_7_n_0
    );
curr_address2_carry_i_8: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => curr_address_reg_rep(1),
      I1 => max_address(1),
      I2 => curr_address_reg_rep(0),
      I3 => max_address(0),
      O => curr_address2_carry_i_8_n_0
    );
\curr_address[0]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"3A"
    )
        port map (
      I0 => Address_Data(14),
      I1 => curr_address_reg_rep(0),
      I2 => Read_EN,
      O => \curr_address[0]_i_1_n_0\
    );
\curr_address[1]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6F60"
    )
        port map (
      I0 => curr_address_reg_rep(0),
      I1 => curr_address_reg_rep(1),
      I2 => Read_EN,
      I3 => Address_Data(15),
      O => \curr_address[1]_i_1_n_0\
    );
\curr_address[2]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"6AFF6A00"
    )
        port map (
      I0 => curr_address_reg_rep(2),
      I1 => curr_address_reg_rep(0),
      I2 => curr_address_reg_rep(1),
      I3 => Read_EN,
      I4 => Address_Data(16),
      O => \curr_address[2]_i_1_n_0\
    );
\curr_address[3]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7F80FFFF7F800000"
    )
        port map (
      I0 => curr_address_reg_rep(1),
      I1 => curr_address_reg_rep(0),
      I2 => curr_address_reg_rep(2),
      I3 => curr_address_reg_rep(3),
      I4 => Read_EN,
      I5 => Address_Data(17),
      O => \curr_address[3]_i_1_n_0\
    );
\curr_address[4]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6F60"
    )
        port map (
      I0 => curr_address_reg(4),
      I1 => \curr_address[6]_i_2_n_0\,
      I2 => Read_EN,
      I3 => Address_Data(18),
      O => \p_0_in__0\(4)
    );
\curr_address[5]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"6AFF6A00"
    )
        port map (
      I0 => curr_address_reg(5),
      I1 => \curr_address[6]_i_2_n_0\,
      I2 => curr_address_reg(4),
      I3 => Read_EN,
      I4 => Address_Data(19),
      O => \p_0_in__0\(5)
    );
\curr_address[6]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6AAAFFFF6AAA0000"
    )
        port map (
      I0 => curr_address_reg(6),
      I1 => curr_address_reg(4),
      I2 => \curr_address[6]_i_2_n_0\,
      I3 => curr_address_reg(5),
      I4 => Read_EN,
      I5 => Address_Data(20),
      O => \p_0_in__0\(6)
    );
\curr_address[6]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8000"
    )
        port map (
      I0 => curr_address_reg_rep(3),
      I1 => curr_address_reg_rep(2),
      I2 => curr_address_reg_rep(0),
      I3 => curr_address_reg_rep(1),
      O => \curr_address[6]_i_2_n_0\
    );
\curr_address[7]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"6AFF6A00"
    )
        port map (
      I0 => curr_address_reg(7),
      I1 => \curr_address[8]_i_3_n_0\,
      I2 => curr_address_reg(6),
      I3 => Read_EN,
      I4 => Address_Data(21),
      O => \p_0_in__0\(7)
    );
\curr_address[8]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => Read_EN,
      I1 => \curr_address2_carry__0_n_3\,
      O => curr_address
    );
\curr_address[8]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6AAAFFFF6AAA0000"
    )
        port map (
      I0 => curr_address_reg(8),
      I1 => curr_address_reg(6),
      I2 => \curr_address[8]_i_3_n_0\,
      I3 => curr_address_reg(7),
      I4 => Read_EN,
      I5 => Address_Data(22),
      O => \p_0_in__0\(8)
    );
\curr_address[8]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000000000000"
    )
        port map (
      I0 => curr_address_reg(5),
      I1 => curr_address_reg_rep(3),
      I2 => curr_address_reg_rep(2),
      I3 => curr_address_reg_rep(0),
      I4 => curr_address_reg_rep(1),
      I5 => curr_address_reg(4),
      O => \curr_address[8]_i_3_n_0\
    );
\curr_address_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => \curr_address[0]_i_1_n_0\,
      Q => curr_address_reg_rep(0),
      R => curr_address
    );
\curr_address_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => \curr_address[1]_i_1_n_0\,
      Q => curr_address_reg_rep(1),
      R => curr_address
    );
\curr_address_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => \curr_address[2]_i_1_n_0\,
      Q => curr_address_reg_rep(2),
      R => curr_address
    );
\curr_address_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => \curr_address[3]_i_1_n_0\,
      Q => curr_address_reg_rep(3),
      R => curr_address
    );
\curr_address_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => \p_0_in__0\(4),
      Q => curr_address_reg(4),
      R => curr_address
    );
\curr_address_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => \p_0_in__0\(5),
      Q => curr_address_reg(5),
      R => curr_address
    );
\curr_address_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => \p_0_in__0\(6),
      Q => curr_address_reg(6),
      R => curr_address
    );
\curr_address_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => \p_0_in__0\(7),
      Q => curr_address_reg(7),
      R => curr_address
    );
\curr_address_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => \p_0_in__0\(8),
      Q => curr_address_reg(8),
      R => curr_address
    );
max_address0_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => max_address0_carry_n_0,
      CO(2) => max_address0_carry_n_1,
      CO(1) => max_address0_carry_n_2,
      CO(0) => max_address0_carry_n_3,
      CYINIT => '0',
      DI(3) => max_address0_carry_i_1_n_0,
      DI(2) => max_address0_carry_i_2_n_0,
      DI(1) => max_address0_carry_i_3_n_0,
      DI(0) => max_address0_carry_i_4_n_0,
      O(3 downto 0) => NLW_max_address0_carry_O_UNCONNECTED(3 downto 0),
      S(3) => max_address0_carry_i_5_n_0,
      S(2) => max_address0_carry_i_6_n_0,
      S(1) => max_address0_carry_i_7_n_0,
      S(0) => max_address0_carry_i_8_n_0
    );
\max_address0_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => max_address0_carry_n_0,
      CO(3 downto 1) => \NLW_max_address0_carry__0_CO_UNCONNECTED\(3 downto 1),
      CO(0) => \max_address0_carry__0_n_3\,
      CYINIT => '0',
      DI(3 downto 1) => B"000",
      DI(0) => \max_address0_carry__0_i_1_n_0\,
      O(3 downto 0) => \NLW_max_address0_carry__0_O_UNCONNECTED\(3 downto 0),
      S(3 downto 1) => B"000",
      S(0) => \max_address0_carry__0_i_2_n_0\
    );
\max_address0_carry__0_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => Address_Data(22),
      I1 => max_address(8),
      O => \max_address0_carry__0_i_1_n_0\
    );
\max_address0_carry__0_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => max_address(8),
      I1 => Address_Data(22),
      O => \max_address0_carry__0_i_2_n_0\
    );
max_address0_carry_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => Address_Data(21),
      I1 => max_address(7),
      I2 => Address_Data(20),
      I3 => max_address(6),
      O => max_address0_carry_i_1_n_0
    );
max_address0_carry_i_2: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => Address_Data(19),
      I1 => max_address(5),
      I2 => Address_Data(18),
      I3 => max_address(4),
      O => max_address0_carry_i_2_n_0
    );
max_address0_carry_i_3: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => Address_Data(17),
      I1 => max_address(3),
      I2 => Address_Data(16),
      I3 => max_address(2),
      O => max_address0_carry_i_3_n_0
    );
max_address0_carry_i_4: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => Address_Data(15),
      I1 => max_address(1),
      I2 => Address_Data(14),
      I3 => max_address(0),
      O => max_address0_carry_i_4_n_0
    );
max_address0_carry_i_5: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => max_address(7),
      I1 => Address_Data(21),
      I2 => max_address(6),
      I3 => Address_Data(20),
      O => max_address0_carry_i_5_n_0
    );
max_address0_carry_i_6: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => max_address(5),
      I1 => Address_Data(19),
      I2 => max_address(4),
      I3 => Address_Data(18),
      O => max_address0_carry_i_6_n_0
    );
max_address0_carry_i_7: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => max_address(3),
      I1 => Address_Data(17),
      I2 => max_address(2),
      I3 => Address_Data(16),
      O => max_address0_carry_i_7_n_0
    );
max_address0_carry_i_8: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => max_address(1),
      I1 => Address_Data(15),
      I2 => max_address(0),
      I3 => Address_Data(14),
      O => max_address0_carry_i_8_n_0
    );
\max_address[8]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \max_address0_carry__0_n_3\,
      I1 => Read_EN,
      O => max_address_0
    );
\max_address_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => max_address_0,
      D => Address_Data(14),
      Q => max_address(0),
      R => '0'
    );
\max_address_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => max_address_0,
      D => Address_Data(15),
      Q => max_address(1),
      R => '0'
    );
\max_address_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => max_address_0,
      D => Address_Data(16),
      Q => max_address(2),
      R => '0'
    );
\max_address_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => max_address_0,
      D => Address_Data(17),
      Q => max_address(3),
      R => '0'
    );
\max_address_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => max_address_0,
      D => Address_Data(18),
      Q => max_address(4),
      R => '0'
    );
\max_address_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => max_address_0,
      D => Address_Data(19),
      Q => max_address(5),
      R => '0'
    );
\max_address_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => max_address_0,
      D => Address_Data(20),
      Q => max_address(6),
      R => '0'
    );
\max_address_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => max_address_0,
      D => Address_Data(21),
      Q => max_address(7),
      R => '0'
    );
\max_address_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => max_address_0,
      D => Address_Data(22),
      Q => max_address(8),
      R => '0'
    );
storageData_reg_0_15_0_5: unisim.vcomponents.RAM32M
     port map (
      ADDRA(4) => '0',
      ADDRA(3 downto 0) => curr_address_reg_rep(3 downto 0),
      ADDRB(4) => '0',
      ADDRB(3 downto 0) => curr_address_reg_rep(3 downto 0),
      ADDRC(4) => '0',
      ADDRC(3 downto 0) => curr_address_reg_rep(3 downto 0),
      ADDRD(4) => '0',
      ADDRD(3 downto 0) => Address_Data(17 downto 14),
      DIA(1 downto 0) => Address_Data(1 downto 0),
      DIB(1 downto 0) => Address_Data(3 downto 2),
      DIC(1 downto 0) => Address_Data(5 downto 4),
      DID(1 downto 0) => B"00",
      DOA(1 downto 0) => WaveformData0(1 downto 0),
      DOB(1 downto 0) => WaveformData0(3 downto 2),
      DOC(1 downto 0) => WaveformData0(5 downto 4),
      DOD(1 downto 0) => NLW_storageData_reg_0_15_0_5_DOD_UNCONNECTED(1 downto 0),
      WCLK => clk,
      WE => p_0_in
    );
storageData_reg_0_15_0_5_i_1: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => Read_EN,
      O => p_0_in
    );
storageData_reg_0_15_12_13: unisim.vcomponents.RAM32X1D
     port map (
      A0 => Address_Data(14),
      A1 => Address_Data(15),
      A2 => Address_Data(16),
      A3 => Address_Data(17),
      A4 => '0',
      D => Address_Data(12),
      DPO => WaveformData0(12),
      DPRA0 => curr_address_reg_rep(0),
      DPRA1 => curr_address_reg_rep(1),
      DPRA2 => curr_address_reg_rep(2),
      DPRA3 => curr_address_reg_rep(3),
      DPRA4 => '0',
      SPO => NLW_storageData_reg_0_15_12_13_SPO_UNCONNECTED,
      WCLK => clk,
      WE => p_0_in
    );
\storageData_reg_0_15_12_13__0\: unisim.vcomponents.RAM32X1D
     port map (
      A0 => Address_Data(14),
      A1 => Address_Data(15),
      A2 => Address_Data(16),
      A3 => Address_Data(17),
      A4 => '0',
      D => Address_Data(13),
      DPO => WaveformData0(13),
      DPRA0 => curr_address_reg_rep(0),
      DPRA1 => curr_address_reg_rep(1),
      DPRA2 => curr_address_reg_rep(2),
      DPRA3 => curr_address_reg_rep(3),
      DPRA4 => '0',
      SPO => \NLW_storageData_reg_0_15_12_13__0_SPO_UNCONNECTED\,
      WCLK => clk,
      WE => p_0_in
    );
storageData_reg_0_15_6_11: unisim.vcomponents.RAM32M
     port map (
      ADDRA(4) => '0',
      ADDRA(3 downto 0) => curr_address_reg_rep(3 downto 0),
      ADDRB(4) => '0',
      ADDRB(3 downto 0) => curr_address_reg_rep(3 downto 0),
      ADDRC(4) => '0',
      ADDRC(3 downto 0) => curr_address_reg_rep(3 downto 0),
      ADDRD(4) => '0',
      ADDRD(3 downto 0) => Address_Data(17 downto 14),
      DIA(1 downto 0) => Address_Data(7 downto 6),
      DIB(1 downto 0) => Address_Data(9 downto 8),
      DIC(1 downto 0) => Address_Data(11 downto 10),
      DID(1 downto 0) => B"00",
      DOA(1 downto 0) => WaveformData0(7 downto 6),
      DOB(1 downto 0) => WaveformData0(9 downto 8),
      DOC(1 downto 0) => WaveformData0(11 downto 10),
      DOD(1 downto 0) => NLW_storageData_reg_0_15_6_11_DOD_UNCONNECTED(1 downto 0),
      WCLK => clk,
      WE => p_0_in
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_WaveformGenerator_1_0 is
  port (
    Address_Data : in STD_LOGIC_VECTOR ( 22 downto 0 );
    Read_EN : in STD_LOGIC;
    clk : in STD_LOGIC;
    WaveformData : out STD_LOGIC_VECTOR ( 13 downto 0 )
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of design_1_WaveformGenerator_1_0 : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of design_1_WaveformGenerator_1_0 : entity is "design_1_WaveformGenerator_1_0,WaveformGenerator,{}";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of design_1_WaveformGenerator_1_0 : entity is "yes";
  attribute IP_DEFINITION_SOURCE : string;
  attribute IP_DEFINITION_SOURCE of design_1_WaveformGenerator_1_0 : entity is "module_ref";
  attribute X_CORE_INFO : string;
  attribute X_CORE_INFO of design_1_WaveformGenerator_1_0 : entity is "WaveformGenerator,Vivado 2022.2";
end design_1_WaveformGenerator_1_0;

architecture STRUCTURE of design_1_WaveformGenerator_1_0 is
  attribute X_INTERFACE_INFO : string;
  attribute X_INTERFACE_INFO of clk : signal is "xilinx.com:signal:clock:1.0 clk CLK";
  attribute X_INTERFACE_PARAMETER : string;
  attribute X_INTERFACE_PARAMETER of clk : signal is "XIL_INTERFACENAME clk, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, PHASE 0.0, CLK_DOMAIN /clk_wiz_1_clk_out1, INSERT_VIP 0";
begin
inst: entity work.design_1_WaveformGenerator_1_0_WaveformGenerator
     port map (
      Address_Data(22 downto 0) => Address_Data(22 downto 0),
      Read_EN => Read_EN,
      WaveformData(13 downto 0) => WaveformData(13 downto 0),
      clk => clk
    );
end STRUCTURE;
