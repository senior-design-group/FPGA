// Copyright 1986-2022 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2022.2 (win64) Build 3671981 Fri Oct 14 05:00:03 MDT 2022
// Date        : Sun Oct 15 16:07:07 2023
// Host        : DESKTOP-QRFTT4G running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim -rename_top design_1_c_counter_binary_0_1 -prefix
//               design_1_c_counter_binary_0_1_ design_1_c_counter_binary_0_0_sim_netlist.v
// Design      : design_1_c_counter_binary_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a100tftg256-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_1_c_counter_binary_0_0,c_counter_binary_v12_0_15,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "c_counter_binary_v12_0_15,Vivado 2022.2" *) 
(* NotValidForBitStream *)
module design_1_c_counter_binary_0_1
   (CLK,
    CE,
    SINIT,
    Q);
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk_intf CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:thresh0_intf:l_intf:load_intf:up_intf:sinit_intf:sset_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, PHASE 0.0, CLK_DOMAIN /clk_wiz_1_clk_out1, INSERT_VIP 0" *) input CLK;
  (* x_interface_info = "xilinx.com:signal:clockenable:1.0 ce_intf CE" *) (* x_interface_parameter = "XIL_INTERFACENAME ce_intf, POLARITY ACTIVE_HIGH" *) input CE;
  (* x_interface_info = "xilinx.com:signal:data:1.0 sinit_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME sinit_intf, LAYERED_METADATA undef" *) input SINIT;
  (* x_interface_info = "xilinx.com:signal:data:1.0 q_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME q_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency bitwidth format long minimum {} maximum {}} value 9} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0} integer {signed {attribs {resolve_type immediate dependency {} format bool minimum {} maximum {}} value false}}}} DATA_WIDTH 9}" *) output [8:0]Q;

  wire CE;
  wire CLK;
  wire [8:0]Q;
  wire SINIT;
  wire NLW_U0_THRESH0_UNCONNECTED;

  (* C_AINIT_VAL = "0" *) 
  (* C_CE_OVERRIDES_SYNC = "0" *) 
  (* C_FB_LATENCY = "0" *) 
  (* C_HAS_CE = "1" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_SINIT = "1" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_IMPLEMENTATION = "0" *) 
  (* C_SCLR_OVERRIDES_SSET = "1" *) 
  (* C_SINIT_VAL = "0" *) 
  (* C_VERBOSITY = "0" *) 
  (* C_WIDTH = "9" *) 
  (* C_XDEVICEFAMILY = "artix7" *) 
  (* c_count_by = "1" *) 
  (* c_count_mode = "0" *) 
  (* c_count_to = "1" *) 
  (* c_has_load = "0" *) 
  (* c_has_thresh0 = "0" *) 
  (* c_latency = "1" *) 
  (* c_load_low = "0" *) 
  (* c_restrict_count = "0" *) 
  (* c_thresh0_value = "1" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  (* is_du_within_envelope = "true" *) 
  design_1_c_counter_binary_0_1_c_counter_binary_v12_0_15 U0
       (.CE(CE),
        .CLK(CLK),
        .L({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .LOAD(1'b0),
        .Q(Q),
        .SCLR(1'b0),
        .SINIT(SINIT),
        .SSET(1'b0),
        .THRESH0(NLW_U0_THRESH0_UNCONNECTED),
        .UP(1'b1));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2022.2"
`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
KdkdvVsuosc8qR9X5PxQ/ghTeTrEz4qKVuenhDR9wRSL/BO/mhSwQtiFj74UO0sGv0zvjAntaq/3
l2/v8gOiVKmM666gbk/2UCISA4OFA3FDR9jYmiXdNXb2qHeS1ywQz5n/sTR5iu4KFEfwrl3IXtQw
aEiGegL+CQMaovJsto4=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
pZCj3qT3VD1SCS5RiZExsqqu16KpMtHXilQL9p5/eBl7qrfQjT1VhFtVbYUusepbChjsCCmCn7hr
72SuHmOmDWG78UARN7MLdO/+sePuyS06ak4nAw5xwjT0g+9970uMWYKvTeeYqoz2i+k+zX60Cuvu
iwBfxWM22DqukHlYzbEFWhNyXIkgJe71p67vGdXBmqu4/2wmlwGApqBxlwR+alwZ9UGHlxNQS4N5
z1wHu3Cp8LwGRjlaXjElcY8RDpvyz5l59ey8ar5HXR9Zqf6e1unE2NdhzHhEGRerRFXoKZppk1HB
6kIEY4EHAWz+HvPcqoP9eoYKDazoAGkJRVP6YA==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
gLgm7VvY3cNcNvdXvikCQd2nRniE4ae4hePOcAUlPDMoHDzQAD7Ngo12MGFns9JNPcCaUXfAmxL2
JNGojjrDRUWrv8FPV6FOEbDHs96fef8+gqLF4OqLck4kWpKhnJwaJjjzQirvXEzZxP+GsBKnkSp8
ceVlZJwP0F6XRv+RpQA=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
GeZP242oKQSNuofqDs4oIIXZEufPhRVrlFFeRSLY4VCxhMEMwfPrNXe33xO0zIEBoPW2X9mvUoTY
izdWQEtWImFzjzPCjkSLhEdIMmUBH02Y+Tw3eW5x23T0cK96pmoV2MH8kl99I27MN6stVd977fuB
Mjao5MnSXIGZ/uXGtgfUO9Zjs4/2wGmsI2/lANN2WOL9Sz4xeA8k40c2dNYgxgHoCwx8Ya/RYIZS
Cpuvzq4ZyFSNT/kMXnUmqj75/flpXT3mmyW+frexux3j9PxpKHmxAE9crvDx85rMamGiA4ftl+ac
H0FtL2cBqdlP60x+FjqleWCJoN6AYdxA0YZaeg==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
URmEGftuxvv0+tViRUdsFNnPXucZlVDfUQpjjXkpOA38QUzsIL9j1pGGp9doC4jcg/9MD149BTSw
vAG8684a3k+Tx/8sFGl/viK1q8ty9nktEABSahv8Etm5ZJVAzQJT7EaOzrYqyywSwabogvGUmN/7
DE3eOn6+sMCiMl6BLUhYyK39ntTWNFYVPiheclbBb36V1vzMOQl0mvPuS4hDXqba/+qBZXhqeYWK
ceNfwci6SsRRef6hLF/1S+20r2uBxJeYJjyfWGGFEGfxlAOz1MiYUUR/bEHWnbjwIcJTBHQNRdq4
4Ryb+iPuKcsXU/8ApD14i6ScW+VBPWSqnH9w+A==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2021_07", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
NtQgA3rUKfJt+21sTot44yr4gmte57FoFl8Q/327tsRJeEyNAiwWZaZN2mbo2NFcvyN2GhDw6avJ
NsF1Oxs36P8shoqOOiloWWrdTcyAdMhdk+UjeZgKcNSqd4Js87w/5LVQTwjB2mcBDfe1jrivv+IW
ZRBC8NvlW5z/1wF7+vzXRMziLQYeOkLB0OkpIY+eT5cZXDKuZ+4l0FMPjd+El96JGAEHG7Q0qS3F
OEApYEp8+nSZnragoytq4pkhVJEC22ye0hBhoBClJpszCcg0u+Ugf+mYZsj8BC2uqSY6Hh/gpjjw
enQ7aEYBaUR7GCwQN7fZmNhZYtBkyvNqydRQcA==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
CpIFM8Y8dBmpjtOVnOKcfppEFV+c1cRgsQtewNUe+5apiLDoRCdMyTqoCay7nz+Xagc0OvfZDg/Y
jSTsDjKVcEIyxOfix7iwjKW8Rz+a5wBIatI8wfCo7uLtuucz9otOWWI7BFQ2gn4VdQ73HJJlZMMY
OyEOd33tGjNSjxz3W07knDr1FwTE3BOfhq+Qj2ErnuV1dQbrTb3MiQMTnHaTCwtz6ip0pD6b5G4K
kBRUYe+UNXCMvSfNIN9MPSmolO4MjNwM5gnZZqLcR1hGuzH/Yeb/jPnhsZ7jFvlTT3nsM9JzMRAE
QwlzVuulHKQDS2I96arFosYPYMsalmn6CQW0gg==

`pragma protect key_keyowner="Atrenta", key_keyname="ATR-SG-RSA-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=384)
`pragma protect key_block
qinIxHFISC9r9LS7OKOuYVGM5EBkuuQNV1nDRui+QVNLn2QFCrWPeEClQIlNViKOt8MX9urHvu4e
l2L+eZKw6+St9cW9yUsYu36yoB4LqwG+vKvfR9CW82LGPyMAxdgk/p3n+F0Xp9Y2HaERwWDL99tW
V7cDvLLhyIwz7w4rI0BWWV+KMjXP2F5MNgykzZn7tzV8oY6MxOykFqRdI8DLAdlYGAs90wjJ3x84
S3fHciSox97FYpDi64v31Vb4RmRrwueXcvCc3w8gzjuwg7qraWLMYyPB+mERB2v1htX80PsWWVHE
QXkWiHWYvvrXEykUS04MmLNHpV8ZgBXO/NBEGn7mrITDEswk3u1Yviqy7CW2wLPQBoo5xW+uiu2e
8YZV/E+bAt+P/EH5RsC9alBgtuVKU1s9DaiEH8eUPEgJQ/TXwQW01pg8ECTYgiBS+IQSbld23aq3
goVo0ZMzRu/SA00Jmwt7upvsMkh9Q+2732ahu1FmlSNmyNGB1+bYf782

`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="CDS_RSA_KEY_VER_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
T1jqx5hmzZZMhPApzUC1oZLMAkHma8Ki4b2CvLNqxSn+MNWoTPomvQ775DMBEDai/gahYALsohdX
0f/e6LuPqt4zYtyAzmH+nRgOG/tilS1J674KsaHxudAfo4sM3awB/C4Q3VdYsO9FgvPQylnYKSGE
gJ46W+1Y789VQqPbt4dpnprhix6sLlwfww7We6cq2wu4PilFzovejouUBZqNMZHYi4suKcMcenp3
C7QRKloo8IF9yKrhGPcRJLQt2nus3bI0Q3ICxRk13Nrfhh/z4cdm0OGXz42q44snFEVy1lLxPOs7
W9tSe5ag3923oCT4NGGgK/gMTx5qXxFhV2MJUw==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
cwVOXRPXtY5nrZ9Sk9qEsX6Uye0NkhRLqJ60A8QGACtac5I/xqqi8e+4iqFuxAXV6C55IT8lzrVF
m1B8iMsY4CSUUWFWQtIVUjT4XGiRQmfafVKN8pf120Q9VtjpxScRa5r5Az0DEN8NS3ZYOqqPfWZR
MlhRrgzA/6Mi6R2n383BIRk13yo0yuYv838CGlLTfDBVqoXfH/HAaQXU5G5C2RufXGf/zPtWjAjY
6srBeUHdCUeeWoWvGm4oG+ZUjmMHWBu6yvdG6WEzhVdmBbKASaWtVeWqvfm8/UF0WFyCfNB3j1Qe
g/+Nrk6U6FAFQ3Db6qndofDf+CRR33FAcqIvqw==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
xCSHCXl0Ftptpm2FrzZM2WH8AVv0ArXax+/TbXG3OH/xIZlONTxKtB7x/T/75fJbaTbtjpx0u+Ys
Hg5w1SI/qorZQsy3SfICx7pgQeNp+jc2YoPk52BqoXZucnO+6qdnHbkix+FyyiSvTdQRTzhv++ok
Jn3HzRaKfJBU0RUNdEailSuQezIQssuYpBv67M/3k3aUQZp6cvEijHOxeMZ1EmCEfozhZsytGBjy
Uhgby9V2AywgCnh++2FWjfcLlNt4LpVtIcXUuUf/H7CMDjio+P8hrLVVzHX+lMCP4qQNg+GyWB8R
HbYB5RRbI/FeQquPnmw9L94R1f/X9ccAu8GZbw==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 10240)
`pragma protect data_block
8efz3kC3o0K1p1ZRDQZOi8MppDTYRIRzVs9mvitrur2bQMDyfQH+YC4g74ZR/Cjz7OJZP3t2ihkN
uccj5o9u10yh0mQOmprFZzOUYDSnVnZGhzpjfypdbdkVUb8MVqa5jdSP4WLvczWADvdAI6R7tYTR
DXSikuYV9ZJh+c82sF76zEiXEP06LAYRWbeT8cXdpa+0HBn5mZoSL3Ot7rVhZGq2D5/PL6aXowrT
sV3jDTDHm6UXK8go1TfpJNQ3fPikJ6WOg29TULnSSLrEycy7pbDyWreyVZMb4ULWUm5cUAluTEpw
ub4QOjJt7XjQiJ8WlhO9kaB+D/ttld+i6cVwAB6Gk4v7mqfyWmmkNHWoKS+v2BEpvLXru50cuNDM
E5GNgYwt/wtMZUgShDMEjc8cyCZp9zJEKEJkQAEcaSOm6gc+IdvtAp8XZ9q10cPTtkZASHwHTE+M
6XKFbaQcHUjCOw3xGIpFwSrPkW3JAk/OV5xfBuv/BS9+mCHvLMuVOBgpVyts4bxhlpWM0HKzoGTN
+mBdtaMEbqYePxv6ykYtUfr+B5qoT4zgrbxzfQgJaaskKo3Ny3d8kOgKkij2vVXVK164hu/CAZIX
WExaQYM/M5fCb9m2JssR8PDuN89ai53f6WEbfs/E+pCTxFijGapKKa+b9BG2BzLCmTnGwGoRkPUx
ZtN5NQbJlv8esUhDC/8GD6g+xXGjhdX5Cb+yg6Z2BG/LYf5b/gt5oy+z7mGpYGBN4LWg43g1Z2To
QnwMMeRu7uiHLZsDz6owb9y4c5jcjxi3+igTR6Md5lX6oYVidDioNMPEbXkVN7Ua1bJETweksrlx
tt4xYARzHSjvzAgRU7cj0qITX/wtt1R56klvP0TcM04l1Hg5zG/NLsJtrY1z4Fdm/XEGle1wqPSH
GklR+OeOIluImUd6WhaZtvbX007WXuWPPcY9LV3mrPUCv+FwEM1BVy7q6OgZEjs0e7LGK2jWLn7+
SgotStDxJgxT4JGiM2ODOdjYwZj+Bcc5uCnNiuUyVX34sH8wmUgjdy7ai5hElNxMlh+ZOx6boOle
8dBCGIpeUE0hCJbXeHKELLg4AhLnesEzJB9Bjl8vfL4aCo7sVWS66rLkpqYC8kv2um4lf+Zd3eT7
HH39x62jtn/wrJqOiWabE63rETfBz5sP7w2EELEXDM2mRG+JoJEjDd1vyrGFRuvPjVnda0otfdnK
AMs0skzonuQjCDkGcQGZdA8hhSe5BBKX5r4VOZSqznuGTMK+u9J1BhbQGyYxqQwm2PSOmRTToR6i
GWo4wFaM1yx7EhGY9Q0M5Ynyvb31waq+BU6RrpuvJ96N28s+vNT++k0cIcJwNNB9jvWYJZCw7R/h
Z8gJjBHdl8/Tgs4uRpvWeb/ByotvXdaAOK9na61V5Ra4xYT3JhSXAJevqP+KSS+G35DSYWYe972+
Yym+oevJHhz0T1/N67f4YfdIU8qlwzqAA6nRHiczqZ2wNQQ6G1bY8UEhKp0JJAtfxTlcDF3IFO/S
YW/33BnW1ubqcwvVriZTjAcGuXnU16Vw1Z7PRmZ9TZArAeyCuazo2iDgMPNxFP4FBJAkbHRcCZvQ
XRm/4FspvPAKLMDGVBzj+YizKmOttaaK6M27tzG4NNXMxLTjQMPzBTkzoPebU34ugqyNOBAtfdzi
z/YNMZXS0KgjD86d7vtpCT9n2cegNb0M51CZDBPisAybQhVboKvW2EhyDcmq37M2FZtdv+5M5dRA
HobOuHckUbKEXQuAwEd4qTpffI7JrOakGEpo7BUtrvZqYmUiHounATLBlv817YSFg4/uld7Ofbyx
f7Rxi2T3G4oaZaiJrHMn/Zo3kEOYvgG0HTfrOsOb5LLfuh1cVx8Q17VNH7+bBrUfhE7NPsWqmO4f
c28mPh9QZ6pnnxQ6nEE1BXrpSWjVxWIbeW1ZXZoviN11tJZVW3U0o8UkS7lcmV05uahBC8JX0ol+
FWmDmzVbMinLf3s/nqtykGeYwPzsDcO/zISY5CWLEqpd4RWv3n8e+kT6mlU7MRWN9HLHEA3GgzCc
k60SOpNIquYiRfOzHVrpvFySJ98TEikRHNbvy8ijv8zR4FNplUE/lA4UshLHddux3i/INnvvpYlb
d8XFwhhYbh8AqgkVMAUOGB0iIOoBacg5BAiRRuTTyMnBJRmdB8N0koD1y7Uz7kq398KFyz7z3eIz
jjJMKlX/Vrdn3tjz9oTzERZBuZ3FTWzkhtndXCZ5oBgcBVq4jF5xgeVGIhta0xl11RVR/eRnvLWz
eVXsNLfQzHX1BwAAFoUcatNbmMjm9/uEFtTnXHIXR7Oyh2/zJdg7/8pKqKZySYBUlOpvFKjcrhyJ
jUYpRAFds66O4duC7vj3hiQ6xoYn3D/2EbkBqB4FFYY48cxEX7rkDf2/MroCPio+rwwj2B0DpeRF
nu8IQfXaLeSEYckK/8Fg+ZiKf8LWr96DMUKX6MeyTCwM/mB67THqAtsYU6wgnDlNeIPY/Z5tC2gE
B0QR8NEI3zCTZnBaDQZhJoAj/2osVZvz/hDXXeYYavypsnf9M7dgxpbedFK8pEArVYS8fr8O4MYk
FQH1lGWMusIM2Omw6DL+IfN0DeiKIVfbyKeWwYB3OGMnYjYrpnP4CLLhKRMu+IHVIpS+InPIuJxR
svMIz+k2J7ZmZzzv7SVg4DAmzLEhEACuJMkkZLZDa2xCDqql/2FRxvX2esdpceazcX+7zJnxlKR9
ikMO78iqqFtBQXvq9vK2Dpwh5BHZLbanBXudoL3p0DFtH5oOiqvlKkDoNYl6ZBQhoMths4Mtmj/d
31/2k+kIqrnOV+qSVaqEfk2T6eTTIYYm+dMDJ8eZFeg7QwoQrNYTSafCcBkbPJrJMCbEcTRcK/3W
pAWJ+LcoJLAI4QLN3U/wLL1iTGuahPjwZdeWnFBE9zblBRmlVgD86gw2I8pV5LCe24gQjtfDHQeY
xTGzL5WdiIoZBHBJavTzGZMMblEyQ38zEArLjaMSB4oSqtKDHOHDfvoAnDNAwI1zmVHuhv8HwM6E
3jvFGu/sCCesAWhJOQ6sF4Fr6ASo+UZjVR2QSmafzpII3fquDxustfU32qHvz8dDsayoYpNvHg7H
GqE3ufTSKrK7z8jNYyCLvfIA9BXEaLulKeHzqn8Hxqab96Jevtb2c2052S2iD90ibON3SAHLaoXN
gSrjvKCzsRv9TXN/d7rPJ4oEprJNZYHZCAMMJ9dCE022q6h4ZjIFx/ft69cyIoEOy36eS/EEOeVZ
rMoAmABUev6wyg3CtLK+ASxg6A39s/UQLEDxEirY7Hx1g4E1+am5OtfbO/Ew9b7FZURb0iWmlHq+
6JCLBIsrFkSAJMyRecljxNGO+mJYMBCGTvL3hxZrX13iwJJ8MloBp/X4TBLnvwjzcX9qhwbZqbbc
LL5GfiXik7AH0hXw0N9qzMiO4Q6V2eXicA/0/gMFii5Z8+GJB1MPiQ25QprRT3WQBKfbdWmYZ5vY
uhWLPuncSdCD3lb5y2fimSswI83gHLVPhorBLOVhY1JobXe2MJMjZwkT77Snb7HBsyyOtiP9HUss
EG8fHAZKQviZ4+MW3BxhLF/4nxr3kpH59W3UIg4B29qjm1CdTZrXY7mIXOvsHSwtmkIvuZqecUlT
Xfj/eAyC4dOLok7kDLmuBKT2BkeZNhBIG9R6QC+m2sHYVMUMnKD1ngmsMZB7Kpn98txRTwEPgzO0
XCdWZ3HOgNHPjDfNFOaWbfmmCioCKpme78JmbDK2qkdn7sdestjkjnJMDYxiJqiGKOXTHlQkZbiW
joq2Fbtlyvett5b1NrZoEMML1CvFuAqIeh0Qda2OEky4uokW7CmzcI8y1jqWZq4iRLhP3U8oHp8W
1ImhhQb00ASk+3W6gX70uFBplxY54p3F/bSm9qHwOB4kBfAouy9wr2SSsAIASNYxT+bsx44o6FjI
N0GDtmw1PEyGS7M4ymY3H27bnw8/Cotxkw69vSDrTPbLVeez5rr9BJcZfXMp/KShAsQpeYepQ2l/
nje4DEnZhjwnuDZKHv5u2QtGbkVFS0KoZ0/IlPa/1Ssohdq3D0s9xpceXJhlGJfAjNm4xjW8wZpK
VYKxefsue9eKawqksRCXxe+s4V1T4ucGYw2n6BTIwthvWjFGr+YMDPR5zuBub3EpdNEiZ38OqUug
OznCchjlew+VxgPwckCIaFMN5+YRxDlE5g8skdVIzD02aQaQEMAp8yB8/qpMUxvwdzuRJii4cCo8
HC1uEzAkIZqn6wn5n4BkC1H6Y0yTPvC56q7QLGoRZmeIvAOS3/PiO2S/eCLIZfdRePrBAaOnajbp
5eYaMz8Xr5Eyf/j44tuZ0LBoTXi31JCQWcJ17XV6jrVz8yFF8XBq8yQPrCWy714tIT7iAyMhmQk0
u+yjxIcbyjYg+OHQMa6BC6YVQcRqR10xgNcqw8vHNIsbNgYpIqIDq8kdNeO7GMqfe0YU+kWRWjGd
x9StZCI4ofgEOIdTBY1KXfhpctYoR8dyq3G/wJQslme2Nw9oBUxnUT1JcjoKn058aystpL+YcX2p
OTFElxgVJA08pyyBSg4e0XBwR7uH6m+huHq3R1pVUukEhrQEzQpFQ7DXWoVhnLh3Kww1iMVzakTk
rLS320Tb1tUAspRstNDIKTWIR0rao1dqdr+4CZCBrtStG4bGg6PTKPyxlcy20wJ6V0/ENaAEVhZm
HJzpg2xe4cAAXhSbrgep6FhIXLelpeWJ/a+TYzpTO4pVH9g1zddSS7o0ZIDW12lgL68w2k0zoVVI
3QXymIFN0254flaRA9w+wos8INvVf6KjHAh7xipLrJdL+WEtbOIF5MqHdfCMkTENxBBiqtYVbcVO
xLb3wKJJ11VTJA2vAk2GzGn30Yw0gPF3rhHldrpwaWRfxF4pOt30MzMUYDKYJiyvTX0HODXbc8MW
cuInqLxTHUx7/kdstUqfUiwi8EchPTUlZIA/sv+bufIbOvNb9Yc6p5EmX6hceHRWksmSQ4McQPQm
mw1BHbkIEMJSo/ABjuyhNTvaVz76qfyl+4o02gY9Zx6QuXSybpvDCnoM2iYFdye8gv2v6+y0cgmF
+9+PhujCXHh+Lerv0Cm0ldK5KZKVJvoHcJMY2s4g2/xCYLga/nLuGP3WOzlW5gtMWw28WsNkhJPK
hO+vmZHciTe9jDY0s7KIz/Mm8hnLNG0DIehol8cAJRA6FBKGIb2jkx7tlZJlebJBrFozcAGFnj5r
jnWzCLKp+Ioi7Wg6YOTJHV4Sq9kZrfVIdVOKGXj1YAvdL0377EwKF1SYTvOPa4xPDiiBmuGQ+ylE
ryYRghjqsYVO+a9xYxlIv3PT3X954grsLvbYYfNgZETVeO9ngipPq9Wk25vRXgu4IFwueXaAsXc1
gFy9GMO8WvocbTv3/VafdEbhHGSNIC0o/i2FA1ekBuJHDunQWxOZNPO17VBGoqn71nKvREIPaL84
A7/6705yI5QBJzPU/sZWC5+aQTt4W8/BhFYrlItgz8vf18T++34a868d7D8z5z9elswP6hpiEyN6
EQCco8jIS3fHGRF07TVgUf+6HdprpAVYy0nidbdDCQI4FWsjTl2eS3UnORTUMbTWb0JZYlKeAD2P
H6ItgLveTjWYoCQJ7+887QEr3mJziAWqwwJeEFpKrN18wc+hECNcw1eNJHX7OmPv3N9E8fRqZWWT
P29i5YNqkvjWjoXrXmAAITIrySrRSTcH3lFKKO5Fm8gO39DIrYNJMf/qepnVJpStdPptGcUHoeC8
IYtQMX/5402EPaubW3OimwouiVHnVIR+M9bDzQdRnS2bNgGsAcZEFX377UpqcbXVvkwirkBmqFxt
GHQZzJFg1l3YETvNMXgggsR041NlRtw11IVj/l2gucIIC1jVzs+pW/I4mdpcHYeimkJgx8t0uL8g
qkawrM1B8FeWw3uZDEPrXl8E9s7KeqFHZTYMr6FioMMCN0laGCn4StUGMhbx9Jv4Cfb5akSkXU/8
+jjE3/830xu1RV72kj+8I95FgndV07ZQBnYiTzpIn5yVGTkMDNFzjfWoWMmm7EfGFbD0+Ynx/p5Y
SovlKg2oDbWtM9sZo2baKgYAkWhIZ4OCKCJOAjplWX8L7BlCRvqwdlUwa+c/K2dzrJkGuJfL+mil
z+vvet7lffdtlRp/1grQKF9K3Pbox6O1ikKxjNAqkymlA2194RrvtaUd8qTwiLfKUH8dChbBtjFG
TrWmc10AWtUC6B8z6SJgq+CdauOfl3EP3sihYLGaZZUiM5/QSEmzzqTZ103q78mbKTzX61Ju7Edi
DNlkir5sVYKWfKDhaI+NRWlLkU1fMIOpLHmiyJGo+jLEWpdbH47pSfC2PVF0+EqVWBylG1OVJu2m
GHuJw1g+1vlIMn1FhcWDQsb8K84m0XY4A5Dl+19GDF8+m3skplAoSnNPuiB6DYP0L/EVlKi28nNA
nnj0YjNPGixwXGMF3eAJgnb44KGIXrRK/HizmmbEUHZvD8IYjGmrS6WV6rRJfApq3kDEOh2e0IHO
Aku9uG3YVgOUAzVTE7PLYL31TAZeJjtY4RW9pdC+fMahACvThlsi4mYxJPpzhwrelTFf+RHbs6uC
1OtpZjwnhuiOj7arOwut4fcA90N8qqkyVdAyuXZRyagomkYm5vpFlI7Gqh/cMBCvEu6XZmK+mKKd
TFEAyhYtmrSt9aH49KNjLdYywaNESvDvBwgv96JcwUxgY6tpMcVvBsCFcgpU+UwNxJXALBIqUKYJ
IBMhdUgtPuJzoaq1dQylzdEpyM9dLBXN9rIfdng91sUEX2rlCh9OwftIBNo2qmNEkyZtgqfIqVAG
/ZdXMdGDSAxU4jrDXEk0S00T7OadsuE4F0U3vLN3x/jmHM9OOOfZwX1IKV+DtVq3prKZApkPPH4f
u0qFCTPb4QRTWd2EX+otNTE4oXTSiWXwYJ9z8Ok5Vrc9C4k2WFkuAM5nIUYwzhc1eSWOIF24ApRt
xNNEmt115PHnjMgS7AQCwLxe+iFMrEuN1dIpPqWrShSgoiHCUNN7vzRpeHkSmqUNNivOd9i8eHlS
PbrdRwbbJUvTRkwtv+gcFn73wwwKMZyZKlXkxXN8YAFIKqey2i+zhSYlfdzo9GKzgn19+rN6gkHp
bsnTudPsMYsfYZhtmi0lub6miUu3Q4DJs1Sl6oY2SIUAzUkNlh2/LQqs0aFI9tLkqfqKoSfmUV9z
fTfGuFruz98CgIaUT5IaWJFWBAuJpVte1eptv13Aq8qkWcL+Nkbv44o4oFSXBMktlLD9+WtHdc2Y
swB9TMhzFSKF034yVLAdMgcJzxIL2dJ6YZ9JF+f3uzQF4lqrU3bTH10P1g42iA/lqYMZvurAUcrK
/ypeq1cNJ/hX5OHb0kDZgLV+7NzR58KcJDmaECxy+MrMBDQAS0FPqwAw+CpEgTtGhQzVBlkHm/Hv
H17d6iUJ1692+bT7HAubF798ZaJwOACHxRFVGrXFfkcIMncQXHyJkeD+zuzKnfhEG8vv9CyJkML2
S/2nhMmM1sNR45b5Ac2Dpb7sni7NMArktuX6PEyWYuLei3M+LnonNCbhZ5s0pFIi1b/VcbzgCjfR
unk8Td/NZXw8x4EDtGEDeP62iz5mJTG7sr/3NBRyunZfgt26VchS+bNf5VoBTwJfK43LybgwDKYH
+tdn9YjmHiZgNHC1dBg18DtCjQVYfGGySD79F8le44nufR5BoqSXfSWujjSJeeKcpdnhTaVLKE9b
VjrwzlfUeME3n7SY7/eiPNMTW3/MLhqvwbju6PPy+ZUy18Nk6CXpVk7qERjrBpNnEJkqFFs253LS
WWVF0Xyyf6Q1uUfOpkwpHWAvyirRdxrxgdaF/EmcBO7qv8Ir/E4vvNaFZ1wIHyQPMWagG5xAsBPv
t6T/92OKdMbZOf9Ma7jiUbON0Jbze3+wzzDRDPmRgukP4To4rn6L6O9T7n50PqW5BYHXRQNRUqmx
qCyvAhaWrpa1RQlRVant3knZqlINpPkOW0F2GHv8xSoDSf/eHp/ZbtMoLfBmYlCu5JzMy8DhKhOM
36c/jPaDwCZCkOwFc/CXJWhQ+07wocIfMUk/3gWEPRaNBxfFvYreB25y+JBzELRAgXY9zX/sWtxo
wNop3xStzLgrp2vRx6ephlKNh+q65Nozw/vAZvRDvCNFBE1v9QvMKEyEL5JI/xQAyB7q1XOjAGu0
5R2CbOp6/Jt2iOd2joWLh5s2NXBB0k3amDjkSAoOPS7DJHF0Xxe3msNT/mgqUlzOu7gLHBTOvk/O
PLikHafgIDr9TgypB1bK8fHLpOmU1X2GugjGV0EFwbqKQrwhfoiR1Nx4t5mSZfUXM1DkbpZ+FhrI
P74qNPJDKvqXD9hchnkc3RsWR/DZkWVD8XbSCngtYGuWnT1XZomfNjeSXuEXr/MfBw6yIU1kYuVz
iuTdYzTNcFKgn/TTTpdeRYYqFF6lMK6sVR8tgroHkVDFyznbv9T+n2qtZRK5hwWh8p5Iye3KoX27
X5ft+ZS4LCmsAY5E1wUgNovMT1V6Gi5ggsCuzIJG1QmnmzdhJxr3k2FO5OcM6UJKpXY4dCoLUjPI
QJcwaiZ4c8DHr84zzlo5iMVvVgeTMwpcCd2dYUU8aRt48bnGampx04Try4wFUFUwvokiXMaAFG+x
gjQCC/CKlIJWr7xxsrOQP/gdUfOYApB4wzbDNokLqMSPtLZAa/Dcg9tJqHdqhi+eWEYMYDAmV2a3
7+cWFZTv0/miC3hPl39GEKTcWATR9pxmRj8SJo539qo01edrmSrLG8DbLAhcnkZk8CeApOl5cE76
mxLuzwsZm7lBO0dw0rHy2qbcbGHJPGTnGAJo0sL6+Tq8icob0/+i0kevL5ydhc5cr3XsHpVyLzlX
M0WyIO8Wqy67VbBjCBUnXUUdZkfB9XrfyAN6wZ7gFq8FaFwlDuMBwh5L0x7PYw15ufAjhpAEqSu3
7MzWKB+g0u90I+iT0WydTfdTBDVsTXKhlIjsnnl29FchonCOCWG6e+kvhV7tQxCBWXNCbhbn6Kul
RpjBu3OY6y+DuAdB4CxOzTcOSJG5q/Uaj/6qGLeuS9kxolSiHAoS8U32pumc/ie68cMvnlMl+6lu
FdtU9N12ZIBZyhcQzt0+8Yj90BlsIhUFWuIhD8AyK/ugJqZr/31yymgIxmUL9cYAK/vvWV+GcGGn
rELL9PTepbi3Gc5Oib39TO7Ur73PyDalGnYfr5ELoeSnZaHLUSdLR6ocpewtOmVodBCBvTjyxMO+
9ZAUn2OXVBLP0Z0111rEUE1HxzqSePpifO7jfx2GNd63wpwWp8rJxb+ae8bu5L4R2oDe+PnjhNbf
XtAXthO7ymTDni1lmd8warDXhTUuHVwjq32rSrfiE2A0FUfPzwsHiwGoLkmucpsEBf0VucrDsXBU
0v4OAYutUubiDmkWS/Onok8G3Dw7pK4/WqcAhaZXhIQ2xHWpur9daYJbyEL2khZsC0xtNP723YVq
yCTitELYvyYJ8ku77TfC/zoB6/gSPyIgBD3Z4gA5SGGwXPfVL0jl95Y7jEUPdO8mnFoygK3uIwMU
vDGlmllrNJ7qFM2CbU4QF4sCYt++lnbwru6gpacRYE+1wKiHVSPLb2Cyxq5BR2Wj3MK4dkS2qUDC
cxBLyyjIcJI9pR9WmEUtPaXOjlZ9EFAzMvmuIkbmECuACnro4i/NXba8ReurMsVr6SEkXoDYu7vB
jkHy65C5W61GS29N0iNuLd6/TDaZz57+K4kXZmqN482Xd9gnsaVr1R2RPM0GxbC5lMb2zm+E/kfO
GXQwMei69hLJrPLj0FePfGcRIhndL9RTQhV2c77NjT19Yq65ZBTewtVK687aeNIsto8oTpmqotmN
Bmy8F/9uJtgHnhapo71hhjRrmFTPwYlWLR31JU1MMl8vDo3998GVUo4I/rh8jk08d2FG0cw5l5/U
Z8ngLoyydnHZbFWMwZ/uMKpUFhAv4cIMc59uqljmx8lHKZiEIWIsM44ypBY5e3pagUmDdqIr1pn/
wejiC718tKRvSQ6yikkfI0P29zdUccIcboSg/+55h2W4+hFD2XWJ9WTS0cx7Sn2O37DL5J6u8qYT
L8F/oVw4l0CuMFGCuA/+TRQnT/ARnllBFRJRE4iyLOVCKhtJztq3RMNbhXxN7X/zm0VFQ/TkdoU3
YwQSKEAED6aDkX5mMaLq4G1AaYDEWSt32yCdzB0xQHhTs0rXDIFFbLh7xau3rSVwVmsydQqkoEil
ByxdpqHwuTAE18MeLo/qxf01xFDkAp/Xcn9tfI5+SFQd6QEWBDtnzk0wEkAz//2dFPbxtaccvU05
VWs9F86igBPPLy5OUEJ89c6ABaG53ltUm9iqNBe6rF3V4Q7cBBrH2zDOMfEwa/hdA8ORcO4n+D5j
GxvZ60b+4tFKkzQyOcBaw6YOy1oVlZ3eXKcjZQyl97II3J6ixDbQVyRNLdo6+7MrXt5GOq2Z8hYB
jpONuBCUv32/cTqb4wfW2zOXyJ3Yks8IxwqYHIJn1RPiEBCcSXHEp4uJDaWbjuX9e0FZf0vf1zm0
eB4GV59Nx0xMBVOMapjOE2lwHidNQ/QpvD+Da3PL0+tp0cvJypHXLeepFr9tmR1dJ0C9tF5qeznh
LYw5fgZcQhhPsO82eRRNoqHJCzVcLv4jzfVanPMS050D2cGta7Gogfqhur9C2OHK7yLXy4B3beQG
y1c6q/F2+oKpCF2CEb1RcVHqRkPJ9Hn6lpUpgsDJj7DYU3fdaEQHo+JgLLL1LNuO6WMEu2kWAOuv
Hm2z0pIIMOB8bxVEOzVUdmk1p+z+uuZenpDEqeAU0Hyv4MGrkjVBox3F+BOrs/QBDysyrUi4nUb5
RZ1IuxemO9utclxdGFwZmDMqrhVhiVFPPH3QqBXPBfOO4+NMuSrd9ikCyOqzobZ98TzAB+IHjkuv
0hMN/qCznd21j1ODmazl6Zc7p2tGCVetWRFKLRaJPWeZawkgJ89CJSD5UsWosVu9Ih36BBBVs7XE
fX52xJ55xM/DsaSLiwZBQkoQL/kWfV3cys/XI1v2T4DYCUF+PCnMHHJpyxazlQynb1IdGOWgSRUo
DquxvN6DVPY61aiSvZYJ+Dz+tlxiXtFgMHh0RvZUaUA64UIILgFIkLOD3x6V/o7vBoR3O2cOEJfL
Qwh8Sbj9EP3tLKyisrCVGTGn3mjmGq3+Mi5SabZRxe3PQezEIRSgh+70e+lXyzCvKsHAMOavcIbJ
JR4ocX/y1e4isYaGgD7iMszDU4se+jOCQyGJ+4g0QSdqwoby04D6YwukKwAQCRh4Vm0vz9zhRXoB
8Pqzo4J0VxbV8jcVSiqxGLKyjVbpiucJ3GDFaEmrzjHPdQAIrMlLvsIDFMiEAS5VdG3qUae5AbTG
hXHyOCsYysz/qMvP0OWDLdvKel4Jc9Flakcc9PmGxh4Uns1FLt7ZFPLWZhjSFikXkpJk7PGH8GN/
ZruBPcmBL5qwFVO4uFK4fmayFHuMQqDOyOsOkZFOf2bkZShrv3tBOFcjsvs3hV/wBYLffljJm3Sq
J1KAZD4tiHr9gKfw8eyG+mApQLpIt7Lyo4WwnpXxrxHwrXXyvIy8omx4nlZmNYgR86GbzqgVUHYK
ePGbOTUmNJMKXbtDRTSgTgODz0JGEBKlYfDdcfakQWEc2YmFJUrI0WaiiDPrFTVtoSGp1BbOxMDh
351D9mqP/nM87qvV92PGl3S4wwzFBkDBmBfVAgqcoUn68fqwtH8jRCBC6R5Ao6lsRQSEDPkyes/w
Ye8tNHcmGqtMyxZPgKeWRoFsNDwB/B5uiVsQvPFLnr+VAn9fl499dcoRualzYhuw7DDk5SnmDYaa
d+p1I3ETAauL91DcynnKWUc/4b20ItR49+wGq1VA8wDXHlEfXE3JiW+zv1k7evI2yLiXMwrKdboK
6Dq2PtAw+NkUAlPD4J+vl4E5M+32B/mQ5kXdndcgPdfzmY/TDztp810+Hf3867xO0aYyLcnl4X0p
qmuIsqsb3ywvFnbsv/ApZln2dLvrYWtfaWuewsf0METStaWwhAaYrcxZ/pbRmxNkzrppL9pB+Gct
fxhqOQFfWb6GpIZi2GbZWffN6zcGn9HRR3yDJn4XIug6IS2TKNb0UVWP2SEsBmNevUJYIfJgXisK
sGzqY/MeQk33Ii5n/eo40oH6Wmh+dmvhzCQfyIz99l7UUwlmf8skOuLXgzXg9XOGs2WCiAsLkqGP
yNwggUOOz6QrL87v89M5UE9HywieG0CqCoQWJr9TROZHwUTm/0ahBql/oRc/Kty7TzR8x21PgEjh
WBuDVTVlCTI5D8qy7byhnKdr1vzIjUmAtem0n7+gHVPnb2AxcmLch1JnWoBb2Gr4qz9cy3P7u5q1
8VQtZe4OvYujYKP0fMbze0rSwS2LlMp9on3Na6yOE4pr8C4/y1YStzXSsNaMNhonkYR5KacAN7BA
17QoqJ8DIH4INbtmNIBh09Uce2eHBf+wIfDUNEONfNud4ggRVGObi41EvM47xIa8K2LMofcIXbnN
yJ8QjNXozuh/EsedpsmEbh1FW4WiZKkGwyXjqGoHE3XLMBVu4+LcEGPqlXqGHI2QdUENm4iz2kqu
sgu4CnuXmirZ/xrYu0a4XZh5KRkjM7q6fHAysVTCr6h+qJM8eJuTQWqswhIEbqQemEcFN2IUe4RO
2Lqs+pxfM5nJzTPJJQ3bPRDf02/iiBQJIeB856sYte3FvRmMhYUnW1W9Yg7wSzCxsxpMuxdaIQal
ylXtHO8lblyPiefd+kz3zPiHwSmGw63xT/BHpgDy2HSWAI/+6haTXllbx0+MmCQIjoOuJsHnKk/8
ziWe9UtaAExxuYekW/jc+YGNQHa8+MuQfXNe6rwx1DkfaDcQxey8gj/KM9awwjBD+MGBHTLt++d8
mLW68OjdiOGq+0mQ8OMH5KT56wiaCLd7cWxsD7pCUhNtU9mzJkb1IirirvmjH7GiUx/Bdy5hQD/Y
tR1Mx3z0NsJEwzhoKu8zgcSsngxNEOrH0QBao5dqNKgXByOzh8HmVz4FE3HH/VjFkQrjErvDjMzq
nrearcXFqvLP5jqsGU0w1H/t7knb0TPZ8GOcjGThW1MmWKDcrRjXyU+xdh1mGwK0UT2HRNm0QMAk
XMnm3Qi6+jTq0lGY+BeCK5LxiSjiDR2hDfxODYg+S1SWmkPcgkw3jMS+YOTrNpGTLO7+6U/tSdFF
svtsUIXGDKg98ugF/bwq0M2zwvxNukyXVHrOKr/4hcFArqiUaiiJRBrkl4AZUTzYa8+QpAea7oh8
1RQG8SLPe3mylut3E42KZhPO4yER3nJcB/tGb988AnBhMN7kgLjw/FrjWJIQEW5yvQSaAEPhXVWi
itUUP5POU9eCEA2CuhiYwLHfXMNH+uY+rAuFDVfIso/oPjoI9XuC5zk2v4RBqD8ifHD60k6eubdy
kiKAmJU7VpWGByIV5D9E8g7YoGHLpHIr/H/s3QnkzcOIdsrww/keUbICzl9hiLQUDH6kggUYxQ4I
F9S2+RG+RvK8vY1+8YKbXgND/wTibVcgzzwUNQwpLR8XPs+35vxvnvMzexi3DaAnmLjSZBEuihjV
mLH0WKX8s6s/3aqiei89/51XlFD1vZxomBTYQF7cIe8f5sIolg==
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
