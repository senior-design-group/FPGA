// Copyright 1986-2022 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2022.2 (win64) Build 3671981 Fri Oct 14 05:00:03 MDT 2022
// Date        : Sun Oct 15 16:06:51 2023
// Host        : DESKTOP-QRFTT4G running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim
//               d:/FPGA/DAQSeniorDesVivadoProj/DAQSeniorDes.gen/sources_1/bd/design_1/ip/design_1_Max_rst_Counters_0_0/design_1_Max_rst_Counters_0_0_sim_netlist.v
// Design      : design_1_Max_rst_Counters_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a100tftg256-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_1_Max_rst_Counters_0_0,Max_rst_Counters,{}" *) (* DowngradeIPIdentifiedWarnings = "yes" *) (* IP_DEFINITION_SOURCE = "module_ref" *) 
(* X_CORE_INFO = "Max_rst_Counters,Vivado 2022.2" *) 
(* NotValidForBitStream *)
module design_1_Max_rst_Counters_0_0
   (Max_Val,
    Load_Val,
    Curr_Val,
    clk,
    Reset_Count);
  input [8:0]Max_Val;
  input Load_Val;
  input [8:0]Curr_Val;
  (* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 clk CLK" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME clk, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, PHASE 0.0, CLK_DOMAIN /clk_wiz_1_clk_out1, INSERT_VIP 0" *) input clk;
  (* X_INTERFACE_INFO = "xilinx.com:signal:reset:1.0 Reset_Count RST" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME Reset_Count, POLARITY ACTIVE_LOW, INSERT_VIP 0" *) output Reset_Count;

  wire [8:0]Curr_Val;
  wire Load_Val;
  wire [8:0]Max_Val;
  wire Reset_Count;
  wire clk;

  design_1_Max_rst_Counters_0_0_Max_rst_Counters inst
       (.Curr_Val(Curr_Val),
        .Load_Val(Load_Val),
        .Max_Val(Max_Val),
        .Reset_Count(Reset_Count),
        .clk(clk));
endmodule

(* ORIG_REF_NAME = "Max_rst_Counters" *) 
module design_1_Max_rst_Counters_0_0_Max_rst_Counters
   (Reset_Count,
    Curr_Val,
    Max_Val,
    Load_Val,
    clk);
  output Reset_Count;
  input [8:0]Curr_Val;
  input [8:0]Max_Val;
  input Load_Val;
  input clk;

  wire [8:0]Curr_Val;
  wire Load_Val;
  wire [8:0]Max_Val;
  wire Reset_Count;
  wire Reset_Count0_carry__0_i_1_n_0;
  wire Reset_Count0_carry__0_i_2_n_0;
  wire Reset_Count0_carry_i_10_n_0;
  wire Reset_Count0_carry_i_1_n_0;
  wire Reset_Count0_carry_i_2_n_0;
  wire Reset_Count0_carry_i_3_n_0;
  wire Reset_Count0_carry_i_4_n_0;
  wire Reset_Count0_carry_i_5_n_0;
  wire Reset_Count0_carry_i_6_n_0;
  wire Reset_Count0_carry_i_7_n_0;
  wire Reset_Count0_carry_i_8_n_0;
  wire Reset_Count0_carry_i_9_n_0;
  wire Reset_Count0_carry_n_0;
  wire Reset_Count0_carry_n_1;
  wire Reset_Count0_carry_n_2;
  wire Reset_Count0_carry_n_3;
  wire [8:0]Reset_Val;
  wire clk;
  wire p_0_in;
  wire [3:0]NLW_Reset_Count0_carry_O_UNCONNECTED;
  wire [3:1]NLW_Reset_Count0_carry__0_CO_UNCONNECTED;
  wire [3:0]NLW_Reset_Count0_carry__0_O_UNCONNECTED;

  (* COMPARATOR_THRESHOLD = "11" *) 
  CARRY4 Reset_Count0_carry
       (.CI(1'b0),
        .CO({Reset_Count0_carry_n_0,Reset_Count0_carry_n_1,Reset_Count0_carry_n_2,Reset_Count0_carry_n_3}),
        .CYINIT(1'b1),
        .DI({Reset_Count0_carry_i_1_n_0,Reset_Count0_carry_i_2_n_0,Reset_Count0_carry_i_3_n_0,Reset_Count0_carry_i_4_n_0}),
        .O(NLW_Reset_Count0_carry_O_UNCONNECTED[3:0]),
        .S({Reset_Count0_carry_i_5_n_0,Reset_Count0_carry_i_6_n_0,Reset_Count0_carry_i_7_n_0,Reset_Count0_carry_i_8_n_0}));
  (* COMPARATOR_THRESHOLD = "11" *) 
  CARRY4 Reset_Count0_carry__0
       (.CI(Reset_Count0_carry_n_0),
        .CO({NLW_Reset_Count0_carry__0_CO_UNCONNECTED[3:1],p_0_in}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,Reset_Count0_carry__0_i_1_n_0}),
        .O(NLW_Reset_Count0_carry__0_O_UNCONNECTED[3:0]),
        .S({1'b0,1'b0,1'b0,Reset_Count0_carry__0_i_2_n_0}));
  LUT5 #(
    .INIT(32'h55560000)) 
    Reset_Count0_carry__0_i_1
       (.I0(Reset_Val[8]),
        .I1(Reset_Val[6]),
        .I2(Reset_Count0_carry_i_9_n_0),
        .I3(Reset_Val[7]),
        .I4(Curr_Val[8]),
        .O(Reset_Count0_carry__0_i_1_n_0));
  LUT5 #(
    .INIT(32'hAAA95556)) 
    Reset_Count0_carry__0_i_2
       (.I0(Reset_Val[8]),
        .I1(Reset_Val[6]),
        .I2(Reset_Count0_carry_i_9_n_0),
        .I3(Reset_Val[7]),
        .I4(Curr_Val[8]),
        .O(Reset_Count0_carry__0_i_2_n_0));
  LUT5 #(
    .INIT(32'h088CCEE0)) 
    Reset_Count0_carry_i_1
       (.I0(Curr_Val[6]),
        .I1(Curr_Val[7]),
        .I2(Reset_Val[6]),
        .I3(Reset_Count0_carry_i_9_n_0),
        .I4(Reset_Val[7]),
        .O(Reset_Count0_carry_i_1_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    Reset_Count0_carry_i_10
       (.I0(Reset_Val[2]),
        .I1(Reset_Val[0]),
        .I2(Reset_Val[1]),
        .I3(Reset_Val[3]),
        .O(Reset_Count0_carry_i_10_n_0));
  LUT5 #(
    .INIT(32'h088CCEE0)) 
    Reset_Count0_carry_i_2
       (.I0(Curr_Val[4]),
        .I1(Curr_Val[5]),
        .I2(Reset_Val[4]),
        .I3(Reset_Count0_carry_i_10_n_0),
        .I4(Reset_Val[5]),
        .O(Reset_Count0_carry_i_2_n_0));
  LUT6 #(
    .INIT(64'h0808088CCECECEE0)) 
    Reset_Count0_carry_i_3
       (.I0(Curr_Val[2]),
        .I1(Curr_Val[3]),
        .I2(Reset_Val[2]),
        .I3(Reset_Val[0]),
        .I4(Reset_Val[1]),
        .I5(Reset_Val[3]),
        .O(Reset_Count0_carry_i_3_n_0));
  LUT4 #(
    .INIT(16'h8CE0)) 
    Reset_Count0_carry_i_4
       (.I0(Curr_Val[0]),
        .I1(Curr_Val[1]),
        .I2(Reset_Val[0]),
        .I3(Reset_Val[1]),
        .O(Reset_Count0_carry_i_4_n_0));
  LUT5 #(
    .INIT(32'h84422118)) 
    Reset_Count0_carry_i_5
       (.I0(Curr_Val[6]),
        .I1(Curr_Val[7]),
        .I2(Reset_Val[6]),
        .I3(Reset_Count0_carry_i_9_n_0),
        .I4(Reset_Val[7]),
        .O(Reset_Count0_carry_i_5_n_0));
  LUT5 #(
    .INIT(32'h84422118)) 
    Reset_Count0_carry_i_6
       (.I0(Curr_Val[4]),
        .I1(Curr_Val[5]),
        .I2(Reset_Val[4]),
        .I3(Reset_Count0_carry_i_10_n_0),
        .I4(Reset_Val[5]),
        .O(Reset_Count0_carry_i_6_n_0));
  LUT6 #(
    .INIT(64'h8484844221212118)) 
    Reset_Count0_carry_i_7
       (.I0(Curr_Val[2]),
        .I1(Curr_Val[3]),
        .I2(Reset_Val[2]),
        .I3(Reset_Val[0]),
        .I4(Reset_Val[1]),
        .I5(Reset_Val[3]),
        .O(Reset_Count0_carry_i_7_n_0));
  LUT4 #(
    .INIT(16'h4218)) 
    Reset_Count0_carry_i_8
       (.I0(Curr_Val[0]),
        .I1(Curr_Val[1]),
        .I2(Reset_Val[0]),
        .I3(Reset_Val[1]),
        .O(Reset_Count0_carry_i_8_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    Reset_Count0_carry_i_9
       (.I0(Reset_Val[4]),
        .I1(Reset_Val[2]),
        .I2(Reset_Val[0]),
        .I3(Reset_Val[1]),
        .I4(Reset_Val[3]),
        .I5(Reset_Val[5]),
        .O(Reset_Count0_carry_i_9_n_0));
  FDRE #(
    .INIT(1'b1),
    .IS_C_INVERTED(1'b1)) 
    Reset_Count_reg
       (.C(clk),
        .CE(1'b1),
        .D(p_0_in),
        .Q(Reset_Count),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \Reset_Val_reg[0] 
       (.C(Load_Val),
        .CE(1'b1),
        .D(Max_Val[0]),
        .Q(Reset_Val[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \Reset_Val_reg[1] 
       (.C(Load_Val),
        .CE(1'b1),
        .D(Max_Val[1]),
        .Q(Reset_Val[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \Reset_Val_reg[2] 
       (.C(Load_Val),
        .CE(1'b1),
        .D(Max_Val[2]),
        .Q(Reset_Val[2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \Reset_Val_reg[3] 
       (.C(Load_Val),
        .CE(1'b1),
        .D(Max_Val[3]),
        .Q(Reset_Val[3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \Reset_Val_reg[4] 
       (.C(Load_Val),
        .CE(1'b1),
        .D(Max_Val[4]),
        .Q(Reset_Val[4]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \Reset_Val_reg[5] 
       (.C(Load_Val),
        .CE(1'b1),
        .D(Max_Val[5]),
        .Q(Reset_Val[5]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \Reset_Val_reg[6] 
       (.C(Load_Val),
        .CE(1'b1),
        .D(Max_Val[6]),
        .Q(Reset_Val[6]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \Reset_Val_reg[7] 
       (.C(Load_Val),
        .CE(1'b1),
        .D(Max_Val[7]),
        .Q(Reset_Val[7]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \Reset_Val_reg[8] 
       (.C(Load_Val),
        .CE(1'b1),
        .D(Max_Val[8]),
        .Q(Reset_Val[8]),
        .R(1'b0));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
