-- Copyright 1986-2022 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2022.2 (win64) Build 3671981 Fri Oct 14 05:00:03 MDT 2022
-- Date        : Sat Oct 14 13:18:00 2023
-- Host        : DESKTOP-QRFTT4G running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode funcsim
--               d:/FPGA/DAQSeniorDesVivadoProj/DAQSeniorDes.gen/sources_1/bd/design_1/ip/design_1_RAM_INTERFACE1_0_0/design_1_RAM_INTERFACE1_0_0_sim_netlist.vhdl
-- Design      : design_1_RAM_INTERFACE1_0_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7a100tftg256-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_RAM_INTERFACE1_0_0_RAM_INTERFACE1 is
  port (
    ADDRESS_OUT : out STD_LOGIC_VECTOR ( 8 downto 0 );
    DATA_IN : out STD_LOGIC_VECTOR ( 13 downto 0 );
    RW : out STD_LOGIC;
    CLK_IN : in STD_LOGIC;
    RW_EN : in STD_LOGIC;
    WRITE_ADDRESS_DATA : in STD_LOGIC_VECTOR ( 22 downto 0 );
    READ_ADDRESS : in STD_LOGIC_VECTOR ( 8 downto 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_1_RAM_INTERFACE1_0_0_RAM_INTERFACE1 : entity is "RAM_INTERFACE1";
end design_1_RAM_INTERFACE1_0_0_RAM_INTERFACE1;

architecture STRUCTURE of design_1_RAM_INTERFACE1_0_0_RAM_INTERFACE1 is
  signal RW_i_1_n_0 : STD_LOGIC;
  signal p_0_in : STD_LOGIC_VECTOR ( 8 downto 0 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \ADDRESS_OUT[0]_i_1\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \ADDRESS_OUT[1]_i_1\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \ADDRESS_OUT[2]_i_1\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \ADDRESS_OUT[3]_i_1\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \ADDRESS_OUT[4]_i_1\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \ADDRESS_OUT[5]_i_1\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \ADDRESS_OUT[6]_i_1\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \ADDRESS_OUT[7]_i_1\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \ADDRESS_OUT[8]_i_1\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of RW_i_1 : label is "soft_lutpair4";
begin
\ADDRESS_OUT[0]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => WRITE_ADDRESS_DATA(14),
      I1 => RW_EN,
      I2 => READ_ADDRESS(0),
      O => p_0_in(0)
    );
\ADDRESS_OUT[1]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => WRITE_ADDRESS_DATA(15),
      I1 => RW_EN,
      I2 => READ_ADDRESS(1),
      O => p_0_in(1)
    );
\ADDRESS_OUT[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => WRITE_ADDRESS_DATA(16),
      I1 => RW_EN,
      I2 => READ_ADDRESS(2),
      O => p_0_in(2)
    );
\ADDRESS_OUT[3]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => WRITE_ADDRESS_DATA(17),
      I1 => RW_EN,
      I2 => READ_ADDRESS(3),
      O => p_0_in(3)
    );
\ADDRESS_OUT[4]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => WRITE_ADDRESS_DATA(18),
      I1 => RW_EN,
      I2 => READ_ADDRESS(4),
      O => p_0_in(4)
    );
\ADDRESS_OUT[5]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => WRITE_ADDRESS_DATA(19),
      I1 => RW_EN,
      I2 => READ_ADDRESS(5),
      O => p_0_in(5)
    );
\ADDRESS_OUT[6]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => WRITE_ADDRESS_DATA(20),
      I1 => RW_EN,
      I2 => READ_ADDRESS(6),
      O => p_0_in(6)
    );
\ADDRESS_OUT[7]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => WRITE_ADDRESS_DATA(21),
      I1 => RW_EN,
      I2 => READ_ADDRESS(7),
      O => p_0_in(7)
    );
\ADDRESS_OUT[8]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => WRITE_ADDRESS_DATA(22),
      I1 => RW_EN,
      I2 => READ_ADDRESS(8),
      O => p_0_in(8)
    );
\ADDRESS_OUT_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => CLK_IN,
      CE => '1',
      D => p_0_in(0),
      Q => ADDRESS_OUT(0),
      R => '0'
    );
\ADDRESS_OUT_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => CLK_IN,
      CE => '1',
      D => p_0_in(1),
      Q => ADDRESS_OUT(1),
      R => '0'
    );
\ADDRESS_OUT_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => CLK_IN,
      CE => '1',
      D => p_0_in(2),
      Q => ADDRESS_OUT(2),
      R => '0'
    );
\ADDRESS_OUT_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => CLK_IN,
      CE => '1',
      D => p_0_in(3),
      Q => ADDRESS_OUT(3),
      R => '0'
    );
\ADDRESS_OUT_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => CLK_IN,
      CE => '1',
      D => p_0_in(4),
      Q => ADDRESS_OUT(4),
      R => '0'
    );
\ADDRESS_OUT_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => CLK_IN,
      CE => '1',
      D => p_0_in(5),
      Q => ADDRESS_OUT(5),
      R => '0'
    );
\ADDRESS_OUT_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => CLK_IN,
      CE => '1',
      D => p_0_in(6),
      Q => ADDRESS_OUT(6),
      R => '0'
    );
\ADDRESS_OUT_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => CLK_IN,
      CE => '1',
      D => p_0_in(7),
      Q => ADDRESS_OUT(7),
      R => '0'
    );
\ADDRESS_OUT_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => CLK_IN,
      CE => '1',
      D => p_0_in(8),
      Q => ADDRESS_OUT(8),
      R => '0'
    );
\DATA_IN_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => CLK_IN,
      CE => RW_EN,
      D => WRITE_ADDRESS_DATA(0),
      Q => DATA_IN(0),
      R => '0'
    );
\DATA_IN_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => CLK_IN,
      CE => RW_EN,
      D => WRITE_ADDRESS_DATA(10),
      Q => DATA_IN(10),
      R => '0'
    );
\DATA_IN_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => CLK_IN,
      CE => RW_EN,
      D => WRITE_ADDRESS_DATA(11),
      Q => DATA_IN(11),
      R => '0'
    );
\DATA_IN_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => CLK_IN,
      CE => RW_EN,
      D => WRITE_ADDRESS_DATA(12),
      Q => DATA_IN(12),
      R => '0'
    );
\DATA_IN_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => CLK_IN,
      CE => RW_EN,
      D => WRITE_ADDRESS_DATA(13),
      Q => DATA_IN(13),
      R => '0'
    );
\DATA_IN_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => CLK_IN,
      CE => RW_EN,
      D => WRITE_ADDRESS_DATA(1),
      Q => DATA_IN(1),
      R => '0'
    );
\DATA_IN_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => CLK_IN,
      CE => RW_EN,
      D => WRITE_ADDRESS_DATA(2),
      Q => DATA_IN(2),
      R => '0'
    );
\DATA_IN_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => CLK_IN,
      CE => RW_EN,
      D => WRITE_ADDRESS_DATA(3),
      Q => DATA_IN(3),
      R => '0'
    );
\DATA_IN_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => CLK_IN,
      CE => RW_EN,
      D => WRITE_ADDRESS_DATA(4),
      Q => DATA_IN(4),
      R => '0'
    );
\DATA_IN_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => CLK_IN,
      CE => RW_EN,
      D => WRITE_ADDRESS_DATA(5),
      Q => DATA_IN(5),
      R => '0'
    );
\DATA_IN_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => CLK_IN,
      CE => RW_EN,
      D => WRITE_ADDRESS_DATA(6),
      Q => DATA_IN(6),
      R => '0'
    );
\DATA_IN_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => CLK_IN,
      CE => RW_EN,
      D => WRITE_ADDRESS_DATA(7),
      Q => DATA_IN(7),
      R => '0'
    );
\DATA_IN_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => CLK_IN,
      CE => RW_EN,
      D => WRITE_ADDRESS_DATA(8),
      Q => DATA_IN(8),
      R => '0'
    );
\DATA_IN_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => CLK_IN,
      CE => RW_EN,
      D => WRITE_ADDRESS_DATA(9),
      Q => DATA_IN(9),
      R => '0'
    );
RW_i_1: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => RW_EN,
      O => RW_i_1_n_0
    );
RW_reg: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => CLK_IN,
      CE => '1',
      D => RW_i_1_n_0,
      Q => RW,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_RAM_INTERFACE1_0_0 is
  port (
    READ_ADDRESS : in STD_LOGIC_VECTOR ( 8 downto 0 );
    WRITE_ADDRESS_DATA : in STD_LOGIC_VECTOR ( 22 downto 0 );
    CLK_IN : in STD_LOGIC;
    RW_EN : in STD_LOGIC;
    ADDRESS_OUT : out STD_LOGIC_VECTOR ( 8 downto 0 );
    DATA_IN : out STD_LOGIC_VECTOR ( 13 downto 0 );
    RW : out STD_LOGIC
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of design_1_RAM_INTERFACE1_0_0 : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of design_1_RAM_INTERFACE1_0_0 : entity is "design_1_RAM_INTERFACE1_0_0,RAM_INTERFACE1,{}";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of design_1_RAM_INTERFACE1_0_0 : entity is "yes";
  attribute IP_DEFINITION_SOURCE : string;
  attribute IP_DEFINITION_SOURCE of design_1_RAM_INTERFACE1_0_0 : entity is "module_ref";
  attribute X_CORE_INFO : string;
  attribute X_CORE_INFO of design_1_RAM_INTERFACE1_0_0 : entity is "RAM_INTERFACE1,Vivado 2022.2";
end design_1_RAM_INTERFACE1_0_0;

architecture STRUCTURE of design_1_RAM_INTERFACE1_0_0 is
begin
inst: entity work.design_1_RAM_INTERFACE1_0_0_RAM_INTERFACE1
     port map (
      ADDRESS_OUT(8 downto 0) => ADDRESS_OUT(8 downto 0),
      CLK_IN => CLK_IN,
      DATA_IN(13 downto 0) => DATA_IN(13 downto 0),
      READ_ADDRESS(8 downto 0) => READ_ADDRESS(8 downto 0),
      RW => RW,
      RW_EN => RW_EN,
      WRITE_ADDRESS_DATA(22 downto 0) => WRITE_ADDRESS_DATA(22 downto 0)
    );
end STRUCTURE;
