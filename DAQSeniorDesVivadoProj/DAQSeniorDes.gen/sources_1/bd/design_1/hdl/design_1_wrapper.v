//Copyright 1986-2022 Xilinx, Inc. All Rights Reserved.
//--------------------------------------------------------------------------------
//Tool Version: Vivado v.2022.2 (win64) Build 3671981 Fri Oct 14 05:00:03 MDT 2022
//Date        : Fri Oct 20 12:50:21 2023
//Host        : DESKTOP-QRFTT4G running 64-bit major release  (build 9200)
//Command     : generate_target design_1_wrapper.bd
//Design      : design_1_wrapper
//Purpose     : IP block netlist
//--------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module design_1_wrapper
   (ADC_CLKOUT,
    ADC_CLK_A,
    ADC_CLK_B,
    BE_tri_io,
    CTRL_tri_o,
    DAC_A,
    DAC_B,
    DAC_CLK1,
    DAC_CLK2,
    DAC_WRT1,
    DAC_WRT2,
    FTDI_D_tri_io,
    RX_TX_tri_i,
    clk,
    led_tri_o,
    rst_n,
    rx_0,
    spi_io0_io,
    spi_io1_io,
    spi_sck_io,
    spi_ss_io,
    tx_0);
  output ADC_CLKOUT;
  output ADC_CLK_A;
  output ADC_CLK_B;
  inout [1:0]BE_tri_io;
  output [2:0]CTRL_tri_o;
  output [13:0]DAC_A;
  output [13:0]DAC_B;
  output DAC_CLK1;
  output DAC_CLK2;
  output DAC_WRT1;
  output DAC_WRT2;
  inout [15:0]FTDI_D_tri_io;
  input [1:0]RX_TX_tri_i;
  input clk;
  output [7:0]led_tri_o;
  input rst_n;
  input rx_0;
  inout spi_io0_io;
  inout spi_io1_io;
  inout spi_sck_io;
  inout [2:0]spi_ss_io;
  output tx_0;

  wire ADC_CLKOUT;
  wire ADC_CLK_A;
  wire ADC_CLK_B;
  wire [0:0]BE_tri_i_0;
  wire [1:1]BE_tri_i_1;
  wire [0:0]BE_tri_io_0;
  wire [1:1]BE_tri_io_1;
  wire [0:0]BE_tri_o_0;
  wire [1:1]BE_tri_o_1;
  wire [0:0]BE_tri_t_0;
  wire [1:1]BE_tri_t_1;
  wire [2:0]CTRL_tri_o;
  wire [13:0]DAC_A;
  wire [13:0]DAC_B;
  wire DAC_CLK1;
  wire DAC_CLK2;
  wire DAC_WRT1;
  wire DAC_WRT2;
  wire [0:0]FTDI_D_tri_i_0;
  wire [1:1]FTDI_D_tri_i_1;
  wire [10:10]FTDI_D_tri_i_10;
  wire [11:11]FTDI_D_tri_i_11;
  wire [12:12]FTDI_D_tri_i_12;
  wire [13:13]FTDI_D_tri_i_13;
  wire [14:14]FTDI_D_tri_i_14;
  wire [15:15]FTDI_D_tri_i_15;
  wire [2:2]FTDI_D_tri_i_2;
  wire [3:3]FTDI_D_tri_i_3;
  wire [4:4]FTDI_D_tri_i_4;
  wire [5:5]FTDI_D_tri_i_5;
  wire [6:6]FTDI_D_tri_i_6;
  wire [7:7]FTDI_D_tri_i_7;
  wire [8:8]FTDI_D_tri_i_8;
  wire [9:9]FTDI_D_tri_i_9;
  wire [0:0]FTDI_D_tri_io_0;
  wire [1:1]FTDI_D_tri_io_1;
  wire [10:10]FTDI_D_tri_io_10;
  wire [11:11]FTDI_D_tri_io_11;
  wire [12:12]FTDI_D_tri_io_12;
  wire [13:13]FTDI_D_tri_io_13;
  wire [14:14]FTDI_D_tri_io_14;
  wire [15:15]FTDI_D_tri_io_15;
  wire [2:2]FTDI_D_tri_io_2;
  wire [3:3]FTDI_D_tri_io_3;
  wire [4:4]FTDI_D_tri_io_4;
  wire [5:5]FTDI_D_tri_io_5;
  wire [6:6]FTDI_D_tri_io_6;
  wire [7:7]FTDI_D_tri_io_7;
  wire [8:8]FTDI_D_tri_io_8;
  wire [9:9]FTDI_D_tri_io_9;
  wire [0:0]FTDI_D_tri_o_0;
  wire [1:1]FTDI_D_tri_o_1;
  wire [10:10]FTDI_D_tri_o_10;
  wire [11:11]FTDI_D_tri_o_11;
  wire [12:12]FTDI_D_tri_o_12;
  wire [13:13]FTDI_D_tri_o_13;
  wire [14:14]FTDI_D_tri_o_14;
  wire [15:15]FTDI_D_tri_o_15;
  wire [2:2]FTDI_D_tri_o_2;
  wire [3:3]FTDI_D_tri_o_3;
  wire [4:4]FTDI_D_tri_o_4;
  wire [5:5]FTDI_D_tri_o_5;
  wire [6:6]FTDI_D_tri_o_6;
  wire [7:7]FTDI_D_tri_o_7;
  wire [8:8]FTDI_D_tri_o_8;
  wire [9:9]FTDI_D_tri_o_9;
  wire [0:0]FTDI_D_tri_t_0;
  wire [1:1]FTDI_D_tri_t_1;
  wire [10:10]FTDI_D_tri_t_10;
  wire [11:11]FTDI_D_tri_t_11;
  wire [12:12]FTDI_D_tri_t_12;
  wire [13:13]FTDI_D_tri_t_13;
  wire [14:14]FTDI_D_tri_t_14;
  wire [15:15]FTDI_D_tri_t_15;
  wire [2:2]FTDI_D_tri_t_2;
  wire [3:3]FTDI_D_tri_t_3;
  wire [4:4]FTDI_D_tri_t_4;
  wire [5:5]FTDI_D_tri_t_5;
  wire [6:6]FTDI_D_tri_t_6;
  wire [7:7]FTDI_D_tri_t_7;
  wire [8:8]FTDI_D_tri_t_8;
  wire [9:9]FTDI_D_tri_t_9;
  wire [1:0]RX_TX_tri_i;
  wire clk;
  wire [7:0]led_tri_o;
  wire rst_n;
  wire rx_0;
  wire spi_io0_i;
  wire spi_io0_io;
  wire spi_io0_o;
  wire spi_io0_t;
  wire spi_io1_i;
  wire spi_io1_io;
  wire spi_io1_o;
  wire spi_io1_t;
  wire spi_sck_i;
  wire spi_sck_io;
  wire spi_sck_o;
  wire spi_sck_t;
  wire [0:0]spi_ss_i_0;
  wire [1:1]spi_ss_i_1;
  wire [2:2]spi_ss_i_2;
  wire [0:0]spi_ss_io_0;
  wire [1:1]spi_ss_io_1;
  wire [2:2]spi_ss_io_2;
  wire [0:0]spi_ss_o_0;
  wire [1:1]spi_ss_o_1;
  wire [2:2]spi_ss_o_2;
  wire spi_ss_t;
  wire tx_0;

  IOBUF BE_tri_iobuf_0
       (.I(BE_tri_o_0),
        .IO(BE_tri_io[0]),
        .O(BE_tri_i_0),
        .T(BE_tri_t_0));
  IOBUF BE_tri_iobuf_1
       (.I(BE_tri_o_1),
        .IO(BE_tri_io[1]),
        .O(BE_tri_i_1),
        .T(BE_tri_t_1));
  IOBUF FTDI_D_tri_iobuf_0
       (.I(FTDI_D_tri_o_0),
        .IO(FTDI_D_tri_io[0]),
        .O(FTDI_D_tri_i_0),
        .T(FTDI_D_tri_t_0));
  IOBUF FTDI_D_tri_iobuf_1
       (.I(FTDI_D_tri_o_1),
        .IO(FTDI_D_tri_io[1]),
        .O(FTDI_D_tri_i_1),
        .T(FTDI_D_tri_t_1));
  IOBUF FTDI_D_tri_iobuf_10
       (.I(FTDI_D_tri_o_10),
        .IO(FTDI_D_tri_io[10]),
        .O(FTDI_D_tri_i_10),
        .T(FTDI_D_tri_t_10));
  IOBUF FTDI_D_tri_iobuf_11
       (.I(FTDI_D_tri_o_11),
        .IO(FTDI_D_tri_io[11]),
        .O(FTDI_D_tri_i_11),
        .T(FTDI_D_tri_t_11));
  IOBUF FTDI_D_tri_iobuf_12
       (.I(FTDI_D_tri_o_12),
        .IO(FTDI_D_tri_io[12]),
        .O(FTDI_D_tri_i_12),
        .T(FTDI_D_tri_t_12));
  IOBUF FTDI_D_tri_iobuf_13
       (.I(FTDI_D_tri_o_13),
        .IO(FTDI_D_tri_io[13]),
        .O(FTDI_D_tri_i_13),
        .T(FTDI_D_tri_t_13));
  IOBUF FTDI_D_tri_iobuf_14
       (.I(FTDI_D_tri_o_14),
        .IO(FTDI_D_tri_io[14]),
        .O(FTDI_D_tri_i_14),
        .T(FTDI_D_tri_t_14));
  IOBUF FTDI_D_tri_iobuf_15
       (.I(FTDI_D_tri_o_15),
        .IO(FTDI_D_tri_io[15]),
        .O(FTDI_D_tri_i_15),
        .T(FTDI_D_tri_t_15));
  IOBUF FTDI_D_tri_iobuf_2
       (.I(FTDI_D_tri_o_2),
        .IO(FTDI_D_tri_io[2]),
        .O(FTDI_D_tri_i_2),
        .T(FTDI_D_tri_t_2));
  IOBUF FTDI_D_tri_iobuf_3
       (.I(FTDI_D_tri_o_3),
        .IO(FTDI_D_tri_io[3]),
        .O(FTDI_D_tri_i_3),
        .T(FTDI_D_tri_t_3));
  IOBUF FTDI_D_tri_iobuf_4
       (.I(FTDI_D_tri_o_4),
        .IO(FTDI_D_tri_io[4]),
        .O(FTDI_D_tri_i_4),
        .T(FTDI_D_tri_t_4));
  IOBUF FTDI_D_tri_iobuf_5
       (.I(FTDI_D_tri_o_5),
        .IO(FTDI_D_tri_io[5]),
        .O(FTDI_D_tri_i_5),
        .T(FTDI_D_tri_t_5));
  IOBUF FTDI_D_tri_iobuf_6
       (.I(FTDI_D_tri_o_6),
        .IO(FTDI_D_tri_io[6]),
        .O(FTDI_D_tri_i_6),
        .T(FTDI_D_tri_t_6));
  IOBUF FTDI_D_tri_iobuf_7
       (.I(FTDI_D_tri_o_7),
        .IO(FTDI_D_tri_io[7]),
        .O(FTDI_D_tri_i_7),
        .T(FTDI_D_tri_t_7));
  IOBUF FTDI_D_tri_iobuf_8
       (.I(FTDI_D_tri_o_8),
        .IO(FTDI_D_tri_io[8]),
        .O(FTDI_D_tri_i_8),
        .T(FTDI_D_tri_t_8));
  IOBUF FTDI_D_tri_iobuf_9
       (.I(FTDI_D_tri_o_9),
        .IO(FTDI_D_tri_io[9]),
        .O(FTDI_D_tri_i_9),
        .T(FTDI_D_tri_t_9));
  design_1 design_1_i
       (.ADC_CLKOUT(ADC_CLKOUT),
        .ADC_CLK_A(ADC_CLK_A),
        .ADC_CLK_B(ADC_CLK_B),
        .BE_tri_i({BE_tri_i_1,BE_tri_i_0}),
        .BE_tri_o({BE_tri_o_1,BE_tri_o_0}),
        .BE_tri_t({BE_tri_t_1,BE_tri_t_0}),
        .CTRL_tri_o(CTRL_tri_o),
        .DAC_A(DAC_A),
        .DAC_B(DAC_B),
        .DAC_CLK1(DAC_CLK1),
        .DAC_CLK2(DAC_CLK2),
        .DAC_WRT1(DAC_WRT1),
        .DAC_WRT2(DAC_WRT2),
        .FTDI_D_tri_i({FTDI_D_tri_i_15,FTDI_D_tri_i_14,FTDI_D_tri_i_13,FTDI_D_tri_i_12,FTDI_D_tri_i_11,FTDI_D_tri_i_10,FTDI_D_tri_i_9,FTDI_D_tri_i_8,FTDI_D_tri_i_7,FTDI_D_tri_i_6,FTDI_D_tri_i_5,FTDI_D_tri_i_4,FTDI_D_tri_i_3,FTDI_D_tri_i_2,FTDI_D_tri_i_1,FTDI_D_tri_i_0}),
        .FTDI_D_tri_o({FTDI_D_tri_o_15,FTDI_D_tri_o_14,FTDI_D_tri_o_13,FTDI_D_tri_o_12,FTDI_D_tri_o_11,FTDI_D_tri_o_10,FTDI_D_tri_o_9,FTDI_D_tri_o_8,FTDI_D_tri_o_7,FTDI_D_tri_o_6,FTDI_D_tri_o_5,FTDI_D_tri_o_4,FTDI_D_tri_o_3,FTDI_D_tri_o_2,FTDI_D_tri_o_1,FTDI_D_tri_o_0}),
        .FTDI_D_tri_t({FTDI_D_tri_t_15,FTDI_D_tri_t_14,FTDI_D_tri_t_13,FTDI_D_tri_t_12,FTDI_D_tri_t_11,FTDI_D_tri_t_10,FTDI_D_tri_t_9,FTDI_D_tri_t_8,FTDI_D_tri_t_7,FTDI_D_tri_t_6,FTDI_D_tri_t_5,FTDI_D_tri_t_4,FTDI_D_tri_t_3,FTDI_D_tri_t_2,FTDI_D_tri_t_1,FTDI_D_tri_t_0}),
        .RX_TX_tri_i(RX_TX_tri_i),
        .clk(clk),
        .led_tri_o(led_tri_o),
        .rst_n(rst_n),
        .rx_0(rx_0),
        .spi_io0_i(spi_io0_i),
        .spi_io0_o(spi_io0_o),
        .spi_io0_t(spi_io0_t),
        .spi_io1_i(spi_io1_i),
        .spi_io1_o(spi_io1_o),
        .spi_io1_t(spi_io1_t),
        .spi_sck_i(spi_sck_i),
        .spi_sck_o(spi_sck_o),
        .spi_sck_t(spi_sck_t),
        .spi_ss_i({spi_ss_i_2,spi_ss_i_1,spi_ss_i_0}),
        .spi_ss_o({spi_ss_o_2,spi_ss_o_1,spi_ss_o_0}),
        .spi_ss_t(spi_ss_t),
        .tx_0(tx_0));
  IOBUF spi_io0_iobuf
       (.I(spi_io0_o),
        .IO(spi_io0_io),
        .O(spi_io0_i),
        .T(spi_io0_t));
  IOBUF spi_io1_iobuf
       (.I(spi_io1_o),
        .IO(spi_io1_io),
        .O(spi_io1_i),
        .T(spi_io1_t));
  IOBUF spi_sck_iobuf
       (.I(spi_sck_o),
        .IO(spi_sck_io),
        .O(spi_sck_i),
        .T(spi_sck_t));
  IOBUF spi_ss_iobuf_0
       (.I(spi_ss_o_0),
        .IO(spi_ss_io[0]),
        .O(spi_ss_i_0),
        .T(spi_ss_t));
  IOBUF spi_ss_iobuf_1
       (.I(spi_ss_o_1),
        .IO(spi_ss_io[1]),
        .O(spi_ss_i_1),
        .T(spi_ss_t));
  IOBUF spi_ss_iobuf_2
       (.I(spi_ss_o_2),
        .IO(spi_ss_io[2]),
        .O(spi_ss_i_2),
        .T(spi_ss_t));
endmodule
