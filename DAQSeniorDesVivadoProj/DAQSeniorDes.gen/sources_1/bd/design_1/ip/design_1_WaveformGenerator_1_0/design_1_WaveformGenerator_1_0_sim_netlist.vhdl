-- Copyright 1986-2022 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2022.2 (win64) Build 3671981 Fri Oct 14 05:00:03 MDT 2022
-- Date        : Thu Oct 19 13:37:40 2023
-- Host        : DESKTOP-QRFTT4G running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode funcsim -rename_top design_1_WaveformGenerator_1_0 -prefix
--               design_1_WaveformGenerator_1_0_ design_1_WaveformGenerator_1_0_sim_netlist.vhdl
-- Design      : design_1_WaveformGenerator_1_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7a100tftg256-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_WaveformGenerator_1_0_WaveformGenerator is
  port (
    WaveformData : out STD_LOGIC_VECTOR ( 13 downto 0 );
    Read_EN : in STD_LOGIC;
    clr : in STD_LOGIC;
    clk : in STD_LOGIC;
    Address_Data : in STD_LOGIC_VECTOR ( 22 downto 0 )
  );
end design_1_WaveformGenerator_1_0_WaveformGenerator;

architecture STRUCTURE of design_1_WaveformGenerator_1_0_WaveformGenerator is
  signal curr_address : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal curr_address1 : STD_LOGIC;
  signal curr_address11_in : STD_LOGIC;
  signal \curr_address[8]_i_2_n_0\ : STD_LOGIC;
  signal curr_address_1 : STD_LOGIC;
  signal iter : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal \iter2_carry__0_i_1_n_0\ : STD_LOGIC;
  signal \iter2_carry__0_i_2_n_0\ : STD_LOGIC;
  signal \iter2_carry__0_n_3\ : STD_LOGIC;
  signal iter2_carry_i_1_n_0 : STD_LOGIC;
  signal iter2_carry_i_2_n_0 : STD_LOGIC;
  signal iter2_carry_i_3_n_0 : STD_LOGIC;
  signal iter2_carry_i_4_n_0 : STD_LOGIC;
  signal iter2_carry_i_5_n_0 : STD_LOGIC;
  signal iter2_carry_i_6_n_0 : STD_LOGIC;
  signal iter2_carry_i_7_n_0 : STD_LOGIC;
  signal iter2_carry_i_8_n_0 : STD_LOGIC;
  signal iter2_carry_n_0 : STD_LOGIC;
  signal iter2_carry_n_1 : STD_LOGIC;
  signal iter2_carry_n_2 : STD_LOGIC;
  signal iter2_carry_n_3 : STD_LOGIC;
  signal \iter[0]_i_1_n_0\ : STD_LOGIC;
  signal \iter[1]_i_1_n_0\ : STD_LOGIC;
  signal \iter[2]_i_1_n_0\ : STD_LOGIC;
  signal \iter[3]_i_1_n_0\ : STD_LOGIC;
  signal \iter[4]_i_1_n_0\ : STD_LOGIC;
  signal \iter[5]_i_2_n_0\ : STD_LOGIC;
  signal \iter[6]_i_1_n_0\ : STD_LOGIC;
  signal \iter[7]_i_1_n_0\ : STD_LOGIC;
  signal \iter[7]_i_2_n_0\ : STD_LOGIC;
  signal \iter[8]_i_1_n_0\ : STD_LOGIC;
  signal \iter__0\ : STD_LOGIC;
  signal max_address : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal \max_address0_carry__0_i_1_n_0\ : STD_LOGIC;
  signal \max_address0_carry__0_i_2_n_0\ : STD_LOGIC;
  signal \max_address0_carry__0_n_3\ : STD_LOGIC;
  signal max_address0_carry_i_1_n_0 : STD_LOGIC;
  signal max_address0_carry_i_2_n_0 : STD_LOGIC;
  signal max_address0_carry_i_3_n_0 : STD_LOGIC;
  signal max_address0_carry_i_4_n_0 : STD_LOGIC;
  signal max_address0_carry_i_5_n_0 : STD_LOGIC;
  signal max_address0_carry_i_6_n_0 : STD_LOGIC;
  signal max_address0_carry_i_7_n_0 : STD_LOGIC;
  signal max_address0_carry_i_8_n_0 : STD_LOGIC;
  signal max_address0_carry_n_0 : STD_LOGIC;
  signal max_address0_carry_n_1 : STD_LOGIC;
  signal max_address0_carry_n_2 : STD_LOGIC;
  signal max_address0_carry_n_3 : STD_LOGIC;
  signal max_address_0 : STD_LOGIC;
  signal NLW_iter2_carry_O_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_iter2_carry__0_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_iter2_carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_max_address0_carry_O_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_max_address0_carry__0_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_max_address0_carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_storageData_reg_DOADO_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal NLW_storageData_reg_DOBDO_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 14 );
  signal NLW_storageData_reg_DOPADOP_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_storageData_reg_DOPBDOP_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  attribute COMPARATOR_THRESHOLD : integer;
  attribute COMPARATOR_THRESHOLD of iter2_carry : label is 11;
  attribute COMPARATOR_THRESHOLD of \iter2_carry__0\ : label is 11;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \iter[1]_i_1\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \iter[2]_i_1\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \iter[3]_i_1\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \iter[6]_i_1\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \iter[7]_i_1\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \iter[8]_i_1\ : label is "soft_lutpair1";
  attribute COMPARATOR_THRESHOLD of max_address0_carry : label is 11;
  attribute COMPARATOR_THRESHOLD of \max_address0_carry__0\ : label is 11;
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ : string;
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ of storageData_reg : label is "p0_d14";
  attribute \MEM.PORTB.DATA_BIT_LAYOUT\ : string;
  attribute \MEM.PORTB.DATA_BIT_LAYOUT\ of storageData_reg : label is "p0_d14";
  attribute METHODOLOGY_DRC_VIOS : string;
  attribute METHODOLOGY_DRC_VIOS of storageData_reg : label is "{SYNTH-6 {cell *THIS*}}";
  attribute RTL_RAM_BITS : integer;
  attribute RTL_RAM_BITS of storageData_reg : label is 7168;
  attribute RTL_RAM_NAME : string;
  attribute RTL_RAM_NAME of storageData_reg : label is "inst/storageData_reg";
  attribute RTL_RAM_TYPE : string;
  attribute RTL_RAM_TYPE of storageData_reg : label is "RAM_SDP";
  attribute ram_addr_begin : integer;
  attribute ram_addr_begin of storageData_reg : label is 0;
  attribute ram_addr_end : integer;
  attribute ram_addr_end of storageData_reg : label is 1023;
  attribute ram_offset : integer;
  attribute ram_offset of storageData_reg : label is 512;
  attribute ram_slice_begin : integer;
  attribute ram_slice_begin of storageData_reg : label is 0;
  attribute ram_slice_end : integer;
  attribute ram_slice_end of storageData_reg : label is 13;
begin
\curr_address[8]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => clr,
      I1 => Read_EN,
      O => curr_address_1
    );
\curr_address[8]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => Read_EN,
      I1 => clr,
      O => \curr_address[8]_i_2_n_0\
    );
\curr_address_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \curr_address[8]_i_2_n_0\,
      D => Address_Data(14),
      Q => curr_address(0),
      R => curr_address_1
    );
\curr_address_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \curr_address[8]_i_2_n_0\,
      D => Address_Data(15),
      Q => curr_address(1),
      R => curr_address_1
    );
\curr_address_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \curr_address[8]_i_2_n_0\,
      D => Address_Data(16),
      Q => curr_address(2),
      R => curr_address_1
    );
\curr_address_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \curr_address[8]_i_2_n_0\,
      D => Address_Data(17),
      Q => curr_address(3),
      R => curr_address_1
    );
\curr_address_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \curr_address[8]_i_2_n_0\,
      D => Address_Data(18),
      Q => curr_address(4),
      R => curr_address_1
    );
\curr_address_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \curr_address[8]_i_2_n_0\,
      D => Address_Data(19),
      Q => curr_address(5),
      R => curr_address_1
    );
\curr_address_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \curr_address[8]_i_2_n_0\,
      D => Address_Data(20),
      Q => curr_address(6),
      R => curr_address_1
    );
\curr_address_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \curr_address[8]_i_2_n_0\,
      D => Address_Data(21),
      Q => curr_address(7),
      R => curr_address_1
    );
\curr_address_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \curr_address[8]_i_2_n_0\,
      D => Address_Data(22),
      Q => curr_address(8),
      R => curr_address_1
    );
iter2_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => iter2_carry_n_0,
      CO(2) => iter2_carry_n_1,
      CO(1) => iter2_carry_n_2,
      CO(0) => iter2_carry_n_3,
      CYINIT => '0',
      DI(3) => iter2_carry_i_1_n_0,
      DI(2) => iter2_carry_i_2_n_0,
      DI(1) => iter2_carry_i_3_n_0,
      DI(0) => iter2_carry_i_4_n_0,
      O(3 downto 0) => NLW_iter2_carry_O_UNCONNECTED(3 downto 0),
      S(3) => iter2_carry_i_5_n_0,
      S(2) => iter2_carry_i_6_n_0,
      S(1) => iter2_carry_i_7_n_0,
      S(0) => iter2_carry_i_8_n_0
    );
\iter2_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => iter2_carry_n_0,
      CO(3 downto 1) => \NLW_iter2_carry__0_CO_UNCONNECTED\(3 downto 1),
      CO(0) => \iter2_carry__0_n_3\,
      CYINIT => '0',
      DI(3 downto 1) => B"000",
      DI(0) => \iter2_carry__0_i_1_n_0\,
      O(3 downto 0) => \NLW_iter2_carry__0_O_UNCONNECTED\(3 downto 0),
      S(3 downto 1) => B"000",
      S(0) => \iter2_carry__0_i_2_n_0\
    );
\iter2_carry__0_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => max_address(8),
      I1 => iter(8),
      O => \iter2_carry__0_i_1_n_0\
    );
\iter2_carry__0_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => iter(8),
      I1 => max_address(8),
      O => \iter2_carry__0_i_2_n_0\
    );
iter2_carry_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => max_address(6),
      I1 => iter(6),
      I2 => iter(7),
      I3 => max_address(7),
      O => iter2_carry_i_1_n_0
    );
iter2_carry_i_2: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => max_address(4),
      I1 => iter(4),
      I2 => iter(5),
      I3 => max_address(5),
      O => iter2_carry_i_2_n_0
    );
iter2_carry_i_3: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => max_address(2),
      I1 => iter(2),
      I2 => iter(3),
      I3 => max_address(3),
      O => iter2_carry_i_3_n_0
    );
iter2_carry_i_4: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => max_address(0),
      I1 => iter(0),
      I2 => iter(1),
      I3 => max_address(1),
      O => iter2_carry_i_4_n_0
    );
iter2_carry_i_5: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => max_address(6),
      I1 => iter(6),
      I2 => max_address(7),
      I3 => iter(7),
      O => iter2_carry_i_5_n_0
    );
iter2_carry_i_6: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => max_address(4),
      I1 => iter(4),
      I2 => max_address(5),
      I3 => iter(5),
      O => iter2_carry_i_6_n_0
    );
iter2_carry_i_7: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => max_address(2),
      I1 => iter(2),
      I2 => max_address(3),
      I3 => iter(3),
      O => iter2_carry_i_7_n_0
    );
iter2_carry_i_8: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => max_address(0),
      I1 => iter(0),
      I2 => max_address(1),
      I3 => iter(1),
      O => iter2_carry_i_8_n_0
    );
\iter[0]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \iter2_carry__0_n_3\,
      I1 => iter(0),
      O => \iter[0]_i_1_n_0\
    );
\iter[1]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"28"
    )
        port map (
      I0 => \iter2_carry__0_n_3\,
      I1 => iter(0),
      I2 => iter(1),
      O => \iter[1]_i_1_n_0\
    );
\iter[2]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2A80"
    )
        port map (
      I0 => \iter2_carry__0_n_3\,
      I1 => iter(0),
      I2 => iter(1),
      I3 => iter(2),
      O => \iter[2]_i_1_n_0\
    );
\iter[3]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"2AAA8000"
    )
        port map (
      I0 => \iter2_carry__0_n_3\,
      I1 => iter(1),
      I2 => iter(0),
      I3 => iter(2),
      I4 => iter(3),
      O => \iter[3]_i_1_n_0\
    );
\iter[4]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"2AAAAAAA80000000"
    )
        port map (
      I0 => \iter2_carry__0_n_3\,
      I1 => iter(2),
      I2 => iter(0),
      I3 => iter(1),
      I4 => iter(3),
      I5 => iter(4),
      O => \iter[4]_i_1_n_0\
    );
\iter[5]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => Read_EN,
      I1 => \iter2_carry__0_n_3\,
      O => \iter__0\
    );
\iter[5]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFFFFFF80000000"
    )
        port map (
      I0 => iter(3),
      I1 => iter(1),
      I2 => iter(0),
      I3 => iter(2),
      I4 => iter(4),
      I5 => iter(5),
      O => \iter[5]_i_2_n_0\
    );
\iter[6]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"28"
    )
        port map (
      I0 => \iter2_carry__0_n_3\,
      I1 => \iter[7]_i_2_n_0\,
      I2 => iter(6),
      O => \iter[6]_i_1_n_0\
    );
\iter[7]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2A80"
    )
        port map (
      I0 => \iter2_carry__0_n_3\,
      I1 => \iter[7]_i_2_n_0\,
      I2 => iter(6),
      I3 => iter(7),
      O => \iter[7]_i_1_n_0\
    );
\iter[7]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000000000000"
    )
        port map (
      I0 => iter(5),
      I1 => iter(3),
      I2 => iter(1),
      I3 => iter(0),
      I4 => iter(2),
      I5 => iter(4),
      O => \iter[7]_i_2_n_0\
    );
\iter[8]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"2AAA8000"
    )
        port map (
      I0 => \iter2_carry__0_n_3\,
      I1 => iter(6),
      I2 => \iter[7]_i_2_n_0\,
      I3 => iter(7),
      I4 => iter(8),
      O => \iter[8]_i_1_n_0\
    );
\iter_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk,
      CE => Read_EN,
      D => \iter[0]_i_1_n_0\,
      Q => iter(0),
      R => '0'
    );
\iter_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk,
      CE => Read_EN,
      D => \iter[1]_i_1_n_0\,
      Q => iter(1),
      R => '0'
    );
\iter_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk,
      CE => Read_EN,
      D => \iter[2]_i_1_n_0\,
      Q => iter(2),
      R => '0'
    );
\iter_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk,
      CE => Read_EN,
      D => \iter[3]_i_1_n_0\,
      Q => iter(3),
      R => '0'
    );
\iter_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk,
      CE => Read_EN,
      D => \iter[4]_i_1_n_0\,
      Q => iter(4),
      R => '0'
    );
\iter_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk,
      CE => Read_EN,
      D => \iter[5]_i_2_n_0\,
      Q => iter(5),
      R => \iter__0\
    );
\iter_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk,
      CE => Read_EN,
      D => \iter[6]_i_1_n_0\,
      Q => iter(6),
      R => '0'
    );
\iter_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk,
      CE => Read_EN,
      D => \iter[7]_i_1_n_0\,
      Q => iter(7),
      R => '0'
    );
\iter_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk,
      CE => Read_EN,
      D => \iter[8]_i_1_n_0\,
      Q => iter(8),
      R => '0'
    );
max_address0_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => max_address0_carry_n_0,
      CO(2) => max_address0_carry_n_1,
      CO(1) => max_address0_carry_n_2,
      CO(0) => max_address0_carry_n_3,
      CYINIT => '0',
      DI(3) => max_address0_carry_i_1_n_0,
      DI(2) => max_address0_carry_i_2_n_0,
      DI(1) => max_address0_carry_i_3_n_0,
      DI(0) => max_address0_carry_i_4_n_0,
      O(3 downto 0) => NLW_max_address0_carry_O_UNCONNECTED(3 downto 0),
      S(3) => max_address0_carry_i_5_n_0,
      S(2) => max_address0_carry_i_6_n_0,
      S(1) => max_address0_carry_i_7_n_0,
      S(0) => max_address0_carry_i_8_n_0
    );
\max_address0_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => max_address0_carry_n_0,
      CO(3 downto 1) => \NLW_max_address0_carry__0_CO_UNCONNECTED\(3 downto 1),
      CO(0) => \max_address0_carry__0_n_3\,
      CYINIT => '0',
      DI(3 downto 1) => B"000",
      DI(0) => \max_address0_carry__0_i_1_n_0\,
      O(3 downto 0) => \NLW_max_address0_carry__0_O_UNCONNECTED\(3 downto 0),
      S(3 downto 1) => B"000",
      S(0) => \max_address0_carry__0_i_2_n_0\
    );
\max_address0_carry__0_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => curr_address(8),
      I1 => max_address(8),
      O => \max_address0_carry__0_i_1_n_0\
    );
\max_address0_carry__0_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => max_address(8),
      I1 => curr_address(8),
      O => \max_address0_carry__0_i_2_n_0\
    );
max_address0_carry_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => curr_address(6),
      I1 => max_address(6),
      I2 => max_address(7),
      I3 => curr_address(7),
      O => max_address0_carry_i_1_n_0
    );
max_address0_carry_i_2: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => curr_address(4),
      I1 => max_address(4),
      I2 => max_address(5),
      I3 => curr_address(5),
      O => max_address0_carry_i_2_n_0
    );
max_address0_carry_i_3: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => curr_address(2),
      I1 => max_address(2),
      I2 => max_address(3),
      I3 => curr_address(3),
      O => max_address0_carry_i_3_n_0
    );
max_address0_carry_i_4: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => curr_address(0),
      I1 => max_address(0),
      I2 => max_address(1),
      I3 => curr_address(1),
      O => max_address0_carry_i_4_n_0
    );
max_address0_carry_i_5: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => curr_address(6),
      I1 => max_address(6),
      I2 => curr_address(7),
      I3 => max_address(7),
      O => max_address0_carry_i_5_n_0
    );
max_address0_carry_i_6: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => curr_address(4),
      I1 => max_address(4),
      I2 => curr_address(5),
      I3 => max_address(5),
      O => max_address0_carry_i_6_n_0
    );
max_address0_carry_i_7: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => curr_address(2),
      I1 => max_address(2),
      I2 => curr_address(3),
      I3 => max_address(3),
      O => max_address0_carry_i_7_n_0
    );
max_address0_carry_i_8: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => curr_address(0),
      I1 => max_address(0),
      I2 => curr_address(1),
      I3 => max_address(1),
      O => max_address0_carry_i_8_n_0
    );
\max_address[8]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"10"
    )
        port map (
      I0 => clr,
      I1 => Read_EN,
      I2 => \max_address0_carry__0_n_3\,
      O => max_address_0
    );
\max_address_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk,
      CE => max_address_0,
      D => curr_address(0),
      Q => max_address(0),
      R => curr_address_1
    );
\max_address_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk,
      CE => max_address_0,
      D => curr_address(1),
      Q => max_address(1),
      R => curr_address_1
    );
\max_address_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk,
      CE => max_address_0,
      D => curr_address(2),
      Q => max_address(2),
      R => curr_address_1
    );
\max_address_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk,
      CE => max_address_0,
      D => curr_address(3),
      Q => max_address(3),
      R => curr_address_1
    );
\max_address_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk,
      CE => max_address_0,
      D => curr_address(4),
      Q => max_address(4),
      R => curr_address_1
    );
\max_address_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk,
      CE => max_address_0,
      D => curr_address(5),
      Q => max_address(5),
      R => curr_address_1
    );
\max_address_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk,
      CE => max_address_0,
      D => curr_address(6),
      Q => max_address(6),
      R => curr_address_1
    );
\max_address_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk,
      CE => max_address_0,
      D => curr_address(7),
      Q => max_address(7),
      R => curr_address_1
    );
\max_address_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk,
      CE => max_address_0,
      D => curr_address(8),
      Q => max_address(8),
      R => curr_address_1
    );
storageData_reg: unisim.vcomponents.RAMB18E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      INIT_A => X"00000",
      INIT_B => X"00000",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "DELAYED_WRITE",
      READ_WIDTH_A => 18,
      READ_WIDTH_B => 18,
      RSTREG_PRIORITY_A => "RSTREG",
      RSTREG_PRIORITY_B => "RSTREG",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"00000",
      SRVAL_B => X"00000",
      WRITE_MODE_A => "READ_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 18,
      WRITE_WIDTH_B => 18
    )
        port map (
      ADDRARDADDR(13) => '1',
      ADDRARDADDR(12 downto 4) => Address_Data(22 downto 14),
      ADDRARDADDR(3 downto 0) => B"1111",
      ADDRBWRADDR(13) => '1',
      ADDRBWRADDR(12 downto 4) => iter(8 downto 0),
      ADDRBWRADDR(3 downto 0) => B"1111",
      CLKARDCLK => clk,
      CLKBWRCLK => clk,
      DIADI(15 downto 14) => B"00",
      DIADI(13 downto 0) => Address_Data(13 downto 0),
      DIBDI(15 downto 0) => B"0011111111111111",
      DIPADIP(1 downto 0) => B"00",
      DIPBDIP(1 downto 0) => B"00",
      DOADO(15 downto 0) => NLW_storageData_reg_DOADO_UNCONNECTED(15 downto 0),
      DOBDO(15 downto 14) => NLW_storageData_reg_DOBDO_UNCONNECTED(15 downto 14),
      DOBDO(13 downto 0) => WaveformData(13 downto 0),
      DOPADOP(1 downto 0) => NLW_storageData_reg_DOPADOP_UNCONNECTED(1 downto 0),
      DOPBDOP(1 downto 0) => NLW_storageData_reg_DOPBDOP_UNCONNECTED(1 downto 0),
      ENARDEN => curr_address1,
      ENBWREN => Read_EN,
      REGCEAREGCE => '0',
      REGCEB => '0',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      WEA(1) => curr_address11_in,
      WEA(0) => curr_address11_in,
      WEBWE(3 downto 0) => B"0000"
    );
storageData_reg_i_1: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => Read_EN,
      O => curr_address1
    );
storageData_reg_i_2: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => clr,
      O => curr_address11_in
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_WaveformGenerator_1_0 is
  port (
    Address_Data : in STD_LOGIC_VECTOR ( 22 downto 0 );
    Read_EN : in STD_LOGIC;
    clk : in STD_LOGIC;
    clr : in STD_LOGIC;
    WaveformData : out STD_LOGIC_VECTOR ( 13 downto 0 )
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of design_1_WaveformGenerator_1_0 : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of design_1_WaveformGenerator_1_0 : entity is "design_1_WaveformGenerator_1_0,WaveformGenerator,{}";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of design_1_WaveformGenerator_1_0 : entity is "yes";
  attribute IP_DEFINITION_SOURCE : string;
  attribute IP_DEFINITION_SOURCE of design_1_WaveformGenerator_1_0 : entity is "module_ref";
  attribute X_CORE_INFO : string;
  attribute X_CORE_INFO of design_1_WaveformGenerator_1_0 : entity is "WaveformGenerator,Vivado 2022.2";
end design_1_WaveformGenerator_1_0;

architecture STRUCTURE of design_1_WaveformGenerator_1_0 is
  attribute X_INTERFACE_INFO : string;
  attribute X_INTERFACE_INFO of clk : signal is "xilinx.com:signal:clock:1.0 clk CLK";
  attribute X_INTERFACE_PARAMETER : string;
  attribute X_INTERFACE_PARAMETER of clk : signal is "XIL_INTERFACENAME clk, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, PHASE 0.0, CLK_DOMAIN /clk_wiz_1_clk_out1, INSERT_VIP 0";
begin
inst: entity work.design_1_WaveformGenerator_1_0_WaveformGenerator
     port map (
      Address_Data(22 downto 0) => Address_Data(22 downto 0),
      Read_EN => Read_EN,
      WaveformData(13 downto 0) => WaveformData(13 downto 0),
      clk => clk,
      clr => clr
    );
end STRUCTURE;
