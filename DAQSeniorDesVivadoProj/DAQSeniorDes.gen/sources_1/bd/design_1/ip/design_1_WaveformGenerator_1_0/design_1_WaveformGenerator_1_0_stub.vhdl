-- Copyright 1986-2022 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2022.2 (win64) Build 3671981 Fri Oct 14 05:00:03 MDT 2022
-- Date        : Thu Oct 19 13:37:40 2023
-- Host        : DESKTOP-QRFTT4G running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode synth_stub -rename_top design_1_WaveformGenerator_1_0 -prefix
--               design_1_WaveformGenerator_1_0_ design_1_WaveformGenerator_1_0_stub.vhdl
-- Design      : design_1_WaveformGenerator_1_0
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xc7a100tftg256-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity design_1_WaveformGenerator_1_0 is
  Port ( 
    Address_Data : in STD_LOGIC_VECTOR ( 22 downto 0 );
    Read_EN : in STD_LOGIC;
    clk : in STD_LOGIC;
    clr : in STD_LOGIC;
    WaveformData : out STD_LOGIC_VECTOR ( 13 downto 0 )
  );

end design_1_WaveformGenerator_1_0;

architecture stub of design_1_WaveformGenerator_1_0 is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "Address_Data[22:0],Read_EN,clk,clr,WaveformData[13:0]";
attribute X_CORE_INFO : string;
attribute X_CORE_INFO of stub : architecture is "WaveformGenerator,Vivado 2022.2";
begin
end;
