set_property BITSTREAM.GENERAL.COMPRESS TRUE [current_design]
set_property BITSTREAM.CONFIG.CONFIGRATE 33 [current_design]
set_property CONFIG_VOLTAGE 3.3 [current_design]
set_property CFGBVS VCCO [current_design]
set_property BITSTREAM.CONFIG.SPI_32BIT_ADDR NO [current_design]
set_property BITSTREAM.CONFIG.SPI_BUSWIDTH 1 [current_design]
set_property BITSTREAM.CONFIG.SPI_FALL_EDGE YES [current_design]

create_clock -period 10.000 -name clk -waveform {0.000 5.000} [get_ports clk]
set_property PACKAGE_PIN N14 [get_ports clk]
set_property IOSTANDARD LVCMOS33 [get_ports clk]

set_property PACKAGE_PIN P6 [get_ports rst_n]
set_property IOSTANDARD LVCMOS33 [get_ports rst_n]

set_property PACKAGE_PIN K13 [get_ports {led_tri_o[0]}]
set_property IOSTANDARD LVCMOS33 [get_ports {led_tri_o[0]}]

set_property PACKAGE_PIN K12 [get_ports {led_tri_o[1]}]
set_property IOSTANDARD LVCMOS33 [get_ports {led_tri_o[1]}]

set_property PACKAGE_PIN L14 [get_ports {led_tri_o[2]}]
set_property IOSTANDARD LVCMOS33 [get_ports {led_tri_o[2]}]

set_property PACKAGE_PIN L13 [get_ports {led_tri_o[3]}]
set_property IOSTANDARD LVCMOS33 [get_ports {led_tri_o[3]}]

set_property PACKAGE_PIN M16 [get_ports {led_tri_o[4]}]
set_property IOSTANDARD LVCMOS33 [get_ports {led_tri_o[4]}]

set_property PACKAGE_PIN M14 [get_ports {led_tri_o[5]}]
set_property IOSTANDARD LVCMOS33 [get_ports {led_tri_o[5]}]

set_property PACKAGE_PIN M12 [get_ports {led_tri_o[6]}]
set_property IOSTANDARD LVCMOS33 [get_ports {led_tri_o[6]}]

set_property PACKAGE_PIN N16 [get_ports {led_tri_o[7]}]
set_property IOSTANDARD LVCMOS33 [get_ports {led_tri_o[7]}]

# DAC_A Pin numbers
set_property PACKAGE_PIN R7 [get_ports {DAC_A[0]}]
set_property IOSTANDARD LVCMOS33 [get_ports {DAC_A[0]}]

set_property PACKAGE_PIN R6 [get_ports {DAC_A[1]}]
set_property IOSTANDARD LVCMOS33 [get_ports {DAC_A[1]}]

set_property PACKAGE_PIN T9 [get_ports {DAC_A[2]}]
set_property IOSTANDARD LVCMOS33 [get_ports {DAC_A[2]}]

set_property PACKAGE_PIN T10 [get_ports {DAC_A[3]}]
set_property IOSTANDARD LVCMOS33 [get_ports {DAC_A[3]}]

set_property PACKAGE_PIN K1 [get_ports {DAC_A[4]}]
set_property IOSTANDARD LVCMOS33 [get_ports {DAC_A[4]}]

set_property PACKAGE_PIN J1 [get_ports {DAC_A[5]}]
set_property IOSTANDARD LVCMOS33 [get_ports {DAC_A[5]}]

set_property PACKAGE_PIN L3 [get_ports {DAC_A[6]}]
set_property IOSTANDARD LVCMOS33 [get_ports {DAC_A[6]}]

set_property PACKAGE_PIN L2 [get_ports {DAC_A[7]}]
set_property IOSTANDARD LVCMOS33 [get_ports {DAC_A[7]}]

set_property PACKAGE_PIN P8 [get_ports {DAC_A[8]}]
set_property IOSTANDARD LVCMOS33 [get_ports {DAC_A[8]}]

set_property PACKAGE_PIN R8 [get_ports {DAC_A[9]}]
set_property IOSTANDARD LVCMOS33 [get_ports {DAC_A[9]}]

set_property PACKAGE_PIN R5 [get_ports {DAC_A[10]}]
set_property IOSTANDARD LVCMOS33 [get_ports {DAC_A[10]}]

set_property PACKAGE_PIN T5 [get_ports {DAC_A[11]}]
set_property IOSTANDARD LVCMOS33 [get_ports {DAC_A[11]}]

set_property PACKAGE_PIN T7 [get_ports {DAC_A[12]}]
set_property IOSTANDARD LVCMOS33 [get_ports {DAC_A[12]}]

set_property PACKAGE_PIN T8 [get_ports {DAC_A[13]}]
set_property IOSTANDARD LVCMOS33 [get_ports {DAC_A[13]}]

#DAC_B pin numbers
set_property PACKAGE_PIN B1 [get_ports {DAC_B[0]}]
set_property IOSTANDARD LVCMOS33 [get_ports {DAC_B[0]}]

set_property PACKAGE_PIN C1 [get_ports {DAC_B[1]}]
set_property IOSTANDARD LVCMOS33 [get_ports {DAC_B[1]}]

set_property PACKAGE_PIN C2 [get_ports {DAC_B[2]}]
set_property IOSTANDARD LVCMOS33 [get_ports {DAC_B[2]}]

set_property PACKAGE_PIN C3 [get_ports {DAC_B[3]}]
set_property IOSTANDARD LVCMOS33 [get_ports {DAC_B[3]}]

set_property PACKAGE_PIN D3 [get_ports {DAC_B[4]}]
set_property IOSTANDARD LVCMOS33 [get_ports {DAC_B[4]}]

set_property PACKAGE_PIN E3 [get_ports {DAC_B[5]}]
set_property IOSTANDARD LVCMOS33 [get_ports {DAC_B[5]}]

set_property PACKAGE_PIN C4 [get_ports {DAC_B[6]}]
set_property IOSTANDARD LVCMOS33 [get_ports {DAC_B[6]}]

set_property PACKAGE_PIN D4 [get_ports {DAC_B[7]}]
set_property IOSTANDARD LVCMOS33 [get_ports {DAC_B[7]}]

set_property PACKAGE_PIN D1 [get_ports {DAC_B[8]}]
set_property IOSTANDARD LVCMOS33 [get_ports {DAC_B[8]}]

set_property PACKAGE_PIN E2 [get_ports {DAC_B[9]}]
set_property IOSTANDARD LVCMOS33 [get_ports {DAC_B[9]}]

set_property PACKAGE_PIN A2 [get_ports {DAC_B[10]}]
set_property IOSTANDARD LVCMOS33 [get_ports {DAC_B[10]}]

set_property PACKAGE_PIN B2 [get_ports {DAC_B[11]}]
set_property IOSTANDARD LVCMOS33 [get_ports {DAC_B[11]}]

set_property PACKAGE_PIN E1 [get_ports {DAC_B[12]}]
set_property IOSTANDARD LVCMOS33 [get_ports {DAC_B[12]}]

set_property PACKAGE_PIN F2 [get_ports {DAC_B[13]}]
set_property IOSTANDARD LVCMOS33 [get_ports {DAC_B[13]}]

#ETC DAC pins

set_property PACKAGE_PIN K5 [get_ports {DAC_CLK1}]
set_property IOSTANDARD LVCMOS33 [get_ports {DAC_CLK1}]

set_property PACKAGE_PIN E6 [get_ports {DAC_CLK2}]
set_property IOSTANDARD LVCMOS33 [get_ports {DAC_CLK2}]

set_property PACKAGE_PIN J5 [get_ports {DAC_WRT1}]
set_property IOSTANDARD LVCMOS33 [get_ports {DAC_WRT1}]

set_property PACKAGE_PIN J3 [get_ports {DAC_WRT2}]
set_property IOSTANDARD LVCMOS33 [get_ports {DAC_WRT2}]

#SPI pins
#SDO (Serial data out)
set_property PACKAGE_PIN R16 [get_ports {spi_io0_io}] 
set_property IOSTANDARD LVCMOS33 [get_ports {spi_io0_io}]

#SDI (Serial data in)
set_property PACKAGE_PIN M5 [get_ports {spi_io1_io}] 
set_property IOSTANDARD LVCMOS33 [get_ports {spi_io1_io}]

#SCLK
set_property PACKAGE_PIN R15 [get_ports {spi_sck_io}]
set_property IOSTANDARD LVCMOS33 [get_ports {spi_sck_io}]

#Chip select up converter
set_property PACKAGE_PIN T15 [get_ports {spi_ss_io[0]}] 
set_property IOSTANDARD LVCMOS33 [get_ports {spi_ss_io[0]}]

#Chip select down converter
set_property PACKAGE_PIN T14 [get_ports {spi_ss_io[1]}] 
set_property IOSTANDARD LVCMOS33 [get_ports {spi_ss_io[1]}]

#Chip select source
set_property PACKAGE_PIN P5 [get_ports {spi_ss_io[2]}] 
set_property IOSTANDARD LVCMOS33 [get_ports {spi_ss_io[2]}]

#ECT ADC pins
set_property PACKAGE_PIN M1 [get_ports {ADC_CLK_A}]
set_property IOSTANDARD LVCMOS33 [get_ports {ADC_CLK_A}]

set_property PACKAGE_PIN M2 [get_ports {ADC_CLK_B}]
set_property IOSTANDARD LVCMOS33 [get_ports {ADC_CLK_B}]

set_property PACKAGE_PIN N4 [get_ports {ADC_CLKOUT}]
set_property IOSTANDARD LVCMOS33 [get_ports {ADC_CLKOUT}]


# FT600 pins
set_property PACKAGE_PIN B7 [get_ports {FTDI_D_tri_io[0]}]
set_property IOSTANDARD LVCMOS33 [get_ports {FTDI_D_tri_io[0]}]

set_property PACKAGE_PIN A7 [get_ports {FTDI_D_tri_io[1]}]
set_property IOSTANDARD LVCMOS33 [get_ports {FTDI_D_tri_io[1]}]

set_property PACKAGE_PIN B6 [get_ports {FTDI_D_tri_io[2]}]
set_property IOSTANDARD LVCMOS33 [get_ports {FTDI_D_tri_io[2]}]

set_property PACKAGE_PIN B5 [get_ports {FTDI_D_tri_io[3]}]
set_property IOSTANDARD LVCMOS33 [get_ports {FTDI_D_tri_io[3]}]

set_property PACKAGE_PIN A5 [get_ports {FTDI_D_tri_io[4]}]
set_property IOSTANDARD LVCMOS33 [get_ports {FTDI_D_tri_io[4]}]

set_property PACKAGE_PIN A4 [get_ports {FTDI_D_tri_io[5]}]
set_property IOSTANDARD LVCMOS33 [get_ports {FTDI_D_tri_io[5]}]

set_property PACKAGE_PIN B4 [get_ports {FTDI_D_tri_io[6]}]
set_property IOSTANDARD LVCMOS33 [get_ports {FTDI_D_tri_io[6]}]

set_property PACKAGE_PIN A3 [get_ports {FTDI_D_tri_io[7]}]
set_property IOSTANDARD LVCMOS33 [get_ports {FTDI_D_tri_io[7]}]

set_property PACKAGE_PIN C7 [get_ports {FTDI_D_tri_io[8]}]
set_property IOSTANDARD LVCMOS33 [get_ports {FTDI_D_tri_io[8]}]

set_property PACKAGE_PIN C6 [get_ports {FTDI_D_tri_io[9]}]
set_property IOSTANDARD LVCMOS33 [get_ports {FTDI_D_tri_io[9]}]

set_property PACKAGE_PIN D6 [get_ports {FTDI_D_tri_io[10]}]
set_property IOSTANDARD LVCMOS33 [get_ports {FTDI_D_tri_io[10]}]

set_property PACKAGE_PIN D5 [get_ports {FTDI_D_tri_io[11]}]
set_property IOSTANDARD LVCMOS33 [get_ports {FTDI_D_tri_io[11]}]

set_property PACKAGE_PIN G5 [get_ports {FTDI_D_tri_io[12]}]
set_property IOSTANDARD LVCMOS33 [get_ports {FTDI_D_tri_io[12]}]

set_property PACKAGE_PIN G4 [get_ports {FTDI_D_tri_io[13]}]
set_property IOSTANDARD LVCMOS33 [get_ports {FTDI_D_tri_io[13]}]

set_property PACKAGE_PIN G2 [get_ports {FTDI_D_tri_io[14]}]
set_property IOSTANDARD LVCMOS33 [get_ports {FTDI_D_tri_io[14]}]

set_property PACKAGE_PIN G1 [get_ports {FTDI_D_tri_io[15]}]
set_property IOSTANDARD LVCMOS33 [get_ports {FTDI_D_tri_io[15]}]

set_property PACKAGE_PIN H5 [get_ports {CTRL_tri_o[0]}]
set_property IOSTANDARD LVCMOS33 [get_ports {CTRL_tri_o[0]}]

set_property PACKAGE_PIN H4 [get_ports {CTRL_tri_o[1]}]
set_property IOSTANDARD LVCMOS33 [get_ports {CTRL_tri_o[1]}]

set_property PACKAGE_PIN H3 [get_ports {CTRL_tri_o[2]}]
set_property IOSTANDARD LVCMOS33 [get_ports {CTRL_tri_o[2]}]


set_property PACKAGE_PIN H2 [get_ports {BE_tri_io[0]}]
set_property IOSTANDARD LVCMOS33 [get_ports {BE_tri_io[0]}]

set_property PACKAGE_PIN H1 [get_ports {BE_tri_io[1]}]
set_property IOSTANDARD LVCMOS33 [get_ports {BE_tri_io[1]}]


set_property PACKAGE_PIN M6 [get_ports {RX_TX_tri_i[0]}]
set_property IOSTANDARD LVCMOS33 [get_ports {RX_TX_tri_i[0]}]

set_property PACKAGE_PIN N6 [get_ports {RX_TX_tri_i[1]}]
set_property IOSTANDARD LVCMOS33 [get_ports {RX_TX_tri_i[1]}]




# serial names are flipped in the schematic (named for the FTDI chip)
set_property PACKAGE_PIN P16 [get_ports {tx_0}]
set_property IOSTANDARD LVCMOS33 [get_ports {tx_0}]

set_property PACKAGE_PIN P15 [get_ports {rx_0}]
set_property IOSTANDARD LVCMOS33 [get_ports {rx_0}]