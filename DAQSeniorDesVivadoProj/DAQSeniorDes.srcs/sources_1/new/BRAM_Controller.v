`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: Matthew Caron
// 
// Create Date: 10/13/2023 01:04:20 PM
// Design Name: 
// Module Name: BRAM_Controller
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module BRAM_Controller(
    input [32:0] WRITE_DATA_A,
    input [14:0] WRITE_ADDRESS_A,
    input [32:0] WRITE_DATA_B,
    input [14:0] WRITE_ADDRESS_B,
    input [8:0] READ_ADDRESS_A,
    input [8:0] READ_ADDRESS_B,
    output [7:0] ADDRESS_A,
    output [7:0] ADDRESS_B,
    input EN_READ_WRITE,
    output [8:0] DATA_WRITE
    );
endmodule
