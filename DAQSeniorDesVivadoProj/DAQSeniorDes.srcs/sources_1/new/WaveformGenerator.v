`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: Senior design Dec 2023
// Engineer: Matthew Caron
// 
// Create Date: 10/16/2023 02:44:25 PM
// Design Name: Arbitry waveform generator for DAC
// Module Name: WaveformGenerator
// Description: This module takes in data and addresses for an arbitry
// function and stores it in RAM to continuously clock out to the DAC on the positive 
// edge of the clock
// Dependencies: 
// 
// Revision: V3
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
module WaveformGenerator(
    input [22:0] Address_Data,
    input Read_EN,
    input clk,
    input clr,
    output reg [13:0] WaveformData = 14'h000
    );
    reg [8:0] curr_address = 9'h000;
    reg [8:0] max_address = 9'h000;
    reg [8:0] iter = 9'h000;
    reg [13:0] storageData [0:512];
    
    always @ (posedge clk) begin
        if (Read_EN) begin
            WaveformData <= storageData[iter];
        end
    end

    always @ (negedge clk) begin
        if (Read_EN) begin
            iter = (iter < max_address) ? (iter + 1'b1): 9'h000;
        end
    end
    
    always @ (posedge clk) begin
	   if (~Read_EN && ~clr) begin
	       curr_address = Address_Data >> 14;
	       storageData[curr_address] = Address_Data & 14'h3FFF;
	   end else if (~Read_EN && clr) begin
	       curr_address = 9'h000;
	   end
	end 

	always @ (negedge clk) begin
		if (~Read_EN && ~clr) begin 
			max_address = (curr_address > max_address) ? curr_address: max_address;
		end else if (~Read_EN && clr) begin
		    max_address = 9'h000;
		end
	end 
endmodule
