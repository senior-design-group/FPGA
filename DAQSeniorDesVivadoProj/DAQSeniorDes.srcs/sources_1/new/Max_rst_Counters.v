`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 10/15/2023 02:58:15 PM
// Design Name: 
// Module Name: Max_rst_Counters
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Max_rst_Counters(
    input [8:0] Max_Val,
    input Load_Val,
    input [8:0] Curr_Val,
    input clk,
    output reg Reset_Count
    );
    reg [8:0] Reset_Val;
    initial Reset_Val = 9'h1FF;
    initial Reset_Count = 1'b1;
    
    always @ (~Load_Val) begin
        Reset_Val = Max_Val;
    end
    
    always @ (negedge clk) begin
        Reset_Count = 1'b0;
        if (Curr_Val >= (Reset_Val - 9'h001)) begin
            Reset_Count = 1'b1;
        end   
    end
endmodule
