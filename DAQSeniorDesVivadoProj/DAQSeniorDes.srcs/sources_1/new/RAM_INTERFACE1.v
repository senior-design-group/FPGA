`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: Matthew Caron
// 
// Create Date: 10/13/2023 01:41:24 PM
// Design Name: 
// Module Name: RAM_INTERFACE1
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module RAM_INTERFACE1(
    input [8:0] READ_ADDRESS,
    input [22:0] WRITE_ADDRESS_DATA,
    input CLK_IN,
    input RW_EN,
    output reg [8:0] ADDRESS_OUT,
    output reg [13:0] DATA_IN,
    output reg RW
    );
    always @ (negedge CLK_IN) begin
        RW = ~RW_EN;
        
        if (RW_EN == 1'b0) begin
            ADDRESS_OUT = READ_ADDRESS;
        end else begin
            DATA_IN = WRITE_ADDRESS_DATA & 14'h3FFF;
            ADDRESS_OUT = (WRITE_ADDRESS_DATA >> 14) & 9'h1FF;
        end 
    end
endmodule
