`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 10/13/2023 01:33:27 PM
// Design Name: 
// Module Name: RAM_INTERFACE
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module RAM_INTERFACE(
    input [8:0] READ_ADDRESS,
    input [32:0] WRITE_DATA,
    input CLK_IN,
    output CLK_OUT,
    input WRITE_READ,
    output WRITE_READ_OUT
    );
endmodule
