-- Copyright 1986-2022 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2022.2 (win64) Build 3671981 Fri Oct 14 05:00:03 MDT 2022
-- Date        : Sat Oct 14 11:52:49 2023
-- Host        : DESKTOP-QRFTT4G running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
--               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_RAM_INTERFACE1_0_0_sim_netlist.vhdl
-- Design      : design_1_RAM_INTERFACE1_0_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7a100tftg256-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  port (
    READ_ADDRESS : in STD_LOGIC_VECTOR ( 8 downto 0 );
    WRITE_ADDRESS_DATA : in STD_LOGIC_VECTOR ( 22 downto 0 );
    CLK_IN : in STD_LOGIC;
    RW_EN : in STD_LOGIC;
    ADDRESS_OUT : out STD_LOGIC_VECTOR ( 8 downto 0 );
    DATA_IN : out STD_LOGIC_VECTOR ( 13 downto 0 );
    RW : out STD_LOGIC
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "design_1_RAM_INTERFACE1_0_0,RAM_INTERFACE1,{}";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "yes";
  attribute IP_DEFINITION_SOURCE : string;
  attribute IP_DEFINITION_SOURCE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "module_ref";
  attribute X_CORE_INFO : string;
  attribute X_CORE_INFO of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "RAM_INTERFACE1,Vivado 2022.2";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  component decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_RAM_INTERFACE1 is
  port (
    READ_ADDRESS : in STD_LOGIC_VECTOR ( 8 downto 0 );
    WRITE_ADDRESS_DATA : in STD_LOGIC_VECTOR ( 22 downto 0 );
    CLK_IN : in STD_LOGIC;
    RW_EN : in STD_LOGIC;
    ADDRESS_OUT : out STD_LOGIC_VECTOR ( 8 downto 0 );
    DATA_IN : out STD_LOGIC_VECTOR ( 13 downto 0 );
    RW : out STD_LOGIC
  );
  end component decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_RAM_INTERFACE1;
begin
inst: component decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_RAM_INTERFACE1
     port map (
      ADDRESS_OUT(8 downto 0) => ADDRESS_OUT(8 downto 0),
      CLK_IN => CLK_IN,
      DATA_IN(13 downto 0) => DATA_IN(13 downto 0),
      READ_ADDRESS(8 downto 0) => READ_ADDRESS(8 downto 0),
      RW => RW,
      RW_EN => RW_EN,
      WRITE_ADDRESS_DATA(22 downto 0) => WRITE_ADDRESS_DATA(22 downto 0)
    );
end STRUCTURE;
