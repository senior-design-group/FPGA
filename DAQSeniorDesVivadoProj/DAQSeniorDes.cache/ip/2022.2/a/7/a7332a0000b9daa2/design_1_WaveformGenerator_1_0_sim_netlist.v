// Copyright 1986-2022 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2022.2 (win64) Build 3671981 Fri Oct 14 05:00:03 MDT 2022
// Date        : Mon Oct 16 16:00:23 2023
// Host        : DESKTOP-QRFTT4G running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_WaveformGenerator_1_0_sim_netlist.v
// Design      : design_1_WaveformGenerator_1_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a100tftg256-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_WaveformGenerator
   (WaveformData,
    clk,
    Address_Data,
    Read_EN);
  output [13:0]WaveformData;
  input clk;
  input [22:0]Address_Data;
  input Read_EN;

  wire [22:0]Address_Data;
  wire Read_EN;
  wire [13:0]WaveformData;
  wire [13:0]WaveformData0;
  wire clk;
  wire curr_address;
  wire curr_address2_carry__0_i_1_n_0;
  wire curr_address2_carry__0_i_2_n_0;
  wire curr_address2_carry__0_n_3;
  wire curr_address2_carry_i_1_n_0;
  wire curr_address2_carry_i_2_n_0;
  wire curr_address2_carry_i_3_n_0;
  wire curr_address2_carry_i_4_n_0;
  wire curr_address2_carry_i_5_n_0;
  wire curr_address2_carry_i_6_n_0;
  wire curr_address2_carry_i_7_n_0;
  wire curr_address2_carry_i_8_n_0;
  wire curr_address2_carry_n_0;
  wire curr_address2_carry_n_1;
  wire curr_address2_carry_n_2;
  wire curr_address2_carry_n_3;
  wire \curr_address[0]_i_1_n_0 ;
  wire \curr_address[1]_i_1_n_0 ;
  wire \curr_address[2]_i_1_n_0 ;
  wire \curr_address[3]_i_1_n_0 ;
  wire \curr_address[6]_i_2_n_0 ;
  wire \curr_address[8]_i_3_n_0 ;
  wire [8:4]curr_address_reg;
  wire [3:0]curr_address_reg_rep;
  wire [8:0]max_address;
  wire max_address0_carry__0_i_1_n_0;
  wire max_address0_carry__0_i_2_n_0;
  wire max_address0_carry__0_n_3;
  wire max_address0_carry_i_1_n_0;
  wire max_address0_carry_i_2_n_0;
  wire max_address0_carry_i_3_n_0;
  wire max_address0_carry_i_4_n_0;
  wire max_address0_carry_i_5_n_0;
  wire max_address0_carry_i_6_n_0;
  wire max_address0_carry_i_7_n_0;
  wire max_address0_carry_i_8_n_0;
  wire max_address0_carry_n_0;
  wire max_address0_carry_n_1;
  wire max_address0_carry_n_2;
  wire max_address0_carry_n_3;
  wire max_address_0;
  wire p_0_in;
  wire [8:4]p_0_in__0;
  wire [3:0]NLW_curr_address2_carry_O_UNCONNECTED;
  wire [3:1]NLW_curr_address2_carry__0_CO_UNCONNECTED;
  wire [3:0]NLW_curr_address2_carry__0_O_UNCONNECTED;
  wire [3:0]NLW_max_address0_carry_O_UNCONNECTED;
  wire [3:1]NLW_max_address0_carry__0_CO_UNCONNECTED;
  wire [3:0]NLW_max_address0_carry__0_O_UNCONNECTED;
  wire [1:0]NLW_storageData_reg_0_15_0_5_DOD_UNCONNECTED;
  wire NLW_storageData_reg_0_15_12_13_SPO_UNCONNECTED;
  wire NLW_storageData_reg_0_15_12_13__0_SPO_UNCONNECTED;
  wire [1:0]NLW_storageData_reg_0_15_6_11_DOD_UNCONNECTED;

  FDRE \WaveformData_reg[0] 
       (.C(clk),
        .CE(Read_EN),
        .D(WaveformData0[0]),
        .Q(WaveformData[0]),
        .R(1'b0));
  FDRE \WaveformData_reg[10] 
       (.C(clk),
        .CE(Read_EN),
        .D(WaveformData0[10]),
        .Q(WaveformData[10]),
        .R(1'b0));
  FDRE \WaveformData_reg[11] 
       (.C(clk),
        .CE(Read_EN),
        .D(WaveformData0[11]),
        .Q(WaveformData[11]),
        .R(1'b0));
  FDRE \WaveformData_reg[12] 
       (.C(clk),
        .CE(Read_EN),
        .D(WaveformData0[12]),
        .Q(WaveformData[12]),
        .R(1'b0));
  FDRE \WaveformData_reg[13] 
       (.C(clk),
        .CE(Read_EN),
        .D(WaveformData0[13]),
        .Q(WaveformData[13]),
        .R(1'b0));
  FDRE \WaveformData_reg[1] 
       (.C(clk),
        .CE(Read_EN),
        .D(WaveformData0[1]),
        .Q(WaveformData[1]),
        .R(1'b0));
  FDRE \WaveformData_reg[2] 
       (.C(clk),
        .CE(Read_EN),
        .D(WaveformData0[2]),
        .Q(WaveformData[2]),
        .R(1'b0));
  FDRE \WaveformData_reg[3] 
       (.C(clk),
        .CE(Read_EN),
        .D(WaveformData0[3]),
        .Q(WaveformData[3]),
        .R(1'b0));
  FDRE \WaveformData_reg[4] 
       (.C(clk),
        .CE(Read_EN),
        .D(WaveformData0[4]),
        .Q(WaveformData[4]),
        .R(1'b0));
  FDRE \WaveformData_reg[5] 
       (.C(clk),
        .CE(Read_EN),
        .D(WaveformData0[5]),
        .Q(WaveformData[5]),
        .R(1'b0));
  FDRE \WaveformData_reg[6] 
       (.C(clk),
        .CE(Read_EN),
        .D(WaveformData0[6]),
        .Q(WaveformData[6]),
        .R(1'b0));
  FDRE \WaveformData_reg[7] 
       (.C(clk),
        .CE(Read_EN),
        .D(WaveformData0[7]),
        .Q(WaveformData[7]),
        .R(1'b0));
  FDRE \WaveformData_reg[8] 
       (.C(clk),
        .CE(Read_EN),
        .D(WaveformData0[8]),
        .Q(WaveformData[8]),
        .R(1'b0));
  FDRE \WaveformData_reg[9] 
       (.C(clk),
        .CE(Read_EN),
        .D(WaveformData0[9]),
        .Q(WaveformData[9]),
        .R(1'b0));
  (* COMPARATOR_THRESHOLD = "11" *) 
  CARRY4 curr_address2_carry
       (.CI(1'b0),
        .CO({curr_address2_carry_n_0,curr_address2_carry_n_1,curr_address2_carry_n_2,curr_address2_carry_n_3}),
        .CYINIT(1'b1),
        .DI({curr_address2_carry_i_1_n_0,curr_address2_carry_i_2_n_0,curr_address2_carry_i_3_n_0,curr_address2_carry_i_4_n_0}),
        .O(NLW_curr_address2_carry_O_UNCONNECTED[3:0]),
        .S({curr_address2_carry_i_5_n_0,curr_address2_carry_i_6_n_0,curr_address2_carry_i_7_n_0,curr_address2_carry_i_8_n_0}));
  (* COMPARATOR_THRESHOLD = "11" *) 
  CARRY4 curr_address2_carry__0
       (.CI(curr_address2_carry_n_0),
        .CO({NLW_curr_address2_carry__0_CO_UNCONNECTED[3:1],curr_address2_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,curr_address2_carry__0_i_1_n_0}),
        .O(NLW_curr_address2_carry__0_O_UNCONNECTED[3:0]),
        .S({1'b0,1'b0,1'b0,curr_address2_carry__0_i_2_n_0}));
  LUT2 #(
    .INIT(4'h2)) 
    curr_address2_carry__0_i_1
       (.I0(max_address[8]),
        .I1(curr_address_reg[8]),
        .O(curr_address2_carry__0_i_1_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    curr_address2_carry__0_i_2
       (.I0(curr_address_reg[8]),
        .I1(max_address[8]),
        .O(curr_address2_carry__0_i_2_n_0));
  LUT4 #(
    .INIT(16'h22B2)) 
    curr_address2_carry_i_1
       (.I0(max_address[7]),
        .I1(curr_address_reg[7]),
        .I2(max_address[6]),
        .I3(curr_address_reg[6]),
        .O(curr_address2_carry_i_1_n_0));
  LUT4 #(
    .INIT(16'h22B2)) 
    curr_address2_carry_i_2
       (.I0(max_address[5]),
        .I1(curr_address_reg[5]),
        .I2(max_address[4]),
        .I3(curr_address_reg[4]),
        .O(curr_address2_carry_i_2_n_0));
  LUT4 #(
    .INIT(16'h22B2)) 
    curr_address2_carry_i_3
       (.I0(max_address[3]),
        .I1(curr_address_reg_rep[3]),
        .I2(max_address[2]),
        .I3(curr_address_reg_rep[2]),
        .O(curr_address2_carry_i_3_n_0));
  LUT4 #(
    .INIT(16'h22B2)) 
    curr_address2_carry_i_4
       (.I0(max_address[1]),
        .I1(curr_address_reg_rep[1]),
        .I2(max_address[0]),
        .I3(curr_address_reg_rep[0]),
        .O(curr_address2_carry_i_4_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    curr_address2_carry_i_5
       (.I0(curr_address_reg[7]),
        .I1(max_address[7]),
        .I2(curr_address_reg[6]),
        .I3(max_address[6]),
        .O(curr_address2_carry_i_5_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    curr_address2_carry_i_6
       (.I0(curr_address_reg[5]),
        .I1(max_address[5]),
        .I2(curr_address_reg[4]),
        .I3(max_address[4]),
        .O(curr_address2_carry_i_6_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    curr_address2_carry_i_7
       (.I0(curr_address_reg_rep[3]),
        .I1(max_address[3]),
        .I2(curr_address_reg_rep[2]),
        .I3(max_address[2]),
        .O(curr_address2_carry_i_7_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    curr_address2_carry_i_8
       (.I0(curr_address_reg_rep[1]),
        .I1(max_address[1]),
        .I2(curr_address_reg_rep[0]),
        .I3(max_address[0]),
        .O(curr_address2_carry_i_8_n_0));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT3 #(
    .INIT(8'h3A)) 
    \curr_address[0]_i_1 
       (.I0(Address_Data[14]),
        .I1(curr_address_reg_rep[0]),
        .I2(Read_EN),
        .O(\curr_address[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT4 #(
    .INIT(16'h6F60)) 
    \curr_address[1]_i_1 
       (.I0(curr_address_reg_rep[0]),
        .I1(curr_address_reg_rep[1]),
        .I2(Read_EN),
        .I3(Address_Data[15]),
        .O(\curr_address[1]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h6AFF6A00)) 
    \curr_address[2]_i_1 
       (.I0(curr_address_reg_rep[2]),
        .I1(curr_address_reg_rep[0]),
        .I2(curr_address_reg_rep[1]),
        .I3(Read_EN),
        .I4(Address_Data[16]),
        .O(\curr_address[2]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h7F80FFFF7F800000)) 
    \curr_address[3]_i_1 
       (.I0(curr_address_reg_rep[1]),
        .I1(curr_address_reg_rep[0]),
        .I2(curr_address_reg_rep[2]),
        .I3(curr_address_reg_rep[3]),
        .I4(Read_EN),
        .I5(Address_Data[17]),
        .O(\curr_address[3]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h6F60)) 
    \curr_address[4]_i_1 
       (.I0(curr_address_reg[4]),
        .I1(\curr_address[6]_i_2_n_0 ),
        .I2(Read_EN),
        .I3(Address_Data[18]),
        .O(p_0_in__0[4]));
  LUT5 #(
    .INIT(32'h6AFF6A00)) 
    \curr_address[5]_i_1 
       (.I0(curr_address_reg[5]),
        .I1(\curr_address[6]_i_2_n_0 ),
        .I2(curr_address_reg[4]),
        .I3(Read_EN),
        .I4(Address_Data[19]),
        .O(p_0_in__0[5]));
  LUT6 #(
    .INIT(64'h6AAAFFFF6AAA0000)) 
    \curr_address[6]_i_1 
       (.I0(curr_address_reg[6]),
        .I1(curr_address_reg[4]),
        .I2(\curr_address[6]_i_2_n_0 ),
        .I3(curr_address_reg[5]),
        .I4(Read_EN),
        .I5(Address_Data[20]),
        .O(p_0_in__0[6]));
  LUT4 #(
    .INIT(16'h8000)) 
    \curr_address[6]_i_2 
       (.I0(curr_address_reg_rep[3]),
        .I1(curr_address_reg_rep[2]),
        .I2(curr_address_reg_rep[0]),
        .I3(curr_address_reg_rep[1]),
        .O(\curr_address[6]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h6AFF6A00)) 
    \curr_address[7]_i_1 
       (.I0(curr_address_reg[7]),
        .I1(\curr_address[8]_i_3_n_0 ),
        .I2(curr_address_reg[6]),
        .I3(Read_EN),
        .I4(Address_Data[21]),
        .O(p_0_in__0[7]));
  LUT2 #(
    .INIT(4'h2)) 
    \curr_address[8]_i_1 
       (.I0(Read_EN),
        .I1(curr_address2_carry__0_n_3),
        .O(curr_address));
  LUT6 #(
    .INIT(64'h6AAAFFFF6AAA0000)) 
    \curr_address[8]_i_2 
       (.I0(curr_address_reg[8]),
        .I1(curr_address_reg[6]),
        .I2(\curr_address[8]_i_3_n_0 ),
        .I3(curr_address_reg[7]),
        .I4(Read_EN),
        .I5(Address_Data[22]),
        .O(p_0_in__0[8]));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \curr_address[8]_i_3 
       (.I0(curr_address_reg[5]),
        .I1(curr_address_reg_rep[3]),
        .I2(curr_address_reg_rep[2]),
        .I3(curr_address_reg_rep[0]),
        .I4(curr_address_reg_rep[1]),
        .I5(curr_address_reg[4]),
        .O(\curr_address[8]_i_3_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \curr_address_reg[0] 
       (.C(clk),
        .CE(1'b1),
        .D(\curr_address[0]_i_1_n_0 ),
        .Q(curr_address_reg_rep[0]),
        .R(curr_address));
  FDRE #(
    .INIT(1'b0)) 
    \curr_address_reg[1] 
       (.C(clk),
        .CE(1'b1),
        .D(\curr_address[1]_i_1_n_0 ),
        .Q(curr_address_reg_rep[1]),
        .R(curr_address));
  FDRE #(
    .INIT(1'b0)) 
    \curr_address_reg[2] 
       (.C(clk),
        .CE(1'b1),
        .D(\curr_address[2]_i_1_n_0 ),
        .Q(curr_address_reg_rep[2]),
        .R(curr_address));
  FDRE #(
    .INIT(1'b0)) 
    \curr_address_reg[3] 
       (.C(clk),
        .CE(1'b1),
        .D(\curr_address[3]_i_1_n_0 ),
        .Q(curr_address_reg_rep[3]),
        .R(curr_address));
  FDRE #(
    .INIT(1'b0)) 
    \curr_address_reg[4] 
       (.C(clk),
        .CE(1'b1),
        .D(p_0_in__0[4]),
        .Q(curr_address_reg[4]),
        .R(curr_address));
  FDRE #(
    .INIT(1'b0)) 
    \curr_address_reg[5] 
       (.C(clk),
        .CE(1'b1),
        .D(p_0_in__0[5]),
        .Q(curr_address_reg[5]),
        .R(curr_address));
  FDRE #(
    .INIT(1'b0)) 
    \curr_address_reg[6] 
       (.C(clk),
        .CE(1'b1),
        .D(p_0_in__0[6]),
        .Q(curr_address_reg[6]),
        .R(curr_address));
  FDRE #(
    .INIT(1'b0)) 
    \curr_address_reg[7] 
       (.C(clk),
        .CE(1'b1),
        .D(p_0_in__0[7]),
        .Q(curr_address_reg[7]),
        .R(curr_address));
  FDRE #(
    .INIT(1'b0)) 
    \curr_address_reg[8] 
       (.C(clk),
        .CE(1'b1),
        .D(p_0_in__0[8]),
        .Q(curr_address_reg[8]),
        .R(curr_address));
  (* COMPARATOR_THRESHOLD = "11" *) 
  CARRY4 max_address0_carry
       (.CI(1'b0),
        .CO({max_address0_carry_n_0,max_address0_carry_n_1,max_address0_carry_n_2,max_address0_carry_n_3}),
        .CYINIT(1'b0),
        .DI({max_address0_carry_i_1_n_0,max_address0_carry_i_2_n_0,max_address0_carry_i_3_n_0,max_address0_carry_i_4_n_0}),
        .O(NLW_max_address0_carry_O_UNCONNECTED[3:0]),
        .S({max_address0_carry_i_5_n_0,max_address0_carry_i_6_n_0,max_address0_carry_i_7_n_0,max_address0_carry_i_8_n_0}));
  (* COMPARATOR_THRESHOLD = "11" *) 
  CARRY4 max_address0_carry__0
       (.CI(max_address0_carry_n_0),
        .CO({NLW_max_address0_carry__0_CO_UNCONNECTED[3:1],max_address0_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,max_address0_carry__0_i_1_n_0}),
        .O(NLW_max_address0_carry__0_O_UNCONNECTED[3:0]),
        .S({1'b0,1'b0,1'b0,max_address0_carry__0_i_2_n_0}));
  LUT2 #(
    .INIT(4'h2)) 
    max_address0_carry__0_i_1
       (.I0(Address_Data[22]),
        .I1(max_address[8]),
        .O(max_address0_carry__0_i_1_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    max_address0_carry__0_i_2
       (.I0(max_address[8]),
        .I1(Address_Data[22]),
        .O(max_address0_carry__0_i_2_n_0));
  LUT4 #(
    .INIT(16'h22B2)) 
    max_address0_carry_i_1
       (.I0(Address_Data[21]),
        .I1(max_address[7]),
        .I2(Address_Data[20]),
        .I3(max_address[6]),
        .O(max_address0_carry_i_1_n_0));
  LUT4 #(
    .INIT(16'h22B2)) 
    max_address0_carry_i_2
       (.I0(Address_Data[19]),
        .I1(max_address[5]),
        .I2(Address_Data[18]),
        .I3(max_address[4]),
        .O(max_address0_carry_i_2_n_0));
  LUT4 #(
    .INIT(16'h22B2)) 
    max_address0_carry_i_3
       (.I0(Address_Data[17]),
        .I1(max_address[3]),
        .I2(Address_Data[16]),
        .I3(max_address[2]),
        .O(max_address0_carry_i_3_n_0));
  LUT4 #(
    .INIT(16'h22B2)) 
    max_address0_carry_i_4
       (.I0(Address_Data[15]),
        .I1(max_address[1]),
        .I2(Address_Data[14]),
        .I3(max_address[0]),
        .O(max_address0_carry_i_4_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    max_address0_carry_i_5
       (.I0(max_address[7]),
        .I1(Address_Data[21]),
        .I2(max_address[6]),
        .I3(Address_Data[20]),
        .O(max_address0_carry_i_5_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    max_address0_carry_i_6
       (.I0(max_address[5]),
        .I1(Address_Data[19]),
        .I2(max_address[4]),
        .I3(Address_Data[18]),
        .O(max_address0_carry_i_6_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    max_address0_carry_i_7
       (.I0(max_address[3]),
        .I1(Address_Data[17]),
        .I2(max_address[2]),
        .I3(Address_Data[16]),
        .O(max_address0_carry_i_7_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    max_address0_carry_i_8
       (.I0(max_address[1]),
        .I1(Address_Data[15]),
        .I2(max_address[0]),
        .I3(Address_Data[14]),
        .O(max_address0_carry_i_8_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    \max_address[8]_i_1 
       (.I0(max_address0_carry__0_n_3),
        .I1(Read_EN),
        .O(max_address_0));
  FDRE #(
    .INIT(1'b0)) 
    \max_address_reg[0] 
       (.C(clk),
        .CE(max_address_0),
        .D(Address_Data[14]),
        .Q(max_address[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \max_address_reg[1] 
       (.C(clk),
        .CE(max_address_0),
        .D(Address_Data[15]),
        .Q(max_address[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \max_address_reg[2] 
       (.C(clk),
        .CE(max_address_0),
        .D(Address_Data[16]),
        .Q(max_address[2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \max_address_reg[3] 
       (.C(clk),
        .CE(max_address_0),
        .D(Address_Data[17]),
        .Q(max_address[3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \max_address_reg[4] 
       (.C(clk),
        .CE(max_address_0),
        .D(Address_Data[18]),
        .Q(max_address[4]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \max_address_reg[5] 
       (.C(clk),
        .CE(max_address_0),
        .D(Address_Data[19]),
        .Q(max_address[5]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \max_address_reg[6] 
       (.C(clk),
        .CE(max_address_0),
        .D(Address_Data[20]),
        .Q(max_address[6]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \max_address_reg[7] 
       (.C(clk),
        .CE(max_address_0),
        .D(Address_Data[21]),
        .Q(max_address[7]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \max_address_reg[8] 
       (.C(clk),
        .CE(max_address_0),
        .D(Address_Data[22]),
        .Q(max_address[8]),
        .R(1'b0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  (* RTL_RAM_BITS = "140" *) 
  (* RTL_RAM_NAME = "inst/storageData_reg_0_15_0_5" *) 
  (* RTL_RAM_TYPE = "RAM_SDP" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "9" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "0" *) 
  (* ram_slice_end = "5" *) 
  RAM32M storageData_reg_0_15_0_5
       (.ADDRA({1'b0,curr_address_reg_rep}),
        .ADDRB({1'b0,curr_address_reg_rep}),
        .ADDRC({1'b0,curr_address_reg_rep}),
        .ADDRD({1'b0,Address_Data[17:14]}),
        .DIA(Address_Data[1:0]),
        .DIB(Address_Data[3:2]),
        .DIC(Address_Data[5:4]),
        .DID({1'b0,1'b0}),
        .DOA(WaveformData0[1:0]),
        .DOB(WaveformData0[3:2]),
        .DOC(WaveformData0[5:4]),
        .DOD(NLW_storageData_reg_0_15_0_5_DOD_UNCONNECTED[1:0]),
        .WCLK(clk),
        .WE(p_0_in));
  LUT1 #(
    .INIT(2'h1)) 
    storageData_reg_0_15_0_5_i_1
       (.I0(Read_EN),
        .O(p_0_in));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  (* RTL_RAM_BITS = "140" *) 
  (* RTL_RAM_NAME = "inst/storageData_reg_0_15_12_13" *) 
  (* RTL_RAM_TYPE = "RAM_SDP" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "9" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "12" *) 
  (* ram_slice_end = "13" *) 
  RAM32X1D storageData_reg_0_15_12_13
       (.A0(Address_Data[14]),
        .A1(Address_Data[15]),
        .A2(Address_Data[16]),
        .A3(Address_Data[17]),
        .A4(1'b0),
        .D(Address_Data[12]),
        .DPO(WaveformData0[12]),
        .DPRA0(curr_address_reg_rep[0]),
        .DPRA1(curr_address_reg_rep[1]),
        .DPRA2(curr_address_reg_rep[2]),
        .DPRA3(curr_address_reg_rep[3]),
        .DPRA4(1'b0),
        .SPO(NLW_storageData_reg_0_15_12_13_SPO_UNCONNECTED),
        .WCLK(clk),
        .WE(p_0_in));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  (* RTL_RAM_BITS = "140" *) 
  (* RTL_RAM_NAME = "inst/storageData_reg_0_15_12_13" *) 
  (* RTL_RAM_TYPE = "RAM_SDP" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "9" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "12" *) 
  (* ram_slice_end = "13" *) 
  RAM32X1D storageData_reg_0_15_12_13__0
       (.A0(Address_Data[14]),
        .A1(Address_Data[15]),
        .A2(Address_Data[16]),
        .A3(Address_Data[17]),
        .A4(1'b0),
        .D(Address_Data[13]),
        .DPO(WaveformData0[13]),
        .DPRA0(curr_address_reg_rep[0]),
        .DPRA1(curr_address_reg_rep[1]),
        .DPRA2(curr_address_reg_rep[2]),
        .DPRA3(curr_address_reg_rep[3]),
        .DPRA4(1'b0),
        .SPO(NLW_storageData_reg_0_15_12_13__0_SPO_UNCONNECTED),
        .WCLK(clk),
        .WE(p_0_in));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  (* RTL_RAM_BITS = "140" *) 
  (* RTL_RAM_NAME = "inst/storageData_reg_0_15_6_11" *) 
  (* RTL_RAM_TYPE = "RAM_SDP" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "9" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "6" *) 
  (* ram_slice_end = "11" *) 
  RAM32M storageData_reg_0_15_6_11
       (.ADDRA({1'b0,curr_address_reg_rep}),
        .ADDRB({1'b0,curr_address_reg_rep}),
        .ADDRC({1'b0,curr_address_reg_rep}),
        .ADDRD({1'b0,Address_Data[17:14]}),
        .DIA(Address_Data[7:6]),
        .DIB(Address_Data[9:8]),
        .DIC(Address_Data[11:10]),
        .DID({1'b0,1'b0}),
        .DOA(WaveformData0[7:6]),
        .DOB(WaveformData0[9:8]),
        .DOC(WaveformData0[11:10]),
        .DOD(NLW_storageData_reg_0_15_6_11_DOD_UNCONNECTED[1:0]),
        .WCLK(clk),
        .WE(p_0_in));
endmodule

(* CHECK_LICENSE_TYPE = "design_1_WaveformGenerator_1_0,WaveformGenerator,{}" *) (* DowngradeIPIdentifiedWarnings = "yes" *) (* IP_DEFINITION_SOURCE = "module_ref" *) 
(* X_CORE_INFO = "WaveformGenerator,Vivado 2022.2" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (Address_Data,
    Read_EN,
    clk,
    WaveformData);
  input [22:0]Address_Data;
  input Read_EN;
  (* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 clk CLK" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME clk, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, PHASE 0.0, CLK_DOMAIN /clk_wiz_1_clk_out1, INSERT_VIP 0" *) input clk;
  output [13:0]WaveformData;

  wire [22:0]Address_Data;
  wire Read_EN;
  wire [13:0]WaveformData;
  wire clk;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_WaveformGenerator inst
       (.Address_Data(Address_Data),
        .Read_EN(Read_EN),
        .WaveformData(WaveformData),
        .clk(clk));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
