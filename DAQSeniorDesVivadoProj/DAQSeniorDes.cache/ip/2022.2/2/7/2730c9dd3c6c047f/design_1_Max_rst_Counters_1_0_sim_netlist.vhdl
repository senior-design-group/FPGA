-- Copyright 1986-2022 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2022.2 (win64) Build 3671981 Fri Oct 14 05:00:03 MDT 2022
-- Date        : Sun Oct 15 16:23:24 2023
-- Host        : DESKTOP-QRFTT4G running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
--               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_Max_rst_Counters_1_0_sim_netlist.vhdl
-- Design      : design_1_Max_rst_Counters_1_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7a100tftg256-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Max_rst_Counters is
  port (
    Reset_Count : out STD_LOGIC;
    Max_Val : in STD_LOGIC_VECTOR ( 8 downto 0 );
    Curr_Val : in STD_LOGIC_VECTOR ( 8 downto 0 );
    clk : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Max_rst_Counters;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Max_rst_Counters is
  signal \Reset_Count0_carry__0_i_1_n_0\ : STD_LOGIC;
  signal \Reset_Count0_carry__0_i_2_n_0\ : STD_LOGIC;
  signal Reset_Count0_carry_i_10_n_0 : STD_LOGIC;
  signal Reset_Count0_carry_i_1_n_0 : STD_LOGIC;
  signal Reset_Count0_carry_i_2_n_0 : STD_LOGIC;
  signal Reset_Count0_carry_i_3_n_0 : STD_LOGIC;
  signal Reset_Count0_carry_i_4_n_0 : STD_LOGIC;
  signal Reset_Count0_carry_i_5_n_0 : STD_LOGIC;
  signal Reset_Count0_carry_i_6_n_0 : STD_LOGIC;
  signal Reset_Count0_carry_i_7_n_0 : STD_LOGIC;
  signal Reset_Count0_carry_i_8_n_0 : STD_LOGIC;
  signal Reset_Count0_carry_i_9_n_0 : STD_LOGIC;
  signal Reset_Count0_carry_n_0 : STD_LOGIC;
  signal Reset_Count0_carry_n_1 : STD_LOGIC;
  signal Reset_Count0_carry_n_2 : STD_LOGIC;
  signal Reset_Count0_carry_n_3 : STD_LOGIC;
  signal p_0_in : STD_LOGIC;
  signal NLW_Reset_Count0_carry_O_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_Reset_Count0_carry__0_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_Reset_Count0_carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  attribute COMPARATOR_THRESHOLD : integer;
  attribute COMPARATOR_THRESHOLD of Reset_Count0_carry : label is 11;
  attribute COMPARATOR_THRESHOLD of \Reset_Count0_carry__0\ : label is 11;
begin
Reset_Count0_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => Reset_Count0_carry_n_0,
      CO(2) => Reset_Count0_carry_n_1,
      CO(1) => Reset_Count0_carry_n_2,
      CO(0) => Reset_Count0_carry_n_3,
      CYINIT => '1',
      DI(3) => Reset_Count0_carry_i_1_n_0,
      DI(2) => Reset_Count0_carry_i_2_n_0,
      DI(1) => Reset_Count0_carry_i_3_n_0,
      DI(0) => Reset_Count0_carry_i_4_n_0,
      O(3 downto 0) => NLW_Reset_Count0_carry_O_UNCONNECTED(3 downto 0),
      S(3) => Reset_Count0_carry_i_5_n_0,
      S(2) => Reset_Count0_carry_i_6_n_0,
      S(1) => Reset_Count0_carry_i_7_n_0,
      S(0) => Reset_Count0_carry_i_8_n_0
    );
\Reset_Count0_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => Reset_Count0_carry_n_0,
      CO(3 downto 1) => \NLW_Reset_Count0_carry__0_CO_UNCONNECTED\(3 downto 1),
      CO(0) => p_0_in,
      CYINIT => '0',
      DI(3 downto 1) => B"000",
      DI(0) => \Reset_Count0_carry__0_i_1_n_0\,
      O(3 downto 0) => \NLW_Reset_Count0_carry__0_O_UNCONNECTED\(3 downto 0),
      S(3 downto 1) => B"000",
      S(0) => \Reset_Count0_carry__0_i_2_n_0\
    );
\Reset_Count0_carry__0_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"55560000"
    )
        port map (
      I0 => Max_Val(8),
      I1 => Max_Val(6),
      I2 => Reset_Count0_carry_i_9_n_0,
      I3 => Max_Val(7),
      I4 => Curr_Val(8),
      O => \Reset_Count0_carry__0_i_1_n_0\
    );
\Reset_Count0_carry__0_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAA95556"
    )
        port map (
      I0 => Max_Val(8),
      I1 => Max_Val(6),
      I2 => Reset_Count0_carry_i_9_n_0,
      I3 => Max_Val(7),
      I4 => Curr_Val(8),
      O => \Reset_Count0_carry__0_i_2_n_0\
    );
Reset_Count0_carry_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"088CCEE0"
    )
        port map (
      I0 => Curr_Val(6),
      I1 => Curr_Val(7),
      I2 => Max_Val(6),
      I3 => Reset_Count0_carry_i_9_n_0,
      I4 => Max_Val(7),
      O => Reset_Count0_carry_i_1_n_0
    );
Reset_Count0_carry_i_10: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => Max_Val(2),
      I1 => Max_Val(0),
      I2 => Max_Val(1),
      I3 => Max_Val(3),
      O => Reset_Count0_carry_i_10_n_0
    );
Reset_Count0_carry_i_2: unisim.vcomponents.LUT5
    generic map(
      INIT => X"088CCEE0"
    )
        port map (
      I0 => Curr_Val(4),
      I1 => Curr_Val(5),
      I2 => Max_Val(4),
      I3 => Reset_Count0_carry_i_10_n_0,
      I4 => Max_Val(5),
      O => Reset_Count0_carry_i_2_n_0
    );
Reset_Count0_carry_i_3: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0808088CCECECEE0"
    )
        port map (
      I0 => Curr_Val(2),
      I1 => Curr_Val(3),
      I2 => Max_Val(2),
      I3 => Max_Val(0),
      I4 => Max_Val(1),
      I5 => Max_Val(3),
      O => Reset_Count0_carry_i_3_n_0
    );
Reset_Count0_carry_i_4: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8CE0"
    )
        port map (
      I0 => Curr_Val(0),
      I1 => Curr_Val(1),
      I2 => Max_Val(0),
      I3 => Max_Val(1),
      O => Reset_Count0_carry_i_4_n_0
    );
Reset_Count0_carry_i_5: unisim.vcomponents.LUT5
    generic map(
      INIT => X"84422118"
    )
        port map (
      I0 => Curr_Val(6),
      I1 => Curr_Val(7),
      I2 => Max_Val(6),
      I3 => Reset_Count0_carry_i_9_n_0,
      I4 => Max_Val(7),
      O => Reset_Count0_carry_i_5_n_0
    );
Reset_Count0_carry_i_6: unisim.vcomponents.LUT5
    generic map(
      INIT => X"84422118"
    )
        port map (
      I0 => Curr_Val(4),
      I1 => Curr_Val(5),
      I2 => Max_Val(4),
      I3 => Reset_Count0_carry_i_10_n_0,
      I4 => Max_Val(5),
      O => Reset_Count0_carry_i_6_n_0
    );
Reset_Count0_carry_i_7: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8484844221212118"
    )
        port map (
      I0 => Curr_Val(2),
      I1 => Curr_Val(3),
      I2 => Max_Val(2),
      I3 => Max_Val(0),
      I4 => Max_Val(1),
      I5 => Max_Val(3),
      O => Reset_Count0_carry_i_7_n_0
    );
Reset_Count0_carry_i_8: unisim.vcomponents.LUT4
    generic map(
      INIT => X"4218"
    )
        port map (
      I0 => Curr_Val(0),
      I1 => Curr_Val(1),
      I2 => Max_Val(0),
      I3 => Max_Val(1),
      O => Reset_Count0_carry_i_8_n_0
    );
Reset_Count0_carry_i_9: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => Max_Val(4),
      I1 => Max_Val(2),
      I2 => Max_Val(0),
      I3 => Max_Val(1),
      I4 => Max_Val(3),
      I5 => Max_Val(5),
      O => Reset_Count0_carry_i_9_n_0
    );
Reset_Count_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '1',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk,
      CE => '1',
      D => p_0_in,
      Q => Reset_Count,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  port (
    Max_Val : in STD_LOGIC_VECTOR ( 8 downto 0 );
    Load_Val : in STD_LOGIC;
    Curr_Val : in STD_LOGIC_VECTOR ( 8 downto 0 );
    clk : in STD_LOGIC;
    Reset_Count : out STD_LOGIC
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "design_1_Max_rst_Counters_1_0,Max_rst_Counters,{}";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "yes";
  attribute IP_DEFINITION_SOURCE : string;
  attribute IP_DEFINITION_SOURCE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "module_ref";
  attribute X_CORE_INFO : string;
  attribute X_CORE_INFO of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "Max_rst_Counters,Vivado 2022.2";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  attribute X_INTERFACE_INFO : string;
  attribute X_INTERFACE_INFO of Reset_Count : signal is "xilinx.com:signal:reset:1.0 Reset_Count RST";
  attribute X_INTERFACE_PARAMETER : string;
  attribute X_INTERFACE_PARAMETER of Reset_Count : signal is "XIL_INTERFACENAME Reset_Count, POLARITY ACTIVE_LOW, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of clk : signal is "xilinx.com:signal:clock:1.0 clk CLK";
  attribute X_INTERFACE_PARAMETER of clk : signal is "XIL_INTERFACENAME clk, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, PHASE 0.0, CLK_DOMAIN /clk_wiz_1_clk_out1, INSERT_VIP 0";
begin
inst: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Max_rst_Counters
     port map (
      Curr_Val(8 downto 0) => Curr_Val(8 downto 0),
      Max_Val(8 downto 0) => Max_Val(8 downto 0),
      Reset_Count => Reset_Count,
      clk => clk
    );
end STRUCTURE;
