// Copyright 1986-2022 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2022.2 (win64) Build 3671981 Fri Oct 14 05:00:03 MDT 2022
// Date        : Sat Oct 14 11:53:07 2023
// Host        : DESKTOP-QRFTT4G running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_c_counter_binary_0_0_sim_netlist.v
// Design      : design_1_c_counter_binary_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a100tftg256-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_1_c_counter_binary_0_0,c_counter_binary_v12_0_15,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "c_counter_binary_v12_0_15,Vivado 2022.2" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (CLK,
    CE,
    Q);
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk_intf CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:thresh0_intf:l_intf:load_intf:up_intf:sinit_intf:sset_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, PHASE 0.0, CLK_DOMAIN /clk_wiz_1_clk_out1, INSERT_VIP 0" *) input CLK;
  (* x_interface_info = "xilinx.com:signal:clockenable:1.0 ce_intf CE" *) (* x_interface_parameter = "XIL_INTERFACENAME ce_intf, POLARITY ACTIVE_HIGH" *) input CE;
  (* x_interface_info = "xilinx.com:signal:data:1.0 q_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME q_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency bitwidth format long minimum {} maximum {}} value 9} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0} integer {signed {attribs {resolve_type immediate dependency {} format bool minimum {} maximum {}} value false}}}} DATA_WIDTH 9}" *) output [8:0]Q;

  wire CE;
  wire CLK;
  wire [8:0]Q;
  wire NLW_U0_THRESH0_UNCONNECTED;

  (* C_AINIT_VAL = "0" *) 
  (* C_CE_OVERRIDES_SYNC = "0" *) 
  (* C_FB_LATENCY = "0" *) 
  (* C_HAS_CE = "1" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_IMPLEMENTATION = "0" *) 
  (* C_SCLR_OVERRIDES_SSET = "1" *) 
  (* C_SINIT_VAL = "0" *) 
  (* C_VERBOSITY = "0" *) 
  (* C_WIDTH = "9" *) 
  (* C_XDEVICEFAMILY = "artix7" *) 
  (* c_count_by = "1" *) 
  (* c_count_mode = "0" *) 
  (* c_count_to = "1" *) 
  (* c_has_load = "0" *) 
  (* c_has_thresh0 = "0" *) 
  (* c_latency = "1" *) 
  (* c_load_low = "0" *) 
  (* c_restrict_count = "0" *) 
  (* c_thresh0_value = "1" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  (* is_du_within_envelope = "true" *) 
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_15 U0
       (.CE(CE),
        .CLK(CLK),
        .L({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .LOAD(1'b0),
        .Q(Q),
        .SCLR(1'b0),
        .SINIT(1'b0),
        .SSET(1'b0),
        .THRESH0(NLW_U0_THRESH0_UNCONNECTED),
        .UP(1'b1));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2022.2"
`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
KdkdvVsuosc8qR9X5PxQ/ghTeTrEz4qKVuenhDR9wRSL/BO/mhSwQtiFj74UO0sGv0zvjAntaq/3
l2/v8gOiVKmM666gbk/2UCISA4OFA3FDR9jYmiXdNXb2qHeS1ywQz5n/sTR5iu4KFEfwrl3IXtQw
aEiGegL+CQMaovJsto4=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
pZCj3qT3VD1SCS5RiZExsqqu16KpMtHXilQL9p5/eBl7qrfQjT1VhFtVbYUusepbChjsCCmCn7hr
72SuHmOmDWG78UARN7MLdO/+sePuyS06ak4nAw5xwjT0g+9970uMWYKvTeeYqoz2i+k+zX60Cuvu
iwBfxWM22DqukHlYzbEFWhNyXIkgJe71p67vGdXBmqu4/2wmlwGApqBxlwR+alwZ9UGHlxNQS4N5
z1wHu3Cp8LwGRjlaXjElcY8RDpvyz5l59ey8ar5HXR9Zqf6e1unE2NdhzHhEGRerRFXoKZppk1HB
6kIEY4EHAWz+HvPcqoP9eoYKDazoAGkJRVP6YA==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
gLgm7VvY3cNcNvdXvikCQd2nRniE4ae4hePOcAUlPDMoHDzQAD7Ngo12MGFns9JNPcCaUXfAmxL2
JNGojjrDRUWrv8FPV6FOEbDHs96fef8+gqLF4OqLck4kWpKhnJwaJjjzQirvXEzZxP+GsBKnkSp8
ceVlZJwP0F6XRv+RpQA=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
GeZP242oKQSNuofqDs4oIIXZEufPhRVrlFFeRSLY4VCxhMEMwfPrNXe33xO0zIEBoPW2X9mvUoTY
izdWQEtWImFzjzPCjkSLhEdIMmUBH02Y+Tw3eW5x23T0cK96pmoV2MH8kl99I27MN6stVd977fuB
Mjao5MnSXIGZ/uXGtgfUO9Zjs4/2wGmsI2/lANN2WOL9Sz4xeA8k40c2dNYgxgHoCwx8Ya/RYIZS
Cpuvzq4ZyFSNT/kMXnUmqj75/flpXT3mmyW+frexux3j9PxpKHmxAE9crvDx85rMamGiA4ftl+ac
H0FtL2cBqdlP60x+FjqleWCJoN6AYdxA0YZaeg==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
URmEGftuxvv0+tViRUdsFNnPXucZlVDfUQpjjXkpOA38QUzsIL9j1pGGp9doC4jcg/9MD149BTSw
vAG8684a3k+Tx/8sFGl/viK1q8ty9nktEABSahv8Etm5ZJVAzQJT7EaOzrYqyywSwabogvGUmN/7
DE3eOn6+sMCiMl6BLUhYyK39ntTWNFYVPiheclbBb36V1vzMOQl0mvPuS4hDXqba/+qBZXhqeYWK
ceNfwci6SsRRef6hLF/1S+20r2uBxJeYJjyfWGGFEGfxlAOz1MiYUUR/bEHWnbjwIcJTBHQNRdq4
4Ryb+iPuKcsXU/8ApD14i6ScW+VBPWSqnH9w+A==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2021_07", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
NtQgA3rUKfJt+21sTot44yr4gmte57FoFl8Q/327tsRJeEyNAiwWZaZN2mbo2NFcvyN2GhDw6avJ
NsF1Oxs36P8shoqOOiloWWrdTcyAdMhdk+UjeZgKcNSqd4Js87w/5LVQTwjB2mcBDfe1jrivv+IW
ZRBC8NvlW5z/1wF7+vzXRMziLQYeOkLB0OkpIY+eT5cZXDKuZ+4l0FMPjd+El96JGAEHG7Q0qS3F
OEApYEp8+nSZnragoytq4pkhVJEC22ye0hBhoBClJpszCcg0u+Ugf+mYZsj8BC2uqSY6Hh/gpjjw
enQ7aEYBaUR7GCwQN7fZmNhZYtBkyvNqydRQcA==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
CpIFM8Y8dBmpjtOVnOKcfppEFV+c1cRgsQtewNUe+5apiLDoRCdMyTqoCay7nz+Xagc0OvfZDg/Y
jSTsDjKVcEIyxOfix7iwjKW8Rz+a5wBIatI8wfCo7uLtuucz9otOWWI7BFQ2gn4VdQ73HJJlZMMY
OyEOd33tGjNSjxz3W07knDr1FwTE3BOfhq+Qj2ErnuV1dQbrTb3MiQMTnHaTCwtz6ip0pD6b5G4K
kBRUYe+UNXCMvSfNIN9MPSmolO4MjNwM5gnZZqLcR1hGuzH/Yeb/jPnhsZ7jFvlTT3nsM9JzMRAE
QwlzVuulHKQDS2I96arFosYPYMsalmn6CQW0gg==

`pragma protect key_keyowner="Atrenta", key_keyname="ATR-SG-RSA-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=384)
`pragma protect key_block
qinIxHFISC9r9LS7OKOuYVGM5EBkuuQNV1nDRui+QVNLn2QFCrWPeEClQIlNViKOt8MX9urHvu4e
l2L+eZKw6+St9cW9yUsYu36yoB4LqwG+vKvfR9CW82LGPyMAxdgk/p3n+F0Xp9Y2HaERwWDL99tW
V7cDvLLhyIwz7w4rI0BWWV+KMjXP2F5MNgykzZn7tzV8oY6MxOykFqRdI8DLAdlYGAs90wjJ3x84
S3fHciSox97FYpDi64v31Vb4RmRrwueXcvCc3w8gzjuwg7qraWLMYyPB+mERB2v1htX80PsWWVHE
QXkWiHWYvvrXEykUS04MmLNHpV8ZgBXO/NBEGn7mrITDEswk3u1Yviqy7CW2wLPQBoo5xW+uiu2e
8YZV/E+bAt+P/EH5RsC9alBgtuVKU1s9DaiEH8eUPEgJQ/TXwQW01pg8ECTYgiBS+IQSbld23aq3
goVo0ZMzRu/SA00Jmwt7upvsMkh9Q+2732ahu1FmlSNmyNGB1+bYf782

`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="CDS_RSA_KEY_VER_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
T1jqx5hmzZZMhPApzUC1oZLMAkHma8Ki4b2CvLNqxSn+MNWoTPomvQ775DMBEDai/gahYALsohdX
0f/e6LuPqt4zYtyAzmH+nRgOG/tilS1J674KsaHxudAfo4sM3awB/C4Q3VdYsO9FgvPQylnYKSGE
gJ46W+1Y789VQqPbt4dpnprhix6sLlwfww7We6cq2wu4PilFzovejouUBZqNMZHYi4suKcMcenp3
C7QRKloo8IF9yKrhGPcRJLQt2nus3bI0Q3ICxRk13Nrfhh/z4cdm0OGXz42q44snFEVy1lLxPOs7
W9tSe5ag3923oCT4NGGgK/gMTx5qXxFhV2MJUw==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
dtKpt7KssVy683CgihR1WznLLl9BQ4IK+6lPvX0cdfuBvNHvef7Web7pfyBxTHVHNhl1+qP/nKTU
8L08GMmdCRRFZaMG3nDgNn0KS2Ehl/4+38cWG6rMZMViGLNxM/O+kjkLymSsi7weWELts5wlr800
WxMcDcdbw6OQk29WI0xpDVaNCmrocsViYdWPkpktYvrAArPS5AlznyxDFmHV02SxtwuIYFTbA3XV
5Z03EQig+rsYVIhSeIpT9A+ZKXt0SptLMWRzxwQ9jjhcAEtudmhN3j/vdfecNKFdXR5QdQPzt9tf
QH8Zwwnia9l6c3EGneYipwchQyx/AYydIDw4DQ==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
Yk9/iPWFaV7Ec749nErsu26bpdZwxMytuKCYDnavdKPUVezR2KOwHf52BCNWjwfZQ87XtBlwicC4
PlNpHYSH89kl2kfNPboEjQjhuTlZvKe52s3voVOphtUJ6QGgOu2bnkbTeRV6Uv4nd1CRDiQLl7Po
H9VisyKFjrqTCXxE8dVZ7jkoe2QAMgNZkpa8g4fsSt8xWrT85F/CPyqjFJfjehI9AuKfJXuqOIjs
4bWXnUHLvC64t1ulGjQFnvTe93WtGpRmq/7oBjTTaHdi1C4RQEGxFRsXAGSXqj9GF9Z9Wznrq6eT
rsX5AlscycjEMsAo/LpYN22KJr4e9vZ6AMRP5w==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 10048)
`pragma protect data_block
l4TtKozI0CNkeop4WgDHTpyn+i44LmkXm10W1B2QMjDZ2wBt+U/CgSI4fQBpKm9ZiocDGCvk8y93
xH4bxQbZoyUhqHkyg9BerRhzGZZleina4E2Qar/aNh8GGedXO7aBZBlU5mFxm5QoDgR6MZ+DHU7b
VyP0476UIEdl/blTlXHvOW5HuZ5Xxvo/iQ4JDsZn+qUb/rjzITS+rK1flLCY/gG7G25Sy8SAoaYr
w6c+tkf0SQ5tjSdvzRC0pH/irrhKFu5KFmAqHZGrfMFJomflfg4e+ucIvhMMP6qelXBJ1NuGSFcv
MEfIC3NdO+3Df2CsyI4oq0bNFhNt/EwuayvquVTFt2rl6fYKBAISLcrnpOsbhMllyCTakwahs1fW
xY3HJ84lzIVJ1S9fnSsdJs+IKD0elKpFQx85MZKVcnx6mNdXqPxx0oWtZlnOGfhOF3yBYi4pZsCn
Xm8ZKfKsGtO9w+e9bWy0A12zSZ+k9KWH/Hy5fUsKG80ikqGBq6mKQaidm2ApwTD1iHmNIpANCRqy
jPMquzeX2sZ7aIpThUrx8vc+aoJ3de1qtcRtBtkn+/VKKadySC10ywRfpgQLLJuJl834IwBjsxKD
OEDxVuGkMXUCWIubJLCqQ+Bjwy7wNYZmphXOPMq7VTjrW6YiNJeP018Py6hOrGZWXgpND3PRK24Q
BjsBl9SiugvA7I4RFaBvvq4Im8ZSiLX4wuWxSkY8kIO2voiJfSEhm+c32SSdSCu6qXRDdZ03AhZ+
JDjQSzVF4CklG4RTkD+2+qg/huQ3VCAy3M/y62OLerBO3/jw7VisRbipzb40U9qch27xYk2EsIdt
3cFDX6w1ld1b7uFL2UwnDYkbh6251gqf79Fx9HCkCfc888WoVim3dItuaCuOHCK4vlvHVOtFtbBD
bp96tr95vjD6K+3UmTzsxdH8sIUyONyaifmxraoyYYgtaDyx6gTWXHkvsHrHUiebEhvXZTu5yGFp
aKM/Py0UzInRdCtsLGl0vGAg8Xv/V8GaTRLCnl/fpTI3bfDfiMR9p4LxUOZ8niWMEwv4x/iMpGWP
szNEWYToZlGsWTKDbd/Y4QrvKCOZvBEG/RDaIf7ae3XKV0T6nZKFA0RdjP1l26ic/Z4rYJM/ML0f
9xtKjcW/s8dg2xtQWItjhChJplrJGct/G6F735HTNrdYxjuGvJ3FhjlxvmqCOnPCcH8uCFKElXr+
qBt3b81MxwD7IhCp2oQEPBpZc+VKi9lq/WTbdn5UCI0e0h+sV1TYABh2UR3mF1h6QjHc3qWOwYhM
i64lLFr32XHibimOWefTLB0QZktJCLobWZ6M/0wCgTPm7mwy0KChdZluf7OAUv4LM5rdGx1Aowtw
EAuH8CUMTneoLP0kyWM53IThSe0TR1KCxayUGwRKXWhRBmRqH7OwSeAUf1BmFXtr8hVyru2AN9O5
ehKWeh8ohydA87sLbyL/lZCznYD1c2MUED0Zc0qfTSxmtIr7QQHWnZBjSPTupJFY6LHgIVOddaxB
3w8sFNbv+qZTAUJ7MzthL3v392xmgIYyblfMwAJ6hxsLnpflrNyvsh7aWjxPOAqRGM/I0pkjo3f4
W3xde/NLHFGOw5Du5ytXciDTiHR7fLQbQIFss7e5D45Smw+yNQXyEY1IYeY4FxZpGVa2UzaVywzL
N6/seSD7IPFp+cauF2qnoQ5ZG9qetYLznV4SMMx9EYcVvpUqk2JOv9fbD4HMKn48EGL6V7bnWLKE
Kc1s5q6aiCC2QkTOrzfIReZNn0I4S2BUTkVGkI/29SCHbF0FERPifVdw0hWb/7dmdagBdMDP9HvS
q6IYt1t1tWiVBYTJyPVDNicpbSSCZDDrgh43H/MCgxFoajEQHHlyueZujPohkeMBpLQhiQ5uV/bK
FmSV0RFp5wLjlWY+KbMpN5KnzwjrpWATS2Ms8JNBlf9u9fXAa4mDGzqZLYBVfmx3hOEoGjA+OHwx
xJJnSXOdU+K6HGYO5qDvJ2Vd2wZTqKdH6F5Ti80aGVFnfg47/EbJy6BUYNSzBP/BT3yEG/ozcV9i
xPviJNslULDGZbuQDU1ZrBs/zYu4U8D/hW2bMo9QQ1igWNweCLhK+1cuno/xEm0IcC3tXEldf44s
+TB48BRNfa+EEy//1hN3+ndY5RU1NyqJcr0xwS1ZItMbud8hcU7171FUP4t/2uZu7yTcx5Ue7Kaq
hYMTn7b8UGowKxlTQM5qb8syRBGzQOlXkhUAxy37e++SQ/QtqKjnToIdHvMln9oU+obf2HgZedPn
hpc+FsRnYmHmOYmQXQtbLBB4AwfNpFyQIUGgUVxDsXocf/Yl+zu8lJSO3l/4EUtt77jUu3czJzo8
2KTcLAo4WiWXV2xnzz6Zq+Au5pPV2Wi/m/yhYSzizPu52uNm89StwPWapAMOv/2yAjkC0D8FnUg5
XbJE6XwyL4zRwWN8MnlZhom+9zjJcbPC4INCowpIl+Sy+MGHaSW3lOSi0PpmxwUlplAtY+FMrM84
+89nh7u+i1KfEzwX9OU1/PIzela2xP9GNSrR+CY/BDARh/HnQKendPhiu4CSGa4Tqg8VA04axypP
jxDuo+h0CYGidkZOICj/LGhvji120QpSrp8rp0phR0kvICblAHGIwI91ve1gEbp/r6ScYBFo3DqY
OcOHmkSA2Sp4bD4SZZPs2FfK2Sa19FYmkW56XUag/P4mmT4kRDmOuduvzGjHPPCnbc0JC3nPBLFp
IbqB9kdx4eLpQY+uacnsajbcJsnVmeWcHt+C0wGGjlTTBVqkULCIa8tD91gyChP0fd5T1MBdXvXe
B9MaaujHJI1tX/XuzBsKMcZNcb2vorl4F3XniI3jw6NjshLSpxWcWIefObPOOvC/vatNsvJk37pW
Rffd7eo+ltDs/In7DuWTI0hwDYCLArFCZJ+0leuc9b7hnZCBZYGQqlrVgOjspAYuOClZ5P7mN/5O
b1RnO0DiKhkgzSUuGBxm+lnDTLSO1yrR3lfbTeKdDiMUxp3+XH1k2CzKDFbR1mXwrpbKxJCx094T
UqktAVNc/gaF5SNYezQu5iEIko51xEx2L0lZYPHjrTijDjAia13rIQSXdKhCTVnKx5zIaW6EC1u5
J8cHEHGswlRapZlF39bzxtxfjw6IdTz5bQC81+h06b0HUKtEWyVVPzzfni5osgrs3fNDENqgv0L6
oDrXTZtK2NVj1TioZNCf1Mqyo/20WX2/Bt5HUJ1PyySEj89eFrq0Ub2SnhhRPBBhfztyqNaLhYMH
opi11Xhz9VbOL3mz7+EkWqWtxUZpYIxWL71+1ejZSbbw7LTQwIsLoxrnFUWckV2D2OLeJxBc28Jw
+4YQ3F+0dYE8EzteSp9KOTvx9ybbU6CcQOU9St0PSKQ5qksirQJeiyoz1ZHDwScBzj7Rn0wJ3VUz
f91LaNzMccQ+OfZ9nQ6bUlu3eVaUsTmOG+jD0Lz6B2rAJv9dhJZRH37gh7iSGyc8HOd4Kt2MDYwV
s3luxj9v/lcjG0nJK4uurSZOHQQQZu/whV/uNMhSG645151W9RN/ioH1zkwJJzbcomKgyRBlbjhK
vPdo3nVIgFDuDXsUYqoBgHfKXk7uIr6dQFg9X48wKpMSAoqm9H2YB61OO8Ptx8J04Fmt0FnVGHRS
tU7lv5wL0+rDj/R5i2yNf4BWLRJ4rmq4Ml6xP4a7UITGvegNIMM+qFDz41OW5cl3rek7X2de666q
TfGZJAku5wdoo3GV5iQn6UFrQBXLtPp0KkRLO7DL9TYFagxd90HflT0UBhHUGY42FD6jCprpP5St
ELxr2ve3jw+75REEBuokjloJl2nSugC6hU2p/MbMCXBqdmU+T1yCtvfVToKhs9RlL6SDkUCM7a3i
I9uDIeAdbxeD1TuxQKgO6EVTGwF6aSdhFgIMdLJm9prFWWbv92Wcwh9HI9plQvcUaaQ0OxvIBcrD
gAcvUew9YzxF639Ziimb8nyXbpzj8IqOSpum8Cv2JudFs/VdGdZT+crMawxUoH2l7mcZZyfmY7aE
6tlsT152QufkieH2FpBXKxrmwFPL47R0V2+6LeS0XesO0Dh1wKPudKMDt7ZXfGrrW5f5o6Xk94L7
j5gR7Mzz6k6wKw+Rt3/tZiVlKar3LV7YUcpO/ekSCurqeg9piHoHktcPuAdhgck/OuvEtotV+l4Y
HzL9XzUi9dmWE5EQxblt63qi/4ugo5OR0EA7dBAlf4jhTPJ+s+RQBYiR470Tao57n7kGM8/9h++s
jF/I2sFv96wuvqSE9UuXIupdzseayVCrdrOaDtmyyKnkm50i0yqk/T5AHQJS8Pd9mD7XHbBX0jkn
oGego2h1SqrOM/bnvzI8mWVC0c+dLAE9FAaNc+ugRgVhvg613ZqnCusMZ04P+if5mSC04GtvvxAW
PcwIAjIDa4pWVMm2Cq+JsZu+pBbpnkxsWd5wlGRtJKsAaclnwo3/JkocUitCiPhhWsrMC32tHVvt
kzklmp6UFlav4iV/i7Mm85lGIIezEGQttgdKk7RsLfIlLSoUSZpStyJG1JMtIkzInp9k7nh+2VCT
ry5WWB4nhsYi3Tt8zKiwcF9b0AG2W7wQOhuUHat+z0GVRsioIao3t8Dqu0qgAeVi8JiiPbhMKegP
0kW5NflHSlmLmHavrKWPyAiT38zE//bSH+0FVQ4OlKikq04Ri1Yec/eSYvrgOJR7sUprRHvvPMC6
nIlcbTvVwfSWa3Wi5Slv2nTcIbdSQ/PlTfnIhaXlaVqzkpxAd/WGxhItNNi+6jJQAYs4BAC2uqmf
61jc3t6hIi8KNwzBDAMP78HgLbtJjs7MDCII2F/m2IoCCLsL6ro3/guh+6e0fZ6cxAB9rfxp9O83
ZYzjWju3Ou9QJ5LAgSo+hztUXdbAdqgkM/IzHQAZcJgqy95+MIVTlumMR9QFHLsXdlKyYxPxtIE2
MgVLN/e92X0WFqxb2P+opSkGNsAqMPZBOMtSKoCRHvBuS21y6DBnmHzrJX1DIzabgdajnl1kcE0P
3NBX6yIwnRqYrZfXAG5uCocSSomFI4bIK3CAjloDg5UpU6OW2+E76AwDZ4uaCEquRQnrq1UZzBRg
Y74LMvn9sAQsaYYEpI/jVIo3PlJPak1CJrJDgrbjqV4qvT0IKa1ZyUGyWof0oj+M8RzovdRh3IU/
vLzl/H+5O8hHwti6IzAOcCpGOumbZDl4ISyMg8i2gy7yZSgjGbpmcRGzbs6mTmQDnqGDt+P5/KG2
U7Os1RkJCcuLj/zytZ9+Fi98rIdqi79Ez5DFTtW5zHslFqIyhTo+gDR5/Bi5y7SCvzFEwGep6gtt
jGBB1wLkleAUrIVGw8i7AbaE/s36UUuq+P3GVezMekqDZ/xzVOI46/kozZiG2INCvgf3PT/rKvLP
U5U/Zo5POedefjY/dxPG5DbNC1DmKqdyASVUsw3TsgsSHpxwRxzHxqXl3K3sjcBmv0DotVuY0h1I
ScUSWH5usnn2qBmyA6wB36bz94KyPXKvRSy+p9XpD98qko5Ch2D3S9r+/g0/ESugrA/kF1qss5S9
Hg0IYwbQpnlG9g08OCvBjF6VDRGOGlLz4rA9R/h1xp2/u7M6m2TsUpUbRcJhPprgHzZypHowWufs
EdzpLMnUn84MZDCJ99uBPVD28RuY7PRWzo1RR3vQOJgqWWDv36UgJ1O0GdMe4U14WRCUrubQ6DjL
gx3MP/vhlb4vp6IyWPKp2HLcIW7NfcLX9ubJyBp7zM+KsdNwV0L5uSIgodoJcNDON0+35dEUejxd
uzx4sO+nsX4yig+8mFFocvOE6+TFyGA7ONmDNsoqzsdJpqIPMYOzg94MbA7ynOOV7e2z10mhPS1h
kU6ISOTzIUHOFdl2jUTPBIya3KU43TCkaZYQA6yz9tP6hLb5iwWH5Z2KtnnFsa9K03kYrZQDFJ+D
GgjcgVlKwpOc5hMLC8ibpZcXS1kX7aU2z7ybaonM6glhLwcbqBILwOBpggatRCHWF72UCYo6Dkrk
Fk2v+D+DwsGCdMXh+XHaHYU0wFUlnLCABhfF8L/CfXByxl8t0c2q87BokFeSXqXfilc2tputi352
fAeO4vN7JeKfbdCojqjezaWxbjXPpgPvwGBQ2YIImLHub8KEGb0Vwb6GNHiV0Uu301DqdQR5NJqz
qTRb3tYz494MaIwJlSG4S0H3w64drDurv68iUXGXSYKnNpSWf2e+Uemrgow7ivzMXIGpE/fMkvgI
oNQSFX0NNV399+ltmwb+F6Gi2JGWbgPwgh6Ogr3LWZSwotjXCpNkRgtCKxcx9bbZipY5nJ6bx5u8
TDKpvBf2bQMll7VRZJzqpZTftVpMe6jIQqLxK8E4YudCOQyqrdG+o8Z7F8KKT8CHO74iFsxZb5Jr
ToRqcAavNc8kc1d0JdLy/Lk2UJ1i4ceJQU1daN979YB+HIvnMWAqsJpeLnshX2yKUCC4lvGxZmvr
xQXXmXkZNGf1FumSdU2BldAXCyloD18Ifqgr+ChpIu5OFfLwfe6ztAkmJjrXxAWeDoNwUa4u5Mo1
rAz5QMsN6x6NXkMV6DOagxgkLV/ueKpf4UuOdPwPe5Ghwty3PwlGHcCpDmloHsNP3K2AwYDL2rTC
zPySzZL4TJb27Q+FFJnqTGf8BI/ps8E7Ztae2iyvN8hM1VbmH4r/yX+nW3JkS4g3LZ9llh9fuQH/
+d++ghD/yr93GLMQJ2RCozDcmEos5BCrytkF0fthSbVormWSAoDNKBdMSA2KTLjZlWBX8HmZeAK7
YabHevFeVk0E8zGhbT8o5wsw4+5tg8bIf7NgF/gKQReIRrkRMewEfg5P/rc8OazOmnBPhlNmqBfL
dlnCPb4GrR9H12Zx+Mgm0p+zR0inhj8pW5rOwEawcIl93E2UGqsh4b5iXRuKbiueZAzAE5W1wNhc
vNm3dW/54Wq5x0TKkWdZNUFekJPVWEju9su33s/W9x1jIY6Fs0ICe6OaP8qMxEO/IkURxf96zxIi
5vLbi5tU/OyieDSgFhqRxHj/t7wYpuaal/8gF37FdA1Cu4zijFn5Fm0RAhoevPTch5nAtjbaHc0X
9Y5+Ctbqbu3cxBTQ2G1z9WlQemXRCrPx1dV/2s3SBNnchkJA5MwJUq56wyWmLmgAdTrbDRFgaO0t
05kNvoEqnlfQXLRiAvFHsCtAu6WNbEWpDoDzDvWbRzqj9qO5LL4otLfoyAT+bpEwS3i4oAolr8QK
ymVU4X2y6fAK1a2trvDLePSGZ4T0HkHG6jzHBVQ/kt4RIqAeByWZZR1343C4e8iWLLXH1Lqjc3Kd
J5Ipm9gXb3V9NgEbn8BfxeZUa5/G/4NLLGz0AUrnntUJYM9e0yJ44VZp2Ybh6Avmbg9bc/j7Z4Gm
5SdcXNaBHGJsZWW5dVPQuHzA7LdUciK+9m09nujEvd6fhknSA+kWzlma5UuOfHhBso+Te9g4cX0x
mIk7sj/oHCkjdeM1uNrXovEKdk4G50xJY1GHEDgHP7NmblHvQ25Ne+N+OIqoVixmJ/Rh8lADiuUA
Dw1A94ad/PuvNkkOl6W5TBqrlVr8KjEaadvFBH45h+TZL6/FjHNq/6vMFeuCOM31pBbtD8HjsmHH
p6+tgbK2nBj0gm1QYBlrclmxYU85eKPnEgQ+kn2HkHRxklYbzuJZTaP4nUHPIMTSGC6IramWXowL
k8eERCcUI7hL5FuHWRbwL84eVZ3dUvW56GQRHhaPrxt98HoTmDu8Q8id+/9WfO1cWpEzldPe5wsw
K0SmJOJmSeO9e+p1Q6O5fpdnj1SGOPhdH6F70a7U84mKBsifqwD1colkQKfnIWf/INN7H8l2gSdZ
jl7iVXbBI0o/lBElYvQIfgIhsZa2kezds23H2YGzalEPPo8GWkOwnwpooXhOeQPMfYM+TTCFW69J
MGhaG7xlo/H6h2td3ovMq+e9Bpvp8F3MTv1fwNXlp5Ht6xVbi8/XabH8q6MDfsVy2x4VlCzVP5HJ
FMTt3URhGQow7DXeU0OXXgFpN+rQh13GYHp132csoYqC63KedXYejK3zX3del3nx809HkQFooLpj
akPmsXXV/zMIthT3mG/5EIquvamu4+EfLAbU/CTzXJtgqctwni4Bnz791AbxLL8/7ntUreTSb6xj
LgMxkthSeNBjk++1aObxR7wzNcHgospgK2/IZIA/H9Y3zuHSmDF8ozwXpLc/suLPudbqTtYqMKGb
2xK9wWBXpK8AQTjG90vRnDWx+nuqfwxLnMsyLOnpHck1gM/BTZ3NptfO20fl/v/vro1LlQOd+zax
iH2qobcm8e+fILKvVAKYJjwjcgoXvRQN0bwVt85QQLxzIaCq+eDJKL8p0zIamQsfMS6w8Lg0OmPj
wFbdS1d65kwZm+kc8c3BHagJHaP33YrlyYOgx3ZlbRvH8apM0K7oS8dZsSajvLKFo9E4WJJ8mtYP
cvN9AEcffYNYQ2Pp75YrNQKezaG4T4JnyXFvQNvjkoCviqKi0yZkVTeFqNTasrDl4R7R8iJR3c6B
VNBjnyOH4rM/XaGz1DBCdvLIVCF6LaFHKM96h93ZLi54YeTnqN+cw6EHCpCR3lmzVkAGtrSlxUOk
B391eqlpTjc8bhDXVchr7jtebCUNG0a5Iztdm2TlCdkKPl0pk9JiAHU99/FD6D7FpnZJTd3iYuZc
faxqqFa1vGz97XwCCGkRNgrP2hseOyTCT5pCNEGix4oxhxrCBD0EJ/7aqD4kRpHx7Hlbt48SjkG1
Y3uEt3prm1S9zJxG9WCvY4lZtiCqn/SsZuAyQLILh8CsBOcWD101QpAow8Ijo+r9GrCokD9fY7zq
N4NxLdIVbQAJhJUJSkNRBuTeCIef/RQn7yGnnkwH8PDtDMiPxboWhf42V2Vlzw1FRnUhKKkiVOWn
weUyIgiZ34dyRO7aZNKKdThd7c19gWwnPgnmUG0Q/BhnW8gui31zx+KGBDDDsDFZ/zPTSmnf06WV
Fu+4hX2lKQURj73e77IdL2X3Uu8Ae/JPcH13ikPTLCFCC2ospgaomSBqu+Umpk7gLTHW5TIvit4P
esBt6xKgLP0XnRLgi0D3n4V6uEV4Z8h935BiRvZwmr5323ZVF0/n8Lza4fU8gImV6Zapf6OclwZS
H25YI13ZFlZfaVbhVIMGOQugtXWwdQLChQjaJCg5CdZF3TZikleg/ezkgQKU5KXBO4tAcjb939Tx
j0zm3g4b9d8Ru1nvMEWZO4WVZiel+mxS7EClFe8JaI7pNQrYinbwAhEeVG8kr6f4nOHZDXflY5Kb
hZ7ffhRadqZNqkWiOcaQLzV28tC3i3jsa1RtZfBRT+1MAgA8fUmJa9pBxg+TfJI0+dJGf5a+2LuP
2Udl+ko8s+dme6nIwODIthuUVNW1eYikKtdWRu1+AOngh6cvYExP3GqMTyOd3jWEnHk3aRpNc6fe
yFww/wvD3zR4c/MRY5ML3IYczLvdbhmFBApLZw/q73gDmBeP8PFqAFZAXMj5q7vWyE1g/zGyD/xx
JUPBq8VmBG4K5ja3DC70q/T5o1tVFvrqRMfxkNUnfr/g/0Q9CFuYmsZJ/mfza2A8kOszFtaPV9qw
hYU848hqWMPfrM7/pxta8i56M5Lp6uWdMV2wjuMwvf0mZF10w2ZJbtyQ6sqPjau21SUGldbWNjRu
CLk1yiW3oA2IXMm0+IwQ0rIQQk8Zp0hBQ0lJTs+g8AFlqYeu298VwMogv5jb/MHz8yBz+7nj6635
N7TELvS7qCbXa61M4dXtZGqBbRYsd+DK9M2OiNjHm+RkAGnDpnXFE7N1mKDhnxTa0Vy2LK1jSLsp
JM8nLC1APs6+NNkgYAUJe/cT2RA1SH/9PxZwFeR9VuinSkUVw7hXoqmTWAAl3rCzamH0EI7AVW3f
Hwv81qC2czVwSAc7A60lwvvvpcf18np67nybgS8oRRiAc7p7NByEp4H/Jk6y+rJaeaMOaR4Tsqa9
p6EXrzHvWZ3NrjrDYJElvAxtExqtv9PoSRjh/mU/41qS+xfiWPp/iJCsktIesVClyAJGwVT3Qxxj
wOrNlDcsUlGQz8Clp8vJ5AqS3i7Gd+F+wMzI+cjVbfzGE3zDdaGVSNi50CjemX9h29j15yQMST+P
r0/pH6gCwJ9VN16kylUGIM2fn3j54iMyTO+5SSoTffydj8FFFTAGOAUTPAkWl5FMkjJmH1KsJoHs
GlPkEjOM9QR/7ap6LmaYy4XyyUQcWK9HF9vHlVbACsHP/sSwBtW/jayT0bM1zU9jOTpU2YQ6WxD1
+Nc2TJaVhGcMYWywrGZ6meVgEgDGoIRcFv6OyouXaRMwY6b8QNISp+/4nHrpTTFXf5a4HpPRN5HG
+l2gNNICmUUelX1w9Lk9y6Hs4poPxl/QYlwO2x0hNgNYy+XwBk+VZ9CZzgXr9LoKf3wCnZrwcLRG
rcru8dtqbnlTj7pO0w3N5GynOjIS6lpMCRX63F4U6I/4JCdtR8hqzgF63XpNzPOm0OdSp2PJp3OT
PAFAFrmdLfiWGS809umTs8j6hWG0l77iBmqYKBh6xdYJjEWgg5eq+UjKg+K+BGW7NPxbFZIGbPne
F3Z94oFbvKJo7/r2krewfaV2yawj9zgZXR1XcGAvq6mB6JHfkTojLzdmdSJUcDpgl9fk56w5Oz05
0en0nlhzZ+hKiFiiYB1j9g4CAi0pCjxM/ge38FmrAlFkg4NsflVL94G3t1fBzGB6eCzOExk1xZgz
7KNjptXRDZ47rYBwfLRO/EWz1AzwViBtvMTSZ0yhxKpkfpzclzgEbdJKC6dWr9vri9g8muUjHfAd
sDKYG66Ty5YSWR0YiYL0keA2wnQX7GHjeb5lYF2po82pJyOrl04WHNhUf16ERP0YA1C6bM4MH3l0
dM3vcts6lJL2v4c4GKs3HO93RWHJzkpTcWQOOGt5nOjyL1hl5f048Pk6Uht4cguYHAjDSTrfIaLw
RlrVBfj2Z3Dv3HQCHTJAyM2GiHwWoHf/4VCRcKFaOPkrVtElksRDN51PosR0RFHY8j7iyH4GY68Z
FLBGCG+Sxtw5ioAkbJ1KxgOyHPG9XHHm8gDSwyCfE5RH3ZA9OBX4NPakS9roZk3fqox1cN3+yqLg
kscrdgggzSLStzr1WMol/y6l8JR08p7LhWPoXaEewgH37rugvio28JmWiMnLvdybynM5vqyRhBvN
oS34+LYEvQGEjxkg7iE5VtUFYSre6YnD0rGNjPZSzK71At2bJPz7oAmTdTPeKPZPEivZubp92IjC
DCfr2lS/yf5wed3KcfXHjNF83gJ0NLkoSPr+yMn3HkUWXzXe/AidY01/9RLDazVeKoxW4FPB+nNM
xpXnBpWhmwi0CzsItlMvkVwqozZ62Gmo0ee+rfxI/mka5I6DwVd4bTjJNvEvUyvSGam4KWY56QUN
GyYa41zEdHkGUWDiRfZ/rc3pqCMmf8lgbCppx8iX75wGiv/Bu/7TXuFkhdyAJPFfYPwYyqvkUYbN
+WCA0HDXJeCdfjRE0pgFc/5XKTETvK+6tuvHe5xYPu+Plp1j5UJw4Y+6K8/eH4OG0nG4YZ6N7x+8
9EtFE8nD4ZY8IvMcLEPY3cHD9O7uL41QCW0jlvqk+PTcqahMAgq4IptoSPqRNQd7lfm1/V0Bf5f/
mc7leerme9XKZWQAHKInif5cgvr452MdvsZ0n4pjio0vnOZT5V+NbrFQWn2YR8kj01XKPSXfMN/h
x6/1PzyjHpsIjwwLtER1re/wORU+lkrqbMH++kOySmpv3BUR0uRbHiqTtDDXDI79YQlRneI9+74e
wpPxrIAXBAgyFbV4ZXpA8UZksg40OB2dOqqyeZD18XfzIAMnrkwbZWkzgfa3Eq6jlLmyzaXjueMC
/2ZHkslLxRalZUpV7xzWpPw4X8SIsen07NMZDmN7DXlX0wFDz8bf3cqqWSeOhMbTsLdX/NA06jL/
JtYYxGNw05IUFtUf/3aIM9l3NDkECKe5Q56LLBxMI8JnkYvpNWl8N/9X8DyrkVPVSp+6cRxgZgd9
a+X2xE05fNDGgHSNY+4uiIZB0eJipZrjCgxKtshMsE9xtczTylUt1kNMH3L1uo/XsMSRen5EQSz9
0ytPXnhlI1YUMDA4ch7tOLeZENaIGNvUme04BvwVzeeESNBYUNxUxjAPSSwuLYp10sV0i7C+HVw0
sue550gyxJJ3PdKFZfzZVFjSqHIoleukB+r6IiXKkVTyhAHq4j0n7Imbv7McBAuSbFEeB35NhU9U
SP2siEAYhsTP5OdtRQMCw33us2mN24areTeJ+oVAnHTZnEnurLuuGRBsVaOQqgtZKQJdRM8MnOEr
lSc9Pto3GQho5vTJMJKFIjo72GEEvcYfwuYCp3bpZfLUscN4R91TVfUVV4ggkQ60bM2IBlQNAMVU
q07gnbBv3Oe185JvhoCtB88X+xuWYmKQGr68aLiYuFKe+ydx0TpTxmry+mmkE9RPqMKkUdemFOwi
EO5Ydb/CW4KI/XiNV1xHNzzSoGzZPgFROvKqbulvZT0YCc2frb4lAy6o68T0tQad0ABf1N6+U/ab
K6HcNNgDVU39Y+WI3MOqYXUP/pohfQ1TypBErXd7fSn+JzysiJv3GUcYr8EZgddvmConmPIczsRe
lj9wDcz3LbLVl9yov8c2BnZdYF2RuVSsxAmaFHKrA2QInwaFvATAUkbqero6Lsz+UHvM9nsQDZFn
aMWA3OFk41wsSIuC4SSs9PG+xB2lLkwjQ80G2wz5PCOta/Dq9OSqY/qc8HVc5wXbikZJsOuAUDeG
sEbjqCftkz1lzIspI3mMbcfU4rMeDZu8TTWuxfezxJWzGRdv7mD7GuCNAjvzY0a0TtrZAL14XgaC
QcYCxa8Nl5AEVt/mkEyuryI3FgY7s9P5ak/DrWot7ImxBJMOWmPif45lc0PQHnvY/864w93stGVd
+5eYLyzCCYkDrA7Zx5PfeljDAN4niQuwTiMJM3fFfeC/f+Oaqbayl6Y535QSevccM3lGSnSpE2xd
SxmCDSpWxC/VhU5hE1tSOlyrhlwZ2hMh+FwV0bV+IhjNZ/2eQgWAzsjX6vSRfq800QwHCNaL0u3P
QVtTWJmmndeETgHTOWOQVmxCIQkuocQsZ8woWd3iuFZqApMnJizMPcxZ1UTQ2g9oVtCsWCsMxgNK
39FxX1jFWi9wyGU2FIho3ek4VG+euiEofSridBo7F2LyUSlv4Z4ol60WDZfChi4xpFF0kx2PsNrs
sHW3BowPw6IyLk1/Kvm12CY2Qc9r3V3nkaGfmJta50MU4ptJ6eeDCtyt/dLJZl88JWRwFl2Nv5aa
3710yuXoA18qNzDqUN+k32qLkqh3nX9M9NXe1K85lo5rvAxKbTOrBTwgA+3vtThU4GdVz9DZdyB0
wdd7XT4i0+hSm7hcN7YjFQ==
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
