// Copyright 1986-2022 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2022.2 (win64) Build 3671981 Fri Oct 14 05:00:03 MDT 2022
// Date        : Wed Oct 18 16:26:27 2023
// Host        : DESKTOP-QRFTT4G running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_WaveformGenerator_0_0_sim_netlist.v
// Design      : design_1_WaveformGenerator_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a100tftg256-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_WaveformGenerator
   (WaveformData,
    Read_EN,
    clk,
    Address_Data);
  output [13:0]WaveformData;
  input Read_EN;
  input clk;
  input [22:0]Address_Data;

  wire [22:0]Address_Data;
  wire Read_EN;
  wire [13:0]WaveformData;
  wire clk;
  wire [8:0]curr_address;
  wire [8:0]iter;
  wire iter2_carry__0_i_1_n_0;
  wire iter2_carry__0_i_2_n_0;
  wire iter2_carry__0_n_3;
  wire iter2_carry_i_1_n_0;
  wire iter2_carry_i_2_n_0;
  wire iter2_carry_i_3_n_0;
  wire iter2_carry_i_4_n_0;
  wire iter2_carry_i_5_n_0;
  wire iter2_carry_i_6_n_0;
  wire iter2_carry_i_7_n_0;
  wire iter2_carry_i_8_n_0;
  wire iter2_carry_n_0;
  wire iter2_carry_n_1;
  wire iter2_carry_n_2;
  wire iter2_carry_n_3;
  wire \iter[0]_i_1_n_0 ;
  wire \iter[1]_i_1_n_0 ;
  wire \iter[2]_i_1_n_0 ;
  wire \iter[3]_i_1_n_0 ;
  wire \iter[4]_i_1_n_0 ;
  wire \iter[5]_i_2_n_0 ;
  wire \iter[6]_i_1_n_0 ;
  wire \iter[7]_i_1_n_0 ;
  wire \iter[7]_i_2_n_0 ;
  wire \iter[8]_i_1_n_0 ;
  wire iter__0;
  wire [8:0]max_address;
  wire max_address0_carry__0_i_1_n_0;
  wire max_address0_carry__0_i_2_n_0;
  wire max_address0_carry__0_n_3;
  wire max_address0_carry_i_1_n_0;
  wire max_address0_carry_i_2_n_0;
  wire max_address0_carry_i_3_n_0;
  wire max_address0_carry_i_4_n_0;
  wire max_address0_carry_i_5_n_0;
  wire max_address0_carry_i_6_n_0;
  wire max_address0_carry_i_7_n_0;
  wire max_address0_carry_i_8_n_0;
  wire max_address0_carry_n_0;
  wire max_address0_carry_n_1;
  wire max_address0_carry_n_2;
  wire max_address0_carry_n_3;
  wire \max_address[8]_i_1_n_0 ;
  wire storageData_reg_i_1_n_0;
  wire [3:0]NLW_iter2_carry_O_UNCONNECTED;
  wire [3:1]NLW_iter2_carry__0_CO_UNCONNECTED;
  wire [3:0]NLW_iter2_carry__0_O_UNCONNECTED;
  wire [3:0]NLW_max_address0_carry_O_UNCONNECTED;
  wire [3:1]NLW_max_address0_carry__0_CO_UNCONNECTED;
  wire [3:0]NLW_max_address0_carry__0_O_UNCONNECTED;
  wire [15:0]NLW_storageData_reg_DOADO_UNCONNECTED;
  wire [15:14]NLW_storageData_reg_DOBDO_UNCONNECTED;
  wire [1:0]NLW_storageData_reg_DOPADOP_UNCONNECTED;
  wire [1:0]NLW_storageData_reg_DOPBDOP_UNCONNECTED;

  FDRE #(
    .INIT(1'b0)) 
    \curr_address_reg[0] 
       (.C(clk),
        .CE(storageData_reg_i_1_n_0),
        .D(Address_Data[14]),
        .Q(curr_address[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \curr_address_reg[1] 
       (.C(clk),
        .CE(storageData_reg_i_1_n_0),
        .D(Address_Data[15]),
        .Q(curr_address[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \curr_address_reg[2] 
       (.C(clk),
        .CE(storageData_reg_i_1_n_0),
        .D(Address_Data[16]),
        .Q(curr_address[2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \curr_address_reg[3] 
       (.C(clk),
        .CE(storageData_reg_i_1_n_0),
        .D(Address_Data[17]),
        .Q(curr_address[3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \curr_address_reg[4] 
       (.C(clk),
        .CE(storageData_reg_i_1_n_0),
        .D(Address_Data[18]),
        .Q(curr_address[4]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \curr_address_reg[5] 
       (.C(clk),
        .CE(storageData_reg_i_1_n_0),
        .D(Address_Data[19]),
        .Q(curr_address[5]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \curr_address_reg[6] 
       (.C(clk),
        .CE(storageData_reg_i_1_n_0),
        .D(Address_Data[20]),
        .Q(curr_address[6]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \curr_address_reg[7] 
       (.C(clk),
        .CE(storageData_reg_i_1_n_0),
        .D(Address_Data[21]),
        .Q(curr_address[7]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \curr_address_reg[8] 
       (.C(clk),
        .CE(storageData_reg_i_1_n_0),
        .D(Address_Data[22]),
        .Q(curr_address[8]),
        .R(1'b0));
  (* COMPARATOR_THRESHOLD = "11" *) 
  CARRY4 iter2_carry
       (.CI(1'b0),
        .CO({iter2_carry_n_0,iter2_carry_n_1,iter2_carry_n_2,iter2_carry_n_3}),
        .CYINIT(1'b0),
        .DI({iter2_carry_i_1_n_0,iter2_carry_i_2_n_0,iter2_carry_i_3_n_0,iter2_carry_i_4_n_0}),
        .O(NLW_iter2_carry_O_UNCONNECTED[3:0]),
        .S({iter2_carry_i_5_n_0,iter2_carry_i_6_n_0,iter2_carry_i_7_n_0,iter2_carry_i_8_n_0}));
  (* COMPARATOR_THRESHOLD = "11" *) 
  CARRY4 iter2_carry__0
       (.CI(iter2_carry_n_0),
        .CO({NLW_iter2_carry__0_CO_UNCONNECTED[3:1],iter2_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,iter2_carry__0_i_1_n_0}),
        .O(NLW_iter2_carry__0_O_UNCONNECTED[3:0]),
        .S({1'b0,1'b0,1'b0,iter2_carry__0_i_2_n_0}));
  LUT2 #(
    .INIT(4'h2)) 
    iter2_carry__0_i_1
       (.I0(max_address[8]),
        .I1(iter[8]),
        .O(iter2_carry__0_i_1_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    iter2_carry__0_i_2
       (.I0(iter[8]),
        .I1(max_address[8]),
        .O(iter2_carry__0_i_2_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    iter2_carry_i_1
       (.I0(max_address[6]),
        .I1(iter[6]),
        .I2(iter[7]),
        .I3(max_address[7]),
        .O(iter2_carry_i_1_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    iter2_carry_i_2
       (.I0(max_address[4]),
        .I1(iter[4]),
        .I2(iter[5]),
        .I3(max_address[5]),
        .O(iter2_carry_i_2_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    iter2_carry_i_3
       (.I0(max_address[2]),
        .I1(iter[2]),
        .I2(iter[3]),
        .I3(max_address[3]),
        .O(iter2_carry_i_3_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    iter2_carry_i_4
       (.I0(max_address[0]),
        .I1(iter[0]),
        .I2(iter[1]),
        .I3(max_address[1]),
        .O(iter2_carry_i_4_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    iter2_carry_i_5
       (.I0(max_address[6]),
        .I1(iter[6]),
        .I2(max_address[7]),
        .I3(iter[7]),
        .O(iter2_carry_i_5_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    iter2_carry_i_6
       (.I0(max_address[4]),
        .I1(iter[4]),
        .I2(max_address[5]),
        .I3(iter[5]),
        .O(iter2_carry_i_6_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    iter2_carry_i_7
       (.I0(max_address[2]),
        .I1(iter[2]),
        .I2(max_address[3]),
        .I3(iter[3]),
        .O(iter2_carry_i_7_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    iter2_carry_i_8
       (.I0(max_address[0]),
        .I1(iter[0]),
        .I2(max_address[1]),
        .I3(iter[1]),
        .O(iter2_carry_i_8_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    \iter[0]_i_1 
       (.I0(iter2_carry__0_n_3),
        .I1(iter[0]),
        .O(\iter[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT3 #(
    .INIT(8'h28)) 
    \iter[1]_i_1 
       (.I0(iter2_carry__0_n_3),
        .I1(iter[0]),
        .I2(iter[1]),
        .O(\iter[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT4 #(
    .INIT(16'h2A80)) 
    \iter[2]_i_1 
       (.I0(iter2_carry__0_n_3),
        .I1(iter[0]),
        .I2(iter[1]),
        .I3(iter[2]),
        .O(\iter[2]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT5 #(
    .INIT(32'h2AAA8000)) 
    \iter[3]_i_1 
       (.I0(iter2_carry__0_n_3),
        .I1(iter[1]),
        .I2(iter[0]),
        .I3(iter[2]),
        .I4(iter[3]),
        .O(\iter[3]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h2AAAAAAA80000000)) 
    \iter[4]_i_1 
       (.I0(iter2_carry__0_n_3),
        .I1(iter[2]),
        .I2(iter[0]),
        .I3(iter[1]),
        .I4(iter[3]),
        .I5(iter[4]),
        .O(\iter[4]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \iter[5]_i_1 
       (.I0(Read_EN),
        .I1(iter2_carry__0_n_3),
        .O(iter__0));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \iter[5]_i_2 
       (.I0(iter[3]),
        .I1(iter[1]),
        .I2(iter[0]),
        .I3(iter[2]),
        .I4(iter[4]),
        .I5(iter[5]),
        .O(\iter[5]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT3 #(
    .INIT(8'h28)) 
    \iter[6]_i_1 
       (.I0(iter2_carry__0_n_3),
        .I1(\iter[7]_i_2_n_0 ),
        .I2(iter[6]),
        .O(\iter[6]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT4 #(
    .INIT(16'h2A80)) 
    \iter[7]_i_1 
       (.I0(iter2_carry__0_n_3),
        .I1(\iter[7]_i_2_n_0 ),
        .I2(iter[6]),
        .I3(iter[7]),
        .O(\iter[7]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \iter[7]_i_2 
       (.I0(iter[5]),
        .I1(iter[3]),
        .I2(iter[1]),
        .I3(iter[0]),
        .I4(iter[2]),
        .I5(iter[4]),
        .O(\iter[7]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT5 #(
    .INIT(32'h2AAA8000)) 
    \iter[8]_i_1 
       (.I0(iter2_carry__0_n_3),
        .I1(iter[6]),
        .I2(\iter[7]_i_2_n_0 ),
        .I3(iter[7]),
        .I4(iter[8]),
        .O(\iter[8]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \iter_reg[0] 
       (.C(clk),
        .CE(Read_EN),
        .D(\iter[0]_i_1_n_0 ),
        .Q(iter[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \iter_reg[1] 
       (.C(clk),
        .CE(Read_EN),
        .D(\iter[1]_i_1_n_0 ),
        .Q(iter[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \iter_reg[2] 
       (.C(clk),
        .CE(Read_EN),
        .D(\iter[2]_i_1_n_0 ),
        .Q(iter[2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \iter_reg[3] 
       (.C(clk),
        .CE(Read_EN),
        .D(\iter[3]_i_1_n_0 ),
        .Q(iter[3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \iter_reg[4] 
       (.C(clk),
        .CE(Read_EN),
        .D(\iter[4]_i_1_n_0 ),
        .Q(iter[4]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \iter_reg[5] 
       (.C(clk),
        .CE(Read_EN),
        .D(\iter[5]_i_2_n_0 ),
        .Q(iter[5]),
        .R(iter__0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \iter_reg[6] 
       (.C(clk),
        .CE(Read_EN),
        .D(\iter[6]_i_1_n_0 ),
        .Q(iter[6]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \iter_reg[7] 
       (.C(clk),
        .CE(Read_EN),
        .D(\iter[7]_i_1_n_0 ),
        .Q(iter[7]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \iter_reg[8] 
       (.C(clk),
        .CE(Read_EN),
        .D(\iter[8]_i_1_n_0 ),
        .Q(iter[8]),
        .R(1'b0));
  (* COMPARATOR_THRESHOLD = "11" *) 
  CARRY4 max_address0_carry
       (.CI(1'b0),
        .CO({max_address0_carry_n_0,max_address0_carry_n_1,max_address0_carry_n_2,max_address0_carry_n_3}),
        .CYINIT(1'b0),
        .DI({max_address0_carry_i_1_n_0,max_address0_carry_i_2_n_0,max_address0_carry_i_3_n_0,max_address0_carry_i_4_n_0}),
        .O(NLW_max_address0_carry_O_UNCONNECTED[3:0]),
        .S({max_address0_carry_i_5_n_0,max_address0_carry_i_6_n_0,max_address0_carry_i_7_n_0,max_address0_carry_i_8_n_0}));
  (* COMPARATOR_THRESHOLD = "11" *) 
  CARRY4 max_address0_carry__0
       (.CI(max_address0_carry_n_0),
        .CO({NLW_max_address0_carry__0_CO_UNCONNECTED[3:1],max_address0_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,max_address0_carry__0_i_1_n_0}),
        .O(NLW_max_address0_carry__0_O_UNCONNECTED[3:0]),
        .S({1'b0,1'b0,1'b0,max_address0_carry__0_i_2_n_0}));
  LUT2 #(
    .INIT(4'h2)) 
    max_address0_carry__0_i_1
       (.I0(curr_address[8]),
        .I1(max_address[8]),
        .O(max_address0_carry__0_i_1_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    max_address0_carry__0_i_2
       (.I0(max_address[8]),
        .I1(curr_address[8]),
        .O(max_address0_carry__0_i_2_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    max_address0_carry_i_1
       (.I0(curr_address[6]),
        .I1(max_address[6]),
        .I2(max_address[7]),
        .I3(curr_address[7]),
        .O(max_address0_carry_i_1_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    max_address0_carry_i_2
       (.I0(curr_address[4]),
        .I1(max_address[4]),
        .I2(max_address[5]),
        .I3(curr_address[5]),
        .O(max_address0_carry_i_2_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    max_address0_carry_i_3
       (.I0(curr_address[2]),
        .I1(max_address[2]),
        .I2(max_address[3]),
        .I3(curr_address[3]),
        .O(max_address0_carry_i_3_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    max_address0_carry_i_4
       (.I0(curr_address[0]),
        .I1(max_address[0]),
        .I2(max_address[1]),
        .I3(curr_address[1]),
        .O(max_address0_carry_i_4_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    max_address0_carry_i_5
       (.I0(curr_address[6]),
        .I1(max_address[6]),
        .I2(curr_address[7]),
        .I3(max_address[7]),
        .O(max_address0_carry_i_5_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    max_address0_carry_i_6
       (.I0(curr_address[4]),
        .I1(max_address[4]),
        .I2(curr_address[5]),
        .I3(max_address[5]),
        .O(max_address0_carry_i_6_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    max_address0_carry_i_7
       (.I0(curr_address[2]),
        .I1(max_address[2]),
        .I2(curr_address[3]),
        .I3(max_address[3]),
        .O(max_address0_carry_i_7_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    max_address0_carry_i_8
       (.I0(curr_address[0]),
        .I1(max_address[0]),
        .I2(curr_address[1]),
        .I3(max_address[1]),
        .O(max_address0_carry_i_8_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    \max_address[8]_i_1 
       (.I0(max_address0_carry__0_n_3),
        .I1(Read_EN),
        .O(\max_address[8]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \max_address_reg[0] 
       (.C(clk),
        .CE(\max_address[8]_i_1_n_0 ),
        .D(curr_address[0]),
        .Q(max_address[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \max_address_reg[1] 
       (.C(clk),
        .CE(\max_address[8]_i_1_n_0 ),
        .D(curr_address[1]),
        .Q(max_address[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \max_address_reg[2] 
       (.C(clk),
        .CE(\max_address[8]_i_1_n_0 ),
        .D(curr_address[2]),
        .Q(max_address[2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \max_address_reg[3] 
       (.C(clk),
        .CE(\max_address[8]_i_1_n_0 ),
        .D(curr_address[3]),
        .Q(max_address[3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \max_address_reg[4] 
       (.C(clk),
        .CE(\max_address[8]_i_1_n_0 ),
        .D(curr_address[4]),
        .Q(max_address[4]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \max_address_reg[5] 
       (.C(clk),
        .CE(\max_address[8]_i_1_n_0 ),
        .D(curr_address[5]),
        .Q(max_address[5]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \max_address_reg[6] 
       (.C(clk),
        .CE(\max_address[8]_i_1_n_0 ),
        .D(curr_address[6]),
        .Q(max_address[6]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \max_address_reg[7] 
       (.C(clk),
        .CE(\max_address[8]_i_1_n_0 ),
        .D(curr_address[7]),
        .Q(max_address[7]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \max_address_reg[8] 
       (.C(clk),
        .CE(\max_address[8]_i_1_n_0 ),
        .D(curr_address[8]),
        .Q(max_address[8]),
        .R(1'b0));
  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d14" *) 
  (* \MEM.PORTB.DATA_BIT_LAYOUT  = "p0_d14" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "7168" *) 
  (* RTL_RAM_NAME = "inst/storageData_reg" *) 
  (* RTL_RAM_TYPE = "RAM_SDP" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "1023" *) 
  (* ram_offset = "512" *) 
  (* ram_slice_begin = "0" *) 
  (* ram_slice_end = "13" *) 
  RAMB18E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .INIT_A(18'h00000),
    .INIT_B(18'h00000),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("DELAYED_WRITE"),
    .READ_WIDTH_A(18),
    .READ_WIDTH_B(18),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(18'h00000),
    .SRVAL_B(18'h00000),
    .WRITE_MODE_A("NO_CHANGE"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(18),
    .WRITE_WIDTH_B(18)) 
    storageData_reg
       (.ADDRARDADDR({1'b1,Address_Data[22:14],1'b1,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,iter,1'b1,1'b1,1'b1,1'b1}),
        .CLKARDCLK(clk),
        .CLKBWRCLK(clk),
        .DIADI({1'b0,1'b0,Address_Data[13:0]}),
        .DIBDI({1'b0,1'b0,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .DIPADIP({1'b0,1'b0}),
        .DIPBDIP({1'b0,1'b0}),
        .DOADO(NLW_storageData_reg_DOADO_UNCONNECTED[15:0]),
        .DOBDO({NLW_storageData_reg_DOBDO_UNCONNECTED[15:14],WaveformData}),
        .DOPADOP(NLW_storageData_reg_DOPADOP_UNCONNECTED[1:0]),
        .DOPBDOP(NLW_storageData_reg_DOPBDOP_UNCONNECTED[1:0]),
        .ENARDEN(storageData_reg_i_1_n_0),
        .ENBWREN(Read_EN),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .WEA({1'b1,1'b1}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0}));
  LUT1 #(
    .INIT(2'h1)) 
    storageData_reg_i_1
       (.I0(Read_EN),
        .O(storageData_reg_i_1_n_0));
endmodule

(* CHECK_LICENSE_TYPE = "design_1_WaveformGenerator_0_0,WaveformGenerator,{}" *) (* DowngradeIPIdentifiedWarnings = "yes" *) (* IP_DEFINITION_SOURCE = "module_ref" *) 
(* X_CORE_INFO = "WaveformGenerator,Vivado 2022.2" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (Address_Data,
    Read_EN,
    clk,
    WaveformData);
  input [22:0]Address_Data;
  input Read_EN;
  (* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 clk CLK" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME clk, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, PHASE 0.0, CLK_DOMAIN /clk_wiz_1_clk_out1, INSERT_VIP 0" *) input clk;
  output [13:0]WaveformData;

  wire [22:0]Address_Data;
  wire Read_EN;
  wire [13:0]WaveformData;
  wire clk;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_WaveformGenerator inst
       (.Address_Data(Address_Data),
        .Read_EN(Read_EN),
        .WaveformData(WaveformData),
        .clk(clk));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
