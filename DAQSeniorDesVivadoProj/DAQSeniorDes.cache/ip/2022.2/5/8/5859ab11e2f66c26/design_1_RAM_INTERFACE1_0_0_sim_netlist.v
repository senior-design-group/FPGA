// Copyright 1986-2022 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2022.2 (win64) Build 3671981 Fri Oct 14 05:00:03 MDT 2022
// Date        : Sat Oct 14 13:18:00 2023
// Host        : DESKTOP-QRFTT4G running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_RAM_INTERFACE1_0_0_sim_netlist.v
// Design      : design_1_RAM_INTERFACE1_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a100tftg256-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_RAM_INTERFACE1
   (ADDRESS_OUT,
    DATA_IN,
    RW,
    CLK_IN,
    RW_EN,
    WRITE_ADDRESS_DATA,
    READ_ADDRESS);
  output [8:0]ADDRESS_OUT;
  output [13:0]DATA_IN;
  output RW;
  input CLK_IN;
  input RW_EN;
  input [22:0]WRITE_ADDRESS_DATA;
  input [8:0]READ_ADDRESS;

  wire [8:0]ADDRESS_OUT;
  wire CLK_IN;
  wire [13:0]DATA_IN;
  wire [8:0]READ_ADDRESS;
  wire RW;
  wire RW_EN;
  wire RW_i_1_n_0;
  wire [22:0]WRITE_ADDRESS_DATA;
  wire [8:0]p_0_in;

  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \ADDRESS_OUT[0]_i_1 
       (.I0(WRITE_ADDRESS_DATA[14]),
        .I1(RW_EN),
        .I2(READ_ADDRESS[0]),
        .O(p_0_in[0]));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \ADDRESS_OUT[1]_i_1 
       (.I0(WRITE_ADDRESS_DATA[15]),
        .I1(RW_EN),
        .I2(READ_ADDRESS[1]),
        .O(p_0_in[1]));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \ADDRESS_OUT[2]_i_1 
       (.I0(WRITE_ADDRESS_DATA[16]),
        .I1(RW_EN),
        .I2(READ_ADDRESS[2]),
        .O(p_0_in[2]));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \ADDRESS_OUT[3]_i_1 
       (.I0(WRITE_ADDRESS_DATA[17]),
        .I1(RW_EN),
        .I2(READ_ADDRESS[3]),
        .O(p_0_in[3]));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \ADDRESS_OUT[4]_i_1 
       (.I0(WRITE_ADDRESS_DATA[18]),
        .I1(RW_EN),
        .I2(READ_ADDRESS[4]),
        .O(p_0_in[4]));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \ADDRESS_OUT[5]_i_1 
       (.I0(WRITE_ADDRESS_DATA[19]),
        .I1(RW_EN),
        .I2(READ_ADDRESS[5]),
        .O(p_0_in[5]));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \ADDRESS_OUT[6]_i_1 
       (.I0(WRITE_ADDRESS_DATA[20]),
        .I1(RW_EN),
        .I2(READ_ADDRESS[6]),
        .O(p_0_in[6]));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \ADDRESS_OUT[7]_i_1 
       (.I0(WRITE_ADDRESS_DATA[21]),
        .I1(RW_EN),
        .I2(READ_ADDRESS[7]),
        .O(p_0_in[7]));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \ADDRESS_OUT[8]_i_1 
       (.I0(WRITE_ADDRESS_DATA[22]),
        .I1(RW_EN),
        .I2(READ_ADDRESS[8]),
        .O(p_0_in[8]));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \ADDRESS_OUT_reg[0] 
       (.C(CLK_IN),
        .CE(1'b1),
        .D(p_0_in[0]),
        .Q(ADDRESS_OUT[0]),
        .R(1'b0));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \ADDRESS_OUT_reg[1] 
       (.C(CLK_IN),
        .CE(1'b1),
        .D(p_0_in[1]),
        .Q(ADDRESS_OUT[1]),
        .R(1'b0));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \ADDRESS_OUT_reg[2] 
       (.C(CLK_IN),
        .CE(1'b1),
        .D(p_0_in[2]),
        .Q(ADDRESS_OUT[2]),
        .R(1'b0));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \ADDRESS_OUT_reg[3] 
       (.C(CLK_IN),
        .CE(1'b1),
        .D(p_0_in[3]),
        .Q(ADDRESS_OUT[3]),
        .R(1'b0));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \ADDRESS_OUT_reg[4] 
       (.C(CLK_IN),
        .CE(1'b1),
        .D(p_0_in[4]),
        .Q(ADDRESS_OUT[4]),
        .R(1'b0));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \ADDRESS_OUT_reg[5] 
       (.C(CLK_IN),
        .CE(1'b1),
        .D(p_0_in[5]),
        .Q(ADDRESS_OUT[5]),
        .R(1'b0));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \ADDRESS_OUT_reg[6] 
       (.C(CLK_IN),
        .CE(1'b1),
        .D(p_0_in[6]),
        .Q(ADDRESS_OUT[6]),
        .R(1'b0));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \ADDRESS_OUT_reg[7] 
       (.C(CLK_IN),
        .CE(1'b1),
        .D(p_0_in[7]),
        .Q(ADDRESS_OUT[7]),
        .R(1'b0));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \ADDRESS_OUT_reg[8] 
       (.C(CLK_IN),
        .CE(1'b1),
        .D(p_0_in[8]),
        .Q(ADDRESS_OUT[8]),
        .R(1'b0));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \DATA_IN_reg[0] 
       (.C(CLK_IN),
        .CE(RW_EN),
        .D(WRITE_ADDRESS_DATA[0]),
        .Q(DATA_IN[0]),
        .R(1'b0));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \DATA_IN_reg[10] 
       (.C(CLK_IN),
        .CE(RW_EN),
        .D(WRITE_ADDRESS_DATA[10]),
        .Q(DATA_IN[10]),
        .R(1'b0));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \DATA_IN_reg[11] 
       (.C(CLK_IN),
        .CE(RW_EN),
        .D(WRITE_ADDRESS_DATA[11]),
        .Q(DATA_IN[11]),
        .R(1'b0));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \DATA_IN_reg[12] 
       (.C(CLK_IN),
        .CE(RW_EN),
        .D(WRITE_ADDRESS_DATA[12]),
        .Q(DATA_IN[12]),
        .R(1'b0));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \DATA_IN_reg[13] 
       (.C(CLK_IN),
        .CE(RW_EN),
        .D(WRITE_ADDRESS_DATA[13]),
        .Q(DATA_IN[13]),
        .R(1'b0));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \DATA_IN_reg[1] 
       (.C(CLK_IN),
        .CE(RW_EN),
        .D(WRITE_ADDRESS_DATA[1]),
        .Q(DATA_IN[1]),
        .R(1'b0));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \DATA_IN_reg[2] 
       (.C(CLK_IN),
        .CE(RW_EN),
        .D(WRITE_ADDRESS_DATA[2]),
        .Q(DATA_IN[2]),
        .R(1'b0));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \DATA_IN_reg[3] 
       (.C(CLK_IN),
        .CE(RW_EN),
        .D(WRITE_ADDRESS_DATA[3]),
        .Q(DATA_IN[3]),
        .R(1'b0));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \DATA_IN_reg[4] 
       (.C(CLK_IN),
        .CE(RW_EN),
        .D(WRITE_ADDRESS_DATA[4]),
        .Q(DATA_IN[4]),
        .R(1'b0));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \DATA_IN_reg[5] 
       (.C(CLK_IN),
        .CE(RW_EN),
        .D(WRITE_ADDRESS_DATA[5]),
        .Q(DATA_IN[5]),
        .R(1'b0));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \DATA_IN_reg[6] 
       (.C(CLK_IN),
        .CE(RW_EN),
        .D(WRITE_ADDRESS_DATA[6]),
        .Q(DATA_IN[6]),
        .R(1'b0));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \DATA_IN_reg[7] 
       (.C(CLK_IN),
        .CE(RW_EN),
        .D(WRITE_ADDRESS_DATA[7]),
        .Q(DATA_IN[7]),
        .R(1'b0));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \DATA_IN_reg[8] 
       (.C(CLK_IN),
        .CE(RW_EN),
        .D(WRITE_ADDRESS_DATA[8]),
        .Q(DATA_IN[8]),
        .R(1'b0));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \DATA_IN_reg[9] 
       (.C(CLK_IN),
        .CE(RW_EN),
        .D(WRITE_ADDRESS_DATA[9]),
        .Q(DATA_IN[9]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT1 #(
    .INIT(2'h1)) 
    RW_i_1
       (.I0(RW_EN),
        .O(RW_i_1_n_0));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    RW_reg
       (.C(CLK_IN),
        .CE(1'b1),
        .D(RW_i_1_n_0),
        .Q(RW),
        .R(1'b0));
endmodule

(* CHECK_LICENSE_TYPE = "design_1_RAM_INTERFACE1_0_0,RAM_INTERFACE1,{}" *) (* DowngradeIPIdentifiedWarnings = "yes" *) (* IP_DEFINITION_SOURCE = "module_ref" *) 
(* X_CORE_INFO = "RAM_INTERFACE1,Vivado 2022.2" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (READ_ADDRESS,
    WRITE_ADDRESS_DATA,
    CLK_IN,
    RW_EN,
    ADDRESS_OUT,
    DATA_IN,
    RW);
  input [8:0]READ_ADDRESS;
  input [22:0]WRITE_ADDRESS_DATA;
  input CLK_IN;
  input RW_EN;
  output [8:0]ADDRESS_OUT;
  output [13:0]DATA_IN;
  output RW;

  wire [8:0]ADDRESS_OUT;
  wire CLK_IN;
  wire [13:0]DATA_IN;
  wire [8:0]READ_ADDRESS;
  wire RW;
  wire RW_EN;
  wire [22:0]WRITE_ADDRESS_DATA;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_RAM_INTERFACE1 inst
       (.ADDRESS_OUT(ADDRESS_OUT),
        .CLK_IN(CLK_IN),
        .DATA_IN(DATA_IN),
        .READ_ADDRESS(READ_ADDRESS),
        .RW(RW),
        .RW_EN(RW_EN),
        .WRITE_ADDRESS_DATA(WRITE_ADDRESS_DATA));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
