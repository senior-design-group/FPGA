-- Copyright 1986-2022 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2022.2 (win64) Build 3671981 Fri Oct 14 05:00:03 MDT 2022
-- Date        : Sat Oct 14 13:18:00 2023
-- Host        : DESKTOP-QRFTT4G running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode synth_stub -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
--               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_RAM_INTERFACE1_0_0_stub.vhdl
-- Design      : design_1_RAM_INTERFACE1_0_0
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xc7a100tftg256-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  Port ( 
    READ_ADDRESS : in STD_LOGIC_VECTOR ( 8 downto 0 );
    WRITE_ADDRESS_DATA : in STD_LOGIC_VECTOR ( 22 downto 0 );
    CLK_IN : in STD_LOGIC;
    RW_EN : in STD_LOGIC;
    ADDRESS_OUT : out STD_LOGIC_VECTOR ( 8 downto 0 );
    DATA_IN : out STD_LOGIC_VECTOR ( 13 downto 0 );
    RW : out STD_LOGIC
  );

end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix;

architecture stub of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "READ_ADDRESS[8:0],WRITE_ADDRESS_DATA[22:0],CLK_IN,RW_EN,ADDRESS_OUT[8:0],DATA_IN[13:0],RW";
attribute X_CORE_INFO : string;
attribute X_CORE_INFO of stub : architecture is "RAM_INTERFACE1,Vivado 2022.2";
begin
end;
