-- Copyright 1986-2022 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2022.2 (win64) Build 3671981 Fri Oct 14 05:00:03 MDT 2022
-- Date        : Sun Oct 15 16:06:51 2023
-- Host        : DESKTOP-QRFTT4G running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode synth_stub -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
--               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_Max_rst_Counters_0_0_stub.vhdl
-- Design      : design_1_Max_rst_Counters_0_0
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xc7a100tftg256-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  Port ( 
    Max_Val : in STD_LOGIC_VECTOR ( 8 downto 0 );
    Load_Val : in STD_LOGIC;
    Curr_Val : in STD_LOGIC_VECTOR ( 8 downto 0 );
    clk : in STD_LOGIC;
    Reset_Count : out STD_LOGIC
  );

end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix;

architecture stub of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "Max_Val[8:0],Load_Val,Curr_Val[8:0],clk,Reset_Count";
attribute X_CORE_INFO : string;
attribute X_CORE_INFO of stub : architecture is "Max_rst_Counters,Vivado 2022.2";
begin
end;
