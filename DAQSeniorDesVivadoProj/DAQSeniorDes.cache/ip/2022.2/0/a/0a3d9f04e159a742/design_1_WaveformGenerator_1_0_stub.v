// Copyright 1986-2022 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2022.2 (win64) Build 3671981 Fri Oct 14 05:00:03 MDT 2022
// Date        : Wed Oct 18 17:37:07 2023
// Host        : DESKTOP-QRFTT4G running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode synth_stub -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_WaveformGenerator_1_0_stub.v
// Design      : design_1_WaveformGenerator_1_0
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7a100tftg256-1
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* X_CORE_INFO = "WaveformGenerator,Vivado 2022.2" *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix(Address_Data, Read_EN, clk, clr, WaveformData)
/* synthesis syn_black_box black_box_pad_pin="Address_Data[22:0],Read_EN,clk,clr,WaveformData[13:0]" */;
  input [22:0]Address_Data;
  input Read_EN;
  input clk;
  input clr;
  output [13:0]WaveformData;
endmodule
