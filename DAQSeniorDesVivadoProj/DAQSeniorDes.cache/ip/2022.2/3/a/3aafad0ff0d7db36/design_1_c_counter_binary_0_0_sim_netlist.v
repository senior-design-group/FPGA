// Copyright 1986-2022 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2022.2 (win64) Build 3671981 Fri Oct 14 05:00:03 MDT 2022
// Date        : Sun Oct 15 14:43:32 2023
// Host        : DESKTOP-QRFTT4G running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_c_counter_binary_0_0_sim_netlist.v
// Design      : design_1_c_counter_binary_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a100tftg256-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_1_c_counter_binary_0_0,c_counter_binary_v12_0_15,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "c_counter_binary_v12_0_15,Vivado 2022.2" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (CLK,
    CE,
    LOAD,
    L,
    Q);
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk_intf CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:thresh0_intf:l_intf:load_intf:up_intf:sinit_intf:sset_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, PHASE 0.0, CLK_DOMAIN /clk_wiz_1_clk_out1, INSERT_VIP 0" *) input CLK;
  (* x_interface_info = "xilinx.com:signal:clockenable:1.0 ce_intf CE" *) (* x_interface_parameter = "XIL_INTERFACENAME ce_intf, POLARITY ACTIVE_HIGH" *) input CE;
  (* x_interface_info = "xilinx.com:signal:data:1.0 load_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME load_intf, LAYERED_METADATA undef" *) input LOAD;
  (* x_interface_info = "xilinx.com:signal:data:1.0 l_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME l_intf, LAYERED_METADATA undef" *) input [8:0]L;
  (* x_interface_info = "xilinx.com:signal:data:1.0 q_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME q_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency bitwidth format long minimum {} maximum {}} value 9} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0} integer {signed {attribs {resolve_type immediate dependency {} format bool minimum {} maximum {}} value false}}}} DATA_WIDTH 9}" *) output [8:0]Q;

  wire CE;
  wire CLK;
  wire [8:0]L;
  wire LOAD;
  wire [8:0]Q;
  wire NLW_U0_THRESH0_UNCONNECTED;

  (* C_AINIT_VAL = "0" *) 
  (* C_CE_OVERRIDES_SYNC = "0" *) 
  (* C_FB_LATENCY = "0" *) 
  (* C_HAS_CE = "1" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_IMPLEMENTATION = "0" *) 
  (* C_SCLR_OVERRIDES_SSET = "1" *) 
  (* C_SINIT_VAL = "0" *) 
  (* C_VERBOSITY = "0" *) 
  (* C_WIDTH = "9" *) 
  (* C_XDEVICEFAMILY = "artix7" *) 
  (* c_count_by = "1" *) 
  (* c_count_mode = "0" *) 
  (* c_count_to = "1" *) 
  (* c_has_load = "1" *) 
  (* c_has_thresh0 = "0" *) 
  (* c_latency = "1" *) 
  (* c_load_low = "0" *) 
  (* c_restrict_count = "0" *) 
  (* c_thresh0_value = "1" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  (* is_du_within_envelope = "true" *) 
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_15 U0
       (.CE(CE),
        .CLK(CLK),
        .L(L),
        .LOAD(LOAD),
        .Q(Q),
        .SCLR(1'b0),
        .SINIT(1'b0),
        .SSET(1'b0),
        .THRESH0(NLW_U0_THRESH0_UNCONNECTED),
        .UP(1'b1));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2022.2"
`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
KdkdvVsuosc8qR9X5PxQ/ghTeTrEz4qKVuenhDR9wRSL/BO/mhSwQtiFj74UO0sGv0zvjAntaq/3
l2/v8gOiVKmM666gbk/2UCISA4OFA3FDR9jYmiXdNXb2qHeS1ywQz5n/sTR5iu4KFEfwrl3IXtQw
aEiGegL+CQMaovJsto4=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
pZCj3qT3VD1SCS5RiZExsqqu16KpMtHXilQL9p5/eBl7qrfQjT1VhFtVbYUusepbChjsCCmCn7hr
72SuHmOmDWG78UARN7MLdO/+sePuyS06ak4nAw5xwjT0g+9970uMWYKvTeeYqoz2i+k+zX60Cuvu
iwBfxWM22DqukHlYzbEFWhNyXIkgJe71p67vGdXBmqu4/2wmlwGApqBxlwR+alwZ9UGHlxNQS4N5
z1wHu3Cp8LwGRjlaXjElcY8RDpvyz5l59ey8ar5HXR9Zqf6e1unE2NdhzHhEGRerRFXoKZppk1HB
6kIEY4EHAWz+HvPcqoP9eoYKDazoAGkJRVP6YA==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
gLgm7VvY3cNcNvdXvikCQd2nRniE4ae4hePOcAUlPDMoHDzQAD7Ngo12MGFns9JNPcCaUXfAmxL2
JNGojjrDRUWrv8FPV6FOEbDHs96fef8+gqLF4OqLck4kWpKhnJwaJjjzQirvXEzZxP+GsBKnkSp8
ceVlZJwP0F6XRv+RpQA=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
GeZP242oKQSNuofqDs4oIIXZEufPhRVrlFFeRSLY4VCxhMEMwfPrNXe33xO0zIEBoPW2X9mvUoTY
izdWQEtWImFzjzPCjkSLhEdIMmUBH02Y+Tw3eW5x23T0cK96pmoV2MH8kl99I27MN6stVd977fuB
Mjao5MnSXIGZ/uXGtgfUO9Zjs4/2wGmsI2/lANN2WOL9Sz4xeA8k40c2dNYgxgHoCwx8Ya/RYIZS
Cpuvzq4ZyFSNT/kMXnUmqj75/flpXT3mmyW+frexux3j9PxpKHmxAE9crvDx85rMamGiA4ftl+ac
H0FtL2cBqdlP60x+FjqleWCJoN6AYdxA0YZaeg==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
URmEGftuxvv0+tViRUdsFNnPXucZlVDfUQpjjXkpOA38QUzsIL9j1pGGp9doC4jcg/9MD149BTSw
vAG8684a3k+Tx/8sFGl/viK1q8ty9nktEABSahv8Etm5ZJVAzQJT7EaOzrYqyywSwabogvGUmN/7
DE3eOn6+sMCiMl6BLUhYyK39ntTWNFYVPiheclbBb36V1vzMOQl0mvPuS4hDXqba/+qBZXhqeYWK
ceNfwci6SsRRef6hLF/1S+20r2uBxJeYJjyfWGGFEGfxlAOz1MiYUUR/bEHWnbjwIcJTBHQNRdq4
4Ryb+iPuKcsXU/8ApD14i6ScW+VBPWSqnH9w+A==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2021_07", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
NtQgA3rUKfJt+21sTot44yr4gmte57FoFl8Q/327tsRJeEyNAiwWZaZN2mbo2NFcvyN2GhDw6avJ
NsF1Oxs36P8shoqOOiloWWrdTcyAdMhdk+UjeZgKcNSqd4Js87w/5LVQTwjB2mcBDfe1jrivv+IW
ZRBC8NvlW5z/1wF7+vzXRMziLQYeOkLB0OkpIY+eT5cZXDKuZ+4l0FMPjd+El96JGAEHG7Q0qS3F
OEApYEp8+nSZnragoytq4pkhVJEC22ye0hBhoBClJpszCcg0u+Ugf+mYZsj8BC2uqSY6Hh/gpjjw
enQ7aEYBaUR7GCwQN7fZmNhZYtBkyvNqydRQcA==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
CpIFM8Y8dBmpjtOVnOKcfppEFV+c1cRgsQtewNUe+5apiLDoRCdMyTqoCay7nz+Xagc0OvfZDg/Y
jSTsDjKVcEIyxOfix7iwjKW8Rz+a5wBIatI8wfCo7uLtuucz9otOWWI7BFQ2gn4VdQ73HJJlZMMY
OyEOd33tGjNSjxz3W07knDr1FwTE3BOfhq+Qj2ErnuV1dQbrTb3MiQMTnHaTCwtz6ip0pD6b5G4K
kBRUYe+UNXCMvSfNIN9MPSmolO4MjNwM5gnZZqLcR1hGuzH/Yeb/jPnhsZ7jFvlTT3nsM9JzMRAE
QwlzVuulHKQDS2I96arFosYPYMsalmn6CQW0gg==

`pragma protect key_keyowner="Atrenta", key_keyname="ATR-SG-RSA-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=384)
`pragma protect key_block
qinIxHFISC9r9LS7OKOuYVGM5EBkuuQNV1nDRui+QVNLn2QFCrWPeEClQIlNViKOt8MX9urHvu4e
l2L+eZKw6+St9cW9yUsYu36yoB4LqwG+vKvfR9CW82LGPyMAxdgk/p3n+F0Xp9Y2HaERwWDL99tW
V7cDvLLhyIwz7w4rI0BWWV+KMjXP2F5MNgykzZn7tzV8oY6MxOykFqRdI8DLAdlYGAs90wjJ3x84
S3fHciSox97FYpDi64v31Vb4RmRrwueXcvCc3w8gzjuwg7qraWLMYyPB+mERB2v1htX80PsWWVHE
QXkWiHWYvvrXEykUS04MmLNHpV8ZgBXO/NBEGn7mrITDEswk3u1Yviqy7CW2wLPQBoo5xW+uiu2e
8YZV/E+bAt+P/EH5RsC9alBgtuVKU1s9DaiEH8eUPEgJQ/TXwQW01pg8ECTYgiBS+IQSbld23aq3
goVo0ZMzRu/SA00Jmwt7upvsMkh9Q+2732ahu1FmlSNmyNGB1+bYf782

`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="CDS_RSA_KEY_VER_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
T1jqx5hmzZZMhPApzUC1oZLMAkHma8Ki4b2CvLNqxSn+MNWoTPomvQ775DMBEDai/gahYALsohdX
0f/e6LuPqt4zYtyAzmH+nRgOG/tilS1J674KsaHxudAfo4sM3awB/C4Q3VdYsO9FgvPQylnYKSGE
gJ46W+1Y789VQqPbt4dpnprhix6sLlwfww7We6cq2wu4PilFzovejouUBZqNMZHYi4suKcMcenp3
C7QRKloo8IF9yKrhGPcRJLQt2nus3bI0Q3ICxRk13Nrfhh/z4cdm0OGXz42q44snFEVy1lLxPOs7
W9tSe5ag3923oCT4NGGgK/gMTx5qXxFhV2MJUw==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
La+7zOyf4g177RhGFopvWaIBBwH0uG7xtqqbzY1RTNA1aub6W+fDDIDaop6giz3TWj0zQi5y8caC
CX7oygeIx90blcXxBJHrhVokJeDpx7Anbtvn7HTCg6SD6eXbTeN2mE3dMNW5PXGxHtS17ZhUND0n
xmYTh5oF6xd0EzYaqVt+Dfqq0Uf/f1HbpKb6rTJLrOoLiqFvqzx3hqhBPPoX4f+tVGS+KI9BOA3G
1g3Xm6iQwq1PovahKMayUu/pcQwQ2jc5tI7+Um6rKtbcCzQF9pS+ZSEeB5gCXXRsbqKs1rHipffd
etosGwjYrRZevx67S6DaNOrU91FNMXmPueP95g==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
Pk6lnDbhcpVWb6iTPND6w9YXYZMb1XQEU3GHkElGF3gNTV2JyPr9NFptl9E6q6I8CT+R1Kt8z9Xc
292oMcIQI5YZwSc1vhyKuvMWMJDwlLeoQ/q1on+DUiQf2M4b6bjKuih89+HXvLCTk2MYSiJYqv3l
wiNvtrDFe4PY19lmWC/aKmY1TnxBq+cXg5a/tD6Mr/NOMW0q+3ap5KsiSCs6eAWtxkD9FbKGfjbx
ONvPAYmav6/WIdOcFCrOdqXvTWy0/xk91aTD2GETEz7fgoBfXxQuI9k5vdkbBNYru+yY7MrYJHa2
OdR8VTFnV0TC/RI+5/zvD7tSkM96lA7KggLDXQ==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 13008)
`pragma protect data_block
V7T0ASHkckC+BGppJ3MWZufVBX+d6XjEU+rAoXsVNKMqkI1KPM2G/71vZjrF6pRGC+LRUcq2Yz37
oeqq5B7FXHny85PWAjgAqV9BOxZGE0AmzMeRd/ksNoL8qjRprTFzIPmy3sh1eqN9l+aD3NWASOE4
+VHhkN0Awj+Kf05bvS+S2VsQr6O6SsU3m86pNqikN94ZFAnYpJqXWPEYnLf+0WmkGW7whAQEMDxG
599cSbmnn58YeYnPlRWoW2C7VFLWyjIAmq/gT+DNugmtyWKfadwykVX850IbQVgOxtpQrTHVHRdg
J5ZSmbAwawIeDlu4UtYgzXqnnc/T2QmFqezRDd2hEiyw/4twd4LGP9p33tpaJ8br9yY6JBtLIGLh
rc04eS3IQ7kw9M7Vzi5lCuCSQn2+NRahOrM3qNbKJhZy8QpngZRVu5XNAn8gvwBdMD56XNo7QNvv
DGoIg0j8hzaNCinN/pVFXkzun/ydC0t6RnLGZGJwl9aGfUSbuG07eqWHlfwSQVRgX2ss/URVf/LZ
pocEj/8eg0Nmc2cTudl9nQw3kXwFlTvQtlBR6BOhzE3XAMKOaHT0tHpNsKJRfMjaVNxTlEN/4ChC
BVGtjaKSXaE7ZiIsFPzg93Tqsnbou6oy2YCCp5vOC01ekIs/aEkdOpP+UPGj2F330zEYOCLMFhdU
0YaZr57QAB98FDxIQOmOif1c0k3Yd7ptezD4UBCg2sBoNYY9PIrQbvcCjP9xZtYAgN0xq6D9HPlR
j96XflzJ+7DwfjjGYSJxO6j3ubff2rZ/iDIuipQ5C8Og4ccMQjYUJ26UR7Z3IWHNbJKnzgDwnK4K
u6PpvQD7jpO7O4gc4IJQgu9QLXHabJ9/uUaqgLM0Eens6wWStiYPMMOVkxwlHhqk01jpsiEt8Y+3
ZPiAT3UBQm6hlSoUAjqTLFK6o52nfM/rzUklka01fn13t+On4KS5RdSPxa1uC/kUCOYpt8+FEpv1
k1sVhtoEyhaSNZNaL6c2s+leoZKjEpuIjBaYSJYcyQGuQiGjESN8GPYzHszgIwhK7uAu75fOetET
7H5LSqkDhBNiGanDMiEKrwyxvmgOd5KM9DngdUzRe+yLPjsPBVNwu6yRjsMgK9vPZYy95PDvbElJ
gbhEmkk81gPGuvGkQV5MKZ6M2GNMZmkKsbmka+QOJgDEsfYEbLudcsO8epKOGXbBpwe7qF4+zaZe
VjIglmTmGUDITDs39KmwNUheXiRa+ix678lj0uttbHWQPzHclxePGzh+SouMcjCW24BD/nfu/0KB
6UhDq+0bxgjEJfDev4fW2vkj0+OVDHs8Z2NGsdQ5UlAg+UavIXR+6GxuEGcs2ZQrkcuEH/Ny/LqL
XPBbbk9RqT8KZLna4ZhnCCau6sIQw2yfLXoatbOGAB7wt4ItxP/IYZW+s/3naakKDEHb5rDGTymC
GoRleir6tTWhqvOR/aMqt3vnb2mqBMYGVheg3u1jrFt1V0q4Uu2Q/WDs0j+zbJ2GbM46UDKMExoC
Mm4S2Kb6ddL82IdGw6iZFd4di8kLXZoJmQkOa5Sj3qICxFq6651UlXj2Svptx68u8Fc67FFghg73
w4+UXZT8lVPob/WQjJhHwb1gV6XfMGxlGxdclTz3FCXBVq/Cait2i2rzGM7GG9DXRHExu6XaaUCN
MeDIcn7jf1Zna3wJQgE0FoVPhUckUngPUdPm8EZ2iuvumbo8e6m1gD6QqJ89977Q0bkVh8/npr6H
pflp11k2WOxLW6G5n17VBD2MIFkhgj2HMb7dvcU/sDvof0ocvyEPHB0vxn/nQKcZ7n41jAOS8Nut
Ee3rqYaxTGsqONbgjlSQ4rsph7jSacVM6tg9I7jpFLOyFJtXSnviEWZQ5PT3Yh86gceY2IjXgL1T
KRB5qiO0tE0/Ios+HLToj3Eu0KJ7KblXdwZGOm1x3yz9INfnNXED0luhZyItuc2CD/Kt0xcKSDe6
Hwvs499mKb0gBn04fTy7MgtnRv47eg56qLG+8jlCGQ7UayCzf9OKBnu+q9liYDZi7Xpy0PBWmW5l
+SkDZ471CCvLJ06T291vQkmWZ0s0IkF4Av3sFpZeGNAq2nQgEWjMt0L6xgD5ZN/BU0lYHljrFHDu
rawA5MkhuEtHyplXKORxsa9F14kLkHMN7wNcKZbqO649OzcSyJ040de0enErqGiq9Dji1CfCAdCs
6jLP18cKYLyOamaBsn3NRmyJ+QHa7ETkZg/Pcz30SjwCRnRnNxigMsuDJIsfiIHmtcKYEu6zbFKL
7qKz/yUYGJEHBEPQTmQQCNU/Jvc2fj2lVsXlTzN+IyQxEPmYLGBU2R64NDWCMcd/CEbkZsfxj4N+
9ObDS0XhA0HgYfjoZlWLOVtUEJrrhpgvNCFqIGt1v6/E85BUQdZhiiOxr4JoQZsz1ioj5R10hEXb
Iq6CNW1umDajuYFsYhzGHSepoNu3MKcBJK/DBAnU3fdECbSo7AtlJxAHYllT0IYM41pXZJBntICs
sCGvwDD3um4kyRK39GkhfukAV+d7NhLgmWGufS4+Yh0V0J1+xkwZclJ6wd3FJkpZv2zUVPM75jLB
0HhKOEjBGMbgseS4GgkMD8G7HMw2gJlwv36DsMbgJaNXoSpccewk5a6QxDVDT8CO2R5XBekbaATX
iJR7XVC20672LLtKe5RFTM0evbaLhZGfAy+/LdHHggXkzRShafFY8ogej7zi0BtVbdKYoFUHKH5G
DTBpEC90MCHIUTn2jkJoTZjqKTTBbphTXut+Uz7VBwY72+KMqkr4YpfuJJbe9ZNdzxPm9tYQaBnt
qHdngkYdS8f7pimPZDuD5mMkIKoVjMX17X+dYT0dI76g/lOkExth0LIRv5xjD5tAaOONRQQzw1Qo
gtdmviJ5WIz1tL4eny71eIJpUyG8A6wfisNUoucePuXdYVHCEWV/wjO4aaq4zfxQc02i29CXtRo8
wo3JZna3uPknqIuvKfuqc7BWNXHMKCfqQ48BqFvzNmZunx10BrE+y6BycGdUyQKTyH4h9KSLLI3a
6WeEFo997XRtYY2pJsD4YnPlEez26Oxsbvqayo857hC3UWK/2+LUpaGQqKvlv2VQjnq3gVVtGC5M
Iab69rBiKetza98nhgkrP7YuRqxyt6jBisrnn2LrZfvZy2fK3fxB6J+HOcYgh4zApriTTxYvp0HP
6krLr+uLvXDqCw+SIsZ4WOPehd1p1jBiSscaJ4hPIlimLVM/1VEb+H5asgqUfqsYZ0g4AIRCGJNZ
OQVKRoLnJO0OXVkyHrjFzUbQPf1Mj5+Bdab1aBHAhTwzL/XpIaz1ZR+mi4qUEQVLkQpS6xaRL/jt
TDWUuDur796bNaJQL8yqYBuofQ/i1XacOsKlt0/VCfUlkTth56d1TXSQbng3IHCTXAQdFgA0PWeQ
5HjP8cQGa3LDJoScmmVXy3XtHw49ouzGdKNGT71JGsMsOLIPJhG2RwGhs/Fvc4BIe9bLzwSNTbKu
VXdHAwQz0Q3u92vWRjSkGTu0QuJ425yZAgPdVlByz+fjlUXRrj1oAO4Ut11VpD6+jhb879L/aygH
XEyqbQvTeqwECCBaht7eYjLf5JnMwD55KN80qgaLeUPxy16p2373/92a9ADzFUaxbRo4shrefUYK
yIq4Xd+QDrCMJOKe0F88pOew8BPzGceLnCFke1t/YMHKT+w6ln6UjfXmhVws8Xy7iN8S68Ju0kKh
2jR8wXaaiDjbaKLaBwswuZPyZxv33OG0ZfDCF+Z7pNK6b0G543xjQEpzGTMeYMgPM2P9SooMtBdy
VU//AYOV4ZoBygMwMNgmoEXkom7CKW841XcQG72CPk7xuzLMBd1QXA5EIsvz5RCnCdfUs6UxCjt9
cs/3K4Lkj/HwfuZwmTyljZsvFfbWeiyifhdJZ0lyf0EH+8x4GSlbKaHxXTlpboWHthj4SdNdwQjd
soY18AxczCVAqZhuc0yTf3LAsRd4TVmDS4ZSeh1eMY5u+7A8y979mTYsAiJtU7ZFfE82PnqcSQo/
9lML/CoHHfhUrPmurOBiiAIgKcQsHLZH+VLQHT6sm3UVdq4t+nL06Y7aKtli7x6lymOCboDa8dBQ
J6wd0JIU9bONBjhD61cu7XZp1DoxTJeZ0q9eAxpJOyonbGsyQdUx1Ek8YSMcv7EAxZEPJ84DtUPi
dm0lG9ou6q7dUmYtUjXEGhmKArjF/Gcn6ZLWDdz5SwJgG94P1wDyoXRXB7kXTxcANiTSE3en4v/E
2Cb3NsXlsxfTRFwIDLXLnVHj6upXsq4K9k7YHPEU56aFttkykOlT4LI0OCtRS96UhZUTtkdeGZlj
MB0OOBBT9cBAg+KcudidjSi7lHeJBEh9sRI2jk12eljezMCvGpXjGwe5XCddM6qoZej+ZXgL86XO
uyaCVa7G54XYtsM1OuxRltI02hC+V2vehqY8MAjC2e2RkWmTwo8UIElx3unNOnxYl9msurWJm1yH
Kdsv5vwTyHbBM5WIaHkYlbiDYD55AHw6Q1miQACiqSK6CYDNvENW3Epx4PACvV1oyirFOlQNbtqz
rq/dRsy6qyHXr8rcQ+0RM97lCcgWgdqdUO7+TsNDtJM0PFAl/7H/cgeQ+ttAIs3qQEF1JlGMJNtG
SpKYcYzqpsXWTIiRsSMAvfkahkqH7foijDPq+JnyuoK763A5J8vfoAKH1uUxrOaMIOF64TWexOJb
yZZBjThphXTbT+JFfA2HceX+Fq0+VNBqe/fjKcXoavyP5e0nGf9WrmzBgitHr95s0+eGLJBtY3bg
vcuSX0m6AXeL+TDRI17ubAoXlbBUdHki/ShmkUWrQQY6IDLxlSeOCf3WoaxxJxOberx5dytYo7Sy
L97uFIRYMvtIAkKgsdKBDOdaa0Bz/j+CfwHx+sKRSPh6/E8ZPUeIeOFZss+p19ny2RYbWPqDmdLD
1KURb43xL9WX+ucR5f/s5kzhu2JbTbqlLjsprYyfMgp0PlmFdjntcjZH2S1xeGlDPcXk2yLKBZPB
QA1A35M9dOr9pFiDWIkqO2CstNWCSl8GKuup/ZCmL89CSfyj1LVn8vIp+BdhVvxvZye9W+YD4kHJ
bAZ9qpl3cCmZmnsVmTs98LE4mzY+UbM6wbyluJ4NbftesRWlsnb08QEgavapKtLoOU9Mxi5ifpSC
fJLaZXpk24RhIa7jlY+Hl9I3qBlGRqWtd3ePzPM9k5b6EPz8D1xgXW2+8lp8wj/agEfWbTsOGoy+
rabSrKHcyuLzUP3TzHTmEwdgsvAauTKGwieErED+AZKTnN7hPanu4YGStuYKriVqdTaHseoy38QJ
t/g2uoDF8Zw4eRu5OYQqV9rgupS1o3rhje3cLR/4BGr9uyF+dZIpSNGWdCmzw8EgliomYToyOviB
llo0hNIDl3aznIuPgMfztJJ7pTsNSW2+KH3UdGMIVX1OcQv5jy2sV4YTK7Mc8X0DkYaOfSaoVo0A
0FH/bC2eCBmGSKMs902AyGPF/kNzAPLrr7kmNpWYyy6TebHddmJhIu+5+GF6JxWWredhScd+U0J4
oFqiSUCU4DbHQiIL9SihWlOfcx6u/k7O2OSrWdcpaieWvALByxmd9wRFgGmI+Z657nBledpo2fps
zxQwxMpayzdOBE0gn/WF5/thMDAkCftUY2m+1Uz6xn46tGCSXOjuJzrPhRioyp7fy3F16bpp6gQy
TJ+fpYqRzTGIVKilhNoF5n+gGu02fTQoD274+Zb5AbjTvOBh40OwItmU6TOZNVXnEZXm1f7gppL8
OUs35YjfV1F7ExQWQCVc932LZvTollO5UurI0CedX9QF1cA8TXZkz7eAe5Yscv1AhIT79Bp19QS/
2h5Rk46j8eC7oZG76PNyBZmu5uBz6UzlJ+lrBabl281SZ5BQPsedHX6nvJ2XFzszYszvo7LTrn9M
DmCsySqncFLumh3bUapsm5qi8YokXEc2dAu/Ua7EZUUNAComfibxRTIIzxC9s5dicivwIBfOY4ta
T4+1X0H9em4j08IR/nt2LcvC0ghiyJKJgURg6Im6F6cIkrEqzvK0/V8ter6ML2akuGvDuVdmyGsm
9fz0mrOChWL2BpnvJ/Z0r+pGtciEcfUcXU3e2eblaXG1TVKx8Yah/wbnayUPZXRVlifjw5PhptET
RbRWTblcTz9+XX6nFOcYdVHEmnxaM2oujO/2GchO0yDFILvpoLjsTt4r2ko+GPHMGJBbQdxy4aBd
NTQYuawc+3E5nEWoNlLaNRCVowBYz5dRffh4A+M3tkRZSnGblaQn7EH4X4gmuMm+JNrO5Egb/9Ax
7m2gY/vShlvrYlme3mE0umUsGINJ1xoF7dQC71ns7TDhBUXQAfxtTdGhru9RoD4UieQNeqHVryFc
HZ51Div3IF5FaXG2RdgupmxbL1FGF4v5YBYZaw8/xci3DeWvDyBgrIeSzBfcZzTRJ4faxv2K4Zpu
lBqYr5/oV5lUd5PmE8AGWBfIIt8d2i75YLYOsXHh7XPZK3KHeFS83FfILRN7KT7y4TeZHKJHNA8e
j1PSFZfj+Q6mwqWp5yOiaVAjF7odol2ye1VYNV3NGdywW6kdp0LBXcBVvfbTY8LtKtD6djNu7ep2
OEp8hbQXVdZM6kUrKkTaSV0gMcoazD7xSk1dhUvLcJdadDbANguWr4uSWwxHdu4RuERvbxeThcfa
+UF25BjytWrQ4GekHueIUzSKNDrCEV+NGEkOUJDkrF6cOwZcvlzJ+hw71PX8ND2tQ5XiXosxtK8p
YjJuN9g9emvmLvDTTDKxOwJvugljztHLLO8f5HEEwhN8Y+1F1znm0tbFz5rjVAlwK961V4094MX0
CtwIOJYxiOgAytmENnKnuN6woIdvfNf3vt7sGtcOY/evld4TLBcEx094ZblUeaMTQY8GoqvWMbuA
kyua+MbqgsRsJ0g6Y/TuJJO2PGx1vXR57yN18uYREab76obWYdiojTGMnQrMwKMqXeeUmLw2zAuk
6ft94NNYV/0igCCNBkL2Rddj6hUzqaxLU+AGBgpjOgG00SA4Avaae0voL251950kn46NcBdaJ6J5
qFQVhHZsB8BBKi/2kiVaR0oOhWC/NHc8ZH36vnFwEy4VB1MKDKc2lHsGpQL1jiH64LrYIys+40rm
QD/FEfhX3+z5Mp+1mbHIxHs4px7lwUhYUiUpBMbUVhtOl94DWB5V5YQsVQ+qO9ugzBe7+vY9Y7if
INi2w9wWoI9iuCeulAtaiJctquXY6yXoie42yE7OgohOMKlKkVUTisMUPW8VXlQpWKYOmZfwUygn
ux1N4WxRHzuURrY1uU+nJhB0b9cxsZOZmrJUCKQ4R9q4nnxj7euJW2HDuYhjdngl4E0odHG8wPD/
NCr9K/libZ4+SRlvQnl0R2fpOJVbFuTJBY0I1JWTX4M3j5h3NBBpMFjZM7sJuz1cpG+O7Xl6Wm+Z
rYnjDboxwyYF6CcAXKcUkAWDV6e/NZhvIyIZ1KDTQA78NEzmMcqhOohrK6UFYmy34J7txSc6XJok
G9diNIsyhhmCGpBi6O2f3kYeSzLXpo+Vn2KyH0wZpi+V0vxAC1xXXjnKmaK8mibla6K2FZi9u16l
oCCr8eL4imtIxZ8Kem2jmH3ftX8kLzPAJpVJ+dO2thg7GvlPMjOVryJ68Ug0Xzv5ctXsBbVeVaVy
miqYHWsElB9kHvzfUHmsoPQwiKbb/zv5UyIGUtWnodSGxsUBbpWJirV2sMfK3EqhIhScsSyzaOei
42ocNw1rC0NVORCwBXSNdAL5269Pq4XGiX4T9yBrJjRWvuU2i612YfVHhxNbveO+n6JuZXUHuwjB
VzGNkXChfLK+6v9HdVAewhL9DLMKOlbyv2xosKYAJonqojCijB1KWseRynWn1N+kR7y6N/CA0o9/
BYkZFAu2fpuN8Ah9f4dGND6glFdO/z4loUNIgKgloKrvy3VcvStAlQF98gd1c2LZMQglIS2/NA5h
7zSpRQ5OVxuuJ7KyYUfm4P7Asu/e1Y1gw0IVei3hBiQJb77j5iR2ypSxzpRUIJ3ESa+w8TjYjOU7
luqdMVAc8XSA++UFDxDdOWUXq6VNWj54GAkxwS3ZUR0rrWKW8vL5NUIBI4r4WVu+iYPWZABQOa0y
6b5fuiPJC8dCkQpM3Xcd09UotG+2dMSWnVawkcjGb9sKEwHOY2kU5H4Krek+Cv3b/K89G5H1gIGo
arhfnL6Iub0iANdjS7PHg5cZm9Omz0nTJ+iUNw/kTI9dCd0DvyoP9gzXIL3Shccq2NmnbkzhKg9T
4CqDkhKfIWGe2kH9AGR4QZRdcWAnX0LgIrw5pmFoKlMWY9AvJFD7cT9hb2V0Yw7N7tyZCLyNVbq0
GgIYn1DcM/io7cupd36Ppxmlbf3NnmzQjAm4EcGPEIBlX3Ktm/96TDy+ixuT+tfkonP+Ykoz+hxi
DmePt5txIVnWArINQe3OL112wgv6rePd7OcENEjlNasoBgk/7Wy97vl5aSD2koni+K90+34r9OTV
M/J9QeW/EXRpc2uN+egFuqSixlxsFmHdq7TPxiPTtnYRhg3p7rKVarfQLBVmNrAePqLD/V1t1Pjk
nS5oZbD6KI8VQdlC9EuJKlGHYTQQ3tbqO3L2jHJ8sjuowdxg4wSdGkXzkevzjkqaNw/UTGNVvM4X
9v155pMoX56OwDUH83fq0nCZzGdQeT+U5ChuUDWbwEzCYB4YmS2MlY0gQfFR8/7xaKB8UPnVnwuL
2ZSWp8dYe5t58Wma14eAt4ufZUlGAX0HM/FmQRovEydq1zNMRtH2yhLOLuWInvoWcGKW35QEU+0o
231nDM3yn42R0U+wF/EmttYkVOlo2uRuq2CqmkW/qA7idwhwoVJHCxPXDnF2eEUGnTHgnlImZlsn
FNU3hcVXFP2azenCFGCUi6zQaHHRyytzJiWb9F0Q6GUocFJeN43Dp0QZKI+t30wpdRd3ngZDKOwU
nEDrcVjtK6W/TPZDpHv1vTDnd48y7268VBpD1DrEkqmcbutQOjZGnXfHaYdyA0Oxv52EELIr0nIh
k4vTE22Og07FJ+epYs3ZJIELlP/0NvTZdwy/0lXFyDT5nG41g10ChDFx0MKeacevhbGx9b+lhKWR
p2MCRYUOnObjbMhY2pzqkMzWlZL4R9CJ4uiJ3ZEoC6ZFA1dj/cooJaEzLWeNdiNj9B/GzA/8G3CO
9eLZXjo9c0f6L4Me7mH4iAmLYdgCtWhr22neQ6a4ibRAtjW5wZ3vy1HUS6zWH90B68bbxOBVi8VJ
o05nLVKTVxdD1wWPXGP2BqF6YtQkcTLetvxP7nUGBMynCUF8msF1xIuCSTmXrLTV4BaiJAfRpX03
BgshGNbGRxJBqIDTRnU/Ia+EeBW2RtrML0jBJHnSQV9MIDQqiqseI515bwYyhKV9o8iJ1Tw61EKF
jR/CZN92NX6OC0IWqrNSXwN/DAojwYragAR8jWkJMXaFuzFWfVCPWmCycfyQMggmQTgySgm2/CHS
0ttNtzOA8noQTZ7TJ2Rmr/B2qA6sjsb8uqqp262VX56mgNrFPPF5rUcwEe0oKd8cSMXCAnRIJksf
G1Sz+BQWXdxrTLdSf8egVo53qz7fvlXG7x8PyUYx2rQX9f8M8s3AoghXt9+EhXG1JkXTh28t17Vi
86ybAZe7dL+fCnQDH+v3IesKvcvDOGwyJtfAR0C9NN9gWjGSWhlC06HXvY1jHWs6lwwqaW3ONq4j
n561nSdQvns8IrnPZHFmOlQY1k3pYxhRwgJC3+ncGwLVYcJb59CYfwASgI+HshLXcVFW0qlArBqV
UTjLsHu3Q62MK2aouF5mDJwHcBOieY0iT76MvZSPKohlyBEjfK283RVXnxRue+0J+Gq+2ZUT7GoH
lsMf+Bg8bwUsJdad9XsLsFZE/Cp9wOnLfnL3QCQd3EF8WV5iS21xxSAuBoc05x2OUR2oKRC5p/zv
D2BsB+uFwlt/WI8pfy23FXwFNy8iCrEd/NF6Sy08NvQXqI1cwME9zQ5h3F+ELYvbbUXdA2r2+715
DX2vHDiFcpGQqawpcvBEpezByraeRLux0SbPplooCznZL5NJDX8f3kHFlsc7TP02QfOVf6w6zC8K
r6BePgCoqfGrx+kgmlVM5SKe9PcxVXKHPpzVncfZgDails3rFbFbQKuGAKCErGilNeSGGH9Pj3OW
4ibySGGz90FUZX5DTWyLmyi3awwsinIemY/Du6Vbi3ChuilMCMVhXdi4II31sr58/UQUSQP0A+SQ
ex++dms60hxcxUmQlkKqCAbjs4Am2JB8A2UZH7bb2NYrHYPXjFYEgzfVKmu2GKRIyHeRRmsczCtI
Bt4kmQwrprUOuhqBO7PekcFDX9UZqomQgAqV9fqUYIVDIBWt5qP35xoFqd86FZRtMvLqF8OZiexy
76rhaZr9+sjSAA2poagWxlNcwAS/Hl27nxkZs7eAXoRnIzHbno0wFauU/+Dwk6RSiYzig5kMd5KO
UY6pJaD8pYA0RuZ9Y8/4oLaF+qXQRKj79Y1EbRv8uFBiCG4RSLFjQ7BQSHjUP3e7FGuAd0xdJyXp
N9vu2a9pt3X1/nBqRU7o1y4leRB5NAcDBhOZnFAgzABEwIzXc9oW6IH8mg8QT0cKN2DkOlF7Xrbw
AjgvYxwlV22aZ72roLB7RgPKmkpQCUNOLjc1l965w3YuLOA6p0ftUc8DB06ClPrHV9Jz5NPuJJRZ
W0IXwDahHYanwkyAzYVaiUa1f3PBg1hhf9lNjc4d3sKbcMBCmn3x9/nuc3FV+7f+crleRnrJ4Ca9
y/FvA5lfV5McC2/aOvRt4eulMwL80wzd14HtjPDaSc6ySd4yRbSQNNW5jNVq5kAiKxEmHTM4nkcj
t5RozlBLMdhCp/u1dmz88hovfLDHY96PBpbRUOUduEX+MgFX7QGQMzWHU2smZcfk6/bHXaGU0CTg
WQCIBEsY3m0j1miaXAo0kXLmkB7xe/xYyUMNiyQaRMGXYXr3vB4/VYCzR7FRXmw4eSThOwtZ3Cjc
hRDBQ2Ypwt8zLMHGTXByvkt+mS0dyto9qtIbzJyZLZI9DXVA2WgR1GzKABUfBAwXpGeI2yz3dHju
KH531J0tcIvvbN7ZBrEDgAFcB4/gcZnB2+2SHdpa4qXvzcIQB6KgCvKZfUw9j9INRp6L6oo0h2u/
nCAJiLO8L16cpdQBnjR13z0SZFEqeX3XUFVj9tCG93bmboLDLv4E9PkSG17wwHf6NsFFWl1XrDJ4
R6l/RcNYoWsRq7oA5gl3rVubpD/K9vlfbpTtyXvZH33bRFb/uPFavedE/9Rpp3vTVOTc7vXWYSdP
okNnSfaqvMZKEPs6gd0y21NUjWMcTxzyf+s+lae8Y4RVhDhAflYUIuwv/0PKQZESBcrTRnP8m33o
f3T3LtSse0LsxnsXEzq9j3adX4AmpebwZSAowsroxHEF0Y+9zvy9wwJzNEjPBLpgqiwuzma/pCOE
58PBqGSzSIL1Ev2egTQngy+PdAk2RHnE4bHJvpldioJyKd/3Z/I0XejrKQtYU1uFJYhT1CfLRJS9
dGAz+L8vfBi+DW3qrHpdju95D46LUbmQdA5yrIr+CVE6vWni79s9LplQ07FOUr0s/4aEvUofPe6x
m+8oCPGSn1N0D1QqFG8Lby//jQs3k4Bq3hnTmVXQcd7dtwxL7UP0nv/QXjhTIn2IEeEmf2JFVywc
TX/x4vR/ktti6l0BlXyYTHBqvAfPahZTiLkIMvjnzu05Qc4I47x64eRJ+G/l9R2zlOfHMZ9BPD0I
97XMO1oz1p53dntfGFSnL+mvaerTZrAryFHtrKsbewO0OzEE1hE+Bi/O4vcxaOUmpJPbteTz4j/P
1Nxf/K67HUauoZxtJygMgQMh1KCcZs9HfOE0iTc/2CAiTBSe/Vx0/qs3wWilllBqbksBdyDtShgU
W7LuDjlcAQQa5bFzyEYbO8HRMNSSuNn92oY0vRsoFcEyTZtX+9AdrtODuEA5cXJOPcFbyHWItc6n
CF7q7pXG2H3ET0Uo4XZTltcQWDtEH5x85NJQ5BGeKEmg8j5eTv1uDXw28IYX0UVBqm8g6ahWRlIn
QTebAfgKQ3r4i96llEFxbnvRC95DKaeeR3u24TX2ldzeQpt6b164NzxotY60+65d07OesvXACdka
6rrPBsgeDVYZzMJ7oMhS00kcgpjGITdrovXxp3peHn4hN/jBQfYw6PPtUk0ln2qJACKWlWyOkFuN
Ric9d+dMv/SYPOlFOR3GuCRQ9/9FKj+dNPKKtgIWp6ktkjlrZ8K5uxgfDF8NVlktBeXX10HrDDmD
lEiQ06u1VVGMhWc7DTUKg/XsyskVdb1lo3jv4X7+/xFhngoT58tU0FiBTEvzHbPDZ8MEbYhMW1no
UuZrbwmdymFELfxylm8ozOhG0gkXyZGQ1e/v4p0lxYGXCM7zTCphJ6PXawAIhh3wtOtYrxwNZxYd
dahBQBhgP0aHHo9x/yOG+F1T7dSN/mB01+43R3qy7gR6YB3Rzl5gvNeuCm0OH6coSWh2pMIwHQ8b
QBhJ6/r2fz2UjUoCs7f/KqdTYh2zd3joibxpoFHYhSSGz9CZZ4xuxqvwRKSadpp97YJbg/IUFRON
b0FDND8f8mM/dZa+d8uT1TUmgvVOw2dEnsOs7gz6d8qwfUs+yRz8kAYKsKfvyu/wZGXtz3UlTEwC
zve1FjmUPL79lPdlCjZ7Cw5lj0eBIIDJ62pPJJs8ZtwGePqF4lM9DkaVYaRpOSKxgfWuRzo3cirg
XPLVYDZpa/+GQfGQ5dqsDI5cav7AvVSR8R3/USwLepGMkUQpWeUPWwy2dv0LPne0O6EcJ4wTLYie
4eHb5xMoqQ2FvhXQHejXuP3/suGp0/o3PTbD2qZzUJCOI00lDVtvcdlKAUO0jRkLUFeEXOyZ0Hri
v04LuXtHNnaCUqjG5X7mhev6nm+pyI8tFfhnaN3T217VJlVl8DXzcxlk/fiNQRxX9HmXVQH+HePf
jrkoSe3MMvf4DdRCxTdYFUg4+r7h5tW4dRTbSBgo+RGFkqZC5sicI5YTCnJicIa3mnBfHHPIlQyG
7O5l/NAi+ydYtr6hULYkj6wGJAEdbF/Rqjro7UgqE4YZGWngnCTWyT2qGGbi+rLf49XW+MclSRdD
16m3mLty3X3KTJn5/SZahFjJXunlgu1/ZyugUwq0qVmnpv654pRe8e6HvSiRe3oumJPsvktVPn4R
Dcmbqi4iRAkV2Oq54wvfn5/9PjIU4wh8M0zxULM/pGmAtrzNjzugWp1OVOJw7fzPVVsFnoosSNy2
QSIa09xb9/KMF14mMOtHZNPJy+k8527gq4NLQc9VqmO4QQYxEt5xT5AGDiBLE0YEmXogj+FHavui
MWzSFKr1+gcO8VfQGmXzVC6i62IfYAw9Gs8KdcUzlCf5LZz6f93ZyteZAxCRRQV3puT4nX4CEG/m
YfpzuNmpHyAh62V1kBpT2MNTDPPGwCF0cOQiso0wmSOMhwGHQ9E4QMELu69vpTe4mNR8OkYS6IWJ
JZQtogkeAzR12U+U2Ivtsz/V0uAx6SClCAqqImhzSNosOhQfkQ7kjdqkjBZwTI4VuILDqYX/ZYOI
Sg41Ak2gVkyuorQg1BQ7lxfaNqcBDks1f8QhKSxGTcFnKXX8NeTmpKtdAK0efKBZdRSagFX7SaJE
kPNQaStLYn+0kCq4bnADHMnnDyi3wgaOMPr8wTG5qyzJNt0y6xWjZwHfyBxc1PwnjOSkGz2JBc+u
9CLCWvOsHF/8/5ArIlVjDS+zoWJJUnVlynBEASu/cSpOrKgPBa5qgz34/W/uvBlUihtUvrQAMhJO
7QuRf+p26Hz+bxQxMY9RWFQEkSBbB8irdiwPqbgwag85sXogMN+K7YWr1IZM4+4c359NLfPpnBSw
z26uEoPIOt5ZENMOvJB0LylySC08gHDlgOknRrgHpq7StXDkyIrTi3fhqNF27yTyAdSEWtnvH/nB
Ef/4S6yVlinSwGxCEdI6pt18/FRUPvHEx4YZR6K/Gv2u6iHtv9ex2y8/KCaJgZNxnT1M8f4muRz6
7Cl0YNapRdUmkuYsac1RLLjNCbAydSVf7ipEJ1i+dwjsulvF+hd+hQpQuN2JVeAuU1DtQP6onVam
IjQy6EzAVg8mlTl68vc7iHIEtWTOwnYaWpBDDH8vf+TdhVJ3GyVtjmhgLLJLjAyfUp08zc0Ou5ty
6PwDMHDDa+ov4mQRdVF85NuMDlzmH95VzqwHA42UaGB2p77QwxLxzwlmirlT/ka1p6MKx0AUlj5b
pOTDTRyAusXYUZPBfLRobEYuvyY8fxOJu4EPiu5mPYmSJYr6V5nuTiOR0KqgW+ZleaVvVtvKKOh5
qOqafsVm+eI6R2A328IjjzLL1WdU5hpKubfKCGTaSIGSt9p1/hCJs6MHKxXaKhtJj/4dkKqvQZLr
49LJAFBZ3rnCopLmitzqvhZ+IvkHD8N6DsdUmRajP5fWk6CmdWl4aoERR7YC9+/noXPTlnGYdBUW
06Tki+/gaE9DJ0ySSPGOf+9w+vb0n6+9Sj6VoSHLKODHKlNiqz9OvE3oRwMHa+25yYYJXDQzKrTg
jNfROpK7b5m/4aqb/EVrSCjyr9Y/25RzjVWUyIj6pUTZsYEoJVtNDhpuNjGBoo5FSgla4S0U0sTB
87hHUgX0ty1GnRh2Z0ZRWPpAH2EVGDGUNnsYnskpyloRzGIcMhte8RublOPwmmuZtVEzRopVNPO/
iv6qP2/y7r4u6x69BY0Ce8NHSls4rrKddwMCe01Y3+adm3l6khZ1nUMGheo5boqaElI9Xed32B4D
e+Jy4jju+WRb8iUqMd7NVKYcMFTAg7Tm+5ic2Kw5aDa6OcB8UCBTdyLdfTt7k9SMe70pfHsxXpyN
iTgGZOPwtlOqIH+dsU654zKIn8OqgnB4pUUl45bh/jj53RKQhNF2ApLd+loQBqS9pH+JVRxeOpu/
ZsLiRACd6fRwgKXPj1sIOihJJqFvok5eSMTdKosKwpV3FXoJIldBXtA5wyl9HziMYNK1yW50UeNh
uNcw7xkAxyfQJYC7sk1B1UHXG+eIB81ashc4WmdTntDhVpAZxgLnbHbQ2q/qtnQcz67+Yh5ymlDB
Ta8izZaFQd/lDf8Pgv7XHd5Hrz3ZVhZosdot2WVF4im1JIqe47V3hhzfmzddrKHbkTpvlldIGsjk
Th6TfyG/J/rq0wXmKxF462rs/CPZCVfV63EdYyjbvTJ7tS4pGZ2t9/6Il3lpWeQCk+kohTEOdgdF
0Gjb+SIz7JQYc+Rw0+9R87l+y3vchYVXXmJvBMOjxsuGS5/Ke+iVxWwfSPwQMJjIJWflVsOKL5d+
jbyBeSEI/cz+THWh+WIwTqUIhbKwM3DBpC4auqcbb67lXHwxpkZ9W6L9SsIrFP6yuMkLuPU4J2ob
2drve34NB5FG43DpMCxcDieJdBVHDeVj/Fnn+bAdOuy8gU/1JR34e0zYcZVKoYhVxw0WpJRF3JLg
z9PVe39cjSS6wPKlZGaoi8VILkhPLZyoim6bKjgh9TczvWK3ESLiAuZtXggfi+AKNHppmrPJzoGN
YkM1XSLjASbTKsNlYXhOAW0cRYHMYiPoIW/HZmrEGqsS0MSfJ/R74lbaioT0wkcaTk+LepOAt/wD
FUOgmra4YXYyssvRIFOUmlRjrrIPoHhbNlUNaUIR3m5xWjeYJbR2P/nQ1dc1yR1rJW32oqgX1frg
dnyUIcWPoyz0ydHU+nwAZnon8ia9T02QrLysBKqSYZM0MkM0qYhTmYh/RUBeCDmJJylZxmiCzfBH
t548PlckyfnA9ODiGS+yL3kGMIe7LWcAt2kcgrtfACbcJjNRjAsMOI05aPa5Cv25ALEyrwn0rhT1
9ayY4H2k60uOfxK1g2NAXNM6MQY4OS65B0Hu4PueHHq/8QxTmO30kJ/Y35nE4xomksVYjOdDyohQ
GtVsq+fJC8orTu/RoZ/goT1CqEXx7OlfBe7nFLhL+LgtJxSMJzlkB4YJEoT5DjuJxOjrvvhSu80T
D9P7HwQhEvcVnzyPIsml+VStcB5RsBwmjbvl3OZ3JrdzdDg2IqUk//1le7x0rnix/uPoapGYXfbq
glguGXkcn0ztJmtdHje+xxU9/Yyv3hNXy6inGmIYuXWNSEZMKodOkuLgekvRSyMMRb2jQei8euv+
MM4sFvf7sazsPz3GSKRPCSfYRpVHLpZY00wCAc8FTd4Ma3hA5N1uRNAGSW23lxlny6vdQOYj499R
8N0Pz1cekGR63YJmV7WcvsXoLRp+XW0ReNnfPX5dXNk+hlPwz3B9ZLCsfS1C1qSO52Pncblvutzq
rQvWczmurJIzmnWADlTpvC8DZOOoU5Q/VFGy6ntn83cGTAUYfun+Pciof9u7asV9MW+N5efXz78Q
zEfGS5vVyYoJzbxPfSwedmV+Jp9E/yoi4hGynBZHFP/HrA1wWeHwhtBYLZMYsYA1LE+J4BgukSbg
cPvWzTC97bbCC3wHVc8etLA7pj2g8JIEBiuyaJ6NhB5i4TMYXhJicgZQANUOd3fG9AyxvlY5rja7
N8iUCrX7eIYv48xM7kqPiSL4DnP0ByeQQ+9awiSOoHghaRbZUs1lfwkaKULW1bpr4FImWbL4Q9LL
vo+4PLMBQKSC7XSqA9pP2wenGVrEsncGK8L0A98CVOobaOTeoKJb31uD3HwKDim90b/4/eDwBYTV
eBgEn6KhkSC7kTFdX9WCKuUAQWkRSFhx/t6/ZvPA8aijAE9iyJMFNLloUEn6NfzOOrvXchi4eEt2
C/YobXVZqV8yr7LwBeW/6QO/fmBvPsg2IOvma+Su99epsSsaMQNaY3eudDSivh36k7hOce+bbX/9
JNiCisBqS8TcnmjTuSp7QVoKdClaIqorZpGtwvifuufv9UCfRcdI9JDR7RkcqMdFXovuX1B1NB2c
U/UbefB5CSOucD87PQ9a3Vd+v88LQ8KopNxkiEZ/dH5kkFjHDblaRR/JTMfNUORbDy0p/egrpqFk
yZ99J8ckPOCrYTfCkjTSgoaBb7thMX4lpLP9g1+Gt6k6ie29rlcRNcUggzKDUC0uqjOsR4Fv/RP/
S/UJddhd//1L2q+3bkFWVyo4VEBhtTuePpBLWR2egn45ZSHNSJ38WcKuLWisrA7EmWsBMmDC3Ov2
ksJ4ldflJm+oQcZ8uV0bxQ6IJRRElygoitloaZtnp87FXOd0O8EEJjfgqZulrcXJFRf2svss7vUq
O9s5tdsz8moxC27a6g63pLXlK1SNFcMA9xku+ESCcY+llcu+MlJ/HMP8xF09UyFtov+1ZspurJUU
+6yoep4Fu/10lSpZ
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
