// Copyright 1986-2022 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2022.2 (win64) Build 3671981 Fri Oct 14 05:00:03 MDT 2022
// Date        : Sun Oct 15 16:07:07 2023
// Host        : DESKTOP-QRFTT4G running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_c_counter_binary_0_0_sim_netlist.v
// Design      : design_1_c_counter_binary_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a100tftg256-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_1_c_counter_binary_0_0,c_counter_binary_v12_0_15,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "c_counter_binary_v12_0_15,Vivado 2022.2" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (CLK,
    CE,
    SINIT,
    Q);
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk_intf CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:thresh0_intf:l_intf:load_intf:up_intf:sinit_intf:sset_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, PHASE 0.0, CLK_DOMAIN /clk_wiz_1_clk_out1, INSERT_VIP 0" *) input CLK;
  (* x_interface_info = "xilinx.com:signal:clockenable:1.0 ce_intf CE" *) (* x_interface_parameter = "XIL_INTERFACENAME ce_intf, POLARITY ACTIVE_HIGH" *) input CE;
  (* x_interface_info = "xilinx.com:signal:data:1.0 sinit_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME sinit_intf, LAYERED_METADATA undef" *) input SINIT;
  (* x_interface_info = "xilinx.com:signal:data:1.0 q_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME q_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency bitwidth format long minimum {} maximum {}} value 9} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0} integer {signed {attribs {resolve_type immediate dependency {} format bool minimum {} maximum {}} value false}}}} DATA_WIDTH 9}" *) output [8:0]Q;

  wire CE;
  wire CLK;
  wire [8:0]Q;
  wire SINIT;
  wire NLW_U0_THRESH0_UNCONNECTED;

  (* C_AINIT_VAL = "0" *) 
  (* C_CE_OVERRIDES_SYNC = "0" *) 
  (* C_FB_LATENCY = "0" *) 
  (* C_HAS_CE = "1" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_SINIT = "1" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_IMPLEMENTATION = "0" *) 
  (* C_SCLR_OVERRIDES_SSET = "1" *) 
  (* C_SINIT_VAL = "0" *) 
  (* C_VERBOSITY = "0" *) 
  (* C_WIDTH = "9" *) 
  (* C_XDEVICEFAMILY = "artix7" *) 
  (* c_count_by = "1" *) 
  (* c_count_mode = "0" *) 
  (* c_count_to = "1" *) 
  (* c_has_load = "0" *) 
  (* c_has_thresh0 = "0" *) 
  (* c_latency = "1" *) 
  (* c_load_low = "0" *) 
  (* c_restrict_count = "0" *) 
  (* c_thresh0_value = "1" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  (* is_du_within_envelope = "true" *) 
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_15 U0
       (.CE(CE),
        .CLK(CLK),
        .L({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .LOAD(1'b0),
        .Q(Q),
        .SCLR(1'b0),
        .SINIT(SINIT),
        .SSET(1'b0),
        .THRESH0(NLW_U0_THRESH0_UNCONNECTED),
        .UP(1'b1));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2022.2"
`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
KdkdvVsuosc8qR9X5PxQ/ghTeTrEz4qKVuenhDR9wRSL/BO/mhSwQtiFj74UO0sGv0zvjAntaq/3
l2/v8gOiVKmM666gbk/2UCISA4OFA3FDR9jYmiXdNXb2qHeS1ywQz5n/sTR5iu4KFEfwrl3IXtQw
aEiGegL+CQMaovJsto4=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
pZCj3qT3VD1SCS5RiZExsqqu16KpMtHXilQL9p5/eBl7qrfQjT1VhFtVbYUusepbChjsCCmCn7hr
72SuHmOmDWG78UARN7MLdO/+sePuyS06ak4nAw5xwjT0g+9970uMWYKvTeeYqoz2i+k+zX60Cuvu
iwBfxWM22DqukHlYzbEFWhNyXIkgJe71p67vGdXBmqu4/2wmlwGApqBxlwR+alwZ9UGHlxNQS4N5
z1wHu3Cp8LwGRjlaXjElcY8RDpvyz5l59ey8ar5HXR9Zqf6e1unE2NdhzHhEGRerRFXoKZppk1HB
6kIEY4EHAWz+HvPcqoP9eoYKDazoAGkJRVP6YA==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
gLgm7VvY3cNcNvdXvikCQd2nRniE4ae4hePOcAUlPDMoHDzQAD7Ngo12MGFns9JNPcCaUXfAmxL2
JNGojjrDRUWrv8FPV6FOEbDHs96fef8+gqLF4OqLck4kWpKhnJwaJjjzQirvXEzZxP+GsBKnkSp8
ceVlZJwP0F6XRv+RpQA=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
GeZP242oKQSNuofqDs4oIIXZEufPhRVrlFFeRSLY4VCxhMEMwfPrNXe33xO0zIEBoPW2X9mvUoTY
izdWQEtWImFzjzPCjkSLhEdIMmUBH02Y+Tw3eW5x23T0cK96pmoV2MH8kl99I27MN6stVd977fuB
Mjao5MnSXIGZ/uXGtgfUO9Zjs4/2wGmsI2/lANN2WOL9Sz4xeA8k40c2dNYgxgHoCwx8Ya/RYIZS
Cpuvzq4ZyFSNT/kMXnUmqj75/flpXT3mmyW+frexux3j9PxpKHmxAE9crvDx85rMamGiA4ftl+ac
H0FtL2cBqdlP60x+FjqleWCJoN6AYdxA0YZaeg==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
URmEGftuxvv0+tViRUdsFNnPXucZlVDfUQpjjXkpOA38QUzsIL9j1pGGp9doC4jcg/9MD149BTSw
vAG8684a3k+Tx/8sFGl/viK1q8ty9nktEABSahv8Etm5ZJVAzQJT7EaOzrYqyywSwabogvGUmN/7
DE3eOn6+sMCiMl6BLUhYyK39ntTWNFYVPiheclbBb36V1vzMOQl0mvPuS4hDXqba/+qBZXhqeYWK
ceNfwci6SsRRef6hLF/1S+20r2uBxJeYJjyfWGGFEGfxlAOz1MiYUUR/bEHWnbjwIcJTBHQNRdq4
4Ryb+iPuKcsXU/8ApD14i6ScW+VBPWSqnH9w+A==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2021_07", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
NtQgA3rUKfJt+21sTot44yr4gmte57FoFl8Q/327tsRJeEyNAiwWZaZN2mbo2NFcvyN2GhDw6avJ
NsF1Oxs36P8shoqOOiloWWrdTcyAdMhdk+UjeZgKcNSqd4Js87w/5LVQTwjB2mcBDfe1jrivv+IW
ZRBC8NvlW5z/1wF7+vzXRMziLQYeOkLB0OkpIY+eT5cZXDKuZ+4l0FMPjd+El96JGAEHG7Q0qS3F
OEApYEp8+nSZnragoytq4pkhVJEC22ye0hBhoBClJpszCcg0u+Ugf+mYZsj8BC2uqSY6Hh/gpjjw
enQ7aEYBaUR7GCwQN7fZmNhZYtBkyvNqydRQcA==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
CpIFM8Y8dBmpjtOVnOKcfppEFV+c1cRgsQtewNUe+5apiLDoRCdMyTqoCay7nz+Xagc0OvfZDg/Y
jSTsDjKVcEIyxOfix7iwjKW8Rz+a5wBIatI8wfCo7uLtuucz9otOWWI7BFQ2gn4VdQ73HJJlZMMY
OyEOd33tGjNSjxz3W07knDr1FwTE3BOfhq+Qj2ErnuV1dQbrTb3MiQMTnHaTCwtz6ip0pD6b5G4K
kBRUYe+UNXCMvSfNIN9MPSmolO4MjNwM5gnZZqLcR1hGuzH/Yeb/jPnhsZ7jFvlTT3nsM9JzMRAE
QwlzVuulHKQDS2I96arFosYPYMsalmn6CQW0gg==

`pragma protect key_keyowner="Atrenta", key_keyname="ATR-SG-RSA-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=384)
`pragma protect key_block
qinIxHFISC9r9LS7OKOuYVGM5EBkuuQNV1nDRui+QVNLn2QFCrWPeEClQIlNViKOt8MX9urHvu4e
l2L+eZKw6+St9cW9yUsYu36yoB4LqwG+vKvfR9CW82LGPyMAxdgk/p3n+F0Xp9Y2HaERwWDL99tW
V7cDvLLhyIwz7w4rI0BWWV+KMjXP2F5MNgykzZn7tzV8oY6MxOykFqRdI8DLAdlYGAs90wjJ3x84
S3fHciSox97FYpDi64v31Vb4RmRrwueXcvCc3w8gzjuwg7qraWLMYyPB+mERB2v1htX80PsWWVHE
QXkWiHWYvvrXEykUS04MmLNHpV8ZgBXO/NBEGn7mrITDEswk3u1Yviqy7CW2wLPQBoo5xW+uiu2e
8YZV/E+bAt+P/EH5RsC9alBgtuVKU1s9DaiEH8eUPEgJQ/TXwQW01pg8ECTYgiBS+IQSbld23aq3
goVo0ZMzRu/SA00Jmwt7upvsMkh9Q+2732ahu1FmlSNmyNGB1+bYf782

`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="CDS_RSA_KEY_VER_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
T1jqx5hmzZZMhPApzUC1oZLMAkHma8Ki4b2CvLNqxSn+MNWoTPomvQ775DMBEDai/gahYALsohdX
0f/e6LuPqt4zYtyAzmH+nRgOG/tilS1J674KsaHxudAfo4sM3awB/C4Q3VdYsO9FgvPQylnYKSGE
gJ46W+1Y789VQqPbt4dpnprhix6sLlwfww7We6cq2wu4PilFzovejouUBZqNMZHYi4suKcMcenp3
C7QRKloo8IF9yKrhGPcRJLQt2nus3bI0Q3ICxRk13Nrfhh/z4cdm0OGXz42q44snFEVy1lLxPOs7
W9tSe5ag3923oCT4NGGgK/gMTx5qXxFhV2MJUw==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
cwVOXRPXtY5nrZ9Sk9qEsX6Uye0NkhRLqJ60A8QGACtac5I/xqqi8e+4iqFuxAXV6C55IT8lzrVF
m1B8iMsY4CSUUWFWQtIVUjT4XGiRQmfafVKN8pf120Q9VtjpxScRa5r5Az0DEN8NS3ZYOqqPfWZR
MlhRrgzA/6Mi6R2n383BIRk13yo0yuYv838CGlLTfDBVqoXfH/HAaQXU5G5C2RufXGf/zPtWjAjY
6srBeUHdCUeeWoWvGm4oG+ZUjmMHWBu6yvdG6WEzhVdmBbKASaWtVeWqvfm8/UF0WFyCfNB3j1Qe
g/+Nrk6U6FAFQ3Db6qndofDf+CRR33FAcqIvqw==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
xCSHCXl0Ftptpm2FrzZM2WH8AVv0ArXax+/TbXG3OH/xIZlONTxKtB7x/T/75fJbaTbtjpx0u+Ys
Hg5w1SI/qorZQsy3SfICx7pgQeNp+jc2YoPk52BqoXZucnO+6qdnHbkix+FyyiSvTdQRTzhv++ok
Jn3HzRaKfJBU0RUNdEailSuQezIQssuYpBv67M/3k3aUQZp6cvEijHOxeMZ1EmCEfozhZsytGBjy
Uhgby9V2AywgCnh++2FWjfcLlNt4LpVtIcXUuUf/H7CMDjio+P8hrLVVzHX+lMCP4qQNg+GyWB8R
HbYB5RRbI/FeQquPnmw9L94R1f/X9ccAu8GZbw==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 10400)
`pragma protect data_block
cDG4Hb+SvPPox3W+94ErJ6pcoCOq632FIKpOCfLY3/zaJkWYQjYjQkBi8kpwchm/WQysl6a/ggMd
Tu2A7mzj0deeUu9kq8/RjjFL2C2gfCX6+nxYypqqoz4WpQo2pII0GKa0xoYH3M8c651+xbkSNYNZ
4voj6dEHx2KN5uB6NoLYf1B8l0AejNcN1hbT+zihI1LyoYjni4JIwEJNPor1m5I7QeauxO4sFgWa
+fiTziEgeGJ2PSbC4wXYwTMpQduo4a//9/cIV9HcN/cUYMoNNk2YcidpGwJELEYgWhHEsdQ/6PqC
iSR2t2WCZgha3nEhsvbe++T/Q6f2VVD2VkH+GT+PTKgfwKA+OWiERt9LM1SlevfIT254kxyJwsW/
J8yAJVltrztHeix14K2Dc7jOKgadSSrijD19en1ehFCcqjiRWk12zvfvVFAiHkc6rYEwv76iq/0P
hQdrMvFx7z+SFwiZ8Kk3syShGWW1WW4i2mKqW2undYtryUCZFiX3mhhQIZv7KMTmZha4AENldzEz
zv32lBTZiVLpOlwH0WvbMWtHGkbAxCRGwLqL/OPKs5tx7egqcwthor3OLBVTCST01UAPONqG6gZY
xS35JFRxq117Em8g4IlGL+GcLadZrd/4O0MuFChIDZW3Ic1fekYwXrQe9M9l/AOs/Ot89NssTlp5
kaHPsJQbE2RjKTYknfHBgaGeqk4u5regWe2ZbqLp7nwSG+maCo6VQ3OwbHVbD5gBryiPy9sCyh2h
Eojw44j7Mn2waksmnzEPXaiLGN6gYg7WinpNmIy7LNrY9Jr+d6tBhNzLMxJhq4YWtzL/mp4FJN/H
GwoYCzGo8jl4d8/XtJuyPuAySyqI6AFgpPc6F1ziemJdsDJyWAQw3E9A18eEVBwdq2wVoNj4PqYA
wqHJuODDK+Od1rCekJVrU2wmMAV5Zxi9nud0bPFB3eJxHfCeec6P3WXJ8h4souTW7XRiqZvF9NXw
kZ3rhIoiC9R2aDOP//7PXj2D3Swrmh0VLTPWBZ9+FCjj4xaZrJuTgQIsY4vFE7Y8kkLt67X44dj9
iXfFEfswChcEYsxTDsAjG4V9SyDxaL8xQPt5FVKKAmOz/1DuJhkwcVa7VyHmRYbHSgCefYpxxBL8
FfgiKFGJhnvm/XTHm70LZI+RLYFJ+Oqji0/1k+TQUDCcNNJToA5fm+GJPGoNz6j9eTZriYcP0cNT
3bYTD4nV24LnjDm643w4CgxRnzaWh+8xEIGtQaX0vyNPqEDPf/Q6LF1EsaWyOK9lA967S3lk0YXw
6n+PBGx4/bP/oZdNomHawOfIG+3ITfu6DiIdE1D4GFCQ3A7yNiYtdO4nXVC+/XB6oGG1zgNVNOUb
W626JeTdbXFzIGNupbImjcDY/HtIMBdIzSp5yhf47t0qKSdJnFiiC/ZRORKd262K4VpkEOVxVjDS
xH9lSH6eHWV5Ciu3c5dr7eWbII6ERJ2B70vFtn7yuojEofbWbY1T8u2B9Ic3jUHmP/tGu4j5buvB
A7eWNoxQgj6fdDKs4yq2VsywGviSNcT7WzYyCjYSIHBP+QcDWdDjfyyEq6PHeUh8rt+uVp7VYNsv
Rl2m773W4U7DtzXrpGI0WSa+nLLBfwSnvtrmXAJ6cpApgHM4mllHF5kPNuZhcV6EyvCYtC9158OY
hoJKmy09VraqglRvUySBYOya7eqcLw3wKxaJdETn52OSk9Qi6d5Q6FqWJrBdNpVgRh2WZkEvnG6q
s6ybeVxETXGV2Cus8WSzyTHclG3upaXoFo2IuG1q078c/daOZ5Wiw+0ZNig2hnBbxDItHRrezACi
rhttiqSEd3jxAo7OiWoT4zenQrK+YBWTu6y0y4azX9o0UN5UaMuZ0RpH5ZK20Z0DSuOi/D+rnWqg
a0AWl1uST86aHAbt3ZS3aVX2RQDgF0rUdhRNNRN8cTRrTZYpKnjFxMLIzrdBv+HgvvoD/1q+aTAX
UhBg6wOsjX0yvrmMYh/gM4dV9/NvPgmnzYbYF9MeeqaEeVIpjGLOHRK//Kxpiy6hsvZmIJKfgBYx
l9M7Ayr5kKVH5UE0+O2htxZIyomUWeMwV78ZKU29/i2hhx52RQ/5DdnsjaAGluDQOERx1z36fiwj
/OwEEy7U34aREjZ++si0pbYDnRBuPLmiYQU0SP1v3+gAzPon3UP0iZJw5NgbucswoetTvLqzpyuo
0z98DHuM8QeaW3WH8q28CPrhRx7mY5dTASrOK/BWYOY3BvjUPdKQjVCY+O6AGzo7TLcAU0kkivRX
SrmWkcecRn9O2duwS3l6m3xcvkNMZfoB/ZrHo+pAQLZSoMXAZq9iLKUkRHf7NstYZJ4mrJimxBE7
Tcy5niaVSWlo9EEWXVH9macN7LvdzmkuP7oqCaxKP6P+rwrEbKgzhUfbV/EMiD2ZXDpt3D6TQeTH
dzotl7zB2TOYAq4gSWJPtmvPw9XXm6RcPuCL55nd6cXFxbVqEifqVNxS8gEpTV6iDL10O6h2RuRj
drGoCNpD2te+qL86e6wGFAjIbhaqFjo8BrOp612Rd8hGdEswU3P1SgoxyrFEGxq4T0omylcdWFCo
85oDhAOIHHhpO0iGGr3Nwo71A3nOic7Jign7mCZKIiqPq0T34VAZvyNLWigtsYHerwOKthachfQR
Oo1Vy0BhOu7K12OPvWnxU8TbydF6/ulQx2hEf5T71kl29YmaVijBJpnU5H3aKJ02KOhQr0lakv+H
BTsqz0l5pu/q/T39bZpX/Eh8vOx7PdXgSiy6Fslv0CBwjmtI8C9vK5ZtmIBRc6Wv7Bz2YNqmFGSd
UJX2Uu8/3h0bRlgeuBpww8gq2iPgyv6iatvFcVrnc9m5prRK2edlfBuHwDo4rr8FJJ+2s9Ml0B8s
QnWtC9DOFl9sJK2aoI9ZsxBmk6tDZ6k4K0YCsGbLJMgmkpdGdkNDo0uZFRzsqdsX1fustCnBoRhy
GhZoVp79/2RLlnyZtPIWk37CBSOdrcoWgrHbG6f54jrbWKrRxGMM4H1cc7a9pORADusgM29sTQG5
V3RoaeRnTDOK91WpfT2b9aYektygSfFEqMzUO+hctjKGaU1kMB2bAX64wsQ/ehOWBr3i6BLxNUQW
3FyBvc2rO9CPLJfYVSY0G3PafP9egfJeqxyiEvCCLYyIQ91cl9mOQVmb3vPoGg1c5OrBnXS+4+HD
X7S4kS4KJ5+QBBiIJsxq4WWahXSyaattUe+sMW0+65GS5eZe44LHsDhFti8QzFlcsP0LzOiT+/UQ
CPu3U6oxm33qzlCPe8DoaI/oH11LPzGC7x2xmgCFe4d9uAMRFE+IrLjw2854zKojqb91A4x8aGwk
zY86G+yKElb8X2G+74je/qJ+NWZj5nzMswszHSiTXqBRr3EPT4gZj+9Os3H6+BWZz11Elre3NHrK
IaaRH4y0/W8ZX16Swyur4T7xT0jW8Wn3SrI5bPld/LZATqOmdg31/yhnsOqfRrvZSpOgic2fJycp
+RnaPtkE58hVN9nSiAP7tze5fhN4oUHRMbpoYxnKdIzGPAynu0KjMZlKX4o61S7soG1VhApjQGoJ
wowMqesZ0nOJFcbPsVq4LsTf861CXenStQ9pJBBJC/6YTwMcurPTwpH85Lo2CGqVr/qGZzsbXN6g
biydXZ2rDiPrm+eRIXeHegpnzAienZ5gIu7U3c6pRkagWF9yylPD3aZSUUYjbSf1UdAnn1vm3HAb
h8KdASIxfB1dKdNDh6PwMozz4PmPe+n2LYZczytK0VawRTCybBWC9ckpA1iiojvFiMZmh2XYMT2K
bE6Ox+w1gTU1jcXd92vkQ8Vzexx+HEP9iSoekiGdUSUR2cLjDIKbuSq4QxnDiDOGjJJcTCUEoMaZ
d0+zO+Btd0arWzRrzHqRe8tjfTU3iTk3wdzgytg/pJMlSW/g6uyw4tLSuK9aFDpoJymFIWaKswCD
uN+qPrCdlsh9B/ymhxdSl7S/5TuD92sI+IvosPkQNoKPEjFy75t85IktT2bvvAXbi6lEM9/BQpNl
Br3OTtfQg8KTwj02PoHdi0/azixNzOjCL7GspEZdDtZwvPUC2qB3EYAqNZ/Sxv5hUqTE8V2GaRsy
m2Ek94W34icsoh64zHEo9ja1h0Wvty+/1WhMzMXMqsLNs1ZwbAosBfxXuZixfvwP97Fiup360cjw
ebYqEfiKLcGNgwZXVTP874AV/yTOHfhFy/mlxvgyZNOeCXGTrcp0bnaoVFulBsa589r8IIaoqXsg
vNwfmN55cVKdGQgJ5h2mYiRPoTTP1COpBUhrdeW8mcjN7F4s470+gZjZ4HHNJbHVCOjPI1Nu313x
CUqxk4xJfMTukmEELy+GNG8EWYFIPYG4f0i+A24mEdJ9xvrwAmQrLEqf4N+LdIVZtZJIqEmWuwZt
2Csb60XfPPWQWszZr27vwf+tbQanel9TjSKduGL5W+YPzg1yjc9DO9Qpwa57MPDsKDKaNVWIt01X
3OdIw8H9ndLueKI090tx107c1LIABdy2uP1yovnbOrkRwjzpNc505HR7BIKeUhXcq4VgBRXIhmZZ
N1b3TpCb0oF/NUu0OzQ5cfmh+PsNyW/MavvCogQnem3EUPLjr5ZJ+LIhl7EJMSrp+Z9IBtYRTZ44
9ghNQM/KrBU7wjK/QcNFW4dS/MC/GUN7hONplXn0lEyePvSt3abw8yv8GESCiBbzDCyv2h4d+Pjj
frZDj1qneFJl0nvtu+sTlws/ahZXiqMWvW7PK5QA11NyLOngApcO7mFRrF4g+IOz/xA6OFNu53rg
KgbVktgC8DWB6tJDPJ7sNHHHLoHVG4IEkj27GHY68l8FYlka0sccYsPR+7XqgLMvVM7qiHTPE6ru
erF27LgBFi3nTyeiHMn92emJVFES7cbxUyyvHKkhlKyF3r/1yPdpsd00SgYaa6Gg8wIguse7dAzq
EGfM7w2mp1NOoYcTb7amrFpmE3nJ/MV9tmbD4deK4gJV/zm4jX+vw7PGTB1/pkZbSrylVNu1MfYb
p0v12RUX4hqcnVOtwPI11vpW1Qd/g8Vts2YKQi6nJZxBKYOTd2bdeK4dwz5vBhvtSDMON7qN2K7r
uhCEctsqmC/E7T+RVPphe8jFmvmaMREcn+U95659HfJe9HgQKWkP66sHLvtHChc+Q8+ygOKUJ4mk
p/6V+eJaIPQHm+RV9ZTesL97MEu+GZffv7zRV8+vJ0jLfhmH1xxT0iwscZMXbkwjyhrhOH1BCgaJ
uvIVfc3ZIxaQqlk/wWCxVk3pdrye9P1VYVHaeRyx3ETiAQWo/G86FScxL50VurLVaI2W/GNRgLFb
9zEJg2iSzga1qg63qQ3B4q6XTP5VrFcwaZsEdaxU/gPlPFsqr+QON6hx06slR8/L3EMHa5aqsICX
K3bzNuJvJpNpZIQiHOII7BIKcA3Hn+SLBpWcrmjE5cTmLz/p9LUYI7nCOXyTEr24609eab37EhE4
MiRqwXaD0LZ3B2greUpQjrddt+9sLFpQPhBB/OlfcGl60iCriKLP9zeqQQSST9Nq28C1czbHxwP9
BplLq0ZwErAImW9OM9pN01GzzCJq7ZbFFeGIzFbnt8ONq+nuXWyJAPK2S7nvTlNnzlnM2gXvdwqh
VEfw4OtD1aL2tYaRDXiNVupR/kKB2J67eoCgzHhYOW1C3V5mDQFyEUkoFQQL1gU5t0qzFxPo2IVB
MZ+1hwh3VIG0sD7pBBfDm0+j1ZNg/kJ7d70Mg5atMepUiG4REPhpJ3UlfDVwJ/J/Ip0DGpl3qyeF
Vr4mH842Hmu8aqJIKuHNY8QY+YDxBdw/BPrlyKj14XPmDrHjMwEaB4HkTkANXLeAK6+C493pTdKP
wvFJzBovqmaTG0L7dH9Qg6aEoDkA9G1KwsKd7l7yWRgbJt32TFsebtpbKaYA20F4RQuuDhEAT3G0
n8QG0qI+GCqdjIJeaVDtO030TLLWxRTU0HLlOswThTxBmAOls3Ocr8cJw+vtACcfuGbjXreLS9ED
nGaKBEEPCr4NYNrQ+Tme7woTWAitVR5M6odck7zrzhc2zZ9vMByfQy+5nZMfBz62JBslJ8t6wX6T
/G0yyjnjuIcUGyLxxLDqbldLUfPb0QAXTRk7ZuUIARexZRIiQhyp6bW6YGIarnaaTzflE8kfLNJG
1hZn3iSMjUVwy92JZHn4X5DhMwrzBR3D7ql4WDjPpSfov+GpUYYa9qzFza/TN1vod3ADzahjrNXP
AjFMWIib7Ovdg809skfblNtawWca6mDQA6iu3n0UbhTls22ew520WL7DVQhcUpGl6mK1U/3GP3cY
jKIavLrtkysPQ2P48carnYqMCuh+e29Rw8vCBz2OSomyTneXeTloWYsCL/WErv9f/mp7F0d0DL/p
BWLZElXwrKIapm/MurrK0ItGdRW4J9nqPti0nDNpg8seR0iGUPLpOqzovk+H6GhCJkeRlf90dGlT
souN2312WhAFQ2wLRAjlJ/re43QnLOEJtRSWcUQFqdC893YYl2RrDYo7uPAMJmcDk88cUuNAZReN
iN9p1FPJXbAJOjwQaSJwg0Mqw0KoSSxWgz2jYztvDvzzDBBf4+uw4mXR5k26qbgi6v8llAATu2uA
jzrlXr51kNmaTYIbXYXcYLDvdBT9uJ/uCFsnXyFdYhT0zxAwOhR8JbRTHek4pwEzu4Ga1RaFJnf5
99xdq8N0cGr7wVvlcptOdmBXyUlzwFbJjr4p3jsqu6RdKOVkXU4GqlzR4CHOCUuvV5q8LLrx26YP
Dx+IIGG6noMgeQ+xkMnl/rj/3N1InIAyFniRARPAYo+nhpxDd+DL8f0xuviPuubSDjd/Bp+XSXA9
JhB00uHLWcCJ46T2pUiDHlKX10XWCyF9g2Aov+HQZKQj72zhUsc+h/r2EEkeE7ldeL1I7geSnZAg
An1vK0a+fc4A+b+g4iN3xFeUPtrtnp0y9+lfKK1sDhVm5Y1LXjFBOrmWH2V60+QupMuIeSaH1VRL
dwSIrNk6aLdIVJe3KAgugfyHzO3eaQyk7wn0Zl2KcX/Pq44r5CNDuzNCvfuDKJUXaMTJYSNSrblS
0SWIEFzwAU6N/aylLzDUAAPp8FTyt5ZPQXJAq0/F3qRlT+749YsuEMa8e/Cx4OrTzVIQSe/E/hc7
h9g4jf1sPhCsKYXdlDJlVfNYDQ3T2e9TM4z47HTwQRFGMgKQaVommqdUxOoO0nrCBfk1ehrlL1B3
PsjpB68JOVjF7unCyoe6E6cIzhxXJwvgpTYDE41/AorQGf68gRFEkDhXdus5LsyvGyXnw0LrOqwW
gkLNv8Xu6tit4dVCwq/uCVv6bw6/tqr1uyLjSNIRs4XYfaS0Id7LkHNBoWiuDjAt1NYGb7vV7BSU
FWShGl+TnhhbFf0wGxbr83dYXSEQXskC3q7CZiYe1UT+EsljW/eya/5VirmN/UoPDxepo+aWcKbP
GioOHI3cLWcH4ej9AmWEu0gPVZ6fw7KwOxJKJJPaYMnKXvpwR6kA+ZL7v5x1aEp17RltS1SN1qbD
HVDYcy8LBtOXiyT/YFFb8ygPIq+TMJHix+WI+AC5zK4zk5m7prXpkQGmYIub0HoPSudXQb+UEFuH
hb9DGSsM4ci8Td8DytjAacRtJ25RHbniYzFSilL8ns0Y5VUkYbK6/zXtTJ1cidU2mKc/ouFxuiyr
C8huFyrjS5E+PlpxljX0VncT12Y4LbxQjnZeEEU2B8ts4h2h8XM++ZNWVpbg8CntsgOmY8QxIwlw
zGWD8uXRbbaOGuzBWDOIEvpwcZ4n+MvuR+MtI0Sc+EcPCuYw8VAnjRfdHSRgzZM02BEOWQ73jcRa
/Z9cJjjItFYyGCkRkkt3AnSCOyqLRmKY5UjWFx1NmlqaHGb955wHjCefezcgjVEwkFV9S8m+aRCo
V5GsN5piydh91oLMrdhR6/yadetQVoyj+ik0NqNoS1TB14Iae3f7RLNgrKclEIYICZtShF94aH9q
hSQmfrcE4klMYUDAZjk1gZylcZ5kUPo2WiHJoeLr62MHI347euK1y5b04ZGdHkxfwH+JFjwQRcv/
xG/ulztRxja1jRSKQrK5xEwHp8FVjQH8yzvfU9hg0MvuO7KGMUNy/mFQrzT82yQgXZCqsH7DFMUh
+wxGepibUNwvimKoaNAzblVxWjtTfWnZFJvUvCq9bctq43YusF2iXJS0a1rTj/mSJ7cX4MybAA4O
j5phi9/mMqIOngBENgFFnSsmeFh4K2Wq8Dv3Kk9KR7FCUUqYgvmsx3JwFVYUEpNP9y7L04xyR7Ts
HxHQnyxQjlRIW8FGVByU3uyu4FvurYid+lKv1Uf6xBJ0GB/3zKo9GQNZOyt8c8VD78dnPVVzM4o0
kZPo7mnZ8/xnYjTyR5wytTJ6yIi6u3QVksSvslwO7udHHu9e1Z4sWp+343bx+3kZn1Zn6OG3KruN
lgS4FLqdb9XEmlK+DjgaIFocwtM0v7bYDFvL6YytCSKOW2sk23lS52AlVcMZDP1MrZ3eyHu+ZhEe
1PO4ra2Z7hqPmjY3TU86/yJ3Vzcm6SBfIlQVDHS+sxILHoeSjlth9J/Z0AJA0QN8nhLdOEid7coV
8KTko6xHsIGHC0K6lRmJPJGLiWm5umGqrZEj+ytSNxunUNC46s0jRZtW/TvSJXGcd9jQnXvV3mMh
+ETEUslEyyK2JON2b5ZVmzwo8ZaJO4G+QEZO7KDXrPa0pzcxpjsoaUIhPoYycFYDmeuiRSrcJbCV
tqcAM2iJesG7+JFJf3SXds0O/DqguyVB5nDBj7aYDqWlDc8yZpLoE16/xT4sRNoWfez8NMu0T9Rh
uq0Nz+PqX2F1qGeRHceIS3ctgCg5ahC6SwrEbI9tbAhtfwY2gQQnKxoK69F6Tom9JMe+hg4GA5V7
8YQpKr+6rhB78lCP3XMyFUjjlCDMOe9dj6EBy9AwPsE/3ecA1IVwPyFtIRmW+bpgavbZVqq3NHhB
aeXkGFsF9yZ3hmFiYeEGTQnw7NA0BLT956nU6GYb1e8WG1+nJp/pII20HXrVZhZpyB49iFzBpTDZ
/6ToNFoR/k6B+SMHi3BKozJp+KgC9MvhzAlfydRpy3GXWvcsYFFBsIETBSOPpcsmiWslN/RWVu3u
4ARy5E81oITqPSph3i29IO6Ha/ZiBu1SLtbRMLplJhLFf7seA5Mz7e2trLwxWN4+zTgB6asY0VtP
c2y5oSBeFG0MvhmDCN5tRTEyJgSr79sq9fiwkMmN54NoF5B7xWdX1G2ZuUF53CnUviHv8u+5xB8L
r3fI1rsAO0e+pFxC7so0P9A+k7Ughrt2qPvgzsL9CiHRabNuI3OGCJx7u3cEwlHPvR5LKzD0k/k3
mWlYpzJChpQq/a0nO5xjBpcb3ckZ7aCI7XMgN9YuyXTLOH9S4+kUEc/F6p9iaThg3MSREI7nUk1D
NR4vxSF2ic4FqynQU/lhR7SGZCnydOyjs4WjV3dX5VZPISRy5Sx8DqCXxs0uMUH4fPtmBruGBwNh
T4ODytzC02wfSvnSMQT58NptYU0R1qrlO91KwxT1n5zuQhzgZgi58yeVk+O/1Sx7d4AeYOhbp0QJ
gTPTY+y503l3sf0xF6ylYcU5oX8KJ2iistzZTYLZSY2D6pussVVJxmI0nr2iWrAibXB+8l6OmkUz
mTIpuo54eWT02ja/rUPP7Lgi4j9R6JuyWvdeVRGIp86ad9uCZoWEViuHXh0Y+z4aHjDw6pO2v+hq
ImOwyB94wC0Y6mnyA2mHsbwTCCh7CstSygEqThQgiktyaiPAAT35t7S59VTTGEDqGwNpbRFLHg8A
qezMgUJJbzX3wMUJVaLJ71/vNsASkuaxLfUAlpl+Y1kLAy78v1Vfl6u46l6g4rxKO449Cl7OsC9r
wKEZ+BDsrfmxoH2V7AkiFmPLCilzcNGchfdSbzB3enode6kFGtOX7Lc08WX0X61J9Z01KQtG9P6E
fVDZLEeJLFzz+60n8YuyRmYAdKzv59oM9YdI7WJDlMcBWgBMwLoWoB5NBDEvUm3yLskAyfCb5jWM
CiROGqUHTUgtfUV60SwyaIBVDndxUipvpvWGbGJ4Hec/U1In8CLBQzVBzJJNGkQoapDbvK4Irw8A
i/kBXprnG3WuckA7iPryaWiuTEALh+d/3BvpScOgBAuY1Ov1sK5g6+m1XyhPuFbY8Uz7WaL6ziwi
bjHxKcKaC5ekQjlOepyE/q1p1WkQhv6uGUD/tI+4/cpZOX+tErXeyRsrIWJcDvB76sBMFZzATZ/O
K4HOHF6Ss9klBMOSCVlnVphOptBrfcZw0uHK33RhF7G+pu7IN8G3KLmz0b7F/IVquv87bNCBU27j
pIxPe2+GUAJ8nPoC17ekoX4yR4dqgWo6E6cR/LG01N/wvRD65hFk8gbNjHvpO4rvtwk+1wpn3/xR
ue3loDLh4mst1+vSS5Se5CINGJa2h9OjrQcg6UOSnwGCO/Iko7iKoAASuVISjQCxZFhASFE+Vled
Qf8yHpIJQNsn+mA/EYyUlZ+N3+VIOGcSgRiK8sfuUdnE9RqZcom50h0+6fyBh6PeaZE9ec3kHBBy
okm/JZqJX0VuBNxkhMKTdnPpnfpXxUDPmWu/G6rI9yKwYcvvua+3WpQ+RS/KB+GbUCCds7wSCGlP
K1EWkRCq9wg2dyad5NUNGcOCiDqJJbcnnUhxqpETTjFG8E0YcL8xGqM4KzeK1jv+onU50v55pL8q
O7Jh9JuBSt68ieFfZC3Nh5QweEQWjWqp0kqKPKt6h+kwAeOAyUijeokZKftDzhlz2RoSsYRikZoH
sZgZ6iPbT/750SjSdEvLOGipuAQz4jQ4QhO9Uxlz3dGfBuV6qPOOtjq+qX2OSUo2Df4cmOCvnPIF
izre390JcTFtLVtt4CHxko3yOBGoUC3ufjQVuprrwX48nj1+tZJi/8xpxFFXUps99iRnC4PbRdda
FwHrV9rrXACOCRYUUgyLXQJaHRggsili7wlpkmsjsbO5s/QDTHmPqWrcDBLXU73ShlBBgTgVzlAz
P6b/HmNi0gGoJ39k8cNM6CHQp8muxKZ4cik1aT8TOlOD/c5rGcMvB8BDasjLEkGbRw4fEG6pRjdK
vKg9S0Yo+1u6K2HFz7RDFl+zYFbkBDSWoDYaIFcJNi7VUGISGClJQvMblPd6P2D5Oe2OH7EddRO0
BM91DElocU5RImXkZYcggYeFq+eAS7r7paS/Zj0WQ/P5EKk/tl6D9ZMB30nu8L4x2wbG+7m66nqi
sO2W9l6euIeEEwj5dyyOP7KLZPzy3Bold/X5VMHl0khah4yi1ZxupsdZnQ8Cr8iGRVDsGUxLPYvT
t87Y9ZsHzaJjmwSa7XRlArsO641LIsjylskV0wOgoPYDxZ08Nq1d8FvNx5oTx1nlnsXvZCgTRfMG
gVxYqabYabwmiaes+RVB/Ei1DPttAwiY9fjbKqHI6cYJELkWsd1LA7GizMWbQNBHA57Y0kealhC4
D4jqTifbS7q/GCYpFSm7bH/QzHuXVd7nm2/vp5NSVZ4WLhMGghsSjHNHhSkNkxYjdhRwdW9RNFxk
N6g40Iy5C4Zmm5vZXo2BTz4VAqEuBlx0ZKmwUmuCZfqAR0ShqpvVD6H6sEiMKCkiHZ/jsjpGu6ik
/IDHCXCnXAG4HmNu8QJ1COz4ivpwjzkJZP5qAKuLGSmqQbESS37muB7Q1wVvhShM0zk+OQP1JII2
3Gbf/2nPUC9UAtEMZMO3QyLQVvVWr0gfYtM5d/QxnUrznXxFFG7E3jFqdwCEBy4JlzUagHRWMfLe
7ls3frmyuo3vITMwm9txX1ti1J3CeGuDhXziwS2d4zCzTBXg4xELpZSTEYHd7Ma0ljnL9PZLhIUY
9OMbC1zK28O9vKlJuACS/5QItPui8WzoEsqk3Mr8R+Z3M7l4reOAW1pBk4+Ex2g9ZkV1jK12AeU9
BMT380CgAZ3moMSkHn+IcAQ0Djp2RzYxRTLyt/qL6PhVoWKX9pRSG2DdPrW5d9knHjwB/6+gp2bJ
yOO94VH2+nSOlMNpRMPtYGBOM0p13bD/9t108/v4ZZ4g/O3w4y1Urr7UUQSsweYzGAX7X/Gk8ctx
4bhvX84MuSgVIsr8OASUYy1FvcgOoIO0Vs1Lcg8YwlaMBzIQV50LWvkIbxm/G0huEMFTXurgGIJ/
+1/s0HdujMBAddxxh0jCm4c3C9tptYPM028YbCt028iDGshw2OjUL1RMnmxsPJcbcrb1Ur+2DNoV
fRv5kA+CihANcOjX17yb43+O0jnNq0VJ6GwynR28Xnx/X1qoennIZQqYuW3J8beG4s101CYmGiUZ
/pX3YAI8Rd+5E7J3o9bTWZHPaxlmgT39CSAb7aEJRLgjFu2i/8f6N4I0bU+skXkBibHHDUYLarw0
BTGw2zQyqt4uLJoGlqbcJ9yiNHJ8yaBx3m1WvOn++gwrXAfLVyhHDENZOnGIN4p1X2vBlXv6s7HB
kCMZ2DjabZGXzJ+D55N/Uc/Rxb8BzspcmzsQHlgmEv/vITDtWpV3opBPIq3eIMXUEsEon2LdhWtO
tyrU4il7vJvsEhzIV/wqfUKgym5c5WxEgx66gM++LQEtjtodH+4KWRapTTC5XThriNUslmLVTThl
5ew6kPbofWlnkj+UCNC5qjI6WQlKztJtXGFwyi4j3OSsHtbnnN8SD8zJi4arG0+jSyRSeqoH6m34
yjDhVeO9d/5PjnWjMqc8CFleiVgIVPYEbFraHJdPj3FQ/AbSIOzC7P/UjiWThAZzxswQB29j/RGK
m86q1g3IM4/N3CjreKlQXxjZ2EgX/Is6KQbxt/T+TYcxWvD4PKy28XYP/HVcKDrqMiFf2BL6K8Tx
yYadpJJ1ywYw4mYaE1BbBdWo9xlQsidnvr046m7ofwPvmDzRBFwxZYeulHlYK5jd+2rWE6EJKjfZ
xoew8CXKJWsbVEqaVEu4dQ4t4s8GJMBuzaFu/YLbUFjmoqvpbVCMkJstFwaeBCg/7tCUOhV4w8Mu
9n0OnKPa3N8rq4q+LtoIh1cf7+GKVmgJrA/KuuNMsHeSxymWUhM1QbdA2s1+ucjkoB+UQHLpxxTD
Z6C3aABPmfGZOoBMTQmtCMGuuE9eIVh1pH/IRK1qoXBs3gtVUEm8CViUkONvgPRJ0i7w38XPTHV/
G8THBR5HkR5w5rzng03+QGerY6+mTn59M12voTDo0qyX8ZzuhUucQeHKu8NIinKfccKRHAign616
XfYkVFxf45cnJ3b59Dqa6avWPhtEATnGQqNVdJ0SfhXfYFvNZkx5PEMe6+ZX/W7vVVIwlpO/ETjk
9ARtlqFOcXrlLYosRc+6dNGASr7MnD/UW6kzYEUsh5UCnVnsExV5SU6MOZ8+qzLEncyvSfw5jVbH
9TuMpzI94Vm6Zr73/xA7IGMDvSL+zqwoVUr1gaVpNfc01ICi4O5jW2sWR4uQSpGXi5MIJFk27IZx
oVZGhjr5Kim/rBU8PJB2tg73kd/8n7GCPSxTAOrmVZNR75WRXPvGPZGj8yJqziT3IoEnQk2th/2f
wLPC/JmipzE4o5UbLVqU5bXIQ6US9qH3kbOUCHblhokDSAKxffjA5oOooNiJYWtj0neumUmKLc0f
9czXk4ztp4KtUdyyc1I8rQ4d1Pxl2P1EFZLr86jOUdP5Tl5TqhI8r/Nhj4D41mIEmDRGi+8clx9f
fQ7Sdbnw2q9+QIXRnl2iSSkrieFjrWaiSzbnvZp8lvjceHA0pEQdTCVYYHovopVBwJkzBz9rJiBH
b3diQbwKBatiks9DoLsr5D2fmas2PRtyyMz1Z9i3nDFOZH+9g7GNWvJXndRdZxmj8K5SRdPAimMg
bGMIpyTk3SGfJAyr80oO1+WB3l1WZzXJl74=
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
