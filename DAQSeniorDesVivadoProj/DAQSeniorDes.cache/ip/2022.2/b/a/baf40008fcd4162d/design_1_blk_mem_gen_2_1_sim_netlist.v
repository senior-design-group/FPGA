// Copyright 1986-2022 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2022.2 (win64) Build 3671981 Fri Oct 14 05:00:03 MDT 2022
// Date        : Sat Oct 14 11:53:44 2023
// Host        : DESKTOP-QRFTT4G running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_blk_mem_gen_2_1_sim_netlist.v
// Design      : design_1_blk_mem_gen_2_1
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a100tftg256-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_1_blk_mem_gen_2_1,blk_mem_gen_v8_4_5,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "blk_mem_gen_v8_4_5,Vivado 2022.2" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (clka,
    wea,
    addra,
    dina,
    douta);
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME BRAM_PORTA, MEM_SIZE 8192, MEM_WIDTH 32, MEM_ECC NONE, MASTER_TYPE OTHER, READ_WRITE_MODE READ_WRITE, READ_LATENCY 1" *) input clka;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA WE" *) input [0:0]wea;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA ADDR" *) input [8:0]addra;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA DIN" *) input [13:0]dina;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA DOUT" *) output [13:0]douta;

  wire [8:0]addra;
  wire clka;
  wire [13:0]dina;
  wire [13:0]douta;
  wire [0:0]wea;
  wire NLW_U0_dbiterr_UNCONNECTED;
  wire NLW_U0_rsta_busy_UNCONNECTED;
  wire NLW_U0_rstb_busy_UNCONNECTED;
  wire NLW_U0_s_axi_arready_UNCONNECTED;
  wire NLW_U0_s_axi_awready_UNCONNECTED;
  wire NLW_U0_s_axi_bvalid_UNCONNECTED;
  wire NLW_U0_s_axi_dbiterr_UNCONNECTED;
  wire NLW_U0_s_axi_rlast_UNCONNECTED;
  wire NLW_U0_s_axi_rvalid_UNCONNECTED;
  wire NLW_U0_s_axi_sbiterr_UNCONNECTED;
  wire NLW_U0_s_axi_wready_UNCONNECTED;
  wire NLW_U0_sbiterr_UNCONNECTED;
  wire [13:0]NLW_U0_doutb_UNCONNECTED;
  wire [8:0]NLW_U0_rdaddrecc_UNCONNECTED;
  wire [3:0]NLW_U0_s_axi_bid_UNCONNECTED;
  wire [1:0]NLW_U0_s_axi_bresp_UNCONNECTED;
  wire [8:0]NLW_U0_s_axi_rdaddrecc_UNCONNECTED;
  wire [13:0]NLW_U0_s_axi_rdata_UNCONNECTED;
  wire [3:0]NLW_U0_s_axi_rid_UNCONNECTED;
  wire [1:0]NLW_U0_s_axi_rresp_UNCONNECTED;

  (* C_ADDRA_WIDTH = "9" *) 
  (* C_ADDRB_WIDTH = "9" *) 
  (* C_ALGORITHM = "1" *) 
  (* C_AXI_ID_WIDTH = "4" *) 
  (* C_AXI_SLAVE_TYPE = "0" *) 
  (* C_AXI_TYPE = "1" *) 
  (* C_BYTE_SIZE = "9" *) 
  (* C_COMMON_CLK = "0" *) 
  (* C_COUNT_18K_BRAM = "1" *) 
  (* C_COUNT_36K_BRAM = "0" *) 
  (* C_CTRL_ECC_ALGO = "NONE" *) 
  (* C_DEFAULT_DATA = "0" *) 
  (* C_DISABLE_WARN_BHV_COLL = "0" *) 
  (* C_DISABLE_WARN_BHV_RANGE = "0" *) 
  (* C_ELABORATION_DIR = "./" *) 
  (* C_ENABLE_32BIT_ADDRESS = "0" *) 
  (* C_EN_DEEPSLEEP_PIN = "0" *) 
  (* C_EN_ECC_PIPE = "0" *) 
  (* C_EN_RDADDRA_CHG = "0" *) 
  (* C_EN_RDADDRB_CHG = "0" *) 
  (* C_EN_SAFETY_CKT = "0" *) 
  (* C_EN_SHUTDOWN_PIN = "0" *) 
  (* C_EN_SLEEP_PIN = "0" *) 
  (* C_EST_POWER_SUMMARY = "Estimated Power for IP     :     2.78965 mW" *) 
  (* C_FAMILY = "artix7" *) 
  (* C_HAS_AXI_ID = "0" *) 
  (* C_HAS_ENA = "0" *) 
  (* C_HAS_ENB = "0" *) 
  (* C_HAS_INJECTERR = "0" *) 
  (* C_HAS_MEM_OUTPUT_REGS_A = "1" *) 
  (* C_HAS_MEM_OUTPUT_REGS_B = "0" *) 
  (* C_HAS_MUX_OUTPUT_REGS_A = "0" *) 
  (* C_HAS_MUX_OUTPUT_REGS_B = "0" *) 
  (* C_HAS_REGCEA = "0" *) 
  (* C_HAS_REGCEB = "0" *) 
  (* C_HAS_RSTA = "0" *) 
  (* C_HAS_RSTB = "0" *) 
  (* C_HAS_SOFTECC_INPUT_REGS_A = "0" *) 
  (* C_HAS_SOFTECC_OUTPUT_REGS_B = "0" *) 
  (* C_INITA_VAL = "0" *) 
  (* C_INITB_VAL = "0" *) 
  (* C_INIT_FILE = "NONE" *) 
  (* C_INIT_FILE_NAME = "no_coe_file_loaded" *) 
  (* C_INTERFACE_TYPE = "0" *) 
  (* C_LOAD_INIT_FILE = "0" *) 
  (* C_MEM_TYPE = "0" *) 
  (* C_MUX_PIPELINE_STAGES = "0" *) 
  (* C_PRIM_TYPE = "1" *) 
  (* C_READ_DEPTH_A = "512" *) 
  (* C_READ_DEPTH_B = "512" *) 
  (* C_READ_LATENCY_A = "1" *) 
  (* C_READ_LATENCY_B = "1" *) 
  (* C_READ_WIDTH_A = "14" *) 
  (* C_READ_WIDTH_B = "14" *) 
  (* C_RSTRAM_A = "0" *) 
  (* C_RSTRAM_B = "0" *) 
  (* C_RST_PRIORITY_A = "CE" *) 
  (* C_RST_PRIORITY_B = "CE" *) 
  (* C_SIM_COLLISION_CHECK = "ALL" *) 
  (* C_USE_BRAM_BLOCK = "0" *) 
  (* C_USE_BYTE_WEA = "0" *) 
  (* C_USE_BYTE_WEB = "0" *) 
  (* C_USE_DEFAULT_DATA = "0" *) 
  (* C_USE_ECC = "0" *) 
  (* C_USE_SOFTECC = "0" *) 
  (* C_USE_URAM = "0" *) 
  (* C_WEA_WIDTH = "1" *) 
  (* C_WEB_WIDTH = "1" *) 
  (* C_WRITE_DEPTH_A = "512" *) 
  (* C_WRITE_DEPTH_B = "512" *) 
  (* C_WRITE_MODE_A = "WRITE_FIRST" *) 
  (* C_WRITE_MODE_B = "WRITE_FIRST" *) 
  (* C_WRITE_WIDTH_A = "14" *) 
  (* C_WRITE_WIDTH_B = "14" *) 
  (* C_XDEVICEFAMILY = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  (* is_du_within_envelope = "true" *) 
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_5 U0
       (.addra(addra),
        .addrb({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .clka(clka),
        .clkb(1'b0),
        .dbiterr(NLW_U0_dbiterr_UNCONNECTED),
        .deepsleep(1'b0),
        .dina(dina),
        .dinb({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .douta(douta),
        .doutb(NLW_U0_doutb_UNCONNECTED[13:0]),
        .eccpipece(1'b0),
        .ena(1'b0),
        .enb(1'b0),
        .injectdbiterr(1'b0),
        .injectsbiterr(1'b0),
        .rdaddrecc(NLW_U0_rdaddrecc_UNCONNECTED[8:0]),
        .regcea(1'b0),
        .regceb(1'b0),
        .rsta(1'b0),
        .rsta_busy(NLW_U0_rsta_busy_UNCONNECTED),
        .rstb(1'b0),
        .rstb_busy(NLW_U0_rstb_busy_UNCONNECTED),
        .s_aclk(1'b0),
        .s_aresetn(1'b0),
        .s_axi_araddr({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arburst({1'b0,1'b0}),
        .s_axi_arid({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arlen({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arready(NLW_U0_s_axi_arready_UNCONNECTED),
        .s_axi_arsize({1'b0,1'b0,1'b0}),
        .s_axi_arvalid(1'b0),
        .s_axi_awaddr({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awburst({1'b0,1'b0}),
        .s_axi_awid({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awlen({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awready(NLW_U0_s_axi_awready_UNCONNECTED),
        .s_axi_awsize({1'b0,1'b0,1'b0}),
        .s_axi_awvalid(1'b0),
        .s_axi_bid(NLW_U0_s_axi_bid_UNCONNECTED[3:0]),
        .s_axi_bready(1'b0),
        .s_axi_bresp(NLW_U0_s_axi_bresp_UNCONNECTED[1:0]),
        .s_axi_bvalid(NLW_U0_s_axi_bvalid_UNCONNECTED),
        .s_axi_dbiterr(NLW_U0_s_axi_dbiterr_UNCONNECTED),
        .s_axi_injectdbiterr(1'b0),
        .s_axi_injectsbiterr(1'b0),
        .s_axi_rdaddrecc(NLW_U0_s_axi_rdaddrecc_UNCONNECTED[8:0]),
        .s_axi_rdata(NLW_U0_s_axi_rdata_UNCONNECTED[13:0]),
        .s_axi_rid(NLW_U0_s_axi_rid_UNCONNECTED[3:0]),
        .s_axi_rlast(NLW_U0_s_axi_rlast_UNCONNECTED),
        .s_axi_rready(1'b0),
        .s_axi_rresp(NLW_U0_s_axi_rresp_UNCONNECTED[1:0]),
        .s_axi_rvalid(NLW_U0_s_axi_rvalid_UNCONNECTED),
        .s_axi_sbiterr(NLW_U0_s_axi_sbiterr_UNCONNECTED),
        .s_axi_wdata({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_wlast(1'b0),
        .s_axi_wready(NLW_U0_s_axi_wready_UNCONNECTED),
        .s_axi_wstrb(1'b0),
        .s_axi_wvalid(1'b0),
        .sbiterr(NLW_U0_sbiterr_UNCONNECTED),
        .shutdown(1'b0),
        .sleep(1'b0),
        .wea(wea),
        .web(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2022.2"
`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
VHPlDkoDlWlBfBMvPBmGYmaek3s9hXXhjF28kllYPnaNm3TSnzzpXHWHc8Ye9/2L2yiQfJ1hTWou
Ia/zeQ8h9/dtr6QB5YkyW4wlb/LbMgXb+DGIXPSllNl0IMsRQIcQDbcQm1bO/nlhb+2pjxiuaQrl
DbvxoDwPs7z3LunRxsg=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
lmIhoX8hXuc7tNV1sXY1K2/gXL7Y7Hq73qQF7+x03UWWTRd3uhGmVQtOMVbhIW+66UkWUHiD26zL
fzqGor8bgSNGpSFyS11k4TwLQT4OfAMGO8C9Qmmh4+VENBnpS9TW+wHzCv8oUwht7xYtYRZvOvYK
F3fMppz2sBkUd1lciw98ZE/UmNkhqBuMfIYF43j45DEJ55PBhOZNg91Ls4v3qBHyBAaYPFFoMry3
d5Fw1PZyFQSEOSSpwgyds2aN0g6oIwl7zm0LJrM9VDAOxBUE50hk+oHr4jj8J8UhHQJnlEHm1Idm
rvxKygNKRvfSpa90NYxZJFYgqnrMYg+19+9aZA==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
VkyCjO2onoeZWEoYQ/4ue7X5mkHyTYVW9xjdoTsGS4GdP/Q64VaCZL/jr6R8DVDXPMnH7tRMrDpo
jpYBnyzSgOkfgqM+96ioC2fDyAaG4gYgGLmrBR6qK3/mxXwAZZX+GJ9R/eWXkc9h8xN+gsSSX6/M
jIQCgeT6q7PB4dWT6KY=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
Iub91V+TnhVlZCSLu6iKmFjix71y6/l83OPTs8uewWvkE7WcqYxEKi9fonXEkzAtWzuKwEUqnOlN
VBsNJqPUdKcd22q523mrdt89mpdosWD+hvZdO7ELhJniY5u9h49FFkubpN2JiUTcIcKEYxVNlds4
wyvaYUqbPVH5v2ooJwDdimS4GVn9HerCOgPwfshvQDNlMTxLcYju4v8BHMc5Rub9Q/ihvpQU74v2
ouZ9XIwA+C6pBLwvaqS8jE7HXOokgqJilaX/W/t+KEgiFry/txRTMU9WMD7tCN7lcfjCydmS3Lq+
3u6Hsr0S8BwNjcaDpZDnBTygUJd4JSqREnk33w==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
U46EWFmKmpZGaWfyL+dokyQtJtaOYsa7HCW/+fdtw9/yHKTWFpmqKBZngBj5rPkNhtTDDCJkqsYj
tUXg1j4tgIBaCQn9B0q/aG+B3gPLrudp9hLL25mVbsfiTzdekiV2hJMmhuMoavKKPJHC6zyW7kZi
80er82OQy8h+Df/fe6TRjH9xEt3/b80tRKUMbxkLfnnkAyyf1KfOhB6/uyI4mwXuQR+DsAbzybKR
YtXpOiW72tGrXTFlzcwbHamWZefqsilVpBw6V5dh33vYKGx50xwWpj76maAkpQrOpB7zufeldJe4
W1UOEN84AZdRTLkVSxamWo/wp8nP9fiGS/ItRw==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2021_07", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
qczgIJYpE/SzErzK7eWJBGcDFEzDLm8cKbwJbPXuM6YnJxx44W+E60R3war7K2QGFAkOoCDUtDC7
SghJGF32btaDLzeKm0tQ669sBtQmMIaBrlt7I9QBkNM8zN9GL92qxNC9o3UVWMOYy5BmH8nUPgcE
O6lRubeltlrTuDe7UJQ2nEPHcXjpUJJ8dxktyW+LovBy1OxW8g4GRAsmEJsoOEg0HuDdWcc4IshJ
PvwPJ7LblELAKsdkSt65y9VaklaEm7MlH4ImlgIa74TgRmutLUbWxM1QYhGE5rAzFhGU5i3RJOdx
L3N7GGGvLMW2z9NSHbIFX+/eNII9fNJ9nZbgLA==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
Ti1NUgDv8YPk90APMwfu/mRr38QYwAxZfv0T6zQ89YS55t2EquEGVqrEafYX6rTydLOw8le1Oucv
f2oERpSSSTih/ScZneSZmuPE/Zh2BU1Ajv0j+/+0uEWXU+5lLPbDJjnapTmJXih1MYPf0SHpZZmE
BKj2IEBI9MPZlh6bxpa5BWJnyPdAvHf+UNaMXU9+pmbtrzUVebql4mFJu45Z3+ehmFY4FBW3zXMF
44C4TlHACLwL3vHVMCVfeKhgdVDbpE+/IFhTStz7mZ9h9RKGanQcs6YDVM1R+2RKA1QT1fX4FiQc
1V+FGmrm1ujxmFGXwpfNKByVlfCY0oWhRJCYYQ==

`pragma protect key_keyowner="Atrenta", key_keyname="ATR-SG-RSA-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=384)
`pragma protect key_block
HuEXFK0NXt09xU2yxxjng1OLsT+ZEM4EhqBgpr9D2ljw2vDaMBrqEsRQTc2B9soDq3ewDduHJXBd
OGYxkPnoN6LhjULtB2nTgjcH6NxA4puZ1ZNcndDndVBo8rTW5W1OqHq6InAG0CqPpTIkuqz3ECPl
EysI++MCDfH6tIzlekxJFIJ1McJsTq5rFuLzMMcrmkBxgcayDpOcCFuzZzCczxmt/cCCIKmDybwT
OQXmOcLJoYLP4sFu6R9c6xO8i6p++crv2N3eIxZHKbek9xBBZqQM9EYuEtsbkqAs9XZpa16i5njR
BDFxTKcP6r7JgFALJE89AZhBbate5JXWp0v4ECZD18aEL17CipwcWPutNMdG1apzSPP5y59n7rMG
yxBPz1gKHc3Emkl4WcO0hjICxqmO6dMXoY8JvBSf6ry2l0sH9Ihr3Bq5WWmlhPHnoaNr5jl//vNe
KfToWtn97eoVSt1LnmXXnSpdigbHr0UIg8AdkpdkuNRaWdVicDdgSo49

`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="CDS_RSA_KEY_VER_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
mokwst2bn6UxD6V9UdIgCIG1QQ/d0FiJqYGOTI2eHPV6YElaLjnJ8DnQmZnGS95o3x93FDOoa58C
RwYsX1fVoVtXkj1LuZq0k7q9vEe4T8xMjpkeYtIHY9k0Xhy1Lq/xRlfzGAf9fvf9e+f4r7aR/Sb/
uCZxxugG5niTwLENY1n3NthYL0jvo8Fmdw4Qg0nTCGWlVCws+09K0g9/lx6I9EcuHHemcHO3fOZG
lMc4NaPNozKwnyDMoWUkwiVxyFEPFaQLNYqzjvR+CqrWfhFLo96JWhL+eaDoNuZoBVYQtNH5ZwBL
BoO27Pw10lgcReGlZBz3BLO7T4ddynCx0+eSnw==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
PiP7AjOQqqouyQMoBQqgWIDhUSViq94rIvGiIJ/UKMDspM/yXw1caE8AhWHTjYckC4yLpPAz5P6s
1Z6flzDPrzVwg4e59X2cc4IMCHhedna0rDO804njcc6amRDTeLsMLTkWfvomB4xwszm2AgT+PRnB
WHd09ZUDVFjiBXT+Oa9AicgGJHrX3w823yBPuAa704kje/SzgtiDpcTU1eLmLhLW7LpEd9KIHd9s
ER7Uk9Orws0Kq9PMTqMX4hMn5K5mFakOeOURiEbUjdv5RiIJ2g/PlQXSItM8fHsBTQa6fOaJwQTI
vHwK3a8ZBHpfT1YH+n7wNiNUZwD4SFXm1QVx4g==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
Ul5ZfTHJwMctaNhYRortUZizYMPYRef7uYqPSuMkxsArnxI/cjGh+KRMwzV86hyp/6TXSJIjm5ec
2wX2UONdPN+DOJ84jYC4JbgJQrPnTj7ioD8uLX/WlyPcQzyF5keqFgj5eR5s13FskVWCuAWf5m9w
mhFEKFjVXDAr7gVgAJh/hL8P6Psrnf+LGfiM8JhnDepsHEYykGlpD3fzru2BGgqHWqPqFMcnyVGl
vysaIXiJz/eYKvO8RGcgd3DJAM/wPm9A0m/DWcmSnczOgTjoqkHcBg2H5uJMLvufzmjImi6LYEqq
v04ESDEN31cSUzqUYcayvMFOnI/WNsWbFIa5+Q==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 20416)
`pragma protect data_block
1REfOrxhjuGj/TY8HA8a0Zfj8O7MCrUs/kydwf7tTbjSWYNiZbfk9XxdGrbwzu6UI/2bYh1yyBFU
HHbFNrlcBqAWCFmzC/E3JXeTApkC0dZ6BPA0fi9TUfQAquOu98BXuFlH5fgIQHVp6hQNV7ixeSTu
mwKLp9D8Er3VwdExVTrqgcui3cbTkvY+95QxNW8JtYkV+eqGkOPotrMOfgFXPDiPYIOwpYpu0bjC
5sbSGKAPgbv6feiRE6IeWdza0WWaSO63+2G4F2X8U+mUDKrwK7kkbCPCtBOxuCHw5mOp5w40x9xH
BYngqpYw/+mjl7OJFV6LGJFJmHGEz13uyCA4ydCunBMzajK+alwRkiv+D9mwgnStu/zwyc3Y5cLj
CfpYQlukuIgXHTHw9e28BMIUtNHecC391ljoT6ktkrNRlczkPCCbWMfMg27IA4FMxvhNuGj3oy74
3uTW243QXRq2c+0UyT8qUHNsPYsl4RTW4ickWfD/Zi1nKeRYm9NryZmucgQyFPHb+9T//5LkYab8
QxZ+ansBQojUlAH6rzhvqC1r5E8P1Y3Jl8qI6PkSwm0fkDIKR8ZvXORTufBki0k7Aql9rBZfTya7
05i05SzDgV4Y+zrxgrruWR/91IO3pVWjQ2qsMpVA0DaTx+ubX7Ha/Z5dhN5yeJFluulcLx4D09zn
MLuv0+FcXho9T4kCsLQ+VKs5H73Q/y7sPYsnBpvNDtLX6lT7np+NWTmetW1mcA1W8khgXBu8sblX
rcZUOYc+kgMfa98J2jHk3v5o7saW6njA1oRtP50u8gwZuLq1BpOT0bsF7PdhgHAcQVb6Ra+Pg8eV
5Kurz75hZbhyT8/+FJOtmkS5y49+1y+ni8XND3KUZkscXOGdMtEffDbXMcWHyTecNi1GF0yblPNF
R3ruzlEnSv6brkikfSvvO7ffyGNgiuU9i9ELxFf1slq3Ht/NOGYyBrO/gGHWDRBszmgFH+WrlM3B
uVtx7XIA2iCGbZcTfh+safN93J7njaXLitBfIXjcM5hFbeHdpYW8ZUXCRaFiufhBsCRqbzUJLL2n
hCjGHpB4QCRGNa74op7bwcO2O2QFoaE3ls8ZuCBCHqSF/3PQP0RmZhFcp/waxk2LE39+HOvdzJc+
zShbqRX3YzvKwdiTaH78DkyTmzsAOJyhNAA32zmb1BQYBgT5cKoQPQICS8553P/OJkGyPXhFdwZD
bcIgB38UJOs384DJdd/V7v3ATpFv8mGE8mZE7OVFeQOVlJ2KYhEM+6KIW5an5pci6uXd3R89+mWx
pekf0WYrj4r96EKf9nZ5Ojnko/LzJ7jIxO2erS8R9xv5cUq1xC71YtMIS/KdvT9erDDJ1EnNNWH8
rFxDzJctaYa2CknPijoJuEE4fZsp9d4nkSRkwU7jd3wzqVtTWieZZ8Oc5DAK3dtC8q+ECjs/LWWy
TGp0dS8VUPnTnbM5YF8HtzFndcNvjsyfke2KozNVCtZzpVrS28bJCL45F6bc8g0VFruXvE9KETwh
CdeKMn/Ni/PnCGT8fc0X/JonRNmMOt52c91+LLUv85tEM3PsAfVAi1kxAgnpxLxUtLFWiXdiJTcy
hvy7Qf5rJrIadUqOO02CdfMTcDjPVW8E3uuIMpMSBNOBAndPGDkkq6EFDpFJqIuYZvBTr/TeH27g
9RItXMD52BqS4W79IN4qqqyZvZNPHgqBXsZpwi+WDkZKeiM3/+m/dTEpZVLbjS5BJB3kUwFQkL8t
19tHbBSKUd+oa7AU5oZopwFQHed3vjXxTYBs2SJBs7m6rczKFJPS/eKHpeFwRh1b8NnF8F+gXCcS
opQ/BY9fzYpVcX8KduT5j00goA6yKA8jttvPM0CE6/Kml43vibBw2wk+J91CHwD+e7EWEaGVTBc4
LBdKG4Qh87A4OwwIqgQnH+lJoCzJkjovXzm/Trv7QjJ5SGPOs50Q8y++Ye08u3GVK8HQJMV4JD6z
/Q4reyCMQu7PF1cS0iHBq3vMAiJlQz/RNjPLGLGmZRnZUma5EfwLt59UsqMyzIN4JGpxX6oXjOg5
GtZHzKCeNqGwXgtR2JPGwiJP8Fz25VAOnBm+3/7t7R5l3u+t6uwiS4GfaXWEs+J5B4OCwws46Wfc
FVplUaQkJhWLuNdUXp5ZYPG/VCA6o+xkpAfFlk7ceUawDgbR4AbA3QpJw5c2sgudlpjEKcHUFQmN
bJIRAFFw1KFL/OD/Ft59l3jdYEaDJ4jfqf6EOdj40RmMqk42Fz9Eye9cLbnqEtwCbx3n7w12XLzP
v9x34dHcRlBV5YHKKqUHkKEQg6GdmsOvIsYI0NOBaqv8jOhRL1yanAizfhvYxqrU+CKSQ7M6c3i2
vuAaOppRNA7e1ONYlFKDaoNzYgjOZbLybgrew9CxI3H4KiZzLfllPKGizcqVzpoqnGG7FhTnbE/v
uOpj1gipjiCu8rhW1ES4nGTxCyAkxzxlQCpNmvOpSgk9hHVlGMkb/bJdwRgw0vZrSuLCcXIOMxWp
YTveX5QKrxvv+pkHfkOAgoensqaX2tIRA4lmODlAbgkB2MtQDWw3qOtZVtFCWbJ4Z2v0x33kNjAP
LZ3a2pz7dYFcLGvjVtjxo3OzpqZc3tnqaq5amvtEtOVCVz+qmCFVYSIER3MMKIV68Biwoz59CXg6
B9RbzO2xKIDX2WzwnIh2CU3RFK5Wf4pq6nY55IwYn2c3iNITs23xNmvjVbU1wXDoGXJQpRNnmsYi
KzkWVoxi6suCNFGf9n3nfOfH0asIG0oOvhSATlFkANPUEDNw7Fe7opF0VFQrV7coqFqKQpyJWrGm
a/hBkp2OGBCdiAKTHq21d6aGSKCQ0LS+DzfZKM7mVOtxmmwIZajbT5WOYsMHnrIQhYLNvJ8HXKFY
gD7Oh/65bQ35vehPf/4y4eu7mYqmrRQZ+TurY6AxrHDGymxCUEB+inRvcS2jbQP0lREfQKWf6Ip9
c2l1D7QTYiKZRdBfacmWfBVtSyP430LsqJKtlimUpLY3CqsoqXFdR9a+PcK31PAisWoaGFYBFrF6
hp9VmpR2yxXI+r2Q1KCgEIWkMBblDH+bkLM7BGbZyUgP8sCfSZqWhUMBafdMovb2c4AV6Tt0TrL/
N6rYZJ4NODAtGG2G1WK2TzHSg0OxdcOd0MgKQKupiG7lubUII8+QMeh/7MZ0gZdBvsJMCXQ46zbz
ChljQ9HT97fJx3RZPBAz6JDVb2ENAeNAejjuTqtS+opLq/qAb96iW6DQ7ufXsYie/gcmF6MCTjnh
jVMKvDfNPHAFUY2cbdhxeIXoYfb8qqL+bhvfj9lPC+eh/e48uVjkMs74sbYL+C4vWo+2z2CLH8pI
3m52XXgiNfZi5UuHi1vtVC1Haj8fegJgOqEdSCamG4GO+MqAApKehQ/xeeSIMbv/LjHAQPqIZF8N
Q9lfUYhiXb8nuLdXL6n6if5QPOyTuW/Zb9ofCQzjWJVRmwgKKYj7o93RecidnfnxZ2QcLnQUz7d4
hU9iSvFiWaQ8xjRQwU2twaiV9T5Z/Lo986/vlVzxuXK8x/VUz7HkEIMnSlzmRvvoFgn2+7X3kI8H
TwXRXXYdpdG0PIbtXWfWEh0NTUeQevKlMTZCi0OICTiHXdFsqrHwfvX1O+9O5D0mOZEY4tQ5HRfZ
XjxkJYoPG7a5v01gglOrI/11BPU6xvPRzOZJlc7/pB+HbjyG4/0SBqO6zPGyhxH6XN9CEesVjw7r
v/ERkgXQtyzC3DXNpfqLKEJRsWd3zBhp9JqVMvL/hTEJyZC4xTOv5j4jjTxrfVMUmWneynqadsM2
zC+uUVGlIR6hbdGpTfyCBu68TJ3xdPXw1MfnuQttPKhasoOT5SOvNi0OuNQg85pgpnjN5nyjC1yz
pBMJ7kg0sUaeDngxAUJrU8DGcuZyoJ6W1EBXKO8Vx9ddtBx3GBeKezZMrUNiX1XzfrF8XtQ/kzPs
XYMqhKgtNbOL8bUTnJRxc15liACsGVt8I9sZzsxYTaawCoM4EaABUuR6gkBahmO6zxdKObk2zkp3
7ymbxlFz46r/0iJiy6mpTeSz7y8dJnqSoDSZ/YEi2QgpqKTrOqMXn18jC19RiuorAEOJ7wxz1Mhw
OsyCyjCbbKlxgs0yKI7dvj49Ln488tLasvSS//CqXX+wFa8mbNAhsIQahaeHkuBvZ5CsjKxoIGvt
/6uctu3NVgsSJvCaOeYKHBb3gyrNweYH8wcDB/vkeNGEO7ie4NsaB4xtfJYImgOlq44wBaJXUS4D
SxHx5dF+lKgqNrmQHKzt1Rwidq7AnEsT2Q3ZWsObQgCmkrSQwVNAGIfGpkLcZ1EzZEf2ZH7t1ZOn
SlMPreB8nQ3Pw5R9Icq4MayXdqs+fVB67y1Pj+vOLtrxeDd6i/Ggu+2iOJPWdfZRgaJ2NyPkMC3I
bcIUA03B50UY3qzuX3FhezDkW17oQ84eQtiMtvL8wafBzDYe+n9IJZc1H8+2yvOF8ndFNSaSk7zZ
2nzKPE6B5b7zByM6lEO0wcbo2G2kRD5bJX0PA58dQOr/JEGshDfvfHM1vtEnxQSuAcLW0nGd4DLr
de4YATDrwu6H4YxaAJKBRhefyfujF3+mWbN1NpJRmRlX+1gaC+J8z903gQw71MC9UT2PFRFJWx/0
0bZVNmig/aufO9gN3UDL+qjDlw1M5o7SijBFmnFULpw4VOmAcCezXy9WICI9hwKVH0OfNMnWkAJA
KFuA1bDJSIke0f+vUiCdFBYW3RMGkEGEKQYTj0ccPnAJ0z7nWnXM24yNzH9UXzNdnp9f2wEIL/7k
9TOjG4j35faNJe5G4H2BkYfN+Zl1n5xz6r+jitHf8R0TmXmM3IlGgyDXP0Y9WT2oY+Jt4waQWtKw
HCdDLeBRdmELnjfXZiIvQB89NLdUdBH5vZi1nfMoZhPGYia2XTmhuOKw6lSZU07YuS4Kw8HOEunv
qHKo3l3AQ6fHgztYqRlpji3icF6J1kqJUa0OOzAYt7SilKA80L9+SpOzlx+I8NACGpK4w8D/cAiO
qRfnAliGfB+1I92OzhuHPXvENfKhEuLkoTev3ARe3WV1DLWKBtFXO3JTPeisO1YByAPSxNmHalXf
xPR3/Ud5MO7I6zh9jMQVpRiIDZXamnKMK6dNSFrOhAERFuyKb9CIwENtlOqya9P5GMMWcDlVNYNK
BKNZ2kOrUEOa9m5wmpErP/L1jRh2eWtfuZzmqraOcUbCPk7cKYvklHGz2zhggyTxf+Kq3sTKZMJQ
sSwhPCUk5UeTogHFuKVpe1j8lbhortTsSTwB6X83wid65dW2WSGCbaIDu05VhQG+5aduenCrYGTP
s74JC6DtcyqY4DIySmKdQ+34l2HvgiBcr2TMbjFX2S7trWRUNBw9QrP+voCMpxUoAEI5NFdaNIS0
NFLb9QWV8ePFt5SEBaYSCKY2hHSsG/i+U4nh1BzLuriqBKu2OGjAglL1AiV7RFIYJJgecrqyBP6e
idhEiOmEkBOv/vNQCTe2SuSvcFQLsOn/J06/RCYCLY8O6dAWGzOJqPFClfTHpjZT92N7zrDXyVu1
xVQ7u0/+Z4k1JFHQhYMqTSQe2gtjJzF+LJjNpAAuAsLvm7wRjCyF2nbsrCuEqLTLMPWd27m61gNc
6IhRvn+/m/513sWjPGlrNyioaru0tmhkuS3zIfQkm5llMlxCTMiZMNzxyRxYLu3jBxuXsvTOHBfs
TyBXs1eeUGX78LdEo7RAgJZ6GSOAHyIYUs6neO3zCiuENFPyfCD/pIpxeR5B6Vl3ysyjSE36/MXx
G5i/ZFsUOEe/WLJ4LL4LqZESVjB8XPANi7e8v/4XKl+d9fGnJV1vyv3eVjJycp3IAtA2Wj1Bvv7W
ymQ5AeEmVzsVkUefCQ2uuPFpd8XoJiq3Bb05xXPmFg4QOERk9A1COI+l7jUYeqw9tZ81P1q8co7D
XgANJJelKkpHl1wGAlnTRwLbYuf2WGwSHlY+81m4a6bX8E5bnDtdLQiYYqmECRtxk4s8OIZfQZWI
BCoTLlNPVJgCMVIkBObuS5cE0Rghv6qy7K5bMJxs1ewe/H+uYdWQ0SYze+K0qSMR5OCFY4gu48B4
mg2SyW9JtIrJ9azBQVvEBpaJTPKsq+dBs801S5ehTybh5c+4E7toKvLmXWf69UBgYaSfAXitM9tp
UJR/oBD4D/nzC0IgnfGb39jHrCVhTjUpj64UJAFu+fGFuS0dTSWA/eOjM5BeUvKiB/y1iAX2/21g
Yc+/r8ZOEAFBSGHLfHsg8Wzx62jBINyuwQonhEgRoWQhMmGt5bXAL9GW01PAVDWOUlKW1XJee5vO
WW/FR6S+Qqsbi1SStv0kMKpGZQKwl24TrTqXzHwpFef0OfY8TvXX5jtU9IIMV1ditCf9eyBxYVB5
q4/ILC4UUdAS2nXH4Vj8lP2pxqwMLJ6jPfyZwvHiXGETNAGLHS8ItCf/31xIexkM7pM8iYAnFDaD
PxOToJE8+jw0ZnF6wrpHgXyUY7P7AXoz/rlMNKjPSqad+0ZJPIxqsExLQKT6z2FlEjc8vqBRoUaK
iW/vBaQLbYZ4PNkJY1+X+Wyk60xtzPCxaiEvbuQfpzzWGkj8U/TMHLOZWtVOAqrKGRVLbvXZU1jf
0tLeMVQ2x8PSM3gLARb4ZDBn2aJHEMIaec1ROPfRD1CJ3UnTkw00nzCrJL3ARKL8ZAVUHQnhSshI
fyqeQ69MgJ3eqAls2eC7a44vH/6rsi3xouA74I2HMHgHXdvdE0LqVCxc0yNr1MiO36gOravV46lQ
YD063/z2hYxdN45DJNXFxKeD2OIStm0gHLV0BiSJ+3mElfcnnT41TSz60tdv+eLrki1179+who5I
zBM3160OSmDCCc/HKpxGG1fGYhDhPtLVTZLVKWOF1M3miyrgNDobobv/BDd4weGAGoXJbTRCg6Ta
7bNtVDRnQBeZl9vTiLk4CeRSV1u5N2XioEK1cvmcyR2C9+dzbks0L1B2IGxxg2rvkWzEyGZQu3D5
5SdQmyeuaRzbnr7Hkvb4dlWUGq+gXSeGhUWkuUy8el3CmDf3vnXo2TdSRkhbdA3k3TF9spzpE1HZ
7RNah7atG5LYtLbHDMkzOhim5uPMx7by/Nsjucmo9EmMQFS1Tebn7AhGd1yctpjLTHO9TASqk5WM
p4VG1P+jYFwew7p6zX8xHTdfKZ0zna668MnXQB8JXPrdXflH+dZNodcvvyb9ViYdZgyciO5RbK6Z
DuDRcTiqTbV5prNSATvYVD8n5vJyMPWTCgZ8aECE4aFF/et4KpxjDNwYociNcnI0CmLxtCnY7CiP
Uw8QDYhOsoL81DEAmd6zYtt3am9c+mBu/MYjKB96tjuCC6R0GGU7geR5rrdqetmhb33YVJSdCe1I
jmik3z+soUfqZ+iYwX5VssdLJo37uWKzwwuq36TDP1xWYPZfrMuu1ozvGgM9cOhMxBd64Hl2ave2
jQOPLa5Eib87lGm3hM9PpkQr7VBTuW597IATEKIV5QJeYCwqpoAVVg2i/cQgafq1OEm29Y26TiOv
ixKx1u0ZigOtt2bBr71j/fa3byY56OwyPkqJpZdF8SqTb6GXkgqoAgjLzLH2vpxMWhhJa+oHXi7G
ad78DgMg5UFKliqz8zDwd2drpz8d6dzfw9Rft3IyIG1OUAY+vgMfAnifzQPthimSkxkTedaoh+xt
MB5nB7XkjcxjeYgd4fyG6mDk81EFzQh0+ySs6SRhHTnere8rCywhftXeXgglOrpk8d8p5uQhEigb
MO/l/wjuuyvicbk/F3XNPpKLtLtPn7n39KKxxUY8GNPfoxO+LwVYeGRWY0LjFE7qtiR/NBchmqwA
pZvBW51AQr+3623Ls7CZH1vAHrxc1nDvQ5Wovyti6o0dAsc/PyRXUfRx866+h/bJi1T4BlhzivL7
OwjSb0FRZNFbKK3qH4xe5FMiCI4lUy6x0mwDl7mSRUAhOx9dMguQcw0tiwyMHIBRRS6q9W9YUJiq
sHAFDnPxrhzjmP6Yu3sLCfG1cV2YcgH9uDflPyRY1Gba0QRCHebx+hf97pl0uK1uTZjMgLSl39w8
AsA5/BQ51s7eALVsqxaxODwHHTAWsDSAfJTPoOi4Jyr2YGPiJcqVW2lw23rh56Kf2kNBRM0DLE5u
iBY2yoC1cLE1q+kTT52n9VuULwDH5doBwoie5mJ3e2wkkAZmcijGvXvy3XiRaNq7o5++IzsFIOa5
yOs5pbQeJROM9EPm6ClVLKPJ2QS4+JQcQV8xi1N8ln/N58hEZkses2fPpbLD/O7TNimvFVZuMSGa
ChK3Rk38W44UXaBminj7ymIoKfZ0ZmAF9AAbppcXYpNm5fhIQgnLyL1YpxBtHaD2sPiS5nR3rJ7J
sX+9tlb0DproBEkDnmP3eHs3Ppq6cGsFDJn6joqXWAaU3og77w8MX6041yF1wRJ3aT+DwGWbcFMA
6efcJONFnZSCK77eo8/wr5HYYDsd4dRsm9sw9HSF0+4B9ClSka3VRexvEVRiJDCTAvzydGiGF+z5
hUK4gF3e4Z9b6tdUMn10TfNAHTWYiisWTjFhWjwPGCra91tvxJNZ9SM67mrk1sHZnk9w8oukbkPE
645WGnplp8u5XHcC3c/yeuxVZx+kGBqwZVeOe+FMzK4/gsp0qW6P1LCo24Zg6PmRgFIp2bODeSd5
UHp7VzC5Y2T9GbfhNsSabL+uh/H7en1lxgVUk08z13PSNNigYBNK74377slSgmyRANViuGrjvxV9
c/8giimFvQfdGwpeNwx8IxwGdGgKkk1eDozKhFMDj6+p1DmhbYzBZ4NZs/L3D52lP26uuJ0DC15Q
e/IpEF+5P9zS3rvU2Uw+MA0SrPkvmNoasXpf3QwsJfSdj8jhUXB9gK5tt9x2m4I1ojiPeKripFqw
JjhbjtwvQQZqjX4KUFbGXv7mDKkdjX1CSWLoYatg+fgSBY4gUkeCJFQfvPt/jV9HaCDCJf2kYO5q
74D5fccBZC+5o2pLq1Lhh3hewxlEvnegNupos/9obQFE3TUQwyDF3O8FiAnsGF2hEdHAbYXYRuNE
fBx3XE15xCJGawjtArezL9IR8YCqMQb+4nNP0c2E3684tYnm1z44BKK+iXS/xBuHUfTNS8iVgUXl
Cu134K8T4nl5C0uCGZbFJB+CHHTCkFLdN3zOpS2wr+xFak1pXW8Q2lJosp9Y9jVmskTxZ+E9pMPS
HGrjKhCpbEbvEMu3EtMQkAr2xhO3KFDSouQhw43QqvFfNykmvlAY5x7SJx4ZC8gsZ/kOVqfeydGr
UO+ElDdbEwLk6N54uZp581PdUB+e6jvH0Sgi3Zw1W2rGNXKlKh0DlZ2isy2SS/DHX220zrz0rMTq
S0xAN2Y+KqsVf+vYRx7ZRRS0lydMUlH6ljc109VSpCmB37Jf+HW0ilDl8H/ftLdiRHiAeMgYjTlc
OgzQwp63zkI1hu8qrbwJeVxlzfuq+bRnkd6TKs/eMR8jptyra7xc9Cs2Ph0NjsvkxfyWHVSZleex
1Q2f/PgTBXo63yMccwB5z2OzF5l98ND/um78fOfR02Gn/fov5HQgUrCYq5QEasB6UDJ8WikhFCOY
YSURweB7RcGU1vd4BkKW9Gm+hG60a62Fq0ip6B5GaKB1BZWgIp4QMXoHtg6zXCOd80aKzUJmYD5g
M2BUSZ+FcNIgULDEoVmpvi8FPhfO2PvzD1OUAQte+s3naEOLCZSrMvFfShxF975vcCYExZjgCShI
sj4j6j/l9so3S+lSc/y/cDvoR0NNj1pXmxfmBhTc4BAVoC54312+VakgxcNpM41TEN55hASjIQFU
AAqE16Xkyz+gpq9HdeLJ5batTpzHAb5Tg1Ag+tBzOd5+iUYU8HF8GtMkMQ1iV6Sh4a9gyL7aH0yg
29VCBbS4MP2KazY3uaFpAq8gZLM/2zvhQPaI1X9Ou0qedMDchwj0SJ77TGECIsm9XGfoe/fd7GI4
xcD11PMQmHX4+MAWj/Sbk5tOq24P3V8ar7inI2vm4JkknWSdwgLiWsPVf/+2V9KaeucCxZEfazxM
7kTpoamM7YoCsJaSpX0AK5c3dL70fn2Q3aN8aG3/OdfRADxGDIIj9l2z+kFSxfziyqZ+PWEBJXby
tHjyCcBDpIAhDGAisllfjwxvNJT91Soxp0Et5o2TDNLhr7lFvnPYf+o/DB99Y/+H6wfHvRAqZWSp
NV+xC2dI/F1lFeLDHbqrFWb6MvZ0mCfNc0GNcxgoAi3wQZU3PE2No0vWqnIgspG7gU3WX9JPJosM
0t71mCvx5rwZa7n/oau4eLBiR/0QT52Epvq5gJppEqR118aoyBQTLCXYVRnpY3I1O011B/ikwdGE
AYFKD5LkiMNT/olc1S18lQjBYVEb3s5ogpuBRRAQW01funoWmxNDpYKQKXdR4O3VjZ/sBqO5eHJH
ACA45oh7askpE2F9E0AK1SPlXLEpOtLtQVccsradRijMtpPuRvhzhfBgw5gQEYN7j7D6BPXDXt63
is/mUkwffgazOa1BkgP5HPDSasAVJGlm44N1DOaLP+gfs1nZ0jWJ/eBAArxrmhIUpK8G+v/Jh49/
SRQFhmda6QaZC+LVTwRmYn41DGG+fG1knLUAJKmBwPJwzIDv70J7U4P3pBr97HokMLuPvJ+fYppH
AyqZbLyMRIynGEfRq9nrwb36RvZdSJuMf6rhJnFcykJVebpJZri9hpEQCY7njBkpUmw8ZDKGKY7B
ZJtvkS8w8a2RxjXxi1bmFcY8nkgNrhMEJXiv5ZYNJ+ZIe2OFJ2AUq0gu+ywBHNIspFkXsQX+3Bda
HHgMulbHf0KwTKcNF8YOWZvxaCJjxqxvostVV59kmhYE4rem5SrajlVn/y1pGYipudCgdPgJwZzn
6K2RLBs+C5AteH7xA7iM+KK4tTj6iGTy7eMiqStzkLwf+T5+bAPd8bbplhp8Hflc4azGeR+2g4eI
e0+GqYksaqEgdv9wLKDGQoGx5ua8zMe3wcnGWseGvy/DZ6qQVzonhvErXLUatVgTJxFFesNVFh2O
spQveCaSvBrFCViRzX4gnkGXWF0U5+7OljI8YiqL5uQLjX1JoX63TBMs6hOc4ZXJAkeS21QDf8iG
mEkwIcKJ/19Fbp2GZ+Zq56db78inMm7xp7VMd2iL17+1fjJcnzPest3M7TXVmZQcdm5JuSeKu8DX
b4dfHq3Nvbj2cV2UU/gDwvZh0WzaljucWH+2WwkO5lp5mI6omAgO+/slUhipT7ZfLdoOlSJ0zYaB
BHEYzcRzy2sMVtn+UPLM/jhq7hCK4V7LDUWquVOh5CG/KwLmgAKZUn/bnROR/v5j6twRN5YgXJ4Q
gAnny81qu6YqcNKHhDSQRoXGo0qEmmLvFQToaJoUnBn9oUTobGmwEmsqDno2eAygqwxhhrvPYGZD
RRQO38b9pknKu4rYVMhNXbPB/K91V5A8QZrQXYqdsaTmEo3D4cBdA/h9LrIadFi4MGqFBlHiBdlo
ftk5I5So6/lLWey3hbBvCg+DthLZuc10DBytdcDUvddQzLNubv7TyrwIEeGABSe36qWQAgs3Zbd3
F0GxxD/ar29mnnMMMCCGLayL/kgnnhkHTF1SZFEMeqkeSJPwTFOc5O7NcRBImKdkA3BSM0hc6bZB
F/xFFqCEY5qLr9ZKYsewJ4mK5V2x9w8wI7csFePpPglGBVoT2RCVeBc5Ra9oO0NbVZSPN77rx6mn
ee9dCIgHqJ6Zo++sw0FAOR3wxZDU6WIU+Nq3xwMAlqLe8BWD5rXiZhpQ0WCJ+tVgZjN2BL2NJZcw
LAKhEWMqHe95AtiEaJKKDQ0kwky9yeon4wtwuIXHkuYqGESWcaVSTMH/Tps3AjuHXBKjXygkHVRx
kpYnLmkYiaGGZQ8JNKZTjE9N75BJ9VbJcHYzClNh/mGnKObRqHaV32glx8DWT/Ocf2oupELFdaIg
BhG6BJyruIDy/7ZSehlHaKp04gmaOYNOVllJ4vNEAvPJ5pby8qEv65v/O1EBbRtkAN1ml9i4LlOc
9VCWVaSLeGTrbGMHQI4eWKpw4hBNsqy5LUXpmpYVKPPCj/nJzSHM3dM1gcMJjygMHQzGZ35CiSpH
iM6kYRug9m91NCsi3QjptvfkQ1HOv4kgMYHQ0SBzy8czxmm9dMw2j9OJZIRM6Plrm1ot6OkU99Ml
Px7Tm0Pyd/wq9lZnqWPNW+Jn7tjgh4MQUhShfKU8WUVwoCkeyb7svutcNxZXD4GGLIDTs/Zsi89y
VPP/u9o/s9sAth6IaUPpcCnMckO3B21ye5NryBZBTtcgSDuumH79pZliX1RXfkE4shOcU3qBTm3+
NXPEMaCUMpdIQqoaQEBu3oAWsRJb0CB3zYUwc6XKHY8Jvsr/7bRGUxj84NApwxKBhmm5h33jFATQ
HYvdCQYBbQ+4GepoGd2XUhCWeWvVuVNlyiig0tWtd9c/KMgE9PJZxRB0TGncJvdA7Arijc/vadyg
rJqCb0YRMdxFi2hfdYBgBIu+euOkJwRiVmqVPlVcW03xhhxPdMdEcud0DGDBljxE8MLaHl3pKVfN
/Ak1cD2pgRMEXg7FVKgiQoDq8huyC/o4bOs/azAINp1abypD0GmU4yW0HOsmPhpNp8QP6STlubTj
v1y3YWeDbm6QwO7HnHEIW1KVfMEdYbKwZZ3iYhGATIuhGoRZn9VI5vEdBrn63uaCDvVY+i3f6i2j
5WcQLcoDNSBgvc73lQkGSAVaO29mE9zHqGI6KrLe/LitOvis4ufYjLfN05DHnvEBffoCWqswHF88
Z2b2+AzIe63ADKsJHX3yYMMWxum34Tz6nErXHU375pS/v3idUWukvoqOZofO0ynGVloOToSNXKRH
u+gInvSgW+ZHloAr7od3Z5r6qTPjIwNHUZpfAzi3nzXtriES/awxM/9q/TaRigP95tm7xEL4zlrU
Pp6IwWsQRwNTV06hUKvWqShaLHWFBLdf7Yoz2EYG4Def81VprGA79N1OGILTOxTM5WF2k39i8Vtg
IeiLzKxXNE0hYl4SN7I3KKrtxgrStaEtwc4hx5Z34AUYHqL6xxJow4ubFwRqgGMrwlV36mhh0XkE
V8bi76ChORIupaouEUr0e4diy3JcQ5h1SO+OjJ8e9sO+w3/MqsX6J7KHNxjmKwpfVbhF8FI1kXAY
6W1Xx9bGFs15u27U/JG5qDZsM///WaC3YnuXoqofVL0pnre2RamAWpoCwZ/M4F1Vj7e/TEitk1og
wSbd/SjawtjDQhsF+xDx4ZqrGIBZ1/Vx9J72fnHUR5wXUWrfqnab+YlHV+SBvFp7UnCX0woPjo/6
yUYFTXhTtQXvp4X9s1MomQzlIy80+fY7uwJm778+7Fi0Y3NUttidrqTTf9O+QQoCc5hdl3r0et4w
0YHmJ6RK4Oj6s/J+1ThmE+kIBOxIf8J/e5bFY0000fzPbS1L0eEchl9K7mTq9hus/2dJ7Di5W0PO
3724hdN0fRKq3URm9s+BYr2D/sdGRGq2eZOoe3aaS6PM2qvfT3uTjq9zLmNUTb8LdLs0jBTK+PPM
SHo68sI3Ov1SqjjRECpeTxEqwKzIQh72KlN362JYbrJNPbPo7fZ5AybmbkMoCdPhvvbdxZuwboRW
mOc7I7KBOtMTwJ/IaVpZ2kqXQ6MWluyfBgRWtwfJPuFENG3TDvcoKsBtkfaxjVtWrObfxttD0woT
WpE4ApcNDQsy0V33br9X4M4qD5eioqQKXHcd1KpHqa6rqKqB8SmohGKeFf3sLpqri+4365jGY5gE
IdGAlVguP9z6ctlA/zuYfgbytDA53Gf39oGr4Aft0I3ilGlDJsyJN8aJn7ioOWuSjdL/Zn/3m8tG
x6/flldSUFa0gA48ehCZ+5M/5I9ip2EOEzitxDh1/N60uHuWQl/AlvEebHBalsXfNgEk+xGuqdOu
vDPtHTt/9aNtexYzthV8LRIY3oxpFfBACPJTlN1+64t/GkZPFam/DmIU22R4lseCIknGg7IkWZWO
Ic5B7+xJlPNMUcKOHO69EynUxJ+smJEly9lBAF1aL1+qB2eQNT7DYlvgjsgPTDOlD2kAnZ7bOGPX
2efW+GUDR2MiIErR2wJD5gruPynHhZQJ5YpW+HdC0UMP6Kt7cQ7s8o8vqq+RXgCzDH+QBcx2f9BB
xLDgrANAvPWhdjweMZBk9WHxtp7g9IraVKid/ryvCWNCgz0yViQf/D3eyG7EwiP8w2iSETLIxZ7k
eNQN/r+t5SvbOYjDUuSDZb3GnLpvtm2kXNnAUCjy80CiIDHnJzkj/4AjSM/KPofKC4JXPliLguh+
1MVKM6T8WLjrl6wgMkSmt5NMLq+SRIOPkcDQNjwLdswyjy84Ykstyno0pBDWxdCAg4KYuN/u6EI6
lkfSB8l9r4kj2hoeSCxQtflqgOArE4vSXYsWCTR4SOxkQEqLe2xhA78mAwGWL6BsuZLH+9OOFyfi
5jzPC5PeVMJdOP0lkbLEBjjNzTbJJ4jxCihE4NaX0s5S4WXGf3n/D0PubphQytz99CLf0xr+NkoK
YB5F+I61Q6i0iL6k8/8ZNUC9+gM19lQKA/mCvWKgiZVR6QtvnPASW65/uFepodQOV7vBY7+PC3qh
775+Klx0YY2smqXB1Ez/kSC7QAxrpWcOMYRShN9mFN3AxA/brydFtRC40mkp98M4I+oSiLE/bRZJ
2ZEpaYK3cq80g0SLtRghuouHz2sZGv6kMvSghwZZr/1mJS4l0pxbrMBum/aW54A2UdUYcxbrCbQC
ZxxRj5yvD/zbm/AlaYuALFEOR0rscjGfCqSrdYV2hjWNBv2y00535hqKyJBcmMgyVaQFX28U5032
zxWqxbT8GfBHhbLljuqjkm28ac1g85fi7+XlHsQn3SMrSjenfv1Yzc8ABwMZwce0DTZtRh1+z2IJ
7HcXjjQQXF6BqdNQwxkfBkL54BDwLRIvORGC8dNNnxvQIapKlJpi7CLLuUCwgSFPZT8AyAPM7pkY
VX1IBoK5ppzVGJvVTC/nuAAF4iZliHebQFxQ4FuYaGVdhGP3aoYG7RHDh8WAH/D5rLFN8m7Hzl98
y+POpDJeatJbiEfWaYom/nx8DLdFMNGxOG7R4Nv9W97vorLLcUFEgV0UHfyCtYjU84EdMfPaYBTY
/xjNIMhPTBFYkceUFRYCn4R6uLyFH14OG7NAeMRUopfhnlp/JMdeKQxCPqy6bjbT9/ZQYKRbg4Wv
a2KhziXGSySL0YztwgjGDWEYn8X7//C0ukyjBkiko0BNGR7GcnRaXUl+tKiLxJ70ngqu3fftOkSL
i7sxJYN6/JrJoHxKT2ZIU7y09UYBidaYkoIJQfvpQ+0nl5u127tEuneClqLkTpW2wOWUOUZYbx8A
REZ4khFYKEi9whTO2kKgHb6VK5sNSPJSmqd+kM4KAru0bwJ5smpWhfE1UT3O7ZY55wv3z+4z8da+
M2Y6zWUf8tTyGeQIqA1aRFS53zvxmGA9sIYlcqMrYnRndSC3OpCb+U6UNFGwuqtwu4FpYONEQRlZ
L7u3+TV3GAYMbQwnaP/GzvkHifSZoFAADiiT5bowzPMrdEX7JLagYGbBTnfyVeugj+U8tliwNc22
PB70CwqL8vd75yMnHcVbWvPQ7+uAuWvnE1V51q4Kb9CkBKGMZCANwTMTUpdH246oQgDtFohbbNEx
idSo9itvSpjFxdi0dx6Z2+lPQPV/EDfnifs/yxRBpVYr4ujeIx5baIaBI98+AgJiBIDCWA0ceoMc
hK58OMJQxP0zKFLLzeTR2ot2XGtZLvKx5bVQtT0ub3+XZ58oRGo/PD0fYrkHsyEjWh7+yZp3yCS+
Mm0KUD0UN+5rHMgtoVQT5JekxeZ2OW8C0JL9SSatrkdwdSmM0j2MabBcXFsKAa18Q/M6fu3NZAv5
1+8ZOaV48Aa+I2/lyiWI1EN5J0W/BJSMvQwBWfjLRWavpF/qUl7DnE+g0Ya1nahfNasCw/eH4b+e
+Ms6Kccb3uFgxwVoCrnV2e8V2jqCkp+AOEPEof7dEq4C+m+hXSQtkqrRKElF10NxAfYf0xZ2Y5wA
Z+ghFPKamjNnvKfEAviM37D3QyqCEYiOo0f8D2Qvs5+w409QHDSVe7IEEpePJLUDJX2z9SnB4U2b
yGnruFZ5JnR5+OYpBeyz8Qkb1hyF0yqMYlctYvmK7+wnW2H3ElqpkkpsTPjjR+Hl58nxp56sDYrP
Xl1On4fNUX2ff8wV36oTXpsj4t/3q2jb9o+kGs+Y7/OjWG87s5dE/28rLZ5o5l0/JJZV0m4fMb51
ImEcrvUr/3U200wk7oAGepC2SHiS/cmoT+6cD8t5vxvpilbFGYKDCUy4Ym4w/x3LpWfqPlhTSIWN
KA4NAGJZNGWfYDPHpO22yKtUTO65FP2yXlz+j3TB21gX8kBr+KZWg7UL4GFQOIeHdIknLpre030I
pKSDJS0GLoaLFA+lcvnyWhglX0hNFZSW3j2Y7W7S3g8lS7ZtttLAuWSpVic8IAxtzvKSoysKcoVE
dCa1G7jgH5xv8XMpHWgThSkw1QlnyOKeWTaPJgh9s83wons6RJBG1KSRlxdI3hOw2ALAUMmEZeQ0
NsijG0HX/MFcaqn1P0oCIWgoGy7g6o37uYv+49XkyKk5IWxurPeO3Ztu4nnbEEDAVMblFn7AWB99
Qa9VSXaF44ApgsOHEmsY/g+7dx+6FTpe0g4udUGnfZ0efNJznNXu9Mt5WPfW21M9OnzJ/iq1aajc
GZatcUCIw4KPXc7a/gbZsqhMajlEPSZboJJGDOylKxvGR+/PEmj8GSy0JYbD+xk6CtyuIydDurMa
DUsDM8cO0KczMp++vkaMbhhsMGYLEPBxKG+byUFd8AK1433T/B5f+Y565ktBy04das0L2VbpldBB
oYQzZg4Po3gTzET9LwRvMb0Ii/LUwfPJzUmSwlMD4RCj+qumLUw7ATRHv4KdDbI2vtgZrxQHYW5M
y/nVpsceZUVm9uK0+kfyrS2OEqxKHjyCn8yBeBRXh1/VDQNRHRCNwa3Dq8ruVKf6lFIxtmuj91qK
O/0qsuuLqFp3LN8D+tQ79PyPdk80QKcWtVtnxSOQsmsqVW5ls9P3aPRNtrGR8J37jTRvDlj9mlEm
VlR7oJAgeGGwQxbTntEITwqfaJXRhlwVknmcdsf+zYijcHTikg5qSeEEOMSCtmq4C4Y22gB5hSZR
uo6F2BtA80CXyGRXOIadINEwQkpqJlNOYUrAjaf95QMCwRNq0zmkXOZjKWu7PbG392Qj2o/8Anf+
gAt08Ynkz4BNBgeipVm8euEMMMC995kgS+WuMkGzcAENMFJYS+4MWIW69Q7qDBsx4v880G1PL0l1
HrQ7XI536ozo6ptn/d6MCEOBJ3h5jTxJYVX7G8UBl9Q8lBluc5WrR/AaojyzkH0uZvkq7oEedWmR
DBgDhMDXBZXk4U3nwLVZ9o3zGXfqbcHkSCcyMAvdDnnqNorI25IzkKs8JdHLQ6v9rV0JK/Ik3h8m
fUrMstkRi7j+Vak2enTcLgbggtDVCrqwrBR2E2dvbJIMVYPeUixX4ayYEk2kcU120g2FSB1ucf+V
cFx9c0eUPeG3V9Sh0AxSFFWkmG1EKzH86EkwNvC/vlrb9my4S/qV94coS+S3hXFytHpDAXXzVmgq
Sy85/iwmeXtl2LBd2UWB0foTdB0xrnMvEvWGHi2I0bArAu1pzdsWqt2ML9US1JhXsd7vQNhD8qIH
2j1Bjt1YMtYwMTkCHcoZIA8dSKoWFQmhS4jQgPxIskX3yNOBm5S/PwzVPiqz8MO4yus7rjiub3sc
xz/0LvYrx5LqvXIvMLF7W91XNtkEB0YqDrPp4XFFDmpnnLmc6ISbR3LxTKG/Bfo6olgjocbNfK8+
ZcbfJXKoBDFzv4bCLdwngyzuTG2KUYNfzKNCqE2T9iRZU5KllSZVBFwbNenXjiFyFeCtbg8srmeP
jg60wREkRzQSMlXl+Recl74r7ZtjXMnFjxpvKcutodBvYQWkc9Hrn47tj+dGkrk7ytyUvBCY1gGL
Lh4kSWUJo0p41XR8OIJ+3uvzL0O8JTps7za7Kbp5mX5z2TQfvM9bq57M54oaDb0vxGd1aQXQInRF
Eu4FO7E22EWOhcnQGoFgiMtSiDi+n2nH3QR233C0PuSxFPpgeSQF9vWkVlsqjcoFCNAl7lkEliQt
b6a/PmDUXiNdFHBRQ6vfsGIRadVvNesIy3gy+4mQD0Lte0qsDfaZKccN5BhpxzDZzGrSD2/ERL1u
6Kpv62cI0YoNgNNOGKBD3snxUR6K4lO6iArusWAMsL2D51MvZRLRdI/yq6XTd/O7OWjw7HUuFm6n
dUtXVL/q9QKZurdqdw+L9+kJzHmyuu10+BZzyacMMpzPHHDsOL6JAtvBJxzrHaZ6n4nGkaUSLsQ5
lnXfUoB4U30sKOPMU6YaS99JM/i1/cJRQCfCW1F6spqPgUjIXZ2ciFmaawvTWOTbqswVlyN+Gli8
6irMTn6a0Mxz0b1/hCqPAeCmVYgrPsFUb42q4JTXVqMDBGoQ1kQTj0eyCyZOAIFOG7pkPzBfwURn
d+7i4dltKhKUX82xUx0kOZyy6LoB87O/QRKYeb4bArkaBy75LeVfN9TlOgc9BHT6nKkoRbtqGfXV
gwF4icvO/IUoAOkN9jq7+2FwgUSZ91qCOCK0xeQa+NXJ01/A+iwhTky6FHYQj2ybj1AYwgyDb6yj
hz0PEkdgjy7UI0U7Ks9dCbk29ft2+YEXfnSt+r8dlh1H4gNWyWJ+AMygw1wCAiGgl7Q6ezw4t0cl
ZQoXfFK7CoKeleDV7EDkrCFrW8uA8z9rxpM6/W5awkVUd3HK7oLS/M5+1p3Yl66W8JYcJeVv8yt+
4Zn+Zf8/kca6u/ZTq8ACa1b9PaoXKu3CSV6cI9OpyoyNIIwpQeBBWnf9ImvI7uPFJ9CO2nWNNFrl
gq3xRyldFb8+6hXbVcLP+n/RgDZQGoRGoIpggO1SAyGbzY6eA6kZdWD9S6oWEy2KrlYBdHpmwHrc
KmMOtKjIybSk7bmHz0Kr4fmHSMTbAKc57992Z7JCwHemKJm1Tx7Em+7ZpngewQx0X8stf0obnPLF
mGjczCPzF7QE2U/+tZ+p8jzZwoz3JokPry40q5B977MlQ+VmxhnB9eNs9MI5zEeQBdeD3H6oh0f/
q6IV7n5iK9lJq0DvaQdxEiFKqBzn0BFfTuAz1Ss0Y7FiTGwbLHqGErA4wRiUC/alL0RJVqyrRfaK
olnhkbWkwrJ4EsXHBS21F1P55Cd8TnLKPkcooyLsxorcfTLjo2lV/AQNA5DNMFSiAafoN+fGQz14
T5Demw4ACdytOU1iG0iNZDioAJbspPnAsI5q6GtQ16Pri8c6WjDv5szwEJngVdaChRUBqvrQUjWD
zVA5MpRSNgdU+edDI9cuoEi3O1WkSwBUWD9FXKdhJQ6iwWUxx95XUSgCQSl49xU43NuwgIG3wOZA
xU7AWPiz5tb07hx/HdK+iZ676/E33/lo6DZbMLk5BShs55fYrD7e/BoE0nyGVYmaaQrPJ/5W5/rj
fzlqw5OAGSacQpQ4CWUfym3fDy8zOwcvKqQMogOIJLUL0jpCQlCz/gpoP8QpZVROKtthnnYq1vq2
4aiuH1iVDg1JM39VODE7SHGLrShxFQItTA0Vj4FTRGLvZ9hYRPmEXOEQXtGb8Rjcj1Vc/yumyqcV
KndusA02wP/XucxVr1x7jdIFkjtPDUzrAqTzkiVm0dxwR0NayJpHtoWxTcDXqRWsfBLMwiy2Uurn
Q2GVEldH7VfCvpbq4CTZhFDaAt38X0zJU736Q7206ZpfXPjAyxkAFH7QFAq3GwNGsjaaqlxdqqgq
Gcba0P5708uupqoXl2XhhNb0XolgQ2Oa+h0KwA5Lc2JYkZFLIoosNuDr15He/6/xD5SYgjdL/VIl
VbrKXAhhtOLeEzZSmpWEushmO1I0WcgK64bUtJcEOc14GS85+siKej0t6+RWzl+OoiNgQC2Iw97y
Q/TTFmLIbQqZuxuhTsy/gTJoOzSIjqK2/hnOkBjYCj++bzuk0jQktk0GYBl/i3yTh7jnHqFb5bED
UlL20AEyjW9xP+lR+g3DroRs+BKDsKAaxA8PuplftDmHrZ/zxiQhlq2Du+a1j33MaOMvpdvRqGvd
bAa/qXbUGCp/okUZSPCAfPDCKg9xVO6+d6opK+qJEGEfQ6BcLIDVol1vfU0uRNbMtbPy1LLl52Jm
Ae2ks7MZz/tY0+cy6+Z5yz3IdT6EO9u22MSdLJSRQQZrmv9X1ANQ6DCmTSFZnlAHs5jZXqIyeHuG
5QXu7x/DAPgrw/f33ZV6+b9Q2i0RbGKIXgkSeM5U2YIyBVX9LRtBEMW9N7n0QE75E+XcGuFOD2sO
TkITGEMDSOgMQ6OC3m4ZkcZqVo6sWLZAgdwAauH6+Z7lrpekk7NVhrknzBOLpu06wwZY+ctWozaO
AkTJ65vtW77xBBr+JA6/s71SyKoxjXc47uyHoOs7JzhD5rhnxSGoJIXZXxnViArJQXqB+PbYC7jV
blLLH8cy3qmTNgh5KZcmZpSxGt7ZPsURQ6xVBIKNPcIL62O/kJiGIF9b8uosLps/x+0dfwOln9t8
aVEN8VMfLMTJcUyIbihMZhTPpBZGyb34MOI/xDi4ZNDuVfs9baecwHCVr31zxUulS7ASNd6Rjhub
DT6XCzgv1J8hvyrwzNgeHL4tMiiDPYjHylAhauY4XLsDBkWnpg1SItKIJYZk8KCEBI8iLzrZEcAV
UUonjtYL5jmIyE7/GjJ/Mb/lfN0MFrs0ZSbm/4BqirIC1aQRIP5ZUGoC0joH6BLcDhei8dWDBMeC
zdsShqWmsMWg2sqaCrw1MDo3c6gp5bQaCejCN1w6fjOQOJ8IVWnHYSHGFmad1L+/61vRI+O3uCBh
M5D49+bM9qui90xBE6zn8+N3TZGFugDB1ZoGfhPph0gaQAwk3BNTQE10IVM5A3v4qxJE2fFmMsml
FWSOChW40ESsZDv0tugqjX6XeMQ8REduj/fDTviV4UoBi3/Fj4LmowGgrZsK/uDt4wSlB8InuGZP
EbKCA7GjclD3549BlyHO2WA87eQa9kRB12pRPzAURndEvG/3ijBlSs6lB8SHMu1JW/FVv/i/Ehol
LNE8VBp6rvBgY8fFJ7xIJD3PdLkwdjskc9eL3L+7rPDIDgA78rNcrka33ct8u/01ZqhdGOurKfsP
HHpgiA5LJPMWd9GQ6gu00knyNkl3jsQ5FBdZIcRMrddkosjHc9aCRsw3PH3ZXFTePe9sfKqnfiEt
ZCE2OlnJWvpUpkrEmi4SJZ7j2gDiGlMoA41Cjp9Ub5YUsXJaMnr8O2Z1/0uaHri5qNMMDqjbHQOD
kSIkS646d5PXFGGabgKiqPV2bNTH91/LSIh0mZe7+YLyEjtIMK5lCMf0lyn0RZ6VK4LQ+gAKH1Gd
ANhZgWl9LwGRXyXSbyf347vd1F1HYkfKYXJpM/ve/zRCWndIkN4UN7ec9hRPIyV55cwr9j4FEb+Q
cqtNux3xkeGrZuBtgG7nh2T/aH4tUiCs+RsHkyOE6gNUs3CkxP+Pn45dgaumN9kyPJGWb7Cbuy+1
4QlW6ga/181iuIHfT/0eCUysmvrG39oGq6WNp4LMaIWAg/J2umrNVMqKj7ewR6WSFMSesUtGnfP8
AnHJbFKt/dVkSUgPgrTq9Oz1gl2XSTk2vpdmIEawjF8v30q6AJGsjtRvQ2hqGKsu2lGQhs+/tlD6
Jn155k4MuLS9oeMTNGSe4zVhec2HKrlZed85By0KcL3oMP/WcbatqK3YZDxLxvaO4ckSOfWuBrUH
8LnOEDCah7m1vVlH+4E803y8zVx3MHG6NOFvyaCCI1y177PkVPyrpouGhZQ6zaLrwfXVPKKMf1Gc
egMa3Yh6NOixJ8NdlEhzKJgJjl8uH+cYrbxvwoBJEqyP14TymQaUTTGdvahTx8lC50XaXobh5QfG
9PBaEivwimIqiqYLy6DWPYcYGqDA2yO0nS92hCdk1RDsbxmiuDCeaCUHnDp2x2MathcQSjpcscNC
/pcXfup3nw4StohwMTe6S2V3iS7E4ep8qUswLyTuhhuCGoKNZT5p9BAQZQQ3S9LTGiVCvIiiLR2+
NYN+HpZRSjNXLVGEgRLXpi7bGo0v2gUMeKfmALC5V+hXgy21J/FjpFMQskh9uOIKZ3SRPNMLawfZ
904yBUDDtITtoKW7teYmsdj1t1pNwEKBrjEvkCazF+8GqASolhTnBPYE4bPHLJdP/jGxQ9KtKAzG
lzaJeHLdwEh1PBMafVCACqF7Z0/VwJ/KRP01wA02frVXLLdjjAFnwuO0OcXOuZax2+FSYdFKbp/1
ycU8eEWyEuXyjF7AUkXRwJlGnb4czEA0fIVMV810u9ugwJLhnczv8QPuw4GC8AvLlq9hd0FhTIVW
OrNrlqEGxdjst0ZIw+uBiMrkEyzg9WX2YEHiJUGF48yBq6fZg9LcGW8qjGyJ4iFaFWRq4swk3Jbp
1xgJ4UfB8TVTO15QNJYt1bCJdpw2kDWpEz0uU4TsQ6k4t+VoPu4nz9VPxRBwOiVSVof31uLUO37N
bTLMHwa9CE3RZoXlkbM83byOXhMEbYstFqTrvkGV+rKMOlZUuZivfQVju4Ah4zZ+RlQaz7rZDLeg
nI6Q8WYXH0dkGm6eDfTBbo2ZW7abBLrVGyPI/EO4hAL07/k7NSDdSzL78I0TaUCYfFrtvgbchhNP
ClTk/nTDIS5FF6FbK31G3k5C91kno3PIyw/4qwKIN3RK0KqHCAkymv1zkqj/i3MrJmr+Q2USo8gM
5sACX71A+e85vfvpk5CTblIUtQr7wiR4xx5h5hSmIEejdrkd8Q6X54jRWI3ztkCtp6gkwYyyTfhV
MKaouJBDKFy/PI92NW0HsPso5kLfwWvDtS9R9hfxM+cNGjE0C6b5YYdgw6moQexKZ85Vhh0Y5pUE
priJ3N00eHHz649KRssBDPULvuvNAM7ighz26N25aBUKqmPICbMOqpcwzRl+YSm9ewmbVro2rP10
F+NLwNLcJ2RQjTEJx9oOAkeJNI3PkbAwJBKKNA5QwoHg3tqZ1DnuJMKPLjKvoda0gKv/sv6r0V64
aGEuQTzmKYeQpX4yy4nJ4zDvzffsvMMI9mpgtw6EYwaOvbswdw7z6EcDysSkO3V0bH6VlGl/Lgc5
CwedPkO4b79vMoz3c0/s6WjAKeKroeW7nq5/RHMzOSe+U2Sa+aNimtfewSBG+E26vJudsk4SEoiM
3BtvzwqVEG5wFJ9bZ6RC4/7pAZSL8tMBXjX3gsGFrUUFNgsI74RlqBqKeZALdBmKZZyJpjJXowCn
/7AdH4V7PBjwceT8Kwi2n8dfgYxF+m6xZyFGeFN2I/2G+Ze+6savWPWN343bP32sQF9E4KhRuijZ
UkAU7jqzvvVeZEpXMM9oEzE48HqJHKYOOqtIpSS6qGUmtB8ek4U9aHr+buac9InKfhHFnLtrbu1n
OXYyGjnHR4hq1KOjpF4QpdYJZ5iDVwcOowuEAgKxd30E0v6hgxPZK9Duhtuaqdjf5sc9T/N7OOOn
NSg5cvd7gjKXsDDNUDJ0mLmrY5MFG+xkRmItgrgB7wvEvgote9jC68fFDfUQT6RA8bRSkaVIsTzR
uDTnPnN6iVzmPR1Jof9Y8La1kxkkoDLVaH1RL21UQHcI0BIUcUiwZQMHt38u4cbgsBW63z2adVXC
poFekdk+ByTFG216vmAEBM4ursvfyfk3x34O1CedN+/UwjDVFmY/obS9A2soccyBRBeuKbprwPwL
48eNbO1PRPCBieezVlW5ATBYfbcu2YSWbnESl5suTH+KxUxxls7H86tReK36yfst7LNZqy3vY4Q/
EEU2vkxDVZknpQkkrtUiSe153yUYgC2eempE7kvsa+03ccJr01uBArahOKB/jcnGp0wCSrFH2Llt
8GBZCDuXZ8jsCYvau2mu+LjgLw7Lpg6GVlNw+NsDO7d9tHxL1fhNF2pLbqiG8rb2h4Ag4GDpQml1
djqI0lHHgBnnfR61sPVas9/NjvRDfAmbF28Zisjvj9yceDLgGSllVUyDVLgoNE5YnRB5GHRTukud
QsxuAmrLl7PorC4OCDy7kK+r/SxGTXLRZhjJbPH7JJjElQHI1l8penblIAEJMu3cnO19yDdzZT5S
5KCSh2jcHPy4u++wYcLbsuK/MUWqNvCLGIwKq1amLIcORwDZLIbkIa16waRG4MTFVo/yu1+y0a+s
t0UdUeTdfriJchToYlXitDJi3yJrKTkjQ8nHt9Ky8uNhK5B/DTgTiZ6rJn5cCYhzmo1UPC4lvFVX
KFz+vbMHNbDYPdJXPMKDuU8SaxJQdzPvAOfnYwQ72HHLvAY4ye/Ye63hcgJWkzvOnfW+qj1RtV2u
ivy25HObyHW+R5+sykxnIfuwwtTNB5Qr7U9NQQCKeyvLdmm+OBi7DfHLqlKtLSvOfLCFTe005J+N
n4Yf8L3gnqokI4bmdY/2o0XzF2HVMUH+lPKeCxD3ESeNTELjKKib8EOgB4zQN4tis174UAHRM2m5
e3IKVmHy8sARuvzVzKQWLOUNCgpIFwnXj+wN9HlHhhLtyrthBy4D2RzmS5vLPLHm/mcwvswjQSLP
nGw0y/1rL7KLHLzvcpdKGXuSuXY0Skm68RR5B9np6c8XJrrVwVbCIV10NSYmk8bctToSm5gx5RN2
+CrGDWS2DLMeyxde/78f0d/644GTzv6KmOElHAXLromV9viBxSm153FOk0otNAjqKHftLA9QkdAw
GsqIYRsU+LXJsgPSnEqO5EwQSvl2+Ex7D+xOcFabr7IZ++8Su9KWw9QFBmU51MmcXfUVXav3OMch
KdaQ/ioIA9u/1jSe0M8urykCOXNicl/GFhfrMGa3SZRTPVazmXLZLY/2F32stGS0axPghwzDN/6W
NlahARUgsrLcgHMRXUs7Xn56+NU1bIAts6iZjnz8sqX0zefWP8sf7iI/sR4mFY4J99B7d/dGErh8
Et/z2aHOUdt8TDKLhShBLb9StjvDoWil+HrrHsTVp7gc7qpggNvLLt9GTiN5MgAmx1FPT88nvaMi
JM5aJ05LYPWswrmQFEaiwo0Vtq6BXdEXNXonMP5kXw8yMRxKa6oEbIdtMWcYWfgJtvuRmbbcm26A
RwGnXvFTusGiD1Tn6KA/fXMRZeVYOnV4HzToIu9JDZxjC5X4yN9tj2SgUcz2U56kMbqhlIffPnDH
0i7sMeQ8dKpbkEqQEamXak6PFzXSs8nOD8LpVIHryI9a3jy7dA2WnaWYCZQZWAAw9Zpgc+wN8lIA
gQBNdSluCc63Z78HrqfjoouHxqnJsS2aP7o35kBGFjwQom85BmrNPUzhph2eo97Aap0PpRxd8fEO
qLH2SSwfCBR2caA35/jl09QlWMIcx06PLF6F9X4AwKDiPEgxjhvVBe6R78LIa+kKmlRT6xfnBGQE
YG0QUOiBUKvrsr3DNtQDQ7e4fmvxJzm0aWgJn3/pvvjZqSZdp4XbbSF3k+k8JvGLztn7G4q9sMpE
Dq7rHKD8YojIrRhCkxtOZ4Xt1DloQ+wr35sHAKoYr+uZrVRZhbZ3Qj+RC+hb9FcCMIhdfYEoGn1N
TddQ1t+GeQkoYCpv9ZE6PhfEvtZHN3H7J1mxl4bJKwm2ZibPtqWKQt0G8B39h1E3V18KbKnPGX6g
0Hr7fKWz/FoW87Rxf9nYS6zmBXdMRL2ths/KnxXcGSH3gvih2VIUlN7V/tXkQNIo3vUrvzv5dBxk
jKEFN63mjGtoShPieEBy6Df0ay+j3kf8ugU7Y/Dz7k7cpnQa/9FaVBWZZlSqnj3MJS00dWd9lcWO
TuXk4jc7rPR64DjTk28mA0sAhlbN/K1fE5rZAxK2867fdP47l96Mpt+wVfsD+b4iPDcACKqSvLFm
I+Vz1Ztd7PYfQwOCydl9RlHi0xt13CxY1Dl3ZVa/zRAL04JgebTew8O0QHD9A9fHcY73lRcgKYVu
hRgS5XDiUz0sKZVgX7aYJ7kH7bkU+Yia9zi8F+EVpGUIxyr2gISReoolXiXF2LNyXYnlJ79/6Puq
3UDvVOPiSqysxCsDX2Kp6fpsI9DSHd/w99IVW/4qnKg1hANYPI6Yndl+pTNjxQSKkBCy4/iLO7+w
IYx15GrFgGjX034YXiASDogo1pEemWDCRkltPsVgcjnhaMHczsA9KlgXdMrscJaKe9OQz2xH+nZO
kh0xCPJqQ12E/Vpiw0qzLFy8ZUqmJUQVXdA8I+ypKucCors2oqUbHY/tqlpOd1PcLiNgC0Ey3mCG
eLFlcPn8eYQ9W4ntf6FNaGQ8qSmeWgBtunX0wBXZz9yqGkXJ/es1YD770q/sJrjaQhdREczyzasO
kJgoFJHMK2utpY4VNsQLRXhMEe7oKeHxsHInPpoTb/FzUMAOi2cZmyawkEeJWTVJYBTP3qCPkg4z
s8yby709oyFnpj8Y1MFzUAPgc4w6KJX/Pw0C58T0NzS/TDkCiaTwd/wVb0ybgk4a7olNs3dDqEhw
sgfK+YYC9FfOieiMY4+s5HvJ5u7UfLuasTTVH96qNpcdub0VXWhMKKtro3V7IkSwYVvcM1DDkBuD
w103wP6dWw8Fty1veDGJgDQnIC8XrqZoQPDrFBDawCcBh+4kyUAWuXEvw4RW3qmjumn7QGQn0C5b
zU6XqllL+/xgCiw/xjYLz5rBFefymI2grNbG97kiSZxBf+jySCUUq9qMUn+6znZwlePakrhjMOux
P9g9dSMKjoCh7fqDk2TNuZpS5o1Xy3FwoZZ/xQrlk5XEHyS98xrFCljICZAio/IScTH+usn39sU1
4i9tlD+nRSJUL6FXrV0/+fIwO6mZv3R6JQAcCJw+SccJ2u5xOX/zvSpfSVHQ0ORUo3L3Je8C1OzU
JRiaM6/Gb11gjVFpi96ad+zhGiKrBr/NxiwNgnAD3fRpKOoHVPZk/eli5vYDeodu/5oKJQLAH1UJ
JQ+qjD+J9ycsDOnD+Fn9PlbunpbQOTu88NsjjtfFfnZ5mjXBbnxnea16gtgZI/R3WjBdq2zLYrL7
VUY3E6NX5QvrDWuShwx9EPX8b0scMTmQ2VOKjrfs6lMwp9PmrsqJpV89DO0R1gOwE9rnI7Kk0Q/K
/4jPw75RAWCL5hrTYzJcTS4a/Gxvk6jyVlZMrUR+7nMP+teRjxwD1jcz3dQLw9B66nBVwwoECH7T
L+iK10/XttIpRw==
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
