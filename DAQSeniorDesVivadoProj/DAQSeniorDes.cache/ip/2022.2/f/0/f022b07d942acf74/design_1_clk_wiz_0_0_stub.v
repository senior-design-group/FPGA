// Copyright 1986-2022 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2022.2 (win64) Build 3671981 Fri Oct 14 05:00:03 MDT 2022
// Date        : Thu Oct 19 18:49:41 2023
// Host        : DESKTOP-QRFTT4G running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode synth_stub -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_clk_wiz_0_0_stub.v
// Design      : design_1_clk_wiz_0_0
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7a100tftg256-1
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix(DAC_CLK1, DAC_CLK2, DAC_WRT1, DAC_WRT2, 
  ADC_CLK_A, ADC_CLK_B, ADC_CLKOUT, reset, clk_in1)
/* synthesis syn_black_box black_box_pad_pin="DAC_CLK1,DAC_CLK2,DAC_WRT1,DAC_WRT2,ADC_CLK_A,ADC_CLK_B,ADC_CLKOUT,reset,clk_in1" */;
  output DAC_CLK1;
  output DAC_CLK2;
  output DAC_WRT1;
  output DAC_WRT2;
  output ADC_CLK_A;
  output ADC_CLK_B;
  output ADC_CLKOUT;
  input reset;
  input clk_in1;
endmodule
