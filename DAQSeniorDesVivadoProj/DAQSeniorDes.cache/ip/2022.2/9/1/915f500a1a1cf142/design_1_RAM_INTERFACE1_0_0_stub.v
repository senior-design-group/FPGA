// Copyright 1986-2022 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2022.2 (win64) Build 3671981 Fri Oct 14 05:00:03 MDT 2022
// Date        : Sat Oct 14 13:13:31 2023
// Host        : DESKTOP-QRFTT4G running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode synth_stub -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_RAM_INTERFACE1_0_0_stub.v
// Design      : design_1_RAM_INTERFACE1_0_0
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7a100tftg256-1
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* X_CORE_INFO = "RAM_INTERFACE1,Vivado 2022.2" *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix(READ_ADDRESS, WRITE_ADDRESS_DATA, CLK_IN, 
  RW_EN, ADDRESS_OUT, DATA_IN, RW)
/* synthesis syn_black_box black_box_pad_pin="READ_ADDRESS[8:0],WRITE_ADDRESS_DATA[22:0],CLK_IN,RW_EN,ADDRESS_OUT[8:0],DATA_IN[13:0],RW" */;
  input [8:0]READ_ADDRESS;
  input [22:0]WRITE_ADDRESS_DATA;
  input CLK_IN;
  input RW_EN;
  output [8:0]ADDRESS_OUT;
  output [13:0]DATA_IN;
  output RW;
endmodule
